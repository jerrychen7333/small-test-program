package main

import (
	"io"
	"log"
	"net/http"
)

func mylistener(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "hello, world!\n")
}

func main() {
	http.HandleFunc("/", mylistener)
	log.Fatal(http.ListenAndServe("0.0.0.0:4343", nil))

}
