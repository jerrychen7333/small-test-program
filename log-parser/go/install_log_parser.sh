#!/bin/bash

#source ./goenv

if [ "$#" -le "1" ] && [ "$1" != "windows" ] ; then
    echo "install mac program"
    env GOPATH=$PWD go install usseg/jerry/tmfilter-log-parser
else 
    echo "install windows program"
    env GOPATH=$PWD GOOS=windows GOARCH=amd64 go install usseg/jerry/tmfilter-log-parser
fi


