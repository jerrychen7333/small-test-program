package logparser

import (
	"fmt"
	"os"
	"path/filepath"
)

type LogFileHandle struct {
	count int
	dir   string
	name  string
	file  *os.File
}

// func NewLogFileHandle(filename string) *LogFileHandle {
// 	return &LogFileHandle{
// 		dir:   filepath.Dir(filename),
// 		name:  filepath.Base(filename),
// 		count: 0,
// 		file:  nil,
// 	}
// }

func NewLogFileHandle(outdir string, basename string) *LogFileHandle {
	return &LogFileHandle{
		dir:   outdir,
		name:  basename,
		count: 0,
		file:  nil,
	}
}

func (l *LogFileHandle) OpenNextLogFile() error {
	var err error
	newFileName := filepath.Join(l.dir, fmt.Sprintf("%s_%d.log", l.name, l.count))
	if l.file, err = os.Create(newFileName); err != nil {
		return err
	}
	l.count += 1
	return nil
}

func (l *LogFileHandle) Write(p []byte) (n int, err error) {
	return l.file.Write(p)
}

func (l *LogFileHandle) Close() error {
	return l.file.Close()
}

func (l *LogFileHandle) Count() int {
	return l.count
}
