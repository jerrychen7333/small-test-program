package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"

	"usseg/jerry/logparser"
)

const (
	osceFilterHeader = "OSCE LOG Commands into map"
)

func splitOSCEFilterLogs(filename string) {
	var err error
	r := regexp.MustCompile(osceFilterHeader)

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer f.Close()

	h := logparser.NewLogFileHandle(filename)
	if err = h.OpenNextLogFile(); err != nil {
		log.Fatal(err)
		return
	}
	defer h.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		b := scanner.Bytes()
		if r.Match(b) {
			h.Close()
			if err = h.OpenNextLogFile(); err != nil {
				log.Fatal(err)
				return
			}
			defer h.Close()
		}
		if _, err = h.Write(b); err != nil {
			log.Fatal(err)
			return
		}
	}

	if err = scanner.Err(); err != nil {
		log.Fatal(err)
		return
	}
}

func splitByThreadID(filename string) {
	const threadExp = ": ([0-9a-fA-F]+)\135"
	var err error

	type LogFile struct {
		name   string
		handle *os.File
	}

	threads := []LogFile{}

	r := regexp.MustCompile(threadExp)

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		l := scanner.Text()

		m := r.FindStringSubmatch(l)
		if len(m) == 2 {
			var h *os.File
			for _, v := range threads {
				if v.name == m[1] {
					h = v.handle
					break
				}
			}
			if h == nil {
				nf := fmt.Sprintf("%s_%s.log", filename, m[1])
				if h, err = os.Create(nf); err != nil {
					break
				}

				threads = append(threads, LogFile{name: m[1], handle: h})
				fmt.Println("New created file: ", nf)
			}

			if _, err = h.WriteString(fmt.Sprintln(l)); err != nil {
				fmt.Println("Failed to write string error: ", err.Error())
				break
			}

		}

	}
	fmt.Println("number of threads: ", len(threads))

}

func main() {
	const (
		splitInputUsage = "Split the log file by pid and tid"
		pidUsage        = "Only print the matched pid"
	)

	flagBool := func(longName, shortName string, defValue bool, usage string) *bool {
		v := flag.Bool(longName, defValue, usage)
		if shortName != "" {
			flag.BoolVar(v, shortName, defValue, usage)
		}
		return v
	}
	flagInt := func(longName, shortName string, defValue int, usage string) *int {
		v := flag.Int(longName, defValue, usage)
		if shortName != "" {
			flag.IntVar(v, shortName, defValue, usage)
		}
		return v
	}

	split := flagBool("split", "s", false, splitInputUsage)
	pid := flagInt("pid", "p", 0, pidUsage)

	flag.Parse()
	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(1)
	}
	target := flag.Args()[0]

	splitByThreadID(target)
	//splitOSCEFilterLogs(target)
	fmt.Println(*split)
	fmt.Println(*pid)
	fmt.Println(target)
}
