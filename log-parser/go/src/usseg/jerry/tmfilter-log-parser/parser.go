package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"

	"usseg/jerry/logparser"
)

const (
	tmFilterHeader = "----------------------"
)

func splitTMFilterLogs(filename string, outdir string) {
	var err error
	r := regexp.MustCompile(tmFilterHeader)

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer f.Close()

	h := logparser.NewLogFileHandle(outdir, filepath.Base(filename))
	if err = h.OpenNextLogFile(); err != nil {
		log.Fatal(err)
		return
	}
	defer h.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		b := scanner.Bytes()
		if r.Match(b) {
			h.Close()
			if err = h.OpenNextLogFile(); err != nil {
				log.Fatal(err)
				return
			}
			defer h.Close()
		}
		if _, err = h.Write(b); err != nil {
			log.Fatal(err)
			return
		}
		h.Write([]byte("\n"))
	}

	if err = scanner.Err(); err != nil {
		log.Fatal(err)
		return
	}
}

func myUsage() {
	flag.PrintDefaults()
	fmt.Printf("Usage: %s <TMFilter.log> (<output-dir>)", os.Args[0])
}

func main() {
	// const (
	// 	logInputUsage = "Input TMFilter.log file"
	// 	OutputUsage   = "Output folder"
	// )
	// flag.String("output-folder", "", OutputUsage)
	// target := flag.String("log", "", logInputUsage)
	flag.Usage = myUsage
	flag.Parse()
	if flag.NArg() < 1 {
		flag.Usage()
		os.Exit(1)
	}
	var output_folder string

	target := flag.Args()[0]
	if flag.NArg() == 1 {
		output_folder = filepath.Dir(target)
	} else if flag.NArg() == 2 {
		output_folder = filepath.Dir(flag.Args()[1])
	}

	splitTMFilterLogs(target, output_folder)
}
