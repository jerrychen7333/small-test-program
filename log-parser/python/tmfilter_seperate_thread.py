import os
import argparse
import re

def formFileName(dirname, name):
    return dirname + os.path.sep + "thread_" + name + ".txt"

def seperateTMFLogFilesByThread(opts):
    dirname = os.path.dirname(opts.logs)
    prog = re.compile("[0-9\s\-\:]*System\(4\)\-([0-9a-fA-F]*).*")
    with open(opts.logs, 'r') as f:
        for line in f:
            m = prog.search(line)
            if m:
                writename = formFileName(dirname, m.group(1))
                try:
                    os.system("echo \"{0}\" >> {1}".format(line.decode('utf8'), writename))
                except:
                    pass

# Do for using UTF8
def splitTMFLogsByLaunch(opts):
    prog = re.compile("----------------------")
    parts = 0
    writename = writename = "{0}.{1}".format(opts.logs, parts)
    with open(opts.logs, 'r') as f:
        for line in f:
            m = prog.match(line)
            if m:
                parts += 1
                writename = "{0}.{1}".format(opts.logs, parts)
            os.system("echo \"{0}\" >> {1}".format(line.encode('utf8'), writename))


def main():
    parser = argparse.ArgumentParser()
    #parser.add_argument('-f', type=int, dest='ff')
    parser.add_argument('logs')
    opts = parser.parse_args()
    seperateTMFLogFilesByThread(opts)

if __name__ == '__main__':
    main()
