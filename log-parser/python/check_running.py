
import argparse
import os
from datetime import datetime

def checkLessThan(opts):
    prev = ''
    prev_time = None
    cur = ''
    with open(opts.logs, 'r') as f:
        for line in f:
            cur = line
            cur_time = None
            columns = cur.split(' ')
            try:
                cur_time = datetime.strptime(columns[2], "%H:%M:%S:%f")
            except:
                pass
            
            if not cur_time:
                continue
            if not prev_time:
                prev_time = cur_time

            print cur_time
            print prev_time
            print (cur_time - prev_time)

def main():
    parser = argparse.ArgumentParser()
    #parser.add_argument('-f', type=int, dest='ff')
    parser.add_argument('logs')
    opts = parser.parse_args()
    checkLessThan(opts)

if __name__ == '__main__':
    main()
