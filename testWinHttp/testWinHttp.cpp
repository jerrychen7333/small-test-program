// testWinHttp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <Winhttp.h>

int _tmain(int argc, _TCHAR* argv[])
{
	HINTERNET   hSession = NULL, hConnect = NULL, hRequest = NULL;
	BOOL  bResults = FALSE;

	WCHAR server[] = L"kmn12tmosp";
	int port = 443;
	//WCHAR server[] = L"192.168.238.200";
	//int port = 4343;
	WCHAR uri[] = L"/officescan/cgi/cgiCheckIP.exe?IP=172.31.30.60&PORT=19147&HttpScheme=3";

	hSession = WinHttpOpen(L"test Agent", WINHTTP_ACCESS_TYPE_NO_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0);
	if (!hSession) {
		printf("Error: WinHttpOpen %d\n", GetLastError());
		return 0;
	}
	
	printf("Connecting to %ws:%d\n", server, port);
	hConnect = WinHttpConnect(hSession, server, port, 0);
	if (!hConnect) {
		printf("Error: WinHttpConnect %d\n", GetLastError());
		goto RET;
	}
	
	hRequest = WinHttpOpenRequest(hConnect, L"GET", uri, 
                                  NULL, WINHTTP_NO_REFERER, 
                                  WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                  WINHTTP_FLAG_SECURE);
	if(!hRequest) {
		printf("Error: try WinHttpOpenRequest %d\n", GetLastError());
		goto RET;
	}
	printf("Open try WinHttpOpenRequest success\n");
    DWORD dwOption = SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID | SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_WRONG_USAGE;              

    DWORD dwSize = sizeof(dwOption);
    bResults = WinHttpSetOption(hRequest, WINHTTP_OPTION_SECURITY_FLAGS, &dwOption,dwSize);
	if (!bResults)
    {
		printf("Error: WinHttpSetOption %d\n", GetLastError());
        goto RET;
    }

    bResults = WinHttpSendRequest(hRequest,
                              WINHTTP_NO_ADDITIONAL_HEADERS,
                              0, 
                              WINHTTP_NO_REQUEST_DATA, 
                              0, 
                              0, 
                              0);
	if (!bResults)
    {
		printf("Error: WinHttpSendRequest %d\n", GetLastError());
        goto RET;
    }
	printf("Open WinHttpOpenRequest success\n");
RET:
	if (hRequest) 
		WinHttpCloseHandle(hRequest);
	if (hConnect) 
		WinHttpCloseHandle(hConnect);
	if (hSession) 
		WinHttpCloseHandle(hSession);
	return 0;
}

