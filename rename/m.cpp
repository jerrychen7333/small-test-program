#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    char* src = argv[0];
    char* dst = argv[1];
    int err = rename(src,dst);
    printf("move from %s to %s: %d\n", src, dst, errno);
    return 0;
}

