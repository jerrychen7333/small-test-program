// decenc.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <iostream>
#include <windows.h>
#include "tchar.h"

#ifdef _UNICODE
std::wostream& tcout = std::wcout;
#else
std::wostream& tcout = std::cout;
#endif

#define MAX_PATH_ENC (1 << 12)

int NewDecryptStrExA(char *sEncryptedPassword);
int NewEncryptStrExA(LPCSTR pszStrContent, LPSTR pszEncryptStr, int nEncryptStrLenInWords);

int NewDecryptStrExCodePageW(_TCHAR *szEncryptStr, int uCodePage)
{
	char *pszEncryptStrA;
	int nEncryptStrLen, nRet;
	
	nEncryptStrLen = wcslen(szEncryptStr);
	pszEncryptStrA = new char [nEncryptStrLen + 1];

	WideCharToMultiByte(uCodePage, 0, szEncryptStr, -1, pszEncryptStrA, nEncryptStrLen + 1, NULL, NULL);
    //tcout << _T("Before: ") << pszEncryptStrA << std::endl;
	nRet = NewDecryptStrExA(pszEncryptStrA);
    //tcout << _T("After: ") << pszEncryptStrA << std::endl;
	MultiByteToWideChar(uCodePage, 0, pszEncryptStrA, -1, szEncryptStr, nEncryptStrLen);

	delete [] pszEncryptStrA;

	return nRet;
}

int NewEncryptStrExCodePageW(LPCWSTR pwszStrContent, LPWSTR pwszEncryptStr, int nEncryptStrLenInWords, UINT uCodePage)
{
    //[OSCE11 SP1][AES256]
    if(NULL == pwszStrContent || NULL == pwszEncryptStr)
    {
        return -1;
    }

    int nRet = -1;
    LPSTR pszStrContent = NULL;
    LPSTR pszEncryptStr = NULL;
    DWORD dwSize = 0;

    dwSize = WideCharToMultiByte(uCodePage, 0, pwszStrContent, -1, NULL, 0, NULL, NULL);
    pszStrContent = (LPSTR)malloc(dwSize);
    pszEncryptStr = (LPSTR)malloc(sizeof(WCHAR) * nEncryptStrLenInWords);

    if(NULL != pszStrContent && NULL != pszEncryptStr)
    {
        WideCharToMultiByte(uCodePage, 0, pwszStrContent, -1, pszStrContent, dwSize, NULL, NULL);
        //tcout << _T("Before: ") << pwszStrContent << std::endl;
        nRet = NewEncryptStrExA(pszStrContent, pszEncryptStr, nEncryptStrLenInWords);
        //tcout << _T("After: ") << pszEncryptStr << std::endl;
        if(1 == nRet)
        {
            MultiByteToWideChar(uCodePage, 0, pszEncryptStr, -1, pwszEncryptStr, nEncryptStrLenInWords);
        }
    }

    if(NULL != pszStrContent)
    {
        free(pszStrContent);
    }

    if(NULL != pszEncryptStr)
    {
        free(pszEncryptStr);
    }

    return nRet;
}

void usage(_TCHAR* prog) {
    tcout << _T("Usage: ") << prog << _T(" [-e|-d] <string>") << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
    bool enc = true;
    if (argc <= 1) {
        usage(argv[0]);
        return 1;
    }
    if (_tcscmp(argv[1], _T("-d")) == 0 || _tcscmp(argv[1], _T("-D")) == 0) {
        enc = false;
    } else if (_tcscmp(argv[1], _T("-e")) == 0 || _tcscmp(argv[1], _T("-E")) == 0) {
        enc = true;
    } else {
        tcout << argv[1] << std::endl;
        usage(argv[0]);
        return 1;
    }

    size_t input_len = _tcslen(argv[2])+1;
    
    
    
    if (enc) {
        _TCHAR message[MAX_PATH_ENC] = {0};
        if (NewEncryptStrExCodePageW(argv[2], message, _countof(message), CP_UTF8) == 1) {
            tcout << message << std::endl;
        } else {
            tcout << _T("Encrypt error!!") << std::endl;
        }
    } else {
        _TCHAR* message = new _TCHAR[input_len];
        _tcscpy(message, argv[2]);
        if (NewDecryptStrExCodePageW(message, CP_UTF8) == 1) {
            tcout << message << std::endl;
        } else {
            tcout << _T("Decrypt error!!") << std::endl;
        }
    }
    
	return 0;
}
