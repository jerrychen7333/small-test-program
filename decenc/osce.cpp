#include <windows.h>
#include <Wincrypt.h>
#include <time.h>
#include <stdio.h>

#include "cmnsrc_Base64Coder.h"

#define ENCEX_SALT_LEN      3

#define CRYPT_TITLE			"!CRYPT!"
#define CRYPT_TITLE_LEN		7
#define CRYPT_TITLE_EX      "!CRYPTEX!"
#define CRYPT_TITLE_EX_LEN  9
#define CRYPT_TITLE_EX3      "!CRYPTEX3!"
#define CRYPT_TITLE_EX3_LEN  10

#define PSEUDO_USER_NAME	  "Virus3761267Trend"
#define PSEUDO_SERVER_NAME	  "Windows7621673NT"

#define ENCRYPT_LEN			40960

typedef struct {
    PUBLICKEYSTRUC  PublicKeyStruc;
    ALG_ID Algid;
    BYTE bEncryptedKey[32];
} ENC_EX;

struct index_mask {
    BYTE index;
    BYTE mask;
};

struct byte32 {
    BYTE c[32];
};

extern "C" {
/* Initial Permutation IP

    58, 50, 42, 34, 26, 18, 10,  2,
    60, 52, 44, 36, 28, 20, 12,  4,
    62, 54, 46, 38, 30, 22, 14,  6,
    64, 56, 48, 40, 32, 24, 16,  8,
    57, 49, 41, 33, 25, 17,  9,  1,
    59, 51, 43, 35, 27, 19, 11,  3,
    61, 53, 45, 37, 29, 21, 13,  5,
    63, 55, 47, 39, 31, 23, 15,  7,

    The table ip[] is the above table with each of the above bit numbers
    broken down into a byte number (index) and a bit mask (mask).
*/

static const struct index_mask ip[] = {
 {7,0x02}, {6,0x02}, {5,0x02}, {4,0x02}, {3,0x02}, {2,0x02}, {1,0x02}, {0,0x02},
 {7,0x08}, {6,0x08}, {5,0x08}, {4,0x08}, {3,0x08}, {2,0x08}, {1,0x08}, {0,0x08},
 {7,0x20}, {6,0x20}, {5,0x20}, {4,0x20}, {3,0x20}, {2,0x20}, {1,0x20}, {0,0x20},
 {7,0x80}, {6,0x80}, {5,0x80}, {4,0x80}, {3,0x80}, {2,0x80}, {1,0x80}, {0,0x80},
 {7,0x01}, {6,0x01}, {5,0x01}, {4,0x01}, {3,0x01}, {2,0x01}, {1,0x01}, {0,0x01},
 {7,0x04}, {6,0x04}, {5,0x04}, {4,0x04}, {3,0x04}, {2,0x04}, {1,0x04}, {0,0x04},
 {7,0x10}, {6,0x10}, {5,0x10}, {4,0x10}, {3,0x10}, {2,0x10}, {1,0x10}, {0,0x10},
 {7,0x40}, {6,0x40}, {5,0x40}, {4,0x40}, {3,0x40}, {2,0x40}, {1,0x40}, {0,0x40},
};


/* Final Permutaion FP = IP - 1

    40,  8, 48, 16, 56, 24, 64, 32,
    39,  7, 47, 15, 55, 23, 63, 31,
    38,  6, 46, 14, 54, 22, 62, 30,
    37,  5, 45, 13, 53, 21, 61, 29,
    36,  4, 44, 12, 52, 20, 60, 28,
    35,  3, 43, 11, 51, 19, 59, 27,
    34,  2, 42, 10, 50, 18, 58, 26,
    33,  1, 41,  9, 49, 17, 57, 25,

    The following table has 1 subtracted from the values above so that the
    indexes can be zero based.
*/

static const char fp[] = {
    39,  7, 47, 15, 55, 23, 63, 31,
    38,  6, 46, 14, 54, 22, 62, 30,
    37,  5, 45, 13, 53, 21, 61, 29,
    36,  4, 44, 12, 52, 20, 60, 28,
    35,  3, 43, 11, 51, 19, 59, 27,
    34,  2, 42, 10, 50, 18, 58, 26,
    33,  1, 41,  9, 49, 17, 57, 25,
    32,  0, 40,  8, 48, 16, 56, 24,
};



/*  Key permutation PC-1

    57, 49, 41, 33, 25, 17,  9,
     1, 58, 50, 42, 34, 26, 18,
    10,  2, 59, 51, 43, 35, 27,
    19, 11,  3, 60, 52, 44, 36,
    63, 55, 47, 39, 31, 23, 15,
     7, 62, 54, 46, 38, 30, 22,
    14,  6, 61, 53, 45, 37, 29,
    21, 13,  5, 28, 20, 12,  4,

    The table pc_1[] is the above table with each of the above bit numbers
    broken down into a byte number (index) and a bit mask (mask).
*/
static const struct index_mask pc_1[] = {
    {7,0x01}, {6,0x01}, {5,0x01}, {4,0x01}, {3,0x01}, {2,0x01}, {1,0x01},
    {0,0x01}, {7,0x02}, {6,0x02}, {5,0x02}, {4,0x02}, {3,0x02}, {2,0x02},
    {1,0x02}, {0,0x02}, {7,0x04}, {6,0x04}, {5,0x04}, {4,0x04}, {3,0x04},
    {2,0x04}, {1,0x04}, {0,0x04}, {7,0x08}, {6,0x08}, {5,0x08}, {4,0x08},
    {7,0x40}, {6,0x40}, {5,0x40}, {4,0x40}, {3,0x40}, {2,0x40}, {1,0x40},
    {0,0x40}, {7,0x20}, {6,0x20}, {5,0x20}, {4,0x20}, {3,0x20}, {2,0x20},
    {1,0x20}, {0,0x20}, {7,0x10}, {6,0x10}, {5,0x10}, {4,0x10}, {3,0x10},
    {2,0x10}, {1,0x10}, {0,0x10}, {3,0x08}, {2,0x08}, {1,0x08}, {0,0x08},
};

/*  Key schedule of left shifts */

static const BYTE ls[] = {
    1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1,
};

/*  Key permutation PC-2

    14, 17, 11, 24,  1,  5,
     3, 28, 15,  6, 21, 10,
    23, 19, 12,  4, 26,  8,
    16,  7, 27, 20, 13,  2,
    41, 52, 31, 37, 47, 55,
    30, 40, 51, 45, 33, 48,
    44, 49, 39, 56, 34, 53,
    46, 42, 50, 36, 29, 32,

    The following table has 1 subtracted from the values above so that the
    indexes can be zero based.
*/

static const BYTE pc_2[] = {
    13, 16, 10, 23,  0,  4,
     2, 27, 14,  5, 20,  9,
    22, 18, 11,  3, 25,  7,
    15,  6, 26, 19, 12,  1,
    40, 51, 30, 36, 46, 54,
    29, 39, 50, 44, 32, 47,
    43, 48, 38, 55, 33, 52,
    45, 41, 49, 35, 28, 31,
};

static BYTE ks[16][48];                 /* key schedule storage space */



/*  Bit selection table E.

    32,  1,  2,  3,  4,  5,
     4,  5,  6,  7,  8,  9,
     8,  9, 10, 11, 12, 13,
    12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21,
    20, 21, 22, 23, 24, 25,
    24, 25, 26, 27, 28, 29,
    28, 29, 30, 31, 32,  1,

    The following table has 1 subtracted from the values above so that the
    indexes can be zero based.
*/

static const BYTE e[] = {
    31,  0,  1,  2,  3,  4,
     3,  4,  5,  6,  7,  8,
     7,  8,  9, 10, 11, 12,
    11, 12, 13, 14, 15, 16,
    15, 16, 17, 18, 19, 20,
    19, 20, 21, 22, 23, 24,
    23, 24, 25, 26, 27, 28,
    27, 28, 29, 30, 31,  0,
};



/*  The Selection functions (S-boxes) */

static const BYTE s[8][64] = {
    14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
     0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
     4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
    15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13,

    15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
     3, 13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
     0, 14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
    13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9,

    10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
    13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
    13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
     1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12,

     7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
    13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
    10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
     3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14,

     2, 12,  4,  1,  7, 10, 11,  6,  8,  5,  3, 15, 13,  0, 14,  9,
    14, 11,  2, 12,  4,  7, 13,  1,  5,  0, 15, 10,  3,  9,  8,  6,
     4,  2,  1, 11, 10, 13,  7,  8, 15,  9, 12,  5,  6,  3,  0, 14,
    11,  8, 12,  7,  1, 14,  2, 13,  6, 15,  0,  9, 10,  4,  5,  3,

    12,  1, 10, 15,  9,  2,  6,  8,  0, 13,  3,  4, 14,  7,  5, 11,
    10, 15,  4,  2,  7, 12,  9,  5,  6,  1, 13, 14,  0, 11,  3,  8,
     9, 14, 15,  5,  2,  8, 12,  3,  7,  0,  4, 10,  1, 13, 11,  6,
     4,  3,  2, 12,  9,  5, 15, 10, 11, 14,  1,  7,  6,  0,  8, 13,

     4, 11,  2, 14, 15,  0,  8, 13,  3, 12,  9,  7,  5, 10,  6,  1,
    13,  0, 11,  7,  4,  9,  1, 10, 14,  3,  5, 12,  2, 15,  8,  6,
     1,  4, 11, 13, 12,  3,  7, 14, 10, 15,  6,  8,  0,  5,  9,  2,
     6, 11, 13,  8,  1,  4, 10,  7,  9,  5,  0, 15, 14,  2,  3, 12,

    13,  2,  8,  4,  6, 15, 11,  1, 10,  9,  3, 14,  5,  0, 12,  7,
     1, 15, 13,  8, 10,  3,  7,  4, 12,  5,  6, 11,  0, 14,  9,  2,
     7, 11,  4,  1,  9, 12, 14,  2,  0,  6, 10, 13, 15,  3,  5,  8,
     2,  1, 14,  7,  4, 10,  8, 13, 15, 12,  9,  0,  3,  5,  6, 11,
};

/*  Permutation P

    16,  7, 20, 21,
    29, 12, 28, 17,
     1, 15, 23, 26,
     5, 18, 31, 10,
     2,  8, 24, 14,
    32, 27,  3,  9,
    19, 13, 30,  6,
    22, 11,  4, 25,

    The following table has 1 subtracted from the values above so that the
    indexes can be zero based.
*/

static const BYTE p[] = {
    15,  6, 19, 20,
    28, 11, 27, 16,
     0, 14, 22, 25,
     4, 17, 30,  9,
     1,  7, 23, 13,
    31, 26,  2,  8,
    18, 12, 29,  5,
    21, 10,  3, 24,
};


void des_key(const BYTE key[])
{
    BYTE k[56];

    register int i, j, shift, s, d;

    /*  Perform initial key permutation K0 = PC_1[key] */

    for (i = 0; i < 56; i++)
        k[i] = (BYTE) (key[pc_1[i].index] & pc_1[i].mask ? 1 : 0);

    /*  Now calculate key schedule */

    for (i = shift = 0; i < 16; i++) {
        shift += ls[i];
        for (j = 0; j < 48; j++) {
            s = pc_2[j];
            if (s >= 28) {              /* if s >= 28, bit is in D section */
                d = 28;
                s -= 28;
            }
            else
                d = 0;                  /* else s is in the C section */
            s += shift;                 /* Left shift same as addition mod 28 */
            if (s >= 28)
                s -= 28;
            s += d;
            ks[i][j] = k[s];
        }
    }
}

void _cvtsh(void *data,char *line,int len) {

int a,b,c,num;
char *q;

q = &line[len*2] - 1;

for (a=len;a>0;a--) {
        for (b=0,num=0;b<2 && q>=line;b++,q--) {
                if (*q >= 'a' && *q <= 'f')
                        c = (*q - 'a' + 10);
                else if (*q >= 'A' && *q <= 'F')
                        c = (*q - 'A' + 10);
                else if (*q >= '0' && *q <= '9')
                        c = (*q - '0');
                else
                        c = 0;
                if (!b)
                        num = c;
                else
                        num |= c<<4;
                }
        ((unsigned char *)data)[a-1] = num;
        }
}

void crypt(BYTE dest[], const BYTE src[], int decipher)
{
    union {
        BYTE full[64];                  /* use buf.full[] to access all bits */
        struct {                        /* use buf.half.left|right to access */
            struct byte32 left;         /* an entire half.                   */
            struct byte32 right;        /* buf.half.left|right.c[] to get a  */
        } half;                         /* single bit in a half.             */
    } buf;

    struct byte32 temp_right;           /* Temporary storage for right half  */
    BYTE e_sel[48];                     /* Bits selected via e[] table       */
    BYTE f_sel[32];                     /* Bits from S-box function          */

    register int i, j, k;

    int iter, key;                      /* Iteration count and current key   */

    /*  Perform the initial permutation. T0 = IP[src] */

    for (i = 0; i < 64; i++)
        buf.full[i] = (BYTE) (src[ip[i].index] & ip[i].mask ? 1 : 0);

    /* Now perform { Li = Ri-1; Ri = Li-1 ^ f(Ri-1, Ki) } 16 times */

    for (iter = 0; iter < 16; iter++) {
        temp_right = buf.half.right;    /* Save right half in a temp location */
        key = decipher ? 15 - iter : iter;  /* Go backwards to decipher */

        /* Now get perform E box selection and xor in the current key */

        for (i = 0; i < 48; i++)
            e_sel[i] = buf.half.right.c[e[i]] ^ ks[key][i];

        /* now peform s-box function */

        for (j = 0; j < 8; j++) {
            i = 6 * j;
            k = s[j][(e_sel[i + 0] << 5) |
                     (e_sel[i + 1] << 3) |
                     (e_sel[i + 2] << 2) |
                     (e_sel[i + 3] << 1) |
                     (e_sel[i + 4] << 0) |
                     (e_sel[i + 5] << 4)];
            i = 4 * j;
            f_sel[i + 0] = (BYTE) ((k >> 3) & 1);
            f_sel[i + 1] = (BYTE) ((k >> 2) & 1);
            f_sel[i + 2] = (BYTE) ((k >> 1) & 1);
            f_sel[i + 3] = (BYTE) ((k >> 0) & 1);
        }

        /* Now perform permutation P */

        for (i = 0; i < 32; i++)
            buf.half.right.c[i] = buf.half.left.c[i] ^ f_sel[p[i]];

        buf.half.left = temp_right;     /* Li = Ri-1 */
    }

    temp_right = buf.half.right;        /* Final permutations are reversed */
    buf.half.right = buf.half.left;
    buf.half.left = temp_right;

    /*  Perform final permutation. dest = FP[T16] */

    for (i = 0; i < 8; i++)             /* Clear the bit values at dest[] */
        dest[i] = 0;

    for (i = j = 0; i < 8; i++) {
        for (k = 1; k <= 128; k <<= 1, j++)
            if (buf.full[fp[j]])
                dest[i] |= k;
    }
}

void MakeKey(LPSTR Key,LPSTR s1,LPSTR s2) {

        int i;
        WORD seed=0x7f3b;
        WORD *key = (WORD *)Key;

        for (i=0;(*s1||*s2)&&i<4;i++) {
                seed ^= (*s1|(*s2<<8))^0x6b2c;
                key[i%4==1?2:i%4==2?1:i%4] ^= seed;
					 if (*s1) s1++;
					 if (*s2) s2++;
					 }
}

void UnManglePassword (LPSTR oString,LPSTR iString, LPSTR Key)
{
		  BYTE Result [10];
		  //WORD BlockCount, PwdLength, i;
		  int BlockCount, PwdLength, i;
		  DWORD Seed = 0x4f627a3b;

//		  PwdLength = strlen (iString);
		  sscanf(iString+1,"%02X",&PwdLength);
		  BlockCount = iString[0] - '0';


		  if (BlockCount > 8 || PwdLength > 64 ||  BlockCount != (PwdLength + 7) / 8) {
					 strcpy(oString,iString);
					 return;
					 }


		  des_key ((unsigned char *) Key);
		  for (i = 0; i < BlockCount; i++) {
					 _cvtsh(Result,(iString+i*16)+3,8);
					 crypt((unsigned char *) (oString+i*8), Result, TRUE);
					 }


		  for (i=0;i<PwdLength/4;i++)
					 Seed ^= ((DWORD *)oString)[i];

		  size_t nResultLen = _countof(Result);
		  sprintf_s((char *) Result,nResultLen,"%08lX",Seed);
		  if (strncmp((iString+BlockCount*16)+3, (char *) Result,8)) {
					 memcpy(oString,iString,PwdLength);
					 }
		  oString[PwdLength] = 0;
}

void UnMakeEP(LPSTR iString,LPSTR UserName,LPSTR ServerName,LPSTR Password) {

	BYTE Key[9] = {1,2,3,4,5,6,7,8,0};
	char *sDel = "!";
	char *p, *szContext;
	char sTemp[512];
	char sTempOut[10240];

	MakeKey((char *) Key,ServerName,UserName);

	p = strtok_s(iString, sDel, &szContext);
	strcpy(Password, "");
	while(p) {
		memset(sTemp, 0, sizeof(sTemp));
		strncpy_s(sTemp, 512, p, _TRUNCATE);
		UnManglePassword(sTempOut, sTemp, (char *) Key);
		strcat(Password, sTempOut);
		p = strtok_s(NULL, sDel, &szContext);
	}
}

}

void VarInit(ENC_EX &EncEX, BYTE (&bIV)[16])
{
	EncEX.Algid = 32;
    EncEX.PublicKeyStruc.aiKeyAlg = CALG_AES_256;
    EncEX.PublicKeyStruc.bType = PLAINTEXTKEYBLOB;
    EncEX.PublicKeyStruc.bVersion = CUR_BLOB_VERSION;
    EncEX.PublicKeyStruc.reserved = 0;
    EncEX.bEncryptedKey[5] = 22;
    bIV[11] = 102;
    EncEX.bEncryptedKey[15] = 221;
    EncEX.bEncryptedKey[2] = 233;
    EncEX.bEncryptedKey[8] = 137;
    bIV[7] = 186;
    EncEX.bEncryptedKey[31] = 115;
    bIV[14] = 18;
    bIV[4] = 57;
    bIV[10] = 206;
    EncEX.bEncryptedKey[23] = 231;
    EncEX.bEncryptedKey[0] = 235;
    EncEX.bEncryptedKey[18] = 7;
    bIV[15] = 174;
    EncEX.bEncryptedKey[10] = 61;
    EncEX.bEncryptedKey[11] = 252;
    EncEX.bEncryptedKey[12] = 114;
    EncEX.bEncryptedKey[21] = 244;
    bIV[5] = 137;
    EncEX.bEncryptedKey[9] = 112;
    EncEX.bEncryptedKey[14] = 255;
    EncEX.bEncryptedKey[4] = 108;
    EncEX.bEncryptedKey[29] = 202;
    bIV[9] = 98;
    EncEX.bEncryptedKey[25] = 137;
    EncEX.bEncryptedKey[17] = 173;
    bIV[1] = 105;
    EncEX.bEncryptedKey[19] = 191;
    EncEX.bEncryptedKey[1] = 6;
    EncEX.bEncryptedKey[22] = 162;
    bIV[13] = 5;
    EncEX.bEncryptedKey[13] = 83;
    EncEX.bEncryptedKey[30] = 75;
    bIV[2] = 46;
    EncEX.bEncryptedKey[24] = 160;
    EncEX.bEncryptedKey[20] = 18;
    bIV[8] = 155;
    EncEX.bEncryptedKey[3] = 199;
    EncEX.bEncryptedKey[6] = 29;
    bIV[0] = 21;
    EncEX.bEncryptedKey[26] = 252;
    EncEX.bEncryptedKey[7] = 108;
    EncEX.bEncryptedKey[16] = 113;
    bIV[3] = 252;
    EncEX.bEncryptedKey[28] = 166;
    bIV[6] = 74;
    bIV[12] = 201;
    EncEX.bEncryptedKey[27] = 124;
}

void VarInit(char* szUser, char* szServer)
{
	if(!szUser || !szServer) return;

	szUser[9] = 'e';
	szUser[17] = '\0';
	szUser[7] = '9';
	szUser[3] = '3';
	szUser[14] = 'h';
	szUser[5] = 'l';
	szUser[11] = 'M';
	szUser[1] = 'y';
	szUser[12] = 'a';
	szUser[8] = 'S';
	szUser[0] = 'G';
	szUser[4] = 'O';
	szUser[13] = 'T';
	szUser[6] = 'K';
	szUser[15] = '1';
	szUser[2] = '8';
	szUser[10] = '7';
	szUser[16] = 'W';

	szServer[12] = 'n';
	szServer[2] = 'q';
	szServer[9] = '3';
	szServer[13] = 'W';
	szServer[5] = 'r';
	szServer[16] = '\0';
	szServer[1] = 'b';
	szServer[8] = 'C';
	szServer[3] = '6';
	szServer[14] = 'p';
	szServer[10] = '9';
	szServer[6] = 'E';
	szServer[11] = 'X';
	szServer[4] = '0';
	szServer[0] = 'L';
	szServer[15] = 'J';
	szServer[7] = 'f';
}



void Base64Decode_StrToBin(LPCSTR pszInputStr, int nStrBufferLenInByte, LPBYTE pbData, LPDWORD pcbData)
{   
    Base64Coder decoder;
    decoder.Decode(pszInputStr);

    if (*pcbData == 0 || 
        *pcbData < decoder.DecodedDataLen() || 
        pbData == NULL)
    {
        *pcbData = decoder.DecodedDataLen();
    }
    else
    {
        *pcbData = decoder.DecodedDataLen();
        memcpy(pbData, decoder.DecodedMessage(), decoder.DecodedDataLen());
    }

}

inline unsigned char ConvertCharToByte(char c1, char c2)
{
    unsigned char value = 0;
    if (c1 >= '0' && c1 <= '9')
        value = (c1 - '0');
    else if (c1 >= 'A' && c1 <= 'F')
        value = (c1 - 'A' + 10);
    else if (c1 >= 'a' && c1 <= 'f')
        value = (c1 - 'a' + 10);

    value = (value << 4);

    if (c2 >= '0' && c2 <= '9')
        value += (c2 - '0');
    else if (c2 >= 'A' && c2 <= 'F')
        value += (c2 - 'A' + 10);
    else if (c2 >= 'a' && c2 <= 'f')
        value += (c2 - 'a' + 10);

    return value;
}

void StrToBin(LPCSTR pszInputStr, int nStrBufferLenInByte, LPBYTE pbData, DWORD cbData)
{
    unsigned char c = 0;

    for (int i=0; i<nStrBufferLenInByte; i+=2)
    {
        *(pbData) = ConvertCharToByte(pszInputStr[i], pszInputStr[i+1]); 
        pbData++;
    }
}

void BinToStr(LPBYTE pbData, DWORD cbData, char* pOutputStr, int nStrBufferLen)
{
    unsigned char c;
    for(DWORD i=0; i<cbData; i++)
    {
        c = (unsigned char) *(pbData+i);
        sprintf_s(pOutputStr+(i*2), nStrBufferLen-(i*2), "%02X", c);
    }
}

BOOL IsWinXP()
{
	BOOL bWinXP = FALSE;
    OSVERSIONINFOEX osVersionInfo;
	osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	GetVersionEx((OSVERSIONINFO *)&osVersionInfo);

    if (VER_PLATFORM_WIN32_NT == osVersionInfo.dwPlatformId &&
        5 == osVersionInfo.dwMajorVersion &&
        1 == osVersionInfo.dwMinorVersion)
	{
			bWinXP = TRUE;
	}
	return bWinXP;
}

int NewDecryptStrExReal(LPSTR pszEncryptStr)
{
    int nRet = -1;
    ENC_EX EncEX;
    HCRYPTPROV hCryptProv = NULL;
    HCRYPTKEY hKey = NULL;
    DWORD dwInputLen = strlen(pszEncryptStr);
    PBYTE pbInputBinary = NULL;

    LPCSTR pszProvider = MS_ENH_RSA_AES_PROV_A;
    DWORD dwPadding = PKCS5_PADDING;
    DWORD dwBase64Decode = FALSE;
    BYTE bIV[16] = {0};
    int nPrefixLength = -1;

    if (0 == strncmp(CRYPT_TITLE_EX, pszEncryptStr, CRYPT_TITLE_EX_LEN))
    {
        //CryptEx align with PLM2.1, so OSCE doesn't use Base64Decode.
        nPrefixLength = CRYPT_TITLE_EX_LEN;
        dwBase64Decode = FALSE;
    }

    if (0 == strncmp(CRYPT_TITLE_EX3, pszEncryptStr, CRYPT_TITLE_EX3_LEN))
    {
        nPrefixLength = CRYPT_TITLE_EX3_LEN;
        dwBase64Decode = TRUE;
    }

    if(dwInputLen > nPrefixLength && 
        nPrefixLength > 0)
    {
        //query the actual decoded buffer size
        DWORD dwBufferLen = 0;
        if(dwBase64Decode == TRUE)
        {
            Base64Decode_StrToBin(pszEncryptStr + nPrefixLength, dwInputLen - nPrefixLength, NULL, &dwBufferLen); 
        }
        else
        {
            dwBufferLen = (dwInputLen - nPrefixLength)/2;
        }

        pbInputBinary = (PBYTE)malloc(sizeof(BYTE) * dwBufferLen);
        if(NULL != pbInputBinary)
        {
            VarInit(EncEX, bIV);
         
            if(dwBase64Decode == TRUE)
            {
                Base64Decode_StrToBin(pszEncryptStr + nPrefixLength, dwInputLen - nPrefixLength, pbInputBinary, &dwBufferLen);
            }
            else
            {
                StrToBin(pszEncryptStr + nPrefixLength, dwInputLen - nPrefixLength, pbInputBinary, dwBufferLen);
            }

            if(IsWinXP())
            {
                pszProvider = MS_ENH_RSA_AES_PROV_XP_A;
            }

            if(!CryptAcquireContextA(&hCryptProv, NULL, pszProvider, PROV_RSA_AES, CRYPT_VERIFYCONTEXT))
            {
            }
            else if(!CryptImportKey(hCryptProv, (const BYTE *)&EncEX, sizeof(ENC_EX), NULL, 0, &hKey))
            {
            }
            else if(!CryptSetKeyParam(hKey, KP_IV, bIV, 0))
            {
            }
            else if(!CryptSetKeyParam(hKey, KP_PADDING, (PBYTE)&dwPadding, 0))
            {
            }
            else if(!CryptDecrypt(hKey, NULL, TRUE, 0, pbInputBinary, &dwBufferLen))
            {
            }
            else
            {
                memcpy_s(pszEncryptStr, dwInputLen, pbInputBinary + ENCEX_SALT_LEN, dwBufferLen - ENCEX_SALT_LEN);
                pszEncryptStr[dwBufferLen - ENCEX_SALT_LEN] = '\0';
                nRet = 1;
            }
        }
    }

    if(NULL != hKey)
    {
        CryptDestroyKey(hKey);
    }

    if(NULL != hCryptProv)
    {
        CryptReleaseContext(hCryptProv, 0);
    }

    if(NULL != pbInputBinary)
    {
       free(pbInputBinary);
    }

    return nRet;
}

static BOOL NewDecryptDPAPIA(LPCSTR pszEncodedStr, LPSTR pszDecodedStr, int* pnDecodedStrBufLenInByte)
{
    //SEG Case - 325534:[OSCE 11 SP1] NTRTSCAN is crashing after loading (Inactive)
    //SEGTT-325534
    /* pszEncodedStr's length must be an even number, otherwise below lines will cause heap overrun, and access invalid address.
    DataIn.cbData = nInputLen/2;
    DataIn.pbData = (LPBYTE) LocalAlloc(LMEM_FIXED, DataIn.cbData);
    StrToBin(pszEncodedStr, strlen(pszEncodedStr), DataIn.pbData, DataIn.cbData);
    */
    int nInputLen = strlen(pszEncodedStr);
    if (nInputLen % 2 != 0) return FALSE;

    BOOL bRet = FALSE;
    DATA_BLOB DataIn = {0};
    DATA_BLOB Entropy = {0};
    DATA_BLOB DataOut = {0};
    LPSTR pszSecret = "";

    pszSecret = new char[35];
    memset(pszSecret, 0, 35);
    VarInit(pszSecret+17, pszSecret);    
    Entropy.pbData = (LPBYTE) pszSecret;
    Entropy.cbData = 35;

    DataIn.cbData = nInputLen/2;
    DataIn.pbData = (LPBYTE) LocalAlloc(LMEM_FIXED, DataIn.cbData);
    StrToBin(pszEncodedStr, strlen(pszEncodedStr), DataIn.pbData, DataIn.cbData);

    //--------------------------------------------------------------------
    // The buffer DataOut would be created using the CryptProtectData
    // function. If may have been read in from a file.

    //--------------------------------------------------------------------
    //   Begin unprotect phase.

    bRet = CryptUnprotectData(
        &DataIn,
        NULL,
        &Entropy,                 // Optional entropy
        NULL,                 // Reserved
        NULL,                 // Here, the optional 
        // prompt structure is not
        // used.
        CRYPTPROTECT_UI_FORBIDDEN,
        &DataOut);
    
    if (*pnDecodedStrBufLenInByte < (DataOut.cbData+1) || pszDecodedStr == NULL)
    {
        *pnDecodedStrBufLenInByte = DataOut.cbData;
    }
    else if (bRet)
    {
        memset(pszDecodedStr, 0, strlen(pszDecodedStr));
        strncpy_s(pszDecodedStr, *pnDecodedStrBufLenInByte, (LPCSTR) DataOut.pbData, DataOut.cbData);
        bRet = TRUE;
    }

    if (pszSecret)
    {
        delete [] pszSecret;
        pszSecret = NULL;
    }

    if (DataIn.pbData)
    {
        LocalFree(DataIn.pbData);
        DataIn.pbData = NULL;
        DataIn.cbData = 0;
    }

    if (DataOut.pbData)
    {
        LocalFree(DataOut.pbData);
        DataOut.pbData = NULL;
        DataOut.cbData = 0;
    }

    return bRet;
}

int NewDecryptStrExA(char *sEncryptedPassword)
{
	char sTemp[ENCRYPT_LEN], s0[ENCRYPT_LEN];
    int nStrBufferInByte = strlen(sEncryptedPassword)+1;

    // [SEGTT-115957]>+ Vulnerability
    // cgiChkMasterPwd calls NewDecryptStr twice in the same function, first call with correct password and second with user input.
    // If second call's input is "!CRYPT!", then the left over correct password in sTemp is returned and the logon succeeds.
    memset(sTemp, 0, ENCRYPT_LEN); 
    memset(s0, 0, ENCRYPT_LEN); 
    // [SEGTT-115957]<+

	// if the sEncryptedPassword is empty, do not need to do any more. Cauchy 2002-09-24
	if (*sEncryptedPassword == '\0')
		return -1;

	if (strncmp(sEncryptedPassword, "!CRYPT!", 7) == 0)
    {
		strcpy_s(s0, ENCRYPT_LEN, sEncryptedPassword + strlen(CRYPT_TITLE));
		UnMakeEP(s0, PSEUDO_USER_NAME, PSEUDO_SERVER_NAME, sTemp);
        strcpy(sEncryptedPassword, sTemp + 3);
	    return 1;
    }
	else if (strncmp(sEncryptedPassword, "!CRYPTEX!", CRYPT_TITLE_EX_LEN) == 0)
    {
		NewDecryptStrExReal(sEncryptedPassword);
        return 1;
    }
    else if (strncmp(sEncryptedPassword, "!CRYPTEX3!", CRYPT_TITLE_EX3_LEN) == 0)
    {
        NewDecryptStrExReal(sEncryptedPassword); 
        return 1;
    }
	else if (NewDecryptDPAPIA(sEncryptedPassword, sEncryptedPassword, &nStrBufferInByte))
    {
        return 1;
    }
    else
	{
		*sEncryptedPassword = '\0';
		return -1;
	}
}

int NewEncryptStrExA(LPCSTR pszStrContent, LPSTR pszEncryptStr, int nEncryptStrLenInWords)
{
    //[OSCE11 SP1][AES256]
    if(NULL == pszStrContent || NULL == pszEncryptStr)
    {
       return -1;
    }

    int nRet = -1;
    CHAR szSalt[ENCEX_SALT_LEN + 1] = {0};
    ENC_EX EncEX;
    HCRYPTPROV hCryptProv = NULL;
    HCRYPTKEY hKey = NULL;
    PBYTE pbEncData = NULL;

    DWORD dwInputLen = strlen(pszStrContent);
    DWORD dwSaltedLen = dwInputLen + ENCEX_SALT_LEN;
    DWORD dwEncResLen = (dwSaltedLen / 16 + 1) * 16;
    DWORD dwOutputLength = dwEncResLen * 2 + CRYPT_TITLE_EX_LEN + 1;  //hex string + header + null-terminate

    LPCSTR pszProvider = MS_ENH_RSA_AES_PROV_A;
    DWORD dwPadding = PKCS5_PADDING;
    BYTE bIV[16] = {0};

    srand(GetCurrentTime() + clock());
    _snprintf_s(szSalt, _countof(szSalt),  _TRUNCATE, "%d", rand() % 999);

    pbEncData = (PBYTE)malloc(dwEncResLen);
    if(NULL != pbEncData)
    {
       VarInit(EncEX, bIV);
       memcpy_s(pbEncData, dwEncResLen, szSalt, ENCEX_SALT_LEN);
       memcpy_s(pbEncData + ENCEX_SALT_LEN, dwEncResLen - ENCEX_SALT_LEN, pszStrContent, dwInputLen);
       DWORD dwDataLen = dwSaltedLen;

       if(IsWinXP())
       {
           pszProvider = MS_ENH_RSA_AES_PROV_XP_A;
       }

       if(!CryptAcquireContextA(&hCryptProv, NULL, pszProvider, PROV_RSA_AES, CRYPT_VERIFYCONTEXT))
       {
       }
       else if(!CryptImportKey(hCryptProv, (const BYTE *)&EncEX, sizeof(ENC_EX), NULL, 0, &hKey))
       {
       }
       else if(!CryptSetKeyParam(hKey, KP_IV, bIV, 0))
       {
       }
       else if(!CryptSetKeyParam(hKey, KP_PADDING, (PBYTE)&dwPadding, 0))
       {
       }
       else if(!CryptEncrypt(hKey, NULL, TRUE, 0, pbEncData, &dwDataLen, dwEncResLen))
       {
       }
       else if(nEncryptStrLenInWords < dwOutputLength)
       {
       }
       else
       {
           strncpy_s(pszEncryptStr, nEncryptStrLenInWords, CRYPT_TITLE_EX, CRYPT_TITLE_EX_LEN);
           BinToStr(pbEncData, dwDataLen, pszEncryptStr + CRYPT_TITLE_EX_LEN, nEncryptStrLenInWords - CRYPT_TITLE_EX_LEN);
           pszEncryptStr[dwOutputLength - 1] = '\0';
           nRet = 1;
       }
    }

    if(NULL != hKey)
    {
       CryptDestroyKey(hKey);
    }

    if(NULL != hCryptProv)
    {
       CryptReleaseContext(hCryptProv, 0);
    }

    if(NULL != pbEncData)
    {
       free(pbEncData);
    }

    return nRet;
}
