#include <stdio.h>
#include <windows.h>

struct DesireAccessClass {
	DWORD desireAccess;
	char str[100];
};

struct DesireAccessClass allDesireAccess[] = {
	{0, ""},
	{GENERIC_READ, "GENERIC_READ"},
	{GENERIC_WRITE, "GENERIC_WRITE"},
	{GENERIC_READ|GENERIC_WRITE, "GENERIC_READ|GENERIC_WRITE"}
	//{GENERIC_ALL, "GENERIC_ALL"}
};

int DesireAccessCnt = sizeof(allDesireAccess)/sizeof(struct DesireAccessClass);

struct ShareModeClass {
	DWORD shareMode;
	char str[100];
};

struct ShareModeClass allShareMode[] = {
	{0, ""},
	{FILE_SHARE_READ, "FILE_SHARE_READ"},
	{FILE_SHARE_WRITE, "FILE_SHARE_WRITE"},
	{FILE_SHARE_DELETE, "FILE_SHARE_DELETE"},
	{FILE_SHARE_READ|FILE_SHARE_WRITE, "FILE_SHARE_READ|FILE_SHARE_WRITE"},
	{FILE_SHARE_READ|FILE_SHARE_DELETE, "FILE_SHARE_READ|FILE_SHARE_DELETE"},
	{FILE_SHARE_WRITE|FILE_SHARE_DELETE, "FILE_SHARE_WRITE|FILE_SHARE_DELETE"},
	{FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, "FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE"},
};

int ShareModeCnt = sizeof(allShareMode)/sizeof(struct ShareModeClass);

HANDLE myCreate(char* name, DWORD desire, DWORD share, char* errstr, DWORD* err) {
	//printf("open with %08x, %d\n", desire, share);
	HANDLE h = CreateFileA(name, desire, share, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (h == INVALID_HANDLE_VALUE && err != NULL) {
		*err = GetLastError();
		if (errstr != NULL) {
			DWORD   dwChars;  // Number of chars returned.
			// Try to get the message from the system errors.
			dwChars = FormatMessageA( FORMAT_MESSAGE_FROM_SYSTEM |
									 FORMAT_MESSAGE_IGNORE_INSERTS,
									 NULL,
									 *err,
									 0,
									 errstr,
									 512,
									 NULL );
		}
	}
	return h;
}

void test1(char* filename) {
	char errstr[1024];
	DWORD err;
	printf("1st DesireAccess, 1st ShareMode, 1st Result, 2nd DesireAccess, 2nd ShareMode, 2nd Result\n");
	for (int d = 0; d < DesireAccessCnt; d++) {
		for (int s = 0; s < ShareModeCnt; s++) {
			HANDLE h1 = myCreate(filename, allDesireAccess[d].desireAccess, allShareMode[s].shareMode, errstr, &err);
			if (h1 == INVALID_HANDLE_VALUE) {
				printf("%s, %s, %d\n", allDesireAccess[d].str, allShareMode[s].str, err);
				continue;
			}

			for (int d2 = 0; d2 < DesireAccessCnt; d2++) {
				for (int s2 = 0; s2 < ShareModeCnt; s2++) {
					HANDLE h2 = myCreate(filename, allDesireAccess[d2].desireAccess, allShareMode[s2].shareMode, errstr, &err);
					if (h2 == INVALID_HANDLE_VALUE) {
						printf("%s, %s, %d, %s, %s, %d\n", allDesireAccess[d].str, allShareMode[s].str, 0, allDesireAccess[d2].str, allShareMode[s2].str, err);
					} else {
						printf("%s, %s, %d, %s, %s, %d\n", allDesireAccess[d].str, allShareMode[s].str, 0, allDesireAccess[d2].str, allShareMode[s2].str, 0);
						CloseHandle(h2);
					}
				}
			}
			
			CloseHandle(h1);
		}
	}
}

void test2(char* filename)
{
	char errstr[1024];
	DWORD err;
	HANDLE h1 = myCreate(filename, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, NULL, &err);
	if (h1 == INVALID_HANDLE_VALUE) {
	}
	HANDLE h2 = myCreate(filename, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, NULL, &err);
	if (h2 == INVALID_HANDLE_VALUE) {
		//printf("error %d\n", GetLastError());
		DWORD   dwChars;  // Number of chars returned.
		// Try to get the message from the system errors.
		dwChars = FormatMessageA( FORMAT_MESSAGE_FROM_SYSTEM |
								 FORMAT_MESSAGE_IGNORE_INSERTS,
								 NULL,
								 err,
								 0,
								 errstr,
								 512,
								 NULL );
		printf("%d, %s\n", err, errstr);
	}
}

int main(int argc, char* argv[]) {	
	char filename[] = "C:\\work\\temp\\testSharingViolation\\test";
	test1(filename);
	//test2(filename);
	return 0;
}