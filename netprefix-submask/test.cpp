#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void check(char* s) {
    in_addr_t ip;
    in_addr addr;
    ip = inet_addr(s);
    addr.s_addr = ip;
    printf("origin: %s, convert: %s, %d\n", s, inet_ntoa(addr), addr.s_addr);
}

void NetworkPrefixToSubmask(int prefix, in_addr* addr) {
    if (addr == NULL) {
        return;
    }
    if (prefix == 0) {
        addr->s_addr = 0; 
        return;
    }
    char submask[20];
    unsigned int mask = (0xFFFFFFFF << (32 - prefix)) & 0x0FFFFFFFF;
    snprintf( submask, sizeof(submask)-1, "%d.%d.%d.%d", mask>>24&0x0FF,mask>>16&0x0FF,mask>>8&0x0FF,mask&0x0FF );
    addr->s_addr = inet_addr(submask);
}
void networkPrefixToSubmask(int prefix, char* submask) {
    if (submask == NULL) {
        return;
    }
    unsigned int mask = 0;
    if (prefix != 0) {
        mask = (0xFFFFFFFF << (32 - prefix)) & 0x0FFFFFFFF;
    }
    snprintf( submask, 16, "%d.%d.%d.%d", mask>>24&0x0FF,mask>>16&0x0FF,mask>>8&0x0FF,mask&0x0FF );
}

void cc(int prefix) {
    char ip[20];
    in_addr addr;
    networkPrefixToSubmask(prefix, ip);
    printf("mask: %d, ip: %s\n", prefix, ip);
    return;
    unsigned int mask = (0xFFFFFFFF << (32 - prefix)) & 0x0FFFFFFFF;
    snprintf( ip, sizeof(ip)-1, "%d.%d.%d.%d", mask>>24&0x0FF,mask>>16&0x0FF,mask>>8&0x0FF,mask&0x0FF );
    addr.s_addr = inet_addr(ip);
    printf("mask: %d, ip: %s, %d\n", prefix, inet_ntoa(addr), addr.s_addr);
}

int main(int argc, char* argv[]) {
    //check("10.0.0.0");
    //check("10.0.0.0/255.0.0.0");
    check("255.0.0.0");
    check("255.254.0.0");
    cc(0);
    cc(8);
    cc(9);
    cc(31);
    cc(32);
    return 0;
}

