#ifndef __IO_LIB_H__
#define __IO_LIB_H__

#include "vsio.h"

struct _CA_HANDLE;

/* open methods */
int _io_open_file_a(const VUCHAR* file_name, VS_SIZE_T start_offset, VS_SIZE_T limit_size, VULONG open_flag, struct _VSIO** p_io);
int _io_open_file_w(const VWCHAR* file_name, VS_SIZE_T start_offset, VS_SIZE_T limit_size, VULONG open_flag, struct _VSIO** p_io);
int _io_plugin_pio(struct _VSIO* io, VS_SIZE_T start_offset, VS_SIZE_T limit_size, VS_SIZE_T ready_size, VULONG alignment, VULONG open_flag, struct _VSIO** p_io);
int _io_plugin_pio_enc(struct _VSIO** p_io, VS_SIZE_T start_offset, VS_SIZE_T limit_size, VS_SIZE_T ready_size, VULONG alignment, VULONG open_flag);
int _io_open_mem(void* mem, VULONG limit_size32, VULONG ready_size32, VULONG alignment, VULONG open_flag, struct _VSIO** p_io);


#endif /* __IO_LIB_H__ */

