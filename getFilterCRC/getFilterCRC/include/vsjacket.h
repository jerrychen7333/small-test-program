/* $Date: 1999/5/10 PM 02:16:14$ */
/*
 * $Log: 
 *  5    NewScanEngine1.4         1999/5/10 PM 02:16:14Viking_ho       char* ->
 *       VCHAR*
 *  4    NewScanEngine1.3         1998/11/9 PM 02:00:50Lily_ho         
 *  3    NewScanEngine1.2         1998/5/21 AM 02:06:22Cliff_liang     Add
 *       TMDScanFileWithOffset() function prototype which can support scan file
 *       with offset.
 *  2    NewScanEngine1.1         1998/4/24 AM 11:38:16Lily_ho         
 *  1    NewScanEngine1.0         1998/4/23 AM 09:40:06Lily_ho         
 * $
 * 
 * 5     99/05/10 2:16p Viking
 * char* -> VCHAR*
 * 
 * 4     11/09/98 2:00p Lily
 * 
 * 8     11/06/98 4:10p Cliff_liang
 * Change vsc(virus scan context) from type long to VSCTYPE. All
 * platforms, except AS400, still have VSCTYPE defined as long. so program
 * don't need to change anything thing.
 * 
 * 3     5/21/98 2:06a Cliff
 * Add TMDScanFileWithOffset() function prototype which can support scan
 * file with offset.
 *
 * 8	 3/20/98 2:59p Cliff
 * Add proejct name and Trend Copy right declaration.
 *
 * 6	 11/03/97 1:23p Cliff
 * replace DWORD with VULONG
 *
 * 5	 10/30/97 4:51p Cliff
 * Add definition of EXPORT and PASCAL if user don't want to include
 * tmvs.h
 *
 * 3	 9/03/97 12:08p Cliff
 * Add Jacket layer to old UNIX engine.
 *
 * 2	 8/05/97 2:06p Cliff
 * Add prototype of ScanFileVirusWithOffset
 *
 * 1	 7/24/97 5:31p Cliff
 * Definition of Jacket layer to old scan engine
 *
 * Revision 1.0  1997/07/23 14:38:16  cliff
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* vsjacket.h */
/* Description: jacket layer definition */
/* */
/**************************************************************************/

#ifndef __VSJACKET_H__
#define __VSJACKET_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef EXPORT
#ifdef WIN16
#define EXPORT _export
#else
#define EXPORT
#endif	/* WIN16 */
#endif	/* EXPORT */

#ifndef PASCAL
#ifdef WIN16
#define PASCAL _pascal
#else
#define PASCAL
#endif	/* WIN16 */
#endif	/* PASCAL */

/* jacket interface to old engine */
typedef struct {	/*This structure only for NT 9/18/95*/
	HANDLE	hConnection;
	void	*Context;
	/* will be zero if this is a new file or if this was a read action*/
	VULONG	OrgHeaderValidBytes;
	VUCHAR	*OrgHeader; /* file header before file was writtento*/
} NTSTRUCT;

typedef struct {
	char *FileName; /*Original file name*/
	char *ExtractFileName;	/*Extract file name (if is a compress file)*/
	char VirusName[20]; /*Virus name*/
	unsigned short VirusNumber; /*Virus Number (for clean)*/
	short VirusType; /*virus Type (ZIP,LZEXE,...,SOFTMICE,TAILSCAN)*/
	short CleanType; /*clean type (CLN_CLEANABLE,CLN_OVERWRITE, */
		/* CLN_TAILSCAN,CLN_NOCLEANPTN)*/
	NTSTRUCT *NTstruct; /*only for NT*/
	short VirusInfo;	/*Pattern Type,please reference above info.*/
	unsigned char VirusMacroNum[256];
} FVFOUNDBLK;

typedef struct {
	/*original file name (full path);*/
	char FullFileName[260];

	/*call back fun ction if virus found*/
	void (*VirusFoundCallBackFun)(FVFOUNDBLK *FVFoundBlk);

	/*scantype (want to detect compressesed, softmice virus */
		short ScanType;

	short NumFileScan;	/*How many files scanned  (OUTPUT)*/
	short NumVirusFound;	/*How many virus detected (OUTPUT)*/
	NTSTRUCT *NTstruct; /*only for NT*/
} SCANFILEBLK;

typedef struct JVI {
	struct JVI *vi_next;	/* pointer to next in list */
	struct JVI *vi_prep;	/* pointer to previous in list */
	char vi_VirusName[20];	/* name of virus */
	long vi_Action;
	short vi_SequenceNumber;	/* sequence number of node in list */
	short vi_VirusNumber;	/* unique number of virus */
	char *vi_ZipArchiveName;	/* pointer to zip archive, null */
					/* if not compressed archive */
	char *vi_OrgFileName;	/* name of original file */
	char vi_FileName[1];	/* file name */
} JVIRINFO;

/* jacket layer */
int PASCAL EXPORT ReadPatternFile(int TestOnly,char *PatternPath,VUSHORT Mask);
void PASCAL EXPORT FreeVirusPattern(void);
int PASCAL EXPORT ScanFileVirus(SCANFILEBLK *p);
int PASCAL EXPORT ScanFileVirusWithOffset(SCANFILEBLK *p,long Offset);
int PASCAL EXPORT OleRemoveVirus(char *file,VUSHORT vn,char *GenericFlag,void *OlePtr,void (*CannotRemove)(int,VCHAR *,VUSHORT));

/* jacket layer for UNIX */
int PASCAL EXPORT TMDVirusScanTerminate(VSCTYPE);
VSCTYPE PASCAL EXPORT TMDVirusScanInit(char *patpath,int UseLitePattern);
int PASCAL EXPORT TMDFileNeedScan(VSCTYPE,int fd,int *NeedVScan);
int PASCAL EXPORT TMDScanFileWithOffset(VSCTYPE,unsigned char *,unsigned char *,JVIRINFO **,int *,long);
int PASCAL EXPORT TMDScanFile(VSCTYPE,unsigned char *,unsigned char *,JVIRINFO **,int *);
void PASCAL EXPORT TMDFreeVIList(JVIRINFO *);
void PASCAL EXPORT TurnOnScanZipFlag(void);
void PASCAL EXPORT TurnOffScanZipFlag(void);
void PASCAL EXPORT TurnOnScanZip1(void);
void PASCAL EXPORT TurnOffScanZip1(void);
void PASCAL EXPORT TurnOnScanZip2(void);
void PASCAL EXPORT TurnOffScanZip2(void);
void PASCAL EXPORT TurnOnSoftMice(void);
void PASCAL EXPORT TurnOffSoftMice(void);
void PASCAL EXPORT TurnOnTailScan(void);
void PASCAL EXPORT TurnOffTailScan(void);
int PASCAL EXPORT SetDecompressLayer(int);

#ifdef __cplusplus
}
#endif

#endif /* __VSJACKET_H__ */
