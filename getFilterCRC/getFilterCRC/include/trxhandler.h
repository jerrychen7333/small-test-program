#ifndef TRXHANDLER_H
#define TRXHANDLER_H

/*! \file trxhandler.h
    The structure document of trend X handler
*/

/*! \brief void pointer */
typedef  void *          xHandle;
/*! \brief void pointer */
typedef  void *          xResultHandle;
/*! \brief unsigned long */
typedef  unsigned long   XHDL_DWORD;
/*! \brief unsigned char */
typedef  unsigned char   XHDL_BYTE;
/*! \brief unsigned short */
typedef  unsigned short  XHDL_WORD;
/*! \brief int */
typedef  int             XHDL_INT;
/*! \brief uint */
typedef  unsigned int    XHDL_UINT;
/*! \brief char */
typedef  char            XHDL_CHAR;

/*! \brief Current File keys structure version */
// #define XHDL_FILE_KEYS_STRUCT_VERSION           0x1001
#define XHDL_FILE_KEYS_STRUCT_VERSION           0x1002
/*! \brief Current Behavior keys structure version */
#define XHDL_BEHAVIOR_KEYS_STRUCT_VERSION       0x2001
/*! \brief Current Query structure version */
#define XHDL_QUERY_STRUCT_VERSION              0x10001

/*! \brief File keys feature version length */
#define FILE_KEYS_FEATURE_VERSION_LENGTH       20
/*! \brief File keys SHA1 length */
#define FILE_KEYS_SHA1_LENGTH                  20
/*! \brief Behavior keys SHA1 length */
#define BEHAVIOR_KEYS_SHA1_LENGTH              20

/*! \brief Engine name max length */
#define ENGINE_NAME_MAX_LENGTH                 128

/*! Keys for File engine */
typedef struct {
  XHDL_DWORD struct_version;   /*!< \brief structure version */
  XHDL_DWORD trigger_id;       /*!< \brief trigger ID */
  XHDL_DWORD file_type;        /*!< \brief file type */
  XHDL_DWORD file_sub_type;    /*!< \brief file subtype */
  XHDL_BYTE* feature_version;  /*!< \brief SHA-1 SIZE, SHA-1 of feature ID */
  XHDL_BYTE* file_sha1;        /*!< \brief SHA-1 SIZE */
  XHDL_CHAR* engine_name;      /*!< \brief (Ex. "VSAPI10.860-1001" or "ATSE10.860-1001") */
  XHDL_BYTE* metadata_version; /*!< \brief SHA-1 SIZE, SHA-1 of metadata ID */
  XHDL_BYTE* blob_sha1;        /*!< \brief SHA-1 SIZE, SHA-1 of blob of features + metadata */
} xFileKeys;

/*! Keys for Behavior engine */
typedef struct {
  XHDL_DWORD struct_version;   /*!< \brief structure version */
  XHDL_DWORD context_id;       /*!< \brief context ID */
  XHDL_DWORD feature_version;  /*!< \brief Example feature version = 0x01020100 Product type id + OS platform id + Falcon Core major version + Falcon Core minor version */
  XHDL_BYTE* file_sha1;        /*!< \brief SHA-1 SIZE */
  XHDL_CHAR* engine_name;      /*!< \brief (Ex. "Falcon-1.0.1001-win32") */
} xBehaviorKeys;

/*! Query struct */
typedef struct { 
  XHDL_DWORD struct_version;   /*!< \brief structure version */
  xHandle handle;              /*!< \brief handle */
} xQueryStruct;


#endif //TRXHANDLER_H
