/* $Date: 2001/9/3 PM 04:13:59$ */
/*
 * $Log: 
 *  10   5.500     1.7.1.1     2001/9/3 PM 04:13:59 Wayne_chu       code review
 *  9    5.500     1.7.1.0     2001/8/13 AM 11:52:13Wayne_chu       add BackupTime
 *  8    NewScanEngine1.7         2001/2/19 PM 05:03:44Wayne_chu       Synchronize
 *       cheetah and cheeath_rel
 *  7    NewScanEngine1.6         2001/1/19 AM 11:58:06Wayne_chu       add
 *       VSGetBackupFileInfo and fix the problem of convert between Unicode and
 *       Ansicode.
 *  6    NewScanEngine1.5         2001/1/5 PM 06:37:22 Wayne_chu       Sync
 *       CheetahRel source code.
 *  5    NewScanEngine1.4         2000/12/12 PM 05:47:28Wayne_chu       Backup file
 *       with encode and restore file with decode
 *  4    NewScanEngine1.3         2000/7/13 PM 06:44:24Viking_ho       modify
 *  3    NewScanEngine1.2         2000/7/13 PM 05:03:30Viking_ho       modify
 *  2    NewScanEngine1.1         2000/7/13 PM 03:53:01Viking_ho       modify
 *       VSStringToShort and VSStringToLong for Alpha
 *  1    NewScanEngine1.0         2000/7/13 AM 10:52:12Viking_ho       
 * $
 * 
 * 8     11/02/99 2:39p Joanna
 * RAR_MAXWINSIZE should be 0x100000
 * some platform like win16 cannot support it, so declare it as 0x10000
 * 
 * 7     99/05/24 1:28p Viking
 * VCHAR should be defined separately by m_xxx.h
 * 
 * 6     11/09/98 2:00p Lily
 * 
 * 15    11/06/98 4:08p Cliff_liang
 * define VSCTYPE(=long) 
 * 
 * 5     8/21/98 10:39a Lily
 * 
 * 3     8/21/98 10:38a Lily
 * modify by lily (8/21/98) :: add VSVirusScanFileW prototype
 * 
 * 3     7/28/98 5:30p Lily
 * ;;;lily(7/28/98)
 * got from roger
 * 
 * 6     7/23/98 7:00p Roger
 * publish three more functions.
 *
 * 4	 7/14/98 6:09p Roger
 * define functions for unicode system.
 *
 * 21	 4/22/98 3:28p Cliff
 * Add definition of DECOMP_MIN_MEM and DECOMP_MAX_MEM
 *
 * 20	 3/23/98 2:25p Cliff
 * VULONG and VLONG should be defined separately by m_xxx.h
 *
 * 19	 3/20/98 2:59p Cliff
 * Add proejct name and Trend Copy right declaration.
 *
 * 16	 11/03/97 12:22p Cliff
 * Move internal use data to header
 *
 * 13	 10/20/97 11:39a Cliff
 * redefine MACOPEN
 *
 * 10	 10/16/97 3:33p Cliff
 * Add MACCLOSE, MACOPEN,MACSEEK, and MACREAD macro
 *
 * 7	 9/23/97 3:05p Cliff
 * Remove VSRotateRight definition because it was used only by getoutvr.c
 *
 * 4	 7/31/97 12:47p Cliff
 * Move pragma pack(1) from tmvsdef.h to here.
 *
 * 3	 7/24/97 12:47p Cliff
 * Remove ALTERNATIVE
 *
 * 2	 7/03/97 11:03a Rogerc
 * merged with Cliff's updated on 0702.
 *
 * 15	 6/16/97 6:38p Roger
 * remove the macro CHSIZE.
 *
 * 14	 6/11/97 11:29a Roger
 *
 * 13	 5/21/97 3:53p Roger
 *
 * 12	 5/19/97 11:14a Roger
 * eliminate the compiler warning about access and unlink function.
 *
 * 11	 5/16/97 4:55p Roger
 *
 * 10	 5/16/97 11:01a Roger
 * define macro VSRotateRight.
 *
 * 9	 5/02/97 3:00p Roger
 *
 * 8	 4/29/97 1:48p Roger
 * after merge with Cliff's source code on 24/4.
 *
 * 7	 4/28/97 12:56p Roger
 * port to dos ole function.
 *
 * 6	 4/23/97 11:00a Roger
 * After merged the source code from Cliff(0417 and 0421) and fixed some
 * bugs.
 *
 * 4	 4/03/97 5:50p Roger
 *
 * 3	 3/26/97 2:05p Roger
 * sync the source codes from Cliff 03/21/97.
 *
 * Revision 1.0  1996/07/31 14:38:16  cliff
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_win32.h */
/* Description: */
/*	Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: Win32 MSVC 4.x */
typedef char          VCHAR;/* 1 byte */
typedef long			VLONG;	/* 4 byte */
typedef unsigned long	VULONG; /* 4 byte */
typedef __int64	    	 VINT64;  /* 8 byte */
typedef unsigned __int64 VUINT64; /* 8 byte */

#pragma pack(1)

#define VSCTYPE	long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR	"c:\\virus"
#define DEFAULT_PATTERN_DIR 	"c:\\dos"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR	"c:\\tmp"
#define EXPORT
#define TMVSAPI
#define MAX_PATH_LEN		1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PACK_STRUCTURE
#define VSLongLongToString(cp,x)	(*(VUINT64 *)(cp) = x)
#define VSLongToString(cp,x)	    (*(VULONG *)(cp) = x)
#define VSShortToString(cp,x)	    (*(VUSHORT *)(cp) = x)
#define VSStringToLongLong(cp)	VSStringToLongLong4Alpha
#define VSStringToLong          VSStringToLong4Alpha
#define VSStringToShort         VSStringToShort4Alpha

/* for Backup file header */
typedef struct
{
	WCHAR FileName[MAX_PATH_LEN];
	char PlatformName[32];
	long FileAttrib;
	VULONG IsUnicode;
	VULONG BackupTime;
} VSBackupFileInfoW;

VUSHORT PASCAL EXPORT VSStringToShort4Alpha(char FAR *cp);
VULONG PASCAL EXPORT VSStringToLong4Alpha(char FAR *cp);

/*==========================================================================*/
/* VSVirusScanFileW(): scan a file for known virus after the file name pass */
/*	the file name filter */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *file: IN : file for scan */
/*	void *para: IN : user defined parameter */
/* Return : */
/*	0: file is clean, or the file failed the file name filter and has */
/*		not been scanned */
/*	>0: file is infected, return virus number */
/*	-1: failed to scan */
/*	PARA_ERR: invalid Parameter */
int PASCAL EXPORT VSVirusScanFileW(VSCTYPE vsc,WCHAR *file,void *para);

/*==========================================================================*/
/* VSFileNeedProcessW() : check if file need process */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *file: IN : file path */
/* Return: */
/*	0: no */
/*	1: yes */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSFileNeedProcessW(VSCTYPE vsc,WCHAR *file);

/*==========================================================================*/
/* VSOpenFileW(): like open(), portable version for all platforms/compilers */
/* Parameters: */
/*	WCHAR *FilePath: IN : file path to open */
/*	int mode: IN : read/write mode */
/* Return: */
/*	0: ok */
/*	OPEN_R_ERR: open error */
/*	PARA_ERR: invalid parameter */
HANDLE PASCAL EXPORT VSOpenFileW(WCHAR *FilePath,int mode);

/*==========================================================================*/
/* VSCopyFileW(): copy a file */
/* Parameters: */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSCopyFileW(WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSCleanVirusW() : Clean Unicode virus file */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *name: IN : file path to be cleaned */
/*	char *bakname: OUT : full path of backuped file */
/*	int len: IN : length of buffer for backuped file */
/*	int* bakrc: OUT : 0 -- backuped ok, other -- backup failed */
/* Return: */
/*	0: clean successed */
/*	-1: unable to clean */
/*	-20: no pattern */
/*	OPEN_W_ERR: Clean file create failed */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSCleanVirusW(VSCTYPE vsc,WCHAR *name,VCHAR *bakname,int len,int *bakrc);

/*==========================================================================*/
/* VSActOnFileW() : Take action on UniCode file */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	short action: IN : override action setting */
/*	WCHAR *file: IN : file path */
/*	WCHAR *NewPath: OUT: buffer to store new path after renamed or moved */
/* Return: */
/*	>0: action return when asked */
/*	0: action successed */
/*	-1: action failed */
/*	-2: failed to create uniq name for new file */
/*	-3: failed to delete original file */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSActOnFileW(VSCTYPE vsc,short action,WCHAR *filepath,WCHAR *NewPath);

/*==========================================================================*/
/* VSBackupResourceW() : Backup from resource (limited to file) */
/* Parameters: */
/*    VSCTYPE vsc: IN: virus scan context */
/*    VSHANDLE *pSrcResource: IN : resource to be backup */
/*    VSHANDLE *pDestResource: IN : resource for backup */
/*    VSBackupFileInfoW: IN: backup information for backup header */
/* Return: */
/*    0: backup successes */
/*    OPEN_W_ERR: Clean file create failed */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    BUFFER_TOO_SHORT_ERR: insufficient buffer */
/*    PARA_ERR: invalid parameter */
/*    BREAK_ERR: VSAPI is forced to stop */
int PASCAL EXPORT VSBackupResourceW(VSCTYPE vsc, struct _VSHANDLE *pSrcResource, struct _VSHANDLE *pDestResource, VSBackupFileInfoW *pBackupInfo);

/*==========================================================================*/
/* VSBackupFileW(): backup a file */
/* Parameters: */
/*  VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSBackupFileW(VSCTYPE vsc,WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSRestoreFileW(): restore a backuped file */
/* Parameters: */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSRestoreFileW(WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSEncBackupFileW(): backup a file with decoding */
/* Parameters: */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSEncBackupFileW(WCHAR *SrcFile, WCHAR *DestFile);

/*==========================================================================*/
/* VSGetBackupFileInfoW(): restore a backuped file */
/* Parameters: */
/*	WCHAR *FileName: IN : backup file name */
/*  VSBackupFileInfo *BackupInfo : OUT : backup file information */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*  -5: Not a backup file */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	PARA_ERR: invalid parameter */
int PASCAL EXPORT VSGetBackupFileInfoW(WCHAR *BackupFileName, VSBackupFileInfoW *BackupInfo);
