#ifdef __VSPDT_DEFINED
Error, multiple machine defined
#endif
#define __VSPDT_DEFINED



/* compiler dependence */
typedef unsigned char               VUINT8;     /* 1 byte unsigned integer */
typedef unsigned short              VUINT16;    /* 2 bytes unsigned integer */
typedef unsigned long               VUINT32;    /* 4 bytes unsigned integer */
typedef unsigned __int64			_VUINT64;   /* 8 bytes unsigned integer */

typedef signed char                 VSINT8;     /* 1 byte signed integer */
typedef signed short                VSINT16;    /* 2 bytes signed integer */
typedef signed long                 VSINT32;    /* 4 bytes signed integer */
typedef signed __int64		        VSINT64;    /* 8 bytes signed integer */

#define VSINT8_C(c)                 ((VSINT8)(c))
#define VSINT16_C(c)                ((VSINT16)(c))
#define VSINT32_C(c)                ((VSINT32)(c))
#define VSINT64_C(c)                ((VSINT64)(c))

#define VUINT8_C(c)                 ((VUINT8)(c))
#define VUINT16_C(c)                ((VUINT16)(c))
#define VUINT32_C(c)                ((VUINT32)(c))
#define VUINT64_C(c)                ((VUINT64)(c))

#define VS_PACK_STRUCTURE



/* os dependence */
typedef _VUINT64                    VS_SIZE_T;  /* max. bytes unsigned integer that file size used */
#define VS_SIZE_T_MAX               VUINT64_C(0x8000000000000000)   /* file size limitation */



/* machine dependence */
#define VS_BYTE_ORDER               1
#define BUS_ERROR_TRAP              V_TRUE



