/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2008 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */

#ifndef __TMVSCOMM_H__
#define __TMVSCOMM_H__

/*==========================================================================*/
/*  VS Config Set ID, Ref. VSSetConfig(..), VSGetConfig(..), VSSetConfigEx(..), VSGetConfigEx(..)  */
#define VSCFG_SPYWARE_FLAG                0x02000001    /* 6.7 (VULONG) On/Off Spyware detection                          */
#define VSCFG_PRODUCT_ID                  0x11000001    /* 6.7 (VULONG) Product ID                                        */
#define VSCFG_GREY_FLAG                   0x03000001    /* 7.0 (VULONG) Scan Type of Grey Area (Ref. VSGREY_???)          */
#define VSCFG_PETRAP_SENSITIVITY          0x22000001    /* 7.0 (VULONG) PE Trap Sensitivity (0~1)                         */
#define VSCFG_GENERIC_SCAN_FLAG           0xFF000000    /* 7.5 (VULONG) On/Off Scan generic script pattern                */
#define VSCFG_PTN_DEC_KEY                 0xFF000001    /* 7.5 (VULONG) Pattern decode key                                */
#define VSCFG_PTN_OPTION_FLAG             0xFF000002    /* 7.5 (VULONG) pattern scan mode (Ref. VSCF_SCRIPT_PTN_???)      */
#define VSCFG_MAILTRAP_FLAG               0xFF000003    /* 7.7 (VULONG) On/Off MailTrap Flag                              */
/*      RESERVED VALUE                    0xFF000004    *//* 8.3 (VULONG) Reserved for internal use                        */
/*      RESERVED VALUE                    0xFF000005    *//* 8.3 (VULONG) Reserved for internal use                        */
#define VSCFG_IGNORE_PTNFMT_FLAG          0xFF000006    /* 9.00 (VULONG) Ignore PtnFmt Flag (POL)                         */
#define VSCFG_AGGRESSIVE_FLAG             0xFF000007    /* 8.5 (VULONG) On/Off Aggressive Flag                            */
/*      RESERVED VALUE                    0xFF000008    *//* 8.5 (VULONG) Reserved for internal use                        */
/*      RESERVED VALUE                    0xFF000009    *//* 8.7 (VULONG) Reserved for internal use                        */
#define VSCFG_PAGING_FLAG                 0xFF00000A    /* 8.9 (VULONG) On/Off paging Flag                                */
#define VSCFG_PAGING_RPF2_CACHE_SIZE      0xFF00000B    /* 8.9 (VULONG) paging rpf2 Cache Size                            */
#define VSCFG_FEEDBACK_FILE_FLAG          0xFF00000C    /* 8.95 (VULONG) On/Off feedback file flag                        */
#define VSCFG_FEEDBACK_FLAG               0xFF00000D    /* 8.95 (VULONG) On/Off feedback flag                             */
#define VSCFG_FEEDBACK_MAXFILESIZE_SHA1   0xFF00000E    /* 8.95 (VULONG) Feedback Max SHA1 size                           */
#define VSCFG_SILENT_PATTERN_REPORT_VIRUS 0xFF00000F    /* 8.95 (VULONG) On/Off silenet pattern to report virus flag      */
#define VSCFG_ICRC_CLIENT_HANDLE          0xFF000010    /* 8.95(void *) Set/Get client VS_ICRC_HANDLE structure */
#define VSCFG_ICRC_SERVER_HANDLE          0xFF000011    /* 8.95(void *) Get server VS_ICRC_HANDLE structure */
#define VSCFG_OLEHEUREXPLOIT_FLAG         0xFF000012    /* 9.00 (VULONG) Get/Set office exploit heuristic flags */
#define VSCFG_SHELLCODE_SM_SCAN_FLAG      0xFF000013    /* 9.00 (VULONG) Get/Set shell code heuristic SM scanning */
#define VSCFG_MEMORY_CHECK_FOR_POL        0xFF000014   /* 9.00 (VULONG) Get/Set memory check for POL function */
#define VSCFG_AGGRESSIVE_SCAN             0xFF000015   /* 9.20 (VULONG) Get/Set Aggressive Scan */
#define VSCFG_PDFEXPLOIT_FLAG             0xFF000016   /* 9.30 (VULONG) Get/Set PDF exploit flags */
#define VSCFG_FORCE_VIRUSSCAN             0xFF000017   /* 9.201 (VULONG) Get/Set Force Virus Scanning Mode */
#define VSCFG_AFI_PE_SECTION_FLAG         0xFF000018    /*[AFI_PE] 9.5 On/Off AFI PE output section block*/
#define VSCFG_AFI_PE_SEC_ENTROPY_FLAG     0xFF000019    /*[AFI_PE] 9.5 On/Off AFI PE output max. section entropy block*/
#define VSCFG_AFI_PE_PACK_FLAG            0xFF00001A    /*[AFI_PE] 9.5 On/Off AFI PE output pack block*/
#define VSCFG_AFI_PE_TRAP_FLAG            0xFF00001B    /*[AFI_PE] 9.5 On/Off AFI PE output trap block*/
#define VSCFG_AFI_PTN_FLAG                0xFF00001C    /*[AFI_PE] 9.5 On/Off AFI PE output pattern block*/
#define VSCFG_AFI_PE_SEC_ENTROPY_SIZE     0xFF00001D    /*[AFI_PE] 9.5 Read section size for calculating section entropy*/
#define VSCFG_NORMALIZE_FUNC              0xFF00001E   /* 9.5 (VULONG) Get/Set Normalizer callback function */
#define VSCFG_SWFHEUREXPLOIT_FLAG         0xFF00001F
#define VSCFG_CHECK_OPLOCK_FLAG           0xFF000020    /* [VSAPI-9.700-000076] SEG merge */
#define VSCFG_CENSUS_THRESHOLD_FLAG       0xFF000021    /* 9.70 (void *) Get VS_CENSUS_THRESHOLD structure */
/*         RESERVED VALUE                 0xFF000022    *//* 9.7 on/off PECRC feedback */
#define VSCFG_AFI_LAYER_FLAG              0xFF000023    /* 9.762 new AFI Layer for TDA */
#define VSCFG_ENLARGE_FIRSTBLOCK          0xFF000024    /* 9.580 set firstblock size for NetApp */
#define VSCFG_AFI_SHA1_FLAG               0xFF000025    /* [AFI_PE] 9.562 new AFI SHA1 for TDA APK SHA1 cloud query */
#define VSCFG_MIME_PASSWORD_CRACK_FLAG    0xFF000026    /* 9.731 On/Off MIME Password Crack */
#define VSCFG_MSCAN_OPEN_LOCKEDFILE_FLAG  0xFF000027
#define VSCFG_POLICY_CATEGORY_FLAG        0xFF000028
#define VSCFG_CUSTOMDEFENSE_FLAG          0xFF000029    /* 9.830 (VULONG) Get/Set Custom Defense */
#define VSCFG_MIP_MODE                    0xFF00002A    /* 9.830 (VULONG) Get/Set MIP Mode */
/*         RESERVED VALUE                 0xFF00002B    *//*9.850 (VULONG) Reserved for internal use */
#define VSCFG_ENABLE_CENSUS_PTN_FLAG      0xFF00002C    /* 9.850 (VULONG) Get/Set Census Threshold */
/*         RESERVED VALUE                 0xFF00002D    *//*9.853 (VULONG) Reserved for internal use */
#define VSCFG_ENABLE_MULTI_DETECTION_FLAG 0xFF00002E    /* 9.85X (VULONG) Retrieve multiple detection information from process file callback */
#define VSCFG_AFI_PTN_EX_FLAG             0xFF00002F    /*[AFI_PE] 9.XX On/Off AFI PE output pattern2 block*/
#define VSCFG_XHDL_HANDLE                 0xFF000030    /* [TrendX] XHandler handle */
/*         RESERVED VALUE                 0xFF000031    *//* [TrendX] File types which support feature extraction by DeepScan */
/*         RESERVED VALUE                 0xFF000032    *//* Redefine config id due to conflict with VSCFG_COMPOUND_SIZE_LIMIT_FLAG *//* [TrendX] XHandler handle */
#define VSCFG_COMPOUND_SIZE_LIMIT_FLAG    0xFF000033    /* Redefine config id due to conflict with VSCFG_XHDL_HANDLE *//* 9.900 (VULONG) Get/Set compound file size limit */
#define VSCFG_XS_CENSUS_THRESHOLD         0xFF000034    /* [TrendX] Census Threshold */
#define VSCFG_PLUGIN_MODULE               0xFF000035    /* [TrendX] Plug-in modules */
#define VSCFG_XSCAN_COUNT_IN_CHILD        0xFF000036    /* [TrendX] Maximum scan count for xscan in child */
#define VSCFG_XS_FTYPE_SUP_LIST_EX        0xFF000037    /* [9.863][ATSE-174][Trendx] Need support new configure id VSCFG_XS_FTYPE_SUP_LIST_EX  */
/*         RESERVED VALUE                 0x01000000 ~ 0x01FFFFFF, this ID range reserved for ATSE */


/*********************************/
/* AFI event switch:  0x0100xxxx */
/*********************************/
enum {
    VSCFG_AFI_PASSWORD_FLAG = 0x01000001,           /* [AFI_PE] 9.740 new AFI PASSWORD for TDA APK SHA1 cloud query */
    VSCFG_AFI_PE_INFO_FLAG,                         /* [AFI_PE] 2013/07/22 ATSE PE info */
    VSCFG_AFI_PE_EXPORT_INFO_FLAG,                  /* [AFI_PE] 2013/07/24 ATSE PE export info */
    VSCFG_AFI_XML_INFO_FLAG,                        /* [AFI_PE] 9.740-1098(2013/10/25) ATSE PE info */
    VSCFG_AFI_RULEMATCHED_FLAG,                     /* [AFI_PE] 9.755     (2014/3/6) Rule Matched (pattern & hardcode rules) */
    VSCFG_AFI_VIRUSFOUND_FLAG,                      /* [AFI_PE] 9.755     (2014/3/6) Virus Found (final report) */
    VSCFG_AFI_HTMLURL_FLAG,                         /* [AFI_PE] 9.755     (2014/4/29) HTML URL */
    VSCFG_AFI_RESERVED0_FLAG,                       /******* ATSE reserved for 2014/6/10 *******/
    VSCFG_AFI_RESERVED1_FLAG,                       /******* ATSE reserved for 2014/6/10 *******/
    VSCFG_AFI_RESERVED2_FLAG,                       /******* ATSE reserved for 2014/6/10 *******/
    VSCFG_AFI_RESERVED3_FLAG,                       /******* ATSE reserved for 2014/6/10 *******/
    VSCFG_AFI_PE_EXTRA_INFO_FLAG,                   /* [AFI_PE] 9.755     (2014/8/14) PE EXTRA INFO (Ex. Installer) */

    /************* Append new AFI flag before this line *************/
    VSCFG_AFI_EVENT_TURN_ON  = 0x0100FFFF,          /* [AFI_PE] 9.755     (2014/4/30) Temporarily turn on AFI Event by AFI ID (ref. AFI_XXX_ID ) */
    VSCFG_AFI_EVENT_TURN_OFF = 0x0100FFFE,          /* [AFI_PE] 9.755     (2014/4/30) Temporarily turn off AFI Event by AFI ID (ref. AFI_XXX_ID ) */
    VSCFG_AFI_EVENT_ENABLE   = 0x0100FFFD,          /* [AFI_PE] 9.755     (2014/8/18) Enable AFI Event by AFI ID (ref. AFI_XXX_ID ) */
    VSCFG_AFI_EVENT_DISABLE  = 0x0100FFFC,          /* [AFI_PE] 9.755     (2014/8/18) Disable AFI Event by AFI ID (ref. AFI_XXX_ID ) */
    VSCFG_AFI_HEADER_INFO    = 0x0100FFFB           /* [AFI_PE] 9.826     (2015/5/07) Query AFI Header Information. (input VSAFIH) */
};

/******************************/
/* ATSE reserved:  0x0101xxxx */
/******************************/
/*         RESERVED VALUE                   0x01010000 ~ 0x0101FFFF, this ID range reserved for ATSE */
#define VSCFG_ATSE_RULE_FLAG                0x01010001  /* ATSE 9.740-1102 On/Off ATSE rule by name (only for VSSetConfigEx. ref. VS_ATSE_RULE_FLAG) */
#define VSCFG_ATSE_AGGRESSIVE_LEVEL         0x01010002  /* ATSE 9.750-1016 Set ATSE aggressive level(0,1,2,3,4) (for VSSetConfig) */
#define VSCFG_PASSWORD_CRACK_WITHOUT_PTN    0x01010003  /* ATSE 9.740-1134 On/Off password crack without pattern */
#define VSCFG_SCAN_WITHOUT_PTN              0x01010004  /* ATSE 9.755-1089 On/Off scan file without pattern */
#define VSCFG_SPECIFY_PASSWORD              0x01010005  /* ATSE 9.755-1129 specify password (ref. VS_PASSWORD) */
#define VSCFG_FEEDBACK_MAXBLOBSIZE          0x01010006  /* ATSE 9.755 Maxmimum size for total blobs, 0 is unlimited. */
#define VSCFG_ATSE_RUEL_TURN_ON             0x01010007  /* ATSE 9.860 turn on ATSE Rule by Rule Name (only for VSSetConfigEx. NewCfgData is name, OldCfgData must be NULL) */
#define VSCFG_ATSE_RUEL_TURN_OFF            0x01010008  /* ATSE 9.860 turn off ATSE Rule by Rule Name (only for VSSetConfigEx. NewCfgData is name, OldCfgData must be NULL) */
#define VSCFG_ENGINE_PROPERTY               0x01010009  /* ATSE 9.860 Get Engine Property (only for VSGetConfigEx, ref. VS_ENGINE_PROPERTY, vsc can be NULL) */

/***************************************************/
/* ATSE Feature Flag (Enable/Disable):  0x0102xxxx */
/***************************************************/
enum {
    VSCFG_ATSE_FFLAG_BGN_ID = 0x01020000,
    VSCFG_ATSE_FFLAG_CRACK_PASSWORD = VSCFG_ATSE_FFLAG_BGN_ID,    /* ATSE 9.755-1212 Enable/Disable crack password. */
    VSCFG_ATSE_FFLAG_MERGE_GFB,                                   /* ATSE 9.826-1036 Merge GenericFeedbackBlob. */
    VSCFG_ATSE_FFLAG_SOAP_SUPPORT,                                /* ATSE 9.826-1051 SOAP support flag (MIME boundary check). */
    VSCFG_ATSE_FFLAG_CHECK_PE_ENCODE,                             /* ATSE 9.860-xxxx Enable/Disable Check PE encode. */

    /*** Append ATSE Feature Flag before this line ***/
    VSCFG_ATSE_FFLAG_END_ID
};



#define VS_PAGING_VIRINFO_FLAG           0x01    /* 8.9 (VULONG) On/Off paging Flag for VIRINFO                           */
#define VS_PAGING_PECRC_FLAG             0x02    /* 8.9 (VULONG) On/Off paging Flag for PECRC                          */
#define VS_PAGING_LEADER_FLAG            0x04    /* 8.9 (VULONG) On/Off paging Flag for LEADER                           */

#define VSCFG_UniversalCallbackInfo      0x00000001    /* reserved for TDA Universal Callback */

/* Support "OR" operation in one API call */
#define VSGREY_SPYWARE                  0x00000001    /* 7.0 Spyware only detection */
#define VSGREY_ADWARE                   0x00000002    /* 7.0 ADware only detection */
#define VSGREY_DIALER                   0x00000004    /* 7.0 Dialer only detection */
#define VSGREY_HACKTOOL                 0x00000008    /* 7.0 HackTools only detection */
#define VSGREY_JOKE                     0x00000010    /* 7.0 Jokes only detection */
#define VSGREY_REMOTEACCESS             0x00000020    /* 7.0 Remote Access only detection */
#define VSGREY_PASSWDCRACK              0x00000040    /* 7.0 Password Cracking only detection */
#define VSGREY_OTHERS                   0x00000080    /* 7.0 Others detection */
#define VSGREY_PUA                      0x00000100    /* 9.850 PUA only detection */




/*==========================================================================*/
/*  Virus ActiveAction function set, Ref. VSGetVirusPropertyByName(..)  */
#define VSVPN_QUERY_MAJOR               0x01000001 /* 6.7   Using VirusName to Get its Major Property , out is (VSVirusTypeProp*) */
#define VSVPN_QUERY_MINOR               0x01000002 /* 6.7   Using VirusName to Get its Minor Property , out is (VSVirusTypePropArray*) */
#define VSVPN_EXIST_MAJOR               0x02000001 /* 6.7   Is MajorType, in is string, out is (int*) */
#define VSVPN_EXIST_MINOR               0x02000002 /* 6.7   Is MinorType, in is string, out is (int*) */
#define VSVPN_LONG_VIRNAME              0x00000000 /* 8.0   Using VirusName to Get its LongVirusName Property, out is (VSVirusNameProp*) */
#define VSVPN_QUERY_DIRECT_ACTION       0x00000001 /* 9.830 Using VirusName to Check if this detection can or cannot apply direct action, out is (VULONG*) */
#define VSVPN_QUERY_CENSUS_THRESHOLD    0x00000002 /* 9.850 Using VirusName to Get its Census Threshold Property, out is (VSCensusThresholdProp*) */



/*==========================================================================*/
/*  ActiveAction Pattern function set, Ref. VSGetPatternProperty(..)  */
#define VSPP_QUERY_MAJOR         0x01000001  /* 6.7 Get all Major Property, out is (VSVirusTypePropArray*) */
#define VSPP_EXIST_MAJOR         0x02000001  /* 6.7 Exist MajorType, in is string, out is (int*) */
#define VSPP_EXIST_MASTER_PTN    0x02000002  /* 6.7 Exist Master Pattern, out is (int*) */
#define VSPP_NEED_SCAN_MAJOR     0x02000003  /* 6.7 Need Scan Master, in is string, out is (int*) */
#define VSPP_VERSION             0x03000001  /* 6.7 Get Pattern Internal Version, in is PatternBaseName, out is (VULONG*) */
/*         RESERVED VALUE        0x04000000 ~ 0x04FFFFFF, this ID range reserved for ATSE */
#define VSPP_ATSE_RULEINFO       0x04000001  /* 9.755 Get ATSE Rule Info, in is (VS_ATSE_RULEINFO*) */



/*==========================================================================*/
/*  CPR pattern loading commands, Ref. VSReadControlPattern(..)  */
#define VSCPR_GLOBAL             0x00000001  /* 8.3 Load global CPR pattern */
#define VSCPR_REGIONAL           0x00000002  /* 8.3 Load regional CPR pattern */



/*==========================================================================*/
/* InterFace */
typedef struct
{
    VUSHORT vp_1st_Action;
    VUSHORT vp_2nd_Action;
    char    vp_Name[17];
} VSVirusTypeProp;

typedef struct
{
    int              count;
    VSVirusTypeProp* pVP;
} VSVirusTypePropArray;

typedef struct
{
    int     name_buffer_size;
    char*   name_buffer;
} VSVirusNameProp;

#define VS_CENSUS_THRESHOLD_REQUIRED 0x01
typedef struct
{
    VULONG  census_flag;
    VULONG  prevalence;
    VULONG  lower_age;
    VULONG  higher_age;
} VSCensusThresholdProp;

typedef struct
{
    char    PatternFileName[16];    /* Filename of the pattern file (excluding path) */
    int     PatternVersion;         /* Pattern version number */
    int     CheckFlag;              /* Indicates if the pattern is valid */
    VULONG  InternalVersion;        /* Pattern internal version number */
} VSPtnCheckInfoEx;


/*==========================================================================*/
typedef int PASCAL VS_ICRC_QUERY_FUNC(void* para, VULONG mode, const VUCHAR* input, VULONG input_size, VUCHAR **output, VULONG *output_size, VULONG alignment, void **pnBucketID);
typedef int PASCAL VS_ICRC_FREEBUCKET_FUNC(void* para, void *nBucketID);
typedef int PASCAL VS_ICRC_LCRCENUM_FUNC(void* para, VUCHAR** output, VULONG *output_size);
typedef int PASCAL VS_ICRC_VINFENUM_FUNC(void* para, VULONG mode, VUCHAR** output, VULONG *output_size);

#define ICRC_LCRC_MODE          0x4C435243  /* 'LCRC' */
#define ICRC_VID_MODE           0x00564944  /* 'VID'  */
#define ICRC_IDX_MODE           0x00494458  /* 'IDX'  */
#define ICRC_NAME_MODE          0x4E414D45  /* 'NAME' */

typedef struct {
    VULONG                      SizeOfIcrcHandle;
    VULONG                      padding;    /* Used for alignment on 64 bits environment */
    void*                       para;
    VS_ICRC_QUERY_FUNC*         Query;      /* Act as open handle and query */
    VS_ICRC_FREEBUCKET_FUNC*    FreeBucket; /* Act as close handle */
    VS_ICRC_LCRCENUM_FUNC*      LcrcEnum;
    VS_ICRC_VINFENUM_FUNC*      VinfEnum;
} VS_ICRC_HANDLE;

typedef struct { /* reserved for TDA */
    int     (*ucb_func)(void * ucb_para, VULONG cb_mode, void *);
    void*   ucb_para;
} VSUniversalCallbackInfo;

struct _VSIO;

typedef struct {
    VULONG size;         /* structure size */
    VULONG prevalence;   /* Census prevalence value read from pattern */
    VULONG lower_age;    /* Census lower age value read from pattern */
    VULONG upper_age;    /* Census upper age value read from pattern */
} VS_CENSUS_THRESHOLD;

#define VS_POLICY_STANDARD_PACKER      1
#define VS_POLICY_CUSTOMIZED_PACKER    2

typedef struct {
    const void* rule_name;  /* atse rule name string */
    VULONG flag;            /* 0 is disable, otherwise enable */
} VS_ATSE_RULE_FLAG;

typedef struct {
    VULONG length;
    const VUCHAR* buffer;  /* utf8 password (donot need null-terminator) */
} VS_PASSWORD;

typedef struct VS_ATSE_RULEINFO {
    VULONG  struct_size;
    VULONG id;
    const char* name;
    VULONG  status;
    VULONG  aggressive_level;
    VULONG  category;
} VS_ATSE_RULEINFO;

/* atse rule status */
#define ATSERULE_DISABLE    0
#define ATSERULE_ENABLE     1
#define ATSERULE_SILENT     2



/* xxx method id: CUSTOM_DEFENSE_LIST.usMethodID */
#define METHOD_SHA1_FLT1 1     /* SHA1 with FILTER_VALUE_V1 */
/* xxx type: CUSTOM_DEFENSE_LIST.usType */
#define CDF_TYPE 1                       /* reference to usType */ 
#define UDF_TYPE 2                       /* reference to usType */
#define InvalidCustomDefenseType(a) (UDF_TYPE < a || a < CDF_TYPE)

typedef struct _CUSTOM_DEFENSE_METHOD_DATA_V1 {
    VULONG ulFilter; /* filter data output from VSAPI */
    VUCHAR SHA1[20]; /* signature data output from VSAPI */
} CUSTOM_DEFENSE_METHOD_DATA_V1;

typedef struct _CUSTOM_DEFENSE_DATA_V1 {
    struct _CUSTOM_DEFENSE_METHOD_DATA_V1 MethodData;
    VULONG ulActionID;
    VUCHAR VirusName[17]; /* short name(16) + null-terminated */
} CUSTOM_DEFENSE_DATA_V1;

typedef struct _CUSTOM_DEFENSE_LIST_HEADER {
    VULONG ulSize;
    VULONG ulNumber; /* number of entries */
    VUSHORT usMethodID; /* reference to method id */
    VUSHORT usType; /* reference to type */
} CUSTOM_DEFENSE_LIST_HEADER;

#define VS_ENGINE_PROPERTY_STRUCTURE_VERSION   1
typedef struct {
    VULONG structure_version;   /* (sv=1) the version of this structure (sv) */
    VULONG engine_type;         /* (sv=1) 0:VSAPI, 1:ATSE */
    VULONG engine_version;      /* (sv=1) au_version is equal to VSGetVersion() (ref. VULONG version format) */
    VULONG feature_version;     /* (sv=1) feature_version < au_version when rollback build (ref. VULONG version format) */
    char*  engine_name;         /* (sv=1) (Ex. "VSAPI10.860-1001" or "ATSE10.860-1001") */

} VS_ENGINE_PROPERTY;

/*  VULONG version Format, Ex. 98601017 is mean 9.860-1017
    Major version: 3-digit numbers
    Minor version: 3-digit numbers
    Build number:  4-digit numbers
*/
#define VS_ENGINE_VERSION_MAJOR(v32)    (v32/10000000)
#define VS_ENGINE_VERSION_MINOR(v32)    ((v32%10000000)/10000)
#define VS_ENGINE_VERSION_BUILD(v32)    (v32%10000)


#endif /* __TMVSCOMM_H__ */


