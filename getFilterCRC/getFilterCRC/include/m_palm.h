/* $Date: 2001/2/19 PM 05:03:32$ */
/*
 * $Log: 
 *  4    NewScanEngine1.3         2001/2/19 PM 05:03:32Wayne_chu       Synchronize
 *       cheetah and cheeath_rel
 *  3    NewScanEngine1.2         2001/1/19 AM 11:58:31Wayne_chu       add
 *       VSGetBackupFileInfo and fix the problem of convert between Unicode and
 *       Ansicode.
 *  2    NewScanEngine1.1         2000/11/3 PM 08:05:51Max Liu         For palm use
 *  1    NewScanEngine1.0         2000/10/31 PM 02:51:17Lily_ho         
 * $
 * 
 * 9     11/05/99 3:09p Joanna
 * [Fix] Tracker #1602
 * 
 * 8     11/02/99 2:39p Joanna
 * RAR_MAXWINSIZE should be 0x100000
 * some platform like win16 cannot support it, so declare it as 0x10000
 * 
 * 7     8/26/99 2:39p Lily
 * rollback (SPNW)
 * 
 * 6     8/25/99 4:03p Lily
 * change MAX_PATH_LEN 1024 => 256 ;; request from kenny SPNW
 * 
 * 5     99/05/24 1:27p Viking
 * VCHAR should be defined separately by m_xxx.h
 * 
 * 4     11/17/98 1:51p Lily
 * modify by lily(11/17/98)
 * => pack(1) for SPNW
 * 
 * 3     11/09/98 2:00p Lily
 * 
 * 8     11/06/98 4:08p Cliff_liang
 * define VSCTYPE(=long) 
 * 
 * 20    4/22/98 3:28p Cliff
 * Add definition of DECOMP_MIN_MEM and DECOMP_MAX_MEM
 * 
 * 19    3/23/98 2:24p Cliff
 * VULONG and VLONG should be defined separately by m_xxx.h
 * 
 * 18    3/20/98 2:58p Cliff
 * Add proejct name and Trend Copy right declaration.
 *
 * 15	 11/03/97 12:22p Cliff
 * Move internal use data to header
 *
 * 12	 10/20/97 11:39a Cliff
 * redefine MACOPEN
 *
 * 10	 10/16/97 3:33p Cliff
 * Add MACCLOSE, MACOPEN,MACSEEK, and MACREAD macro
 *
 * 5	 9/23/97 3:05p Cliff
 * Remove VSRotateRight definition because it was used only by getoutvr.c
 *
 * 4	 8/15/97 11:58a Cliff
 * Remove definition of VSReadDir
 *
 * 2	 7/24/97 1:23p Cliff
 * Remove ALTERNATIVE
 *
 * 2	 7/03/97 11:03a Rogerc
 * merged with Cliff's updated on 0702.
 *
 * 13	 6/11/97 11:29a Roger
 *
 * 12	 5/21/97 3:52p Roger
 *
 * 11	 5/16/97 4:55p Roger
 *
 * 10	 5/16/97 11:01a Roger
 * define macro VSRotateRight.
 *
 * 9	 4/29/97 1:48p Roger
 * after merge with Cliff's source code on 24/4.
 *
 * 8	 4/28/97 12:56p Roger
 * port to dos ole function.
 *
 * 7	 4/23/97 11:00a Roger
 * After merged the source code from Cliff(0417 and 0421) and fixed some
 * bugs.
 *
 * 5	 4/03/97 10:58a Roger
 *
 * 4	 3/26/97 2:05p Roger
 * sync the source codes from Cliff 03/21/97.
 *
 * Revision 1.0  1996/07/31 14:38:16  cliff
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_nlm.h */
/* Description: */
/*		Machine dependent code definition */
/* */
/**************************************************************************/
#include <PalmTypes.h>
#include <Core/System/DataMgr.h>

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

#pragma pack(1)

/* platform dependent environment */
/* platform: NLM Watcom C/C++ 10.0 */
typedef char          VCHAR;/* 1 byte */
typedef long			VLONG;	/* 4 byte */
typedef unsigned long	VULONG; /* 4 byte */
typedef __int64	    	 VINT64;  /* 8 byte */
typedef unsigned __int64 VUINT64; /* 8 byte */

typedef struct {
      unsigned char *recPtr;
      MemHandle recHandle;
      UInt32  recNum;
      UInt32  recSize;
      UInt32  begIndex;
      UInt32  endIndex;
} RecInfo;

typedef struct {
      UInt32  isOpen;
      DmOpenRef refDB;
      UInt32    DBSize;
      UInt32     numRec;
      RecInfo   *recList;
      RecInfo   CurPtr;
} DBFileHandle;

typedef DBFileHandle *DBFileHD;
typedef DBFileHD FILE;

#define VSCTYPE	long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR	"SYS:VIRUS"
#define DEFAULT_PATTERN_DIR 	"SYS:PUBLIC"
#define DEFAULT_PATTERN_FILE	"LPT$VPN."
#define DEFAULT_TEMP_DIR	"SYS:TMP"
#define EXPORT
#define TMVSAPI
#define HANDLE			DBFileHD // Max
#define MAX_PATH_LEN		1024 
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PASCAL

#define VSLongLongToString(cp,x)	(*(VUINT64 *)(cp) = x)
#define VSLongToString(cp,x)	(*(VULONG *)(cp) = x)
#define VSShortToString(cp,x)	(*(VUSHORT *)(cp) = x)
VULONG  VSStringToLong(unsigned char *cp);
VUSHORT VSStringToShort(unsigned char *cp);
VUINT64 VSStringToLongLong(unsigned char *cp);
//#define VSStringToLong(cp)	(*(VULONG *)(cp))
//#define VSStringToShort(cp) 	(*(VUSHORT *)(cp))
//void *memcpy(void *dest, const void *src, int n);
int _VSMEMICMP(VUCHAR *buf1, VUCHAR *buf2, int len);
#ifndef __NLM__
#define __NLM__
#endif

#define SEEK_SET	0
#define SEEK_CUR	1
#define SEEK_END	2
