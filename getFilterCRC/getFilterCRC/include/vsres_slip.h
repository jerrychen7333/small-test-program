/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2005 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* vsres.h */
/* Description: */
/* */
/**************************************************************************/

/** @file vsres.h
 *    @brief Virus Scan API (VSAPI)
 */

#ifndef __VSRES_SLIP_H__
#define __VSRES_SLIP_H__

#include "vsres.h"

#ifdef  VSOpenResource
#undef  VSOpenResource
#endif
#define VSOpenResource(r,t,m,o,s,h)     VSOpenResource_Slip(r,t,m,o,s,(VSRES**)h)
int VSOpenResource_Slip(char *res, short type, short mode, long ofs, long len, VSRES **hd);


#ifdef  VSResourceSize
#undef  VSResourceSize
#endif
#define VSResourceSize(h)     VSResourceSize_Slip((VSRES*)h)
long VSResourceSize_Slip(VSRES* Res);


#ifdef  VSLseekResource
#undef  VSLseekResource
#endif
#define VSLseekResource(h,o,m)     VSLseekResource_Slip((VSRES*)h, o, m)
long VSLseekResource_Slip(VSRES* res, long ofs, int mod);


#ifdef  VSReadResource
#undef  VSReadResource
#endif
#define VSReadResource(h,b,s,l)         VSReadResource_Slip((VSRES*)h, (void*)b, s, l)
int VSReadResource_Slip(VSRES* Res, void* buf, UINT16 len, UINT16 *rlen);


#ifdef  VSWriteResource
#undef  VSWriteResource
#endif
#define VSWriteResource(h,b,s,l)        VSWriteResource_Slip((VSRES*)h, (void*)b, s, l)
int VSWriteResource_Slip(VSRES* Res, void* buf, UINT16 len, UINT16 *rlen);



#endif /* __VSRES_H__ */
