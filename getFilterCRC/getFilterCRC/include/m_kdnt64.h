/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2006 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/

#ifndef __M_KDNT64_H__
#define __M_KDNT64_H__

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: Win64 IFS Kit 3790   */
typedef char            VCHAR;    /* 1 byte */
typedef LONG            VLONG;    /* 4 byte */
typedef unsigned long    VULONG; /* 4 byte */
typedef __int64             VINT64;  /* 8 byte */
typedef unsigned __int64 VUINT64; /* 8 byte */
typedef UCHAR            BYTE, *PBYTE;
typedef USHORT            WORD, *PWORD;
typedef ULONG            DWORD, *PDWORD;
typedef LARGE_INTEGER    FILETIME, *PFILETIME;
typedef LONG_PTR        VSCTYPE;

#define VSPTN_HANDLE    LONG_PTR

#define ACCESS_EXIST            0
#define ACCESS_READ                2
#define ACCESS_WRITE            4
#define ACCESS_READWRITE        6
#define ACCESS_DIRECTORY        1

#define DEFAULT_MOVE_DIR        "c:\\virus"
#define DEFAULT_PATTERN_DIR     "c:\\dos"
#define DEFAULT_PATTERN_FILE    "lpt$vpn."
#define DEFAULT_TEMP_DIR        "c:\\tmp"
#define PASCAL    
#define EXPORT                    
#define TMVSAPI
#define FAR                        
#define VSAPI                    _declspec(dllexport)
#define INVALID_HANDLE_VALUE    (HANDLE) -1
#define INVALID_FILE_LENGTH        (DWORD) 0xFFFFFFFF
#define INVALID_FILE_ATTRIBUTES (DWORD) 0xFFFFFFFF
#define INVALID_FILE_TIME        RtlConvertUlongToLargeInteger(0xFFFFFFFF)
#define MAX_PATH_LEN    300
#define MAX_PATHNAME    MAX_PATH_LEN
/* minimum memory size for decomp module when decomp into memory */
//#define DECOMP_MIN_MEM 0    
/* Maximum memory size for decomp module when decomp into memory */
//#define DECOMP_MAX_MEM 0
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400    
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PACK_STRUCTURE

VUSHORT VSStringToShort(char *);
VULONG VSStringToLong(char *);
VUINT64 VSStringToLongLong(char *);
void VSShortToString(char *,VUSHORT);
void VSLongToString(char *,VULONG);
void VSLongLongToString(char *,VUINT64);

/* IntelliTrap for Desktop in VSAPI 8.3 -SeanFeng 03/20/2006 */
#define VSCFG_MALPACKTRAP_FLAG      0xFF000004      /* 8.3 (VULONG) On/Off MalPackTrap Flag */
#define VSCFG_IGNORE_PTNFMT_FLAG    0xFF000006      /* 8.3 (VULONG) each bit is an on/off flag of one pattern section */


#endif  // endof __M_KDNT64_H__
