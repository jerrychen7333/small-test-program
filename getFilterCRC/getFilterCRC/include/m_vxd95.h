/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2006 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/

/**************************************************************************/
/* m_vxd95.h */
/* Description: */
/*    Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

#ifdef DebugPrint
#undef DebugPrint
#endif

#pragma pack(1)

typedef char          VCHAR;/* 1 byte */
typedef long                VLONG;    /* 4 byte */
typedef unsigned long        VULONG;    /* 4 byte */
typedef __int64             VINT64;  /* 8 byte */
typedef unsigned __int64 VUINT64; /* 8 byte */

#define VSCTYPE    long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR    "c:\\virus"
#define DEFAULT_PATTERN_DIR     "c:\\dos"
#define DEFAULT_PATTERN_FILE    "lpt$vpn."
#define DEFAULT_TEMP_DIR    "c:\\tmp"
#define PASCAL                _cdecl        /* in ring0, without PASCAL */
#define EXPORT
#define TMVSAPI
#define MAX_PATH_LEN        MAX_PATH
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
/* decrease the size 409600 jason's suggestion , vxd could not set so max */
#define DECOMP_MAX_MEM 102400 
#define PACK_STRUCTURE
#define VSLongLongToString(cp,x)    (*(VUINT64 *)(cp) = x)
#define VSLongToString(cp,x)    (*(ULONG *)(cp) = x)
#define VSShortToString(cp,x)    (*(USHORT *)(cp) = x)
#define VSStringToLongLong(cp)    (*(VUINT64 *)(cp))
#define VSStringToLong(cp)    (*(ULONG *)(cp))
#define VSStringToShort(cp)     (*(USHORT *)(cp))


#define VSCFG_VMM_VERSION_FLAG      0xFF000005      /* 8.3 (VULONG) Flag of getting VMM version */
#define VSCFG_IGNORE_PTNFMT_FLAG    0xFF000006      /* 8.3 (VULONG) each bit is an on/off flag of one pattern section */
#define GET_VMM_VERSION             {\
    extern VULONG g_VMM_Version;\
    if (CfgID == VSCFG_VMM_VERSION_FLAG) {\
        *Value = g_VMM_Version;\
        return 0;\
    }\
}


/*=========================== */


/* structure defined by Jason Lin 12-24-1997 */
/* for use with ring 0 file */

#ifndef FILE_DEFINED

#define MAX_PATHNAME    260
typedef struct R0_File
{
    HANDLE    hFileHandle;            /* ring 0 file handle */
    DWORD   dwFilePointer;            /* current file position */
    DWORD   dwFileSize;
    WORD    wAccessMode;            /* Access mode, read, write ... */
    WORD    wAttribute;                /* original file attribute */
    WORD    wOpenMode;                /* binary mode : 0, text mode : 1 */
    WORD    wAttChanged;            /* if 0, don't need to set att  */
                                    /* back, if 1, set att back when close */
    char    szFileName[MAX_PATHNAME];
} R0_FILE, *PR0_FILE;

typedef R0_FILE FILE;
/* we don't have a stderr, just ignore this kind of reuests, Jason Lin 12-26-1997 */
#define stderr                    (FILE *) INVALID_VALUE

#define FILE_DEFINED
#endif
/* ============================= */
