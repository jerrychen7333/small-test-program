#ifndef __SMV_DEF_H__
#define __SMV_DEF_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef LINUX
  #include <stdio.h>
  #include <stdlib.h>
#endif

#ifdef KDWINNT
    #include "basetsd.h"
    #ifdef USE_IFS
        #include <ntifs.h>
    #else
        #include <ntddk.h>
    #endif
#else
    #ifdef WIN64
        #include "basetsd.h"
        #include <windows.h>
    #elif defined W32DBG
        #include <windows.h>
    #elif defined WIN32
        #include <windows.h>
    #endif
#endif

#include "vspdt.h"

#ifdef VS_PACK_STRUCTURE
#pragma pack(push, 8)
#endif
/*======= pack begin =======*/



#define SM_EXPORT_API           VS_EXPORT_API

#define SMV_VERBOSE_NONE             0
#define SMV_DEBUG_INST              700
#define SMV_DEBUG_PAGING            701
#define SMV_DEBUG_API               702
#define SMV_DEBUG_EXCEPTION         703
#define SMV_DEBUG_OS                704
#define SMV_DEBUG_HOOK              705
#define SMV_DEBUG_MEM_MODIFIED      706
#define SMV_DEBUG_STOP_CONDITION    707
#define SMV_DEBUG_PMM               708
#define SMV_DEBUG_VMM               709
#define SMV_DEBUG_TRACE             710


typedef struct _SM_HANDLE *     SMHANDLE;

typedef int SM_OPEN_FUNC_A(void* ctx, const VUCHAR* file_name, VS_SIZE_T limit_size, VULONG open_flag, struct _VSIO** p_io);
typedef int SM_OPEN_FUNC_W(void* ctx, const VWCHAR* file_name, VS_SIZE_T limit_size, VULONG open_flag, struct _VSIO** p_io);
typedef int SM_OPEN_FUNC_T(void* ctx, VS_SIZE_T limit_size, struct _VSIO** p_io);

typedef struct {
    VUINT32 size;
    void*   ctx;
    SM_OPEN_FUNC_T* open_t;
    SM_OPEN_FUNC_A* open_a;
    SM_OPEN_FUNC_W* open_w;
    VUINT32 temp_file_sequence;
} SM_FILE_GEN;

typedef struct _SM_CALLBACK_HANDLE {
    int     cb_size;               /* size of SM_CALLBACK_HANDLE */
    int     (SM_EXPORT_API *cb_func)(SMHANDLE sm, void * cb_user_para, VUINT32 cb_type, void * cb_info);
    void *  cb_user_para;          
    /* any future expansion should be added here */
} SM_CALLBACK_HANDLE;

/* SM callback function MUST return one of the following error code */
#define SM_CB_CONTINUE              1
#define SM_CB_UNHANDLED_EVENT       0
#define SM_CB_BREAK                 -1

/* SM callback type */
#define SM_CB_TYPE_INIT                 0x00000001
#define SM_CB_TYPE_EXEC_FILTER          0x00000002
#define SM_CB_TYPE_EXEC_END             0x00000003
#define SM_CB_TYPE_API_PRE              0x00000004
#define SM_CB_TYPE_API_POST             0x00000005
#define SM_CB_TYPE_EXCEPTION            0x00000006
#define SM_CB_TYPE_PROCESS_CREATE       0x00000007
#define SM_CB_TYPE_PROCESS_TERMINATE    0x00000008
#define SM_CB_TYPE_THREAD_CREATE        0x00000009
#define SM_CB_TYPE_THREAD_TERMINATE     0x0000000a
#define SM_CB_TYPE_CONTEXT_SWITCH       0x0000000b
#define SM_CB_TYPE_STOP_CONDITION       0x0000000c

typedef struct _SM_CALLBACK_EXEC_FILTER_INFO {
    int size;             /* size of user data (for compatibility) */
    int result;
    VUINT32 filter_code;  /* SMV Reserve */
} SM_CALLBACK_EXEC_FILTER_INFO;

typedef struct _SM_CALLBACK_STOP_INFO{
    int size;             /* size of user data (for compatibility) */
    int result;
} SM_CALLBACK_STOP_INFO;

typedef struct _SM_CALLBACK_API_INFO {
    int             size;             /* size of user data (for compatibility) */
    const char    * api_name;         /* ascii-base string */
    const char    * module_name;
    VUINT32         r_esp;
    VUINT32         api_paramnum;
    VUINT32         api_crc32;
}SM_CALLBACK_API_INFO;

typedef struct _SM_CALLBACK_EXCEPTION_INFO {
    int             size;             /* size of user data (for compatibility) */
    int             exception_type;
}SM_CALLBACK_EXCEPTION_INFO ;

typedef struct _SM_CALLBACK_PROCSS_INFO {
    int size;
    VUINT32 process_id;
} SM_CALLBACK_PROCESS_INFO;

typedef struct _SM_CALLBACK_THREAD_INFO {
    int size;
    VUINT32 process_id;
    VUINT32 thread_id;
} SM_CALLBACK_THREAD_INFO;

typedef struct _SM_CALLBACK_CONTEXT_SWITCH_INFO {
    int size;
    VUINT32 thread_id;
    VUINT32 process_id;
    VUINT64 mileage;
} SM_CALLBACK_CONTEXT_SWITCH_INFO;

typedef struct _SM_CALLBACK_EXEC_END_INFO {
    int size;
    VUINT32 exec_status;
} SM_CALLBACK_EXEC_END_INFO;

/* SM query information type */
#define SM_INFO_VERSION         0x00000001
#define SM_INFO_INST            0x00000002
#define SM_INFO_MODULE          0x00000003
#define SM_INFO_PROCESS         0x00000004
#define SM_INFO_CPU				0x00000005
#define SM_INFO_THREAD          0x00000006
#define SM_INFO_VMEM_BLOCK      0x00000008
#define SM_INFO_VMEM_PAGE       0x00000009
#define SM_INFO_EXEC            0x0000000a

typedef struct _SM_VERSIONINFO {
    int      info_size;         /* size of user data (for compatibility) */
    VUINT32  major;
    VUINT32  minor;
    VUINT32  revision;
    VUINT32  build;
    VUINT32  type;
} SM_VERSIONINFO;

/* for type of SM_VERSION structure */
#define SM_ENG_TYPE_SMV         0x534D5600
#define SM_ENG_TYPE_PROCESS     0x50524F43

typedef struct _SM_EXECINFO {
    int      info_size;
    VUINT64  cpu_mileage;
    VUINT32  cur_pid;
    VUINT32  cur_tid;
    VUINT32  exec_status;
} SM_EXECINFO;

typedef struct _SM_INSTINFO {
    int      info_size;         /* size of user data (for compatibility) */
    VUINT8 * buffer;
    VUINT32  buf_len;
    VUINT32  machine_type;
    VUINT32  code_len;
    void   * p_op;
} SM_INSTINFO;

typedef struct _SM_MODULEINFO {
    int      info_size;         /* size of user data (for compatibility) */
    VUINT32  idx;
    VUINT32  pid;
    VUINT64  bgn;
    VUINT64  end;
} SM_MODULEINFO;

typedef struct _SM_THREADINFO {
    int      info_size;
    VUINT32  pid;
    VUINT32  tid;
    VUINT32  next_tid;
    VUINT8   state;
    struct  {
        VUINT64 thread_context_ea;
        VUINT64 rax;
        VUINT64 teb_ea;
    } context_info;
    VUINT32  exception_eip;
    VUINT32  exception_code;
    VUINT32  exception_count;
    VUINT32  exception_total;
    VUINT32  exit_code;
} SM_THREADINFO;

typedef struct _SM_PROCESSINFO {
    int      info_size;         /* size of user data (for compatibility) */
    VUINT32  pid;
    VUINT32  parent_pid;
    VUINT32  next_pid;
    VUINT16  process_name[260 + 1];
    struct _VSIO* mem_io;
} SM_PROCESSINFO;

typedef struct _SM_CPUINFO {
    int      info_size;         /* size of user data (for compatibility) */
    VUINT32  pid;
    VUINT32  tid;
    union {
        struct {
            VUINT64 rax;
            VUINT64 rcx;
            VUINT64 rdx;
            VUINT64 rbx;
            VUINT64 rsp;
            VUINT64 rbp;
            VUINT64 rsi;
            VUINT64 rdi;
            VUINT64 r08;
            VUINT64 r09;
            VUINT64 r10;
            VUINT64 r11;
            VUINT64 r12;
            VUINT64 r13;
            VUINT64 r14;
            VUINT64 r15;
        } reg64;
        struct {
            VUINT32 eax;
            unsigned : 4;
            VUINT32 ecx;
            unsigned : 4;
            VUINT32 edx;
            unsigned : 4;
            VUINT32 ebx;
            unsigned : 4;
            VUINT32 esp;
            unsigned : 4;
            VUINT32 ebp;
            unsigned : 4;
            VUINT32 esi;
            unsigned : 4;
            VUINT32 edi;
            unsigned : 4;
        } reg32;
    }gpr;
    
    VUINT64  rip;
    VUINT32  eip;
    VUINT32  eflag;
    struct {
        VUINT32     es;
        VUINT32     cs;
        VUINT32     ss;
        VUINT32     ds;
        VUINT32     fs;
        VUINT32     gs;
    } seg;
} SM_CPUINFO;

typedef struct _SM_VIRTUALMEMBLOCKINFO {
    int      info_size;
    VUINT32  pid;
    VUINT64  bgn;               /* =VirtualAddress+ImageBase             */
    VUINT64  end;               /* =VirtualAddress+ImageBase+VirtualSize */
    VUINT32  characteristics; 
    VUINT64  virtual_ofs;       /* =VirtualAddress+ImageBase-Point2Data  */
    VUINT64  virtual_end;
    VUINT64  physical_size;     /* =RawDataSize */
    VUINT64  rawdata_offset;    /* =RawDataOffset */
    VUINT64  next_bgn;
    VUINT8   name[0x20];        /* name of current block from raw file (maximum length of name is limited)*/
} SM_VIRTUALMEMBLOCKINFO;

#define SM_PTE_INFO_VALID                 (1 << 0)
#define SM_PTE_INFO_WRITE                 (1 << 1)                       /* Access permission bit */
#define SM_PTE_INFO_OWNER                 (1 << 2)                       /* kernel/user mode bit */
#define SM_PTE_INFO_ACCESS                (1 << 5)
#define SM_PTE_INFO_DIRTY                 (1 << 6)                       /* Dirty bit */
#define SM_PTE_INFO_EXECUTE               (1 << 9)                       /* Access permission bit */
#define SM_PTE_INFO_PROTOTYPE             (1 << 10)
#define SM_PTE_INFO_ABSOLUTE_DIRTY        (1 << 12)                      /* Absolute dirty bit. it can not beeb cleaned */                   
#define SM_PTE_INFO_COPY_ON_WRITE         (1 << 13)
#define SM_PTE_INFO_INITIALIZED           (1 << 15)

typedef struct _SM_VIRTUALMEMPAGEINFO {
    int      info_size;
    VUINT32  pid;
    VUINT64  ea;                /* =VirtualAddress+ImageBase             */
    VUINT16  flag;
} SM_VIRTUALMEMPAGEINFO;

/* PEB, TEB structure (TODO: Remove unnecessary entries */
/*
typedef struct _SM_PEB32 {
    VUINT8 InheritedAddressSpace;
    VUINT8 ReadImageFileExecOptions;
    VUINT8 BeingDebugged;
    VUINT8 Spare;
    VUINT32 Mutant;
    VUINT32 ImageBaseAddress;
    VUINT32 LoaderData;
    VUINT32 ProcessParameters;
    VUINT32 SubSystemData;
    VUINT32 ProcessHeap;
    VUINT32 FastPebLock;
    VUINT32 FastPebLockRoutine;
    VUINT32 FastPebUnlockRoutine;
    VUINT32 EnvironmentUpdateCount;
    VUINT32 KernelCallbackTable;
    VUINT32 EventLogSection;
    VUINT32 EventLog;
    VUINT32 FreeList;
    VUINT32 TlsExpansionCounter;
    VUINT32 TlsBitmap;
    VUINT32 TlsBitmapBits[0x2];
    VUINT32 ReadOnlySharedMemoryBase;
    VUINT32 ReadOnlySharedMemoryHeap;
    VUINT32 ReadOnlyStaticServerData;
    VUINT32 AnsiCodePageData;
    VUINT32 OemCodePageData;
    VUINT32 UnicodeCaseTableData;
    VUINT32 NumberOfProcessors;
    VUINT32 NtGlobalFlag;
    VUINT8 Spare2[0x4];
    union {
        struct {
            VUINT32 LowPart;
            VSINT32 HighPart;
        } ;
        struct {
            VUINT32 LowPart;
            VSINT32 HighPart;
        } u;
        VUINT64 QuadPart;
    } CriticalSectionTimeout;
    VUINT32 HeapSegmentReserve;
    VUINT32 HeapSegmentCommit;
    VUINT32 HeapDeCommitTotalFreeThreshold;
    VUINT32 HeapDeCommitFreeBlockThreshold;
    VUINT32 NumberOfHeaps;
    VUINT32 MaximumNumberOfHeaps;
    VUINT32 ProcessHeaps;
    VUINT32 GdiSharedHandleTable;
    VUINT32 ProcessStarterHelper;
    VUINT32 GdiDCAttributeList;
    VUINT32 LoaderLock;
    VUINT32 OSMajorVersion;
    VUINT32 OSMinorVersion;
    VUINT32 OSBuildNumber;
    VUINT32 OSPlatformId;
    VUINT32 ImageSubSystem;
    VUINT32 ImageSubSystemMajorVersion;
    VUINT32 ImageSubSystemMinorVersion;
    VUINT32 GdiHandleBuffer[0x22];
    VUINT32 PostProcessInitRoutine;
    VUINT32 TlsExpansionBitmap;
    VUINT8 TlsExpansionBitmapBits[0x80];
    VUINT32 SessionId;
} SM_PEB32, *PSM_PEB32;

typedef struct _SM_TEB32 {
    struct {
        VUINT32 ExceptionList;
        VUINT32 StackBase;
        VUINT32 StackLimit;
        VUINT32 SubSystemTib;
        union {
            VUINT32 FiberData;
            VUINT32 Version;
        };
        VUINT32 ArbitraryUserPointer;
        VUINT32 Self;
    } Tib;
    VUINT32 EnvironmentPointer;
    struct
    {
        VUINT32 UniqueProcess;
        VUINT32 UniqueThread;
    } Cid;
    VUINT32 ActiveRpcInfo;
    VUINT32 ThreadLocalStoragePointer;
    VUINT32 Peb;
    VUINT32 LastErrorValue;
    VUINT32 CountOfOwnedCriticalSections;
    VUINT32 CsrClientThread;
    VUINT32 Win32ThreadInfo;
    VUINT32 Win32ClientInfo[0x1F];
    VUINT32 WOW32Reserved;
    VUINT32 CurrentLocale;
    VUINT32 FpSoftwareStatusRegister;
    VUINT32 SystemReserved1[0x36];
    VUINT32 Spare1;
    VUINT32 ExceptionCode;
    VUINT32 SpareBytes1[0x28];
    VUINT32 SystemReserved2[0xA];
    VUINT32 GdiRgn;
    VUINT32 GdiPen;
    VUINT32 GdiBrush;
    struct
    {
        VUINT32 UniqueProcess;
        VUINT32 UniqueThread;
    } RealClientId;
    VUINT32 GdiCachedProcessHandle;
    VUINT32 GdiClientPID;
    VUINT32 GdiClientTID;
    VUINT32 GdiThreadLocaleInfo;
    VUINT32 UserReserved[5];
    VUINT32 GlDispatchTable[0x118];
    VUINT32 GlReserved1[0x1A];
    VUINT32 GlReserved2;
    VUINT32 GlSectionInfo;
    VUINT32 GlSection;
    VUINT32 GlTable;
    VUINT32 GlCurrentRC;
    VUINT32 GlContext;
    VUINT32 LastStatusValue;
    struct {
        VUINT16 Length;
        VUINT16 MaximumLength;
        VUINT32 Buffer;
    } StaticUnicodeString;
    VUINT16 StaticUnicodeBuffer[0x105];
    VUINT32 DeallocationStack;
    VUINT32 TlsSlots[0x40];
    VUINT32 TlsLinks;
    VUINT32 Vdm; 
    VUINT32 ReservedForNtRpc;
    VUINT32 DbgSsReserved[0x2];
    VUINT32 HardErrorDisabled;
    VUINT32 Instrumentation[0x10];
    VUINT32 WinSockData;
    VUINT32 GdiBatchCount;
    VUINT32 Spare2;
    VUINT32 Spare3;
    VUINT32 Spare4;
    VUINT32 ReservedForOle;
    VUINT32 WaitingOnLoaderLock;
    VUINT32 StackCommit;
    VUINT32 StackCommitMax;
    VUINT32 StackReserved;
} SM_TEB32;
*/


#define SMCFG_MAX_PAGE_FILE_SIZE                1
#define SMCFG_MAX_PHYSICAL_MEM_SIZE             2
#define SMCFG_MAX_TOTAL_EXCEPTION               3
#define SMCFG_MAX_DUPLICATED_EXCEPTION          4
#define SMCFG_MAX_PROCESS_COUNT                 5
#define SMCFG_FILE_GEN                          6
#define SMCFG_MAX_THREAD_COUNT                  7
#define SMCFG_MAX_MILEAGE                       8
#define SMCFG_ENABLE_UNSUPPORT_API_PASSING      9
#define SMCFG_EXEC_FILTER_FLAG                  10
#define SMCFG_VM_OS_LANGUAGE                    11
#define SMCFG_ENABLE_STOP_CONDITION             12


#ifdef SM_DYNAMIC_LOAD
/* dynamic link: typedef */
#define SM_EXPORT_SYMBOL(type, symbol)       typedef type SM_EXPORT_API   t ## symbol
#else
/* static link: declare */
#define SM_EXPORT_SYMBOL(type, symbol)       type SM_EXPORT_API  symbol
#endif

SM_EXPORT_SYMBOL(int, SMVInit)(SMHANDLE * p_sm);
SM_EXPORT_SYMBOL(int, SMVDeInit)(SMHANDLE sm_handle);
SM_EXPORT_SYMBOL(int, SMVGetVersion)(SM_VERSIONINFO *sm_version);
SM_EXPORT_SYMBOL(int, SMVLoadAndExecuteA)(SMHANDLE sm_handle, struct _VSIO* io, const VUINT8 * real_name_sys, VUINT8 *command_line, SM_CALLBACK_HANDLE *callback);
SM_EXPORT_SYMBOL(int, SMVLoadAndExecuteW)(SMHANDLE sm_handle, struct _VSIO* io, const VWCHAR * real_name_sys, VUINT8 *command_line, SM_CALLBACK_HANDLE *callback);
SM_EXPORT_SYMBOL(int, SMVSetConfig)(SMHANDLE sm_handle, VUINT32 id, void* input);
SM_EXPORT_SYMBOL(int, SMVGetConfig)(SMHANDLE sm_handle, VUINT32 id, void* output);
SM_EXPORT_SYMBOL(int, SMVReadProcessMemory)(SMHANDLE sm_handle, VUINT32 pid, VUINT8 *buf, VUINT64 ea, VUINT32 buf_size, VUINT32 *read_length);
SM_EXPORT_SYMBOL(int, SMVQueryInfo)(SMHANDLE sm_handle, VUINT32 id, void *user_param);
SM_EXPORT_SYMBOL(int, SMVSetTempPathA)(SMHANDLE sm_handle, VUINT8* path_name);
SM_EXPORT_SYMBOL(int, SMVGetTempPathA)(SMHANDLE sm_handle, VUINT8* path_name, VUINT32* path_len);
SM_EXPORT_SYMBOL(int, SMVUnload)(SMHANDLE sm_handle);
SM_EXPORT_SYMBOL(int, SMVSetTempPathW)(SMHANDLE sm_handle, VUINT16* path_name);
SM_EXPORT_SYMBOL(int, SMVGetTempPathW)(SMHANDLE sm_handle, VUINT16* path_name, VUINT32* path_len);

SM_EXPORT_SYMBOL(int, SMVSetDbgMode)(SMHANDLE sm_handle, VUINT32 mode);
SM_EXPORT_SYMBOL(int, SMVSetVerboseLevel)(VUINT32 id, VUINT32 value);
SM_EXPORT_SYMBOL(int, SMVSetDbgLogFileName)(SMHANDLE sm_handle, VUINT8 * temp_path);
SM_EXPORT_SYMBOL(int, SMVSetDbgDumpDirName)(SMHANDLE sm_handle, VUINT8 * temp_path);



/*======= pack end =======*/
#ifdef VS_PACK_STRUCTURE
#pragma pack(pop)
#endif

#ifdef __cplusplus
}
#endif

#endif


