/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2015 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* afi_def.h */
/* Description: */
/* */
/**************************************************************************/

/** @file afi_def.h
 *    @brief Virus Scan API (VSAPI/ATSE)
 */


#ifndef __AFI_DEF_H__
#define __AFI_DEF_H__

/*[AFI_PE] block version*/
#define AFI_BASIC_VER                       0x00000001
#define AFI_PE_SECTION_VER                  0x00000001
#define AFI_PE_SEC_ENTROPY_VER              0x00000001
#define AFI_PE_PACK_VER                     0x00000001
#define AFI_PE_TRAP_VER                     0x00000001
#define AFI_PTN_VER                         0x00000001
#define AFI_LAYER_BGN_VER                   0x00000001
#define AFI_LAYER_END_VER                   0x00000001
#define AFI_SHA1_VER                        0x00000001
#define AFI_PASSWORD_VER                    0x00000001
#define AFI_PE_EXPORT_INFO_VER              0x00000001
#define AFI_PE_INFO_VER                     0x00000001
#define AFI_XML_INFO_VER                    0x00000001
#define AFI_RULEMATCHED_VER                 0x00000001
#define AFI_VIRUSFOUND_VER                  AFI_RULEMATCHED_VER
#define AFI_HTMLURL_VER                     0x00000001
#define AFI_BGNEXTRACT_VER                  0x00000002
#define AFI_PREEXTRACT_VER                  0x00000001
#define AFI_POSTEXTRACT_VER                 0x00000001
#define AFI_ENDEXTRACT_VER                  0x00000001
#define AFI_PE_EXTRA_INFO_VER               0x00000001
#define AFI_CDMD_V1_VER                     0x00000001
#define AFI_MACRO_INFO_VER                  0x00000001
#define AFI_SOURCE_QUERY_VER                0x00000001
#define AFI_PW_PROTECT_VER                  0x00000001
#define AFI_IFRAME_VER                      0x00000001
#define AFI_FILEDECLARATION_VER             0x00000001
#define AFI_PTN_EX_VER                      0x00000001
#define AFI_FT_ADDITION_VER                 0x00000002
#define AFI_XSCAN_RSP_VER                   0x00000001
#define AFI_XSCAN_IMPTBL_VER                0x00000001
#define AFI_XSCAN_OPCODE_VER                0x00000001
#define AFI_SHA256_VER                      0x00000001
#define AFI_4WCTX_QUERY_VER                 0x00000001

/*[AFI_PE] block ID*/
enum {
    AFI_BASIC_ID            =  1,
    AFI_PE_SECTION_ID       =  2,
    AFI_PE_SEC_ENTROPY_ID   =  3,
    AFI_PE_PACK_ID          =  4,
    AFI_PE_TRAP_ID          =  5,
    AFI_PTN_ID              =  6,
    AFI_LAYER_BGN_ID        =  7, /* VSCFG_AFI_LAYER_FLAG */
    AFI_LAYER_END_ID        =  8, /* VSCFG_AFI_LAYER_FLAG */
    AFI_SHA1_ID             =  9,
    AFI_PASSWORD_ID         = 10,
    AFI_PE_EXPORT_INFO_ID   = 11,
    AFI_PE_INFO_ID          = 12,
    AFI_XML_INFO_ID         = 13,
    AFI_RULEMATCHED_ID      = 14,
    AFI_VIRUSFOUND_ID       = 15,
    AFI_HTMLURL_ID          = 16,
    AFI_BGNEXTRACT_ID       = 17, /* VSCFG_AFI_EXTRACT_FLAG */
    AFI_PREEXTRACT_ID       = 18, /* VSCFG_AFI_EXTRACT_FLAG */
    AFI_POSTEXTRACT_ID      = 19, /* VSCFG_AFI_EXTRACT_FLAG */
    AFI_ENDEXTRACT_ID       = 20, /* VSCFG_AFI_EXTRACT_FLAG */
    AFI_PE_EXTRA_INFO_ID    = 21,
    AFI_CDMD_V1_ID          = 22, /* Custom Defense Method Data V1 */
    AFI_MACRO_INFO_ID       = 23, /* Microsoft Office Macro Info. */
    AFI_SOURCE_QUERY_ID     = 24,
    AFI_PW_PROTECT_ID       = 25, /* password protect info. */
    AFI_IFRAME_ID           = 26, /* zero pixel iframe */
    AFI_FILEDECLARATION_ID  = 27, /* dangerous files declaration */
    AFI_PTN_EX_ID           = 28, /* reserved for VSAPI */
    AFI_FT_ADDITION_ID      = 29, /* file type addition */
    AFI_XSCAN_RSP_ID        = 30, /* XScan response */
    AFI_XSCAN_IMPTBL_ID     = 31, /* XScan ImportTable raw data */
    AFI_XSCAN_OPCODE_ID     = 32, /* XScan OPCode raw data */
    AFI_SHA256_ID           = 33, /* Whole file SHA256 */
    AFI_4WCTX_QUERY_ID      = 34, /* Query 4W contex */

    /************* Append new AFI ID before this line *************/
    VSCFG_AFI_MAX_ID, /* AFI ID < this value */
    VSCFG_AFI_DUMMY_ID = VSCFG_AFI_MAX_ID
};

/* [AFI] layer */
/* layer type */
#define AFI_NONE_LAYER_TYPE                 0x00000000
#define AFI_COMP_LAYER_TYPE                 0x00000001
#define AFI_PACK_LAYER_TYPE                 0x00000002
#define AFI_EMBED_LAYER_TYPE                0x00000003
#define AFI_GRAFT_LAYER_TYPE                0x00000004 /* a kind of embed, but not real file */
#define AFI_OVERLAY_LAYER_TYPE              0x00000005
#define AFI_SCRIPT_LAYER_TYPE               0x00000006
/* file flag */
#define AFI_STORED_FLAG                     0x00000001 /* only support cab, rar and zip(include Office 2007 files, jar, apk ...) */

typedef struct
{
    VSAFIH  header;
    VULONG  ulLayerType;
    VULONG  ulDataType; /* Unknown Type: (ulDataType, ulSubType) = (0xFFFFFFFF, 0xFFFFFFFF) */
    VULONG  ulSubType;
    VCHAR*  pFileName;
    VULONG  ulFileNameType;
    VULONG  ulFileSize;
    VULONG  ulFileFlag;
    VSHANDLE* pVshandle; /* May be NULL. (ex. AFI_GRAFT_LAYER_TYPE) */
} VSAFI_LAYER_BGN;

typedef struct
{
    VSAFIH  header;
} VSAFI_LAYER_END;

typedef struct
{
    VSAFIH  header;
    VULONG  flag;
} VSAFI_XML_INFO;
#define AFI_XML_INFO_HTML                   0x00000001  /* exist <html ...> */
#define AFI_XML_INFO_SCRIPT                 0x00000002  /* exist <script ...> */
#define AFI_XML_INFO_APPLET                 0x00000004  /* exist <applet ...> or <jar ...> */

typedef struct
{
    const VUCHAR* data;     /* include quote-symbol if it exists. */
    VULONG data_size;
    VULONG truncated_size;  /* data too large so truncated it */

    VULONG scheme_crc;
    VULONG scheme_size;
} VSAFI_URL_INFO;
/* html url scheme crc (lower case) */
#define AFI_HTMLURL_HTTP                0xDFB6CEB4 /* http:// */
#define AFI_HTMLURL_HTTPS               0x9BF38A2E /* https:// */
#define AFI_HTMLURL_FILE                0x0796DF42 /* file:// */
#define AFI_HTMLURL_FTP                 0x29F16FA6 /* ftp:// */
#define AFI_HTMLURL_WWW                 0x1BFE040E /* www. */

typedef struct
{
    VSAFIH  header;
    VSAFI_URL_INFO url_info;
} VSAFI_HTMLURL;


typedef struct
{
    const char* short_virname;
    const char* long_virname;
    VULONG  atse_rule_aggressive_level; /* 0 is not ATSE rule */
    VULONG  atse_rule_category; /* ATSE_RC_xxx */
    VULONG  reserve1;
    VULONG  reserve2;
    VULONG  detection_class;
} VSAFI_RULEMATCHED_INFO;

/* detection class */
#define VSAFI_DETECTIONCLASS_NOTVIRUS           0
#define VSAFI_DETECTIONCLASS_VIRUSFOUND         1
#define VSAFI_DETECTIONCLASS_HEURISTIC          2
#define VSAFI_DETECTIONCLASS_SILENT             3
#define VSAFI_DETECTIONCLASS_INFORMATION        4
#define VSAFI_DETECTIONCLASS_UDF                5
#define VSAFI_DETECTIONCLASS_CDF                6
#define VSAFI_DETECTIONCLASS_POLICY             7 

typedef struct
{
    VSAFIH  header;
    VSAFI_RULEMATCHED_INFO rmi;
} VSAFI_RULEMATCHED, VSAFI_VIRUSFOUND;


typedef struct
{
    VSAFIH  header;
    VULONG  flag;   /* ref. AFI_PWPF_XXX */
} VSAFI_PW_PROTECT;
#define AFI_PWPF_OLE                0x00000001
#define AFI_PWPF_ZIP                0x00000002
#define AFI_PWPF_PDF                0x00000004

#define AFI_PWPF_DEFAULT_PW         0x80000000

typedef struct
{
    VSAFIH  header;

    const VUCHAR* data; /* not include quote-symbol */
    VULONG data_size;
    VULONG truncated_size; /* data too large so truncated it */
    VULONG width;
    VULONG height;
} VSAFI_IFRAME;

typedef struct
{
    VSAFIH  header;

    VULONG  severity;       /* 0:lowest ~ 100:highest */
    const VUCHAR* ext_name; /* !=NULL, but can be "", multiple UTF-8 string */
} VSAFI_FILEDECLARATION;

typedef struct
{
    VSAFIH  header;

    VULONG  category;       /* File type addition category */
    VULONG  type;           /* File type addition type */
    const VUCHAR* ext_name; /* !=NULL, but can be "", multiple UTF-8 string */
} VSAFI_FT_ADDITION;

/* File Type Addition Category */
#define FTAC_UNKNOWN        0
#define FTAC_MSOLE          1 /* Microsoft OLE: doc/xls/ppt... */
#define FTAC_MIMEHTML       2 /* MIME HTML(single web page): mht */
#define FTAC_MSOPENXML      3 /* Microsoft open XML: docx/xlsx/ppts... */

/* File Type Addition Type */
#define FTAT_UNKNOWN        0
#define FTAT_DOC            1
#define FTAT_XLS            2
#define FTAT_PPT            3
#define FTAT_MHT_DOC        4
#define FTAT_MHT_XLS        5
#define FTAT_MHT_PPT        6
#define FTAT_MHT_GEN        7
#define FTAT_DOCX           8
#define FTAT_DOCM           9
#define FTAT_DOTX           10
#define FTAT_DOTM           11
#define FTAT_XLSX           12
#define FTAT_XLSM           13
#define FTAT_XLTX           14
#define FTAT_XLTM           15
#define FTAT_XLAM           16
#define FTAT_XLSB           17
#define FTAT_PPTX           18
#define FTAT_PPTM           19
#define FTAT_POTX           20
#define FTAT_POTM           21
#define FTAT_PPSX           22
#define FTAT_PPSM           23
#define FTAT_PPAM           24



/***************/
/* AFI Extract */
/***************/
typedef struct {
    VULONG struct_size;         /* input/output: size of structure */
    VULONG flag_version;        /* output: the version of flags */

    const VUCHAR* name;
    VULONG flag;
    VULONG size;
    VULONG packed_size;
} VSDC_FILEINFO;

#define VSDC_FILEINFO_STRUCTSIZE        sizeof(VSDC_FILEINFO)
#define VSDC_FILEINFO_FLAGVERSION       1

/* VSDC_FILEINFO.flag Version 1 */
#define VSDC_FIFLAG_STORED                        0x00000001
#define VSDC_FIFLAG_SOLID                         0x00000002
#define VSDC_FIFLAG_UTF8NAME                      0x00000004


struct VSDC_HANDLE;
/*
    function:
        enum file. (ref. VSDC_FILEINFO)
    return:
        0:succeed; 1:no more files; <0:VSAPI standard error; >1:undefine
*/
typedef int VSDC_READDIR_FUNC(struct VSDC_HANDLE* vsdc, VSDC_FILEINFO* dcfi);


/*
    function:
        open file. (return VSHANDLE)
    return:
        0:succeed; <0:VSAPI standard error; >0:undefine
    note:
        The VSHANDLE be close by VSAPI/ATSE.
*/
typedef int VSDC_EXTRACT_FUNC(struct VSDC_HANDLE* vsdc, VSHANDLE** p_vsh);


/*
    function:
        release VSDC_HANDLE
    return:
        0:succeed; <0:VSAPI standard error; >0:undefine
*/
typedef int VSDC_CLEANUP_FUNC(struct VSDC_HANDLE** p_vsdc);

typedef struct VSDC_HANDLE {
    VSDC_READDIR_FUNC* readdir;
    VSDC_EXTRACT_FUNC* extract;
    VSDC_CLEANUP_FUNC* cleanup;
} VSDC_HANDLE;

typedef struct
{
    VSAFIH  header;
    VSHANDLE* vsh;      /* Valid file handle */
    const VSDTYPE* dt;  /* Valid data type */
    VSDC_HANDLE* vsdc;  /* Output (optional) */
    VULONG flag;        /* Additional archive flag */
} VSAFI_BGNEXTRACT;

#define AFI_BE_FLAG_NOT_SUPPORT         0x00000001 /* this archive file ATSE/VSAPI not support yet. */


typedef struct
{
    VSAFIH  header;
    int status;         /* status!=0 following items may be invalid */
    const VUCHAR* name;
    VULONG flag;
    VULONG size;
    VULONG packed_size;
} VSAFI_PREEXTRACT;

/* AFI_PREEXTRACT_VER = 1 */
#define AFI_PREEX_STORED                        0x00000001
#define AFI_PREEX_SOLID                         0x00000002
#define AFI_PREEX_UTF8NAME                      0x00000004


typedef struct
{
    VSAFIH  header;
    int status;
    RESOURCE *res; /* Valid Resource handle */
} VSAFI_POSTEXTRACT;

typedef struct
{
    VSAFIH  header;
    int status; /* 0:invalid, 1:matched, 2:inherit */
} VSAFI_ENDEXTRACT;


/*[AFI_PE] basic flags*/
#define AFI_ANSI_FILE_NAME                  0x00000000 /* RawData null-terminated string */
#define AFI_UNICODE_FILE_NAME               0x00000001 /* Unicode null-terminated string */
#define AFI_UTF8_FILE_NAME                  0x00000002 /* UTF-8 null-terminated string */

typedef struct _VSAFI_BASIC
{
    VSAFIH  header;
    VULONG  ulDataType; /* Unknown Type: (ulDataType, ulSubType) = (0xFFFFFFFF, 0xFFFFFFFF) */
    VULONG  ulSubType;
    VCHAR*  pFileName;
    VULONG  ulFileNameType;
} VSAFI_BASIC;

/*[AFI_PE] section flags*/
#define AFI_PE_SECTION_ZERO_RSIZE           0x00000001
#define AFI_PE_SECTION_RWE                  0x00000002
#define AFI_PE_SECTION_ALL_RWE              0x00000004
#define AFI_PE_SECTION_FL_EXECUTE           0x00000008

typedef struct _VSAFI_PE_SECTION
{
    VSAFIH  header;
    VULONG  ulFlags;
    VUSHORT usNumOfSections;
    VUCHAR** ppSectionNames;
} VSAFI_PE_SECTION;

typedef struct _VSAFI_PE_SEC_ENTROPY
{
    VSAFIH  header;
    VULONG  ulMaxSecEntropy;
} VSAFI_PE_SEC_ENTROPY;

typedef struct _VSAFI_PE_PACK
{
    VSAFIH  header;
    VULONG  ulPackLayer;
} VSAFI_PE_PACK;


typedef struct _VSAFI_SHA1
{
    VSAFIH  header;
    VUCHAR  sha1[20];
} VSAFI_SHA1;

typedef struct _VSAFI_SHA256
{
    VSAFIH  header;
    VUCHAR  sha256[32];
} VSAFI_SHA256;

typedef struct VSAFI_PS {
    struct VSAFI_PS* next;
    VULONG size;
    VUCHAR data[1];
} VSAFI_PS;

typedef struct {
    VSAFIH  header;
    VSAFI_PS* ps;
} VSAFI_PASSWORD;


/*[AFI_PE] trap flags*/
#define AFI_PE_TRAP_CALLRING0               0x00000001
#define AFI_PE_TRAP_EP_NOT_FIRST_SEC        0x00000002
#define AFI_PE_TRAP_SOFTMICE_DUMP           0x00000004
#define AFI_PE_TRAP_STACK_DUMP              0x00000008
#define AFI_PE_TRAP_ABNOR_IMPORT_TBL        0x00000010
#define AFI_PE_TRAP_ABNOR_BASE_RELOC_TBL    0x00000020
#define AFI_PE_TRAP_READ_KERNEL32_DLL       0x00000040
#define AFI_PE_TRAP_SEARCH_KERNEL32_API     0x00000080
#define AFI_PE_TRAP_MATCH_SCORE             0x00000100

typedef struct _VSAFI_PE_TRAP
{
    VSAFIH  header;
    VULONG  ulFlags;
} VSAFI_PE_TRAP;

typedef struct _VSAFI_PTN
{
    VSAFIH                  header;
    VULONG                  ulDetectNameCnt;
    VUCHAR**                ppDetectNames;
} VSAFI_PTN;

typedef struct VSAFI_PTN_EX
{
    VSAFIH      header;
    const char* pDetectionName;
    VULONG      ulInfo;
    VULONG      ulPatternVersion;
} VSAFI_PTN_EX;

/*[AFI_PE] PE export info flags */
#define AFI_PE_EXPORT_INFO_CPL  0x00000001

typedef struct _VSAFI_PE_EXPORT_INFO
{
    VSAFIH  header;
    VULONG  flags;   /* ref PE export info flags */
} VSAFI_PE_EXPORT_INFO;


/*[AFI_PE] PE info flags */
#define AFI_PE_INFO_DLL     0x40000000
#define AFI_PE_INFO_X86_64  0x80000000

typedef struct _VSAFI_PE_INFO
{
    VSAFIH  header;
    VUSHORT machine;
    VUSHORT num_of_sections;
    VUSHORT major_version;
    VUSHORT minor_version;
    VULONG  flags;      /* ref PE info flags */
} VSAFI_PE_INFO;


/*[AFI_PE] PE extra info flags */
#define AFI_PE_EXTRA_INFO_INNO          0x00000001
#define AFI_PE_EXTRA_INFO_NSIS          0x00000002
#define AFI_PE_EXTRA_INFO_INSTALLSHIELD 0x00000004
#define AFI_PE_EXTRA_INFO_ZIP_SFX       0x00000008
#define AFI_PE_EXTRA_INFO_7Z_SFX        0x00000010
#define AFI_PE_EXTRA_INFO_RAR_SFX       0x00000020
#define AFI_PE_EXTRA_INFO_CAB_SFX       0x00000040

typedef struct _VSAFI_PE_EXTRA_INFO
{
    VSAFIH  header;
    VULONG  flags;   /* ref PE extra info flags */
} VSAFI_PE_EXTRA_INFO;

/* Custom Defense Method Data V1 */
typedef struct VSAFI_CDMD_V1
{
    VSAFIH  header;
    VULONG  filter;
    VUCHAR  sha1[20];
} VSAFI_CDMD_V1;

/* Microsoft Office Macro Info. */
/* In a file (AFI layer), it may not VSAFI_MACRO_INFO event, or more than one VSAFI_MACRO_INFO events. */
typedef struct VSAFI_MACRO_INFO
{
    VSAFIH  header;
    VULONG  macro_number;   /* how many macro in current event. (0 < macro_number) */
    VULONG  reserve1;       /* reserve for internal use */
} VSAFI_MACRO_INFO;

typedef struct AFI_BUF
{
    VULONG  len;
    const VUCHAR* buf;
} AFI_BUF;

typedef struct VSAFI_SOURCE_QUERY
{
    VSAFIH  header;

    VUSHORT source_type, source_subtype;  /* for response */ /* initail (AFI_ST_UNKNOWN, 0), means "not avaliable" */
    VUSHORT file_type, file_subtype;      /* for response */ /* initial (VSDT_UNKNOWN, 0), means "not avaliable" */
    AFI_BUF content_type;                 /* for response */ /* initial (0, NULL), means "not avaliable" */ /* case insensitive */
    AFI_BUF charset;                      /* for response */ /* initial (0, NULL), means "not avaliable" */ /* case insensitive */
} VSAFI_SOURCE_QUERY;

/* AFI object source type */
#define AFI_ST_UNKNOWN              0
#define AFI_ST_DISK                 1
#define AFI_ST_NETWORK              2
#define AFI_ST_MESSAGE              3
#define AFI_ST_CLOUD                4

/* AFI object source subtype */
#define AFI_ST_DISK_ENUM            0   /* TMOS: manual scan */
#define AFI_ST_DISK_READ            1   /* TMOS: realtime scan */
#define AFI_ST_DISK_READWRITE       2   /* TMOS: realtime scan */
#define AFI_ST_DISK_EXECUTE         3   /* TMOS: realtime scan */
#define AFI_ST_DISK_RUNKEY          4   /* TMOS: quick scan */
#define AFI_ST_DISK_DROPFILE        5   /* Sanbox: drop file */

#define AFI_ST_NETWORK_HTTP         0   /* DDI: http/https */
#define AFI_ST_NETWORK_EMAIL        1   /* an email(mime or *.msg) in the network protocol hook */
#define AFI_ST_NETWORK_FTP          2   /* DDI: ftp/ftps */
#define AFI_ST_NETWORK_SAMBA        3   /* DDI: samba */

#define AFI_ST_MESSAGE_ATTACHMENT   0   /* IM: email attachment */

#define AFI_ST_CLOUD_URL            0   /* URL download ? */
#define AFI_ST_CLOUD_DOWNLOAD       1   /* Sanbox: downloader */


#endif /* __AFI_DEF_H__ */

