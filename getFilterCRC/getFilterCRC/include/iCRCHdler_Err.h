#ifndef __ICRCHDLER_ERR_H__
#define __ICRCHDLER_ERR_H__

/** \defgroup	ICRC_ERROR_CODE		Error codes returned by iCRCHandler */
/*@{*/
/**
*  \brief  Success
*/
#define ICRC_ERROR_OK                           0     
/**
*  \brief  Initial cURL library failed. Call GetLastError() to get cURL Error Code.
*/
#define ICRC_INIT_CURL_FAILED                   0x20000001        
/**
*  \brief  Create STOP Event failed. Might have other iCRCHdler exist. Call GetLastError() to get Error Code.
*/
#define ICRC_CREATE_STOP_EVENT_FAILED           0x20000002        
/**
*  \brief  This function not implemented yet.
*/
#define ICRC_ERROR_NOT_IMPLEMENTED              0x20000003        
/**
*  \brief  Bloom Filter is not loaded
*/
#define ICRC_BLOOM_FILTER_NOT_LOADED            0x20000004        
/**
*  \brief  Initial IOT ICRC Server failed. Call GetLastError() to get Error Code return from IOT Library.
*/
#define ICRC_INIT_IOT_ICRC_FAILED               0x20000005        
/**
*  \brief  Need to call iCRCHandlerInitialize() first.
*/
#define ICRC_NOT_INIT                           0x20000006             
/**
*  \brief  HTTP error (the same as ICRC_CHK_SERVER_NETWORK_ERROR)
*/
#define ICRC_HTTP_ERROR                         0x20000007 
/**
*  \brief  Network error. Error code refer cURL error codes define. http://curl.haxx.se/libcurl/c/libcurl-errors.html .
*/
#define ICRC_CHK_SERVER_NETWORK_ERROR           0x20000007   
/**
*  \brief  Network error (the same as ICRC_CHK_SERVER_SERVER_ERROR)
*/
#define ICRC_NETWORK_ERROR                      0x20000008 
/**
*  \brief  Server error. Error code will be 404 http not found, or other error return from Scan Server.
*/
#define ICRC_CHK_SERVER_SERVER_ERROR            0x20000008        
/**
*  \brief  Check response integrity failed. Content may be corrupt. Do not have detail error code.
*/
#define ICRC_CHK_SERVER_INTEGRITY               0x20000009        
/**
*  \brief  Maybe load wrong iCRCHdler version, or forgot to set ICRC_GLOBAL_CONFIG.dwStructSize value. For ICRC_GLOBAL_CONFIG_EX, you may forgot to set dwStructSize or dwStructVer values.
*/
#define ICRC_GLOBAL_CONFIG_STRUCTURE_WRONG      0x2000000a    
/**
*  \brief  iCRCHandler Have already initialized. 
*/
#define ICRC_ALREADY_INITIALIZED                0x2000000b        
/**
*  \brief  Invalid parameters
*/
#define ICRC_INVALID_PARAM                      0x2000000c        

/**
*  \brief  System error. You could get information by calling GetLastError()
*/
#define ICRC_SYSTEM_ERROR                       0x2000000d 
/**
*  \brief  Patch process failed.
*/
#define ICRC_PATCH_ERROR                        0x2000000e  
/**
*  \brief  There is another update process is going.
*/
#define ICRC_IS_UPDATING                        0x2000000f  
/**
*  \brief  User aborted the update process.
*/
#define ICRC_ABORT_UPDATE                       0x20000010  

/**
*  \brief  Register iCRC built-in multi-thread callback functions failed.
*/
#define ICRC_REGISTER_OPENSSL_CALLBACK_FAILED   0x20000011     
/**
*  \brief  Success but product should register its own OpenSSL multithread callback functions.
*/
#define ICRC_ERROR_OK_WITHOUT_REGISTER_OPENSSL_MT_CALLBACK  0x20000012     
/**
*  \brief  Maybe load wrong iCRCHdler version. For ICRC_SERVER_CONFIG_EX, you may forgot to set dwStructVer value.
*/
#define ICRC_SERVER_CONFIG_STRUCTURE_WRONG      0x20000013    
/**
*  \brief  Maybe load wrong iCRCHdler version. For ICRC_SSL_OPTION_EX, you may forgot to set dwStructVer value.
*/
#define ICRC_SSL_OPT_STRUCTURE_WRONG            0x20000014    
/**
*  \brief  The input buffer size is too small.
*/
#define ICRC_BUFFER_TOO_SMALL                   0x20000015
/**
* \brief  This operations is not not supported.
*/
#define ICRC_NOT_SUPPORT_ERROR                  0x20000016
/**
* \brief  The checksum is invalid.
*/
#define ICRC_INVALID_CHECKSUM                   0x20000017
/**
* \brief  The file format is invalid.
*/
#define ICRC_INVALID_FILE_FORMAT                0x20000018
/**
* \brief  The pattern version is not correct.
*/
#define ICRC_ERROR_PTN_VERSION                  0x20000019
/**
* \brief  The item is not found.
*/
#define ICRC_NOT_FOUND                          0x2000001a
/**
\brief  The input data is not enough to complete the operation.
*/
#define ICRC_NO_MORE_DATA                       0x2000001b
/**
*  \brief  Initial shared memory failed.
*/
#define ICRC_INIT_SHM_FAILED                    0x2000001c
/**
*  \brief  Initial shared memory failed.
*/
#define ICRC_RESELECT_ADDRESS_FAMILY_FAILED     0x2000001d
/**
* \brief  No available resource could be found.
*/
#define ICRC_NO_AVAILABLE_RESOURCE              0x2000001e
/**
* \brief  Froze download action.
*/
#define ICRC_FROZE_DOWNLOADING                  0x2000001f
/**
* \brief  Initialize global object failed.
*/
#define ICRC_INIT_GLOBAL_OBJECT_FAILED          0x20000020
/*@}*/

/**
*  \brief  Failed to get result
*/
#define ICRC_QUERY_FAIL_GET_RESULT              -1                
/**
*  \brief  timeout
*/
#define ICRC_QUERY_TIMEOUT                      -2                
/**
*  \brief  Input value nBucketID invalid
*/
#define ICRC_QUERY_INVALID_BUCKETID             -3                
/**
*  \brief  Require query Scan Server in CLOUD SCAN MODE
*/
#define ICRC_QUERY_REQUIRE_SS                   -4                


#endif // __ICRCHDLER_ERR_H__ 
