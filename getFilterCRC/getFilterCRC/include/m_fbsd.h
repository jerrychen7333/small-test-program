/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-2007 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: LINUX */
typedef char          VCHAR;/* 1 byte */
#ifdef __amd64
    typedef int			VLONG;	/* 4 byte */
    typedef unsigned int	VULONG; /* 4 byte */
#else
    typedef long			VLONG;	/* 4 byte */
    typedef unsigned long	VULONG; /* 4 byte */
#endif
typedef long long int  	            VINT64;  /* 8 byte */
typedef unsigned long long int		VUINT64; /* 8 byte */

#define VSCTYPE	long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR	"/virus"
#define DEFAULT_PATTERN_DIR "/etc/iscan"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR	"/tmp"
#define EXPORT
#define TMVSAPI
#define HANDLE			int
#define MAX_PATH_LEN		1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PASCAL
#define VSLongLongToString(cp,x)	(*(VUINT64 *)(cp) = x)
#define VSLongToString(cp,x)	(*(VULONG *)(cp) = x)
#define VSShortToString(cp,x)	(*(VUSHORT *)(cp) = x)
#define VSStringToLongLong(cp)	(*(VUINT64 *)(cp))
#define VSStringToLong(cp)	(*(VULONG *)(cp))
#define VSStringToShort(cp) (*(VUSHORT *)(cp))

#define VS_IO_PLUGIN_SUPPORTED 1 /* support IO plug-in in Resource */
