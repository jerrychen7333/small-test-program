/* $Date: 2001/2/19 PM 05:03:48$ */
/*
 * $Log: 
 *  8    NewScanEngine1.7         2001/2/19 PM 05:03:48Wayne_chu       Synchronize
 *       cheetah and cheeath_rel
 *  7    NewScanEngine1.6         2001/1/19 AM 11:58:03Wayne_chu       add
 *       VSGetBackupFileInfo and fix the problem of convert between Unicode and
 *       Ansicode.
 *  6    NewScanEngine1.5         1999/12/13 PM 06:27:56Joanna_tsai     for RAR,
 *       first using 0x100000, if fail to malloc such big space
 *       then using 0x10000
 *       move the maxwinsize definition to rar files
 *  5    NewScanEngine1.4         1999/11/2 PM 02:39:38Joanna_tsai    
 *       RAR_MAXWINSIZE should be 0x100000
 *       some platform like win16 cannot support it, so declare it as 0x10000
 *  4    NewScanEngine1.3         1999/5/24 PM 01:28:02Viking_ho       VCHAR should
 *       be defined separately by m_xxx.h
 *  3    NewScanEngine1.2         1998/11/9 PM 02:00:46Lily_ho         
 *  2    NewScanEngine1.1         1998/4/24 AM 11:38:14Lily_ho         
 *  1    NewScanEngine1.0         1998/4/23 AM 09:40:12Lily_ho         
 * $
 * 
 * 5     11/02/99 2:39p Joanna
 * RAR_MAXWINSIZE should be 0x100000
 * some platform like win16 cannot support it, so declare it as 0x10000
 * 
 * 4     99/05/24 1:28p Viking
 * VCHAR should be defined separately by m_xxx.h
 * 
 * 3     11/09/98 2:00p Lily
 * 
 * 8     11/06/98 4:08p Cliff_liang
 * define VSCTYPE(=long) 
 * 
 * 24    4/23/98 12:08p Cliff
 * cange default DECOMP_MAX_MEM size from 40960 to 65535
 * 
 * 23    4/22/98 3:28p Cliff
 * Add definition of DECOMP_MIN_MEM and DECOMP_MAX_MEM
 * 
 * 22    3/23/98 2:25p Cliff
 * VULONG and VLONG should be defined separately by m_xxx.h
 * 
 * 21    3/20/98 2:59p Cliff
 * Add proejct name and Trend Copy right declaration.
 *
 * 17	 11/03/97 12:22p Cliff
 * Move internal use data to header
 *
 * 14	 10/20/97 11:39a Cliff
 * redefine MACOPEN
 *
 * 11	 10/16/97 3:33p Cliff
 * Add MACCLOSE, MACOPEN,MACSEEK, and MACREAD macro
 *
 * 6	 9/23/97 3:05p Cliff
 * Remove VSRotateRight definition because it was used only by getoutvr.c
 *
 * 3	 7/31/97 12:47p Cliff
 * Move pragma pack(1) from tmvsdef.h to here.
 *
 * 2	 7/24/97 12:47p Cliff
 * Remove ALTERNATIVE
 *
 * 2	 7/03/97 11:03a Rogerc
 * merged with Cliff's updated on 0702.
 *
 * 14	 6/11/97 11:29a Roger
 *
 * 13	 5/21/97 3:52p Roger
 *
 * 12	 5/16/97 4:55p Roger
 *
 * 11	 5/16/97 11:01a Roger
 * define macro VSRotateRight.
 *
 * 10	 5/02/97 2:59p Roger
 *
 * 9	 4/29/97 1:48p Roger
 * after merge with Cliff's source code on 24/4.
 *
 * 8	 4/28/97 12:56p Roger
 * port to dos ole function.
 *
 * 7	 4/23/97 11:00a Roger
 * After merged the source code from Cliff(0417 and 0421) and fixed some
 * bugs.
 *
 * 5	 97/04/03 5:50p Roger
 *
 * 4	 3/26/97 2:05p Roger
 * sync the source codes from Cliff 03/21/97.
 *
 * Revision 1.0  1996/07/31 14:38:16  cliff
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_win16.h */
/* Description: */
/*	Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: Win 16 MSVC 1.5 */
typedef char          VCHAR;/* 1 byte */
typedef long			VLONG;	/* 4 byte */
typedef unsigned long	VULONG; /* 4 byte */
typedef __int64	    	 VINT64;  /* 8 byte */
typedef unsigned __int64 VUINT64; /* 8 byte */

#pragma pack(1)

#define VSCTYPE	long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR		"c:\\virus"
#define DEFAULT_PATTERN_DIR 	"c:\\dos"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR		"c:\\tmp"
#define EXPORT					_export
#define TMVSAPI
#define MAX_PATH_LEN			256
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM	10240
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 65535
#define PACK_STRUCTURE
#define PASCAL					_pascal
#define HANDLE					HFILE
#define VSLongLongToString(cp,x)	(*(VUINT64 *)(cp) = x)
#define VSLongToString(cp,x)	(*(VULONG *)(cp) = x)
#define VSShortToString(cp,x)	(*(VUSHORT *)(cp) = x)
#define VSStringToLongLong(cp)	(*(VUINT64 *)(cp))
#define VSStringToLong(cp)		(*(VULONG *)(cp))
#define VSStringToShort(cp) 	(*(VUSHORT *)(cp))
