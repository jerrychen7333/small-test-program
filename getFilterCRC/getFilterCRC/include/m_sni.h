/* $Date: 2001/2/19 PM 05:04:02$ */
/*
 * $Log: 
 *  8    NewScanEngine1.7         2001/2/19 PM 05:04:02Wayne_chu       Synchronize
 *       cheetah and cheeath_rel
 *  7    NewScanEngine1.6         2001/1/19 AM 11:57:53Wayne_chu       add
 *       VSGetBackupFileInfo and fix the problem of convert between Unicode and
 *       Ansicode.
 *  6    NewScanEngine1.5         1999/12/13 PM 06:27:53Joanna_tsai     for RAR,
 *       first using 0x100000, if fail to malloc such big space
 *       then using 0x10000
 *       move the maxwinsize definition to rar files
 *  5    NewScanEngine1.4         1999/11/2 PM 02:39:38Joanna_tsai    
 *       RAR_MAXWINSIZE should be 0x100000
 *       some platform like win16 cannot support it, so declare it as 0x10000
 *  4    NewScanEngine1.3         1999/5/24 PM 01:28:00Viking_ho       VCHAR should
 *       be defined separately by m_xxx.h
 *  3    NewScanEngine1.2         1998/11/9 PM 02:00:44Lily_ho         
 *  2    NewScanEngine1.1         1998/4/24 AM 11:38:12Lily_ho         
 *  1    NewScanEngine1.0         1998/4/23 AM 09:40:14Lily_ho         
 * $
 * 
 * 5     11/02/99 2:39p Joanna
 * RAR_MAXWINSIZE should be 0x100000
 * some platform like win16 cannot support it, so declare it as 0x10000
 * 
 * 4     99/05/24 1:27p Viking
 * VCHAR should be defined separately by m_xxx.h
 * 
 * 3     11/09/98 2:00p Lily
 * 
 * 8     11/06/98 4:08p Cliff_liang
 * define VSCTYPE(=long) 
 * 
 * 4     4/22/98 3:28p Cliff
 * Add definition of DECOMP_MIN_MEM and DECOMP_MAX_MEM
 * 
 * 3     3/23/98 2:24p Cliff
 * VULONG and VLONG should be defined separately by m_xxx.h
 * 
 * 2     3/20/98 2:58p Cliff
 * Add proejct name and Trend Copy right declaration.
 * 
 * 1     3/10/98 10:43a Cliff
 * SNI port
 * 
 * 14    3/05/98 12:56p Cliff
 *
 * Revision 1.0 1998/02/27 11:38:16 cliff
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_sni.h */
/* Description: */
/*	Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: SNI running SINIX 5.43 */
typedef char          VCHAR;/* 1 byte */
typedef long			VLONG;	/* 4 byte */
typedef unsigned long	VULONG; /* 4 byte */
typedef __int64	    	 VINT64;  /* 8 byte */
typedef unsigned __int64 VUINT64; /* 8 byte */

#define VSCTYPE	long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR	"/virus"
#define DEFAULT_PATTERN_DIR "/etc/iscan"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR	"/tmp"
#define EXPORT
#define TMVSAPI
#define HANDLE			int
#define MAX_PATH_LEN		1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PASCAL

VUSHORT VSStringToShort(char *);
VULONG VSStringToLong(char *);
VUINT64 VSStringToLongLong(char *);
void VSShortToString(char *,VUSHORT);
void VSLongToString(char *,VULONG);
void VSLongLongToString(char *,VUINT64);
