/* $Date: 2001/2/19 PM 05:03:28$ */
/*
 * $Log: 
 *  5    NewScanEngine1.4         2001/2/19 PM 05:03:28Wayne_chu       Synchronize
 *       cheetah and cheeath_rel
 *  4    NewScanEngine1.3         2001/1/19 AM 11:57:31Wayne_chu       add
 *       VSGetBackupFileInfo and fix the problem of convert between Unicode and
 *       Ansicode.
 *  3    NewScanEngine1.2         2001/1/12 PM 03:38:55Joanna_tsai     
 *  2    NewScanEngine1.1         2001/1/12 PM 03:36:51Joanna_tsai     swapbyte
 *  1    NewScanEngine1.0         2000/10/30 PM 05:25:56Joanna_tsai     
 * $
 * 
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_dgux.h */
/* Description: */
/*	Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: DG UNIX */
typedef char          VCHAR;/* 1 byte */
typedef long			VLONG;	/* 4 byte */
typedef unsigned long	VULONG; /* 4 byte */
typedef long long int  	            VINT64;  /* 8 byte */
typedef unsigned long long int		VUINT64; /* 8 byte */

#define VSCTYPE	long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR	"/virus"
#define DEFAULT_PATTERN_DIR "/etc/iscan"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR	"/tmp"
#define EXPORT
#define TMVSAPI
#define HANDLE			int
#define MAX_PATH_LEN		1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PASCAL

VUSHORT VSStringToShortFunc(char *);
VULONG VSStringToLongFunc(char *);
VUINT64 VSStringToLongLong(char *);
void VSShortToStringFunc(char *,VUSHORT);
void VSLongToStringFunc(char *,VULONG);
void VSLongLongToString(char *,VUINT64);

#define VSStringToShort(pBuf)       VSStringToShortFunc((char*)pBuf)
#define VSStringToLong(pBuf)        VSStringToLongFunc((char*)pBuf)
#define VSShortToString(pBuf, val)  VSShortToStringFunc((char*)pBuf, val)
#define VSLongToString(pBuf, val)   VSLongToStringFunc((char*)pBuf, val)
