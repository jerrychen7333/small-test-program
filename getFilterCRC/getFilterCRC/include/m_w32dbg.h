/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2006 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_win32.h */
/* Description: */
/*    Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: Win32 MSVC 4.x */
typedef char          VCHAR;/* 1 byte */
typedef long            VLONG;    /* 4 byte */
typedef unsigned long    VULONG; /* 4 byte */
typedef __int64             VINT64;  /* 8 byte */
typedef unsigned __int64 VUINT64; /* 8 byte */
typedef double        VDOUBLE; /* 8 byte */

#pragma pack(1)

typedef void* VSCTYPE;
typedef void* VSPTN_HANDLE;
#define DEFAULT_MOVE_DIR    "c:\\virus"
#define DEFAULT_PATTERN_DIR     "c:\\dos"
#define DEFAULT_PATTERN_FILE    "lpt$vpn."
#define DEFAULT_TEMP_DIR    "c:\\tmp"
#define EXPORT
#define TMVSAPI
#define MAX_PATH_LEN        1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PACK_STRUCTURE

VUSHORT _DBG_VSStringToShort(char *cp           , char*,int);
VULONG  _DBG_VSStringToLong (char *cp           , char*,int);
VUINT64 _DBG_VSStringToLongLong(char *cp        , char*,int);
void    _DBG_VSShortToString(char *cp, VUSHORT v, char*,int);
void    _DBG_VSLongToString (char *cp, VULONG  v, char*,int);
void    _DBG_VSLongLongToString (char *cp, VUINT64  v, char*,int);

/**/
#define VSLongLongToString(cp,x)	(*(VUINT64 *)(cp) = x)
#define VSLongToString(cp,x)	(*(VULONG *)(cp) = x)
#define VSShortToString(cp,x)	(*(VUSHORT *)(cp) = x)
#define VSStringToLongLong(cp)	(*(VUINT64 *)(cp))
#define VSStringToLong(cp)	    (*(VULONG *)(cp))
#define VSStringToShort(cp) 	(*(VUSHORT *)(cp))
/*/
#define VSStringToShort(p)      _DBG_VSStringToShort((char*)(p)  ,__FILE__,__LINE__)
#define VSStringToLong(p)       _DBG_VSStringToLong((char*)(p)  ,__FILE__,__LINE__)
#define VSStringToLongLong(p)    _DBG_VSStringToLongLong((char*)(p)  ,__FILE__,__LINE__)
#define VSShortToString(p,v)    _DBG_VSShortToString((char*)(p),(VUSHORT)(v),__FILE__,__LINE__)
#define VSLongToString(p,v)     _DBG_VSLongToString((char*)(p),(v),__FILE__,__LINE__)
#define VSLongLongToString(p,v)    _DBG_VSLongLongToString((char*)(p),(v),__FILE__,__LINE__)
/**/

/* for Backup file header */
typedef struct
{
    WCHAR FileName[MAX_PATH_LEN];
    char PlatformName[32];
    long FileAttrib;
    VULONG IsUnicode;
    VULONG BackupTime;
} VSBackupFileInfoW;

/* for Unicode version VSGetLastPatternW */
typedef struct
{
    WCHAR PatternFileName[16];
    int  PatternVersion;
    int  CheckFlag;
} VSPtnCheckInfoW;

/* dbg info for Pattern benchmark */

typedef struct _VSAPI_bench{
		
	VULONG FileScanTimeLimit;
	VULONG pattern_num;
	VULONG cf_bench_flag;

	VULONG pfilelist_node_num_limit;
	VULONG pfilelist_node_num;	

	VUCHAR *pattern_name_list;

	short cf_Bench_ConfChange;

    HANDLE hDataFile;            /* MM file handle */
    HANDLE hMMView;              /* MM object handle */
    LPVOID pBuf;                 /* buffer used for index and scan time array */
    VULONG startIndex;           /* start index in index array */
    VULONG endIndex;             /* end index in index array */
} VSAPI_Bench;

#define __VSChkBenchFlag(x,y) ((x)&&((x)->vs_bench.cf_bench_flag & (y)) ? 1:0)
#define VS_BENCH_PATTERN_EXEC_TIME            0x00000001    /* log pattern execute time*/
/* NOTE: Bench configuration flag index, MAKE SURE NOT TO OVERLAP WITH CONFIGURATION ABOVE*/


/*==========================================================================*/
/* VSVirusScanFileW(): scan a file for known virus after the file name pass */
/*    the file name filter */
/* Parameters: */
/*    VSCTYPE vsc: IN : virus scan context */
/*    WCHAR *file: IN : file for scan */
/*    void *para: IN : user defined parameter */
/* Return : */
/*    0: file is clean, or the file failed the file name filter and has */
/*        not been scanned */
/*    >0: file is infected, return virus number */
/*    -1: failed to scan */
/*    PARA_ERR: invalid Parameter */
int PASCAL EXPORT VSVirusScanFileW(VSCTYPE vsc,WCHAR *file,void *para);

/*==========================================================================*/
/* VSFileNeedProcessW() : check if file need process */
/* Parameters: */
/*    VSCTYPE vsc: IN : virus scan context */
/*    WCHAR *file: IN : file path */
/* Return: */
/*    0: no */
/*    1: yes */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSFileNeedProcessW(VSCTYPE vsc,WCHAR *file);

/*==========================================================================*/
/* VSOpenFileW(): like open(), portable version for all platforms/compilers */
/* Parameters: */
/*    WCHAR *FilePath: IN : file path to open */
/*    int mode: IN : read/write mode */
/* Return: */
/*    0: ok */
/*    OPEN_R_ERR: open error */
/*    PARA_ERR: invalid parameter */
HANDLE PASCAL EXPORT VSOpenFileW(WCHAR *FilePath,int mode);

/*==========================================================================*/
/* VSCopyFileW(): copy a file */
/* Parameters: */
/*    WCHAR *SrcFile: IN : path of source file */
/*    WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*    0: ok */
/*    -1: source file lseek error */
/*    -2: dest file lseek error */
/*    -3: failed to get source file size */
/*    -4: Invalid copy offset */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    OPEN_R_ERR: file open error */
/*    OPEN_W_ERR: dest file create error */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSCopyFileW(WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSCleanVirusW() : Clean Unicode virus file */
/* Parameters: */
/*    VSCTYPE vsc: IN : virus scan context */
/*    WCHAR *name: IN : file path to be cleaned */
/*    char *bakname: OUT : full path of backuped file */
/*    int len: IN : length of buffer for backuped file */
/*    int* bakrc: OUT : 0 -- backuped ok, other -- backup failed */
/* Return: */
/*    0: clean successed */
/*    -1: unable to clean */
/*    -20: no pattern */
/*    OPEN_W_ERR: Clean file create failed */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSCleanVirusW(VSCTYPE vsc,WCHAR *name,VCHAR *bakname,int len,int *bakrc);

/*==========================================================================*/
/* VSActOnFileW() : Take action on UniCode file */
/* Parameters: */
/*    VSCTYPE vsc: IN : virus scan context */
/*    short action: IN : override action setting */
/*    WCHAR *file: IN : file path */
/*    WCHAR *NewPath: OUT: buffer to store new path after renamed or moved */
/* Return: */
/*    >0: action return when asked */
/*    0: action successed */
/*    -1: action failed */
/*    -2: failed to create uniq name for new file */
/*    -3: failed to delete original file */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSActOnFileW(VSCTYPE vsc,short action,WCHAR *filepath,WCHAR *NewPath);

/*==========================================================================*/
/* VSBackupResourceW() : Backup from resource */
/* Parameters: */
/*    VSCTYPE vsc: IN: virus scan context */
/*    VSHANDLE *pSrcResource: IN : resource to be backup */
/*    VSHANDLE *pDestResource: IN : resource for backup */
/*    VSBackupFileInfoW: IN: backup information for backup header */
/* Return: */
/*    0: backup successes */
/*    OPEN_W_ERR: Clean file create failed */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    BUFFER_TOO_SHORT_ERR: insufficient buffer */
/*    PARA_ERR: invalid parameter */
/*    BREAK_ERR: VSAPI is forced to stop */
int PASCAL EXPORT VSBackupResourceW(VSCTYPE vsc, struct _VSHANDLE *pSrcResource, struct _VSHANDLE *pDestResource, VSBackupFileInfoW *pBackupInfo);

/*==========================================================================*/
/* VSBackupFileW(): backup a file */
/* Parameters: */
/*  VSCTYPE vsc: IN : virus scan context */
/*    WCHAR *SrcFile: IN : path of source file */
/*    WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*    0: ok */
/*    -1: source file lseek error */
/*    -2: dest file lseek error */
/*    -3: failed to get source file size */
/*    -4: Invalid copy offset */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    OPEN_R_ERR: file open error */
/*    OPEN_W_ERR: dest file create error */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSBackupFileW(VSCTYPE vsc,WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSRestoreFileW(): restore a backuped file */
/* Parameters: */
/*    WCHAR *SrcFile: IN : path of source file */
/*    WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*    0: ok */
/*    -1: source file lseek error */
/*    -2: dest file lseek error */
/*    -3: failed to get source file size */
/*    -4: Invalid copy offset */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    OPEN_R_ERR: file open error */
/*    OPEN_W_ERR: dest file create error */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSRestoreFileW(WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSEncBackupFileW(): backup a file with decoding */
/* Parameters: */
/*    WCHAR *SrcFile: IN : path of source file */
/*    WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*    0: ok */
/*    -1: source file lseek error */
/*    -2: dest file lseek error */
/*    -3: failed to get source file size */
/*    -4: Invalid copy offset */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    OPEN_R_ERR: file open error */
/*    OPEN_W_ERR: dest file create error */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSEncBackupFileW(WCHAR *SrcFile, WCHAR *DestFile);

/*==========================================================================*/
/* VSGetBackupFileInfoW(): restore a backuped file */
/* Parameters: */
/*    WCHAR *FileName: IN : backup file name */
/*  VSBackupFileInfo *BackupInfo : OUT : backup file information */
/* Return: */
/*    0: ok */
/*    -1: source file lseek error */
/*    -2: dest file lseek error */
/*    -3: failed to get source file size */
/*    -4: Invalid copy offset */
/*  -5: Not a backup file */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    OPEN_R_ERR: file open error */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSGetBackupFileInfoW(WCHAR *BackupFileName, VSBackupFileInfoW *BackupInfo);

/*==========================================================================*/
/* VSGetPatternPathW() : get current pattern directory path */
/* Parameters: */
/*    VSCTYPE vsc: IN : virus scan context */
/*    WCHAR *patternpath: OUT : store pattern directory path */
/*    int cbLength: IN : maximum length for log path */
/* Return: */
/*    0: ok */
/*    BUFFER_TOO_VSHORT_ERR: buffer legnth too short for log path */
/*    PARA_ERR: invalid parameter */
int PASCAL EXPORT VSGetPatternPathW(VSCTYPE vsc,WCHAR *patternpath,int cbLength);

/*==========================================================================*/
/* VSGetLastPatternW(): finds the last version of correct pattern (Unicode) */
/* Parameters:                                                                */
/*    WCHAR *Path [in]: the path for search patterns                            */
/*  WCHAR *FileNameMask [in] : pattern file name mask                        */
/*    VSPtnCheckInfoW *PatternInfo [out] : buffer to receive the last version    */
/*                                       of pattern information                */
/*  int CheckPattern : [in] 1: check pattern file. 0: do not check pattern  */
/* Return:                                                                    */
/*    0 : get the last version of pattern successfully                        */
/*    PATTERN_NOT_FOUND_ERR  -2 : there is no pattern in the path                */
/*    PARA_ERR              -99 : the parameter is error                        */
int PASCAL EXPORT VSGetLastPatternW(WCHAR *Path, WCHAR *FileNameMask, VSPtnCheckInfoW *PatternInfo, int CheckPattern);

/*==========================================================================*/
/* VSReadPatternInFileW(): read in virus pattern from file (Unicode) */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *Path: IN : Pattern file name */
/*	long Offset: IN : offset of pattern file starts */
/* Return: */
/*	0:  ok */
/*	-2: unknown pattern file verion */
/*	-3: pattern file corrupted */
/*	-4: internal error:not read new pattern structure */
/*	OPEN_R_ERR:     fail to open pattern file */
/*	READ_ERR:       fail to read pattern file */
/*	PTN_UPDATE_ERR: fail to update pattern file */
/*	NO_MEM_ERR:     out of memory */
/*	PARA_ERR:       invalid parameter(s) */
/*	BAD_VSC_ERR:    invalid VSC handle */
/*  BUFFER_TOO_SHORT_ERR : buffer is insufficient to hold the path */
int PASCAL EXPORT VSReadPatternInFileW(VSCTYPE vsc,WCHAR *Path,long Offset);

/*==========================================================================*/
/* VSCheckPatternFileW():    validate pattern (Unicode) */
/* Parameters: */
/*    WCHAR *FileName: IN : file name of pattern */
/* Return: */
/*    >0 : pattern version */
/*    PATTERN_CRC_ERR : something wrong with pattern */
/*    PATTERN_VERSION_ERR : pattern number incorrect */
/*    OLD_PATTERN_ERR : pattern too old error */
/*    OPEN_R_ERR : pattern file openning error */
/*    READ_ERR : pattern file reading error */
/*    PARA_ERR : pattern file name is null */
int PASCAL EXPORT VSCheckPatternFileW(WCHAR *FileName);

/*==========================================================================*/
/**    GetPatternInternalVersion
 *    @ingroup Pattern
 *    @param[in]     FileName        pattern full path
 *    @param[out]    InternalVer     Internal pattern file version number
 *    @param[out]    PtnVer          Pattern file version number (pattern extension name)
 *    @retval 0                      success
 *    @retval OPEN_R_ERR             Cannot open pattern file
 *    @retval PARA_ERR               parameter error
 *    @retval NO_MEM_ERR             memory allocate error
 */
TMVSAPI int PASCAL EXPORT VSGetPatternInternalVersionW(WCHAR *FileName, unsigned long *InternalVer, unsigned short *PtnVer);

TMVSAPI int PASCAL EXPORT VSBenchFileScanTimeLimit(VSCTYPE vsc, VULONG TimeLimit);
TMVSAPI int PASCAL EXPORT VSBenchFileScanNumLimit(VSCTYPE vsc, VULONG FileNumLimit);
TMVSAPI int PASCAL EXPORT VSBenchPatternExecTimeInfo(VSCTYPE vsc, int NewSetting, VUCHAR* pattern_name_list ,VULONG pattern_num);
