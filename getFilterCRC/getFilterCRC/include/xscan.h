#include "tmvsdef.h"
#include "trxhandler_api.h"

#ifndef __XSCAN_H__
#define __XSCAN_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Trigger ID is used by product only.
 */
enum {
    VSXSCAN_TRIGGER_ID_STATIC_RESERVE                  = 0,
    VSXSCAN_TRIGGER_ID_STATIC_WEB_DOWNLOADED           = 1,
    VSXSCAN_TRIGGER_ID_STATIC_EMAIL_ATTACHED           = 2,
    VSXSCAN_TRIGGER_ID_STATIC_REMOVABLE_DEVICE         = 3,
    VSXSCAN_TRIGGER_ID_STATIC_AEGIS_SUSPICIOUS_POLICY  = 4,
    VSXSCAN_TRIGGER_ID_STATIC_MANUAL_SCAN              = 5,
    VSXSCAN_TRIGGER_ID_STATIC_THREAT_INFO              = 6,
    VSXSCAN_TRIGGER_ID_STATIC_SCRIPT_EXECUTION         = 7,
    VSXSCAN_TRIGGER_ID_STATIC_DROPPER                  = 8,
    VSXSCAN_TRIGGER_ID_STATIC_SMB                      = 9,
    VSXSCAN_TRIGGER_ID_STATIC_USANDBOX                 = 10,
    VSXSCAN_TRIGGER_ID_STATIC_SPECIFIC_FOLDER          = 11,
    VSXSCAN_TRIGGER_ID_STATIC_SPECIFIC_PROGRAM         = 12,
    VSXSCAN_TRIGGER_ID_STATIC_FILE_CHANGE_NOTIFICATION = 13,

    VSXSCAN_TRIGGER_ID_STATIC_MAX_ID /* Do not insert new ID after this. */
};

enum {
    VSXSCAN_DETECTION_STATUS_PASS      = 0,    /* log only */
    VSXSCAN_DETECTION_STATUS_FEEDBACK  = 1,    /* silent feedback */
    VSXSCAN_DETECTION_STATUS_DETECTED  = 2     /* do clean */
};

typedef X_RCTYPE PASCAL XHDL_QUERY_FUNC(xQueryStruct *para, void *keys, const XHDL_BYTE *input, XHDL_DWORD input_size, xResultHandle *result_handle);
typedef X_RCTYPE PASCAL XHDL_DELETERESULT_FUNC(xResultHandle result_handle);

typedef struct VS_X_HANDLE {
    VULONG                      SizeOfVSXHandle;
    VULONG                      padding;    /* Used for alignment on 64 bits environment */
    void*                       para;
    XHDL_QUERY_FUNC*            Query;      /* Act as open handle and query */
    XHDL_DELETERESULT_FUNC*     DeleteResult;
} VS_X_HANDLE;

/* --- VSCFG_XS_FTYPE_SUP_LIST --- */
typedef struct VS_FILETYPE {
    VULONG  vsd_Type;
    VULONG  vsd_SubType;
} VS_FILETYPE;

/*
 * Steps:
 *   1. Allocate enough size for VS_XS_FTYPE_SUP_LIST.
 *   2. Specify the needed number in reqCount.
 *   3. Use VSCFG_DS_FTYPE_SUP_LIST to get support list.
 *   4. Engine check if requested number of data is big enough.
 *   5. If requested number is smaller than total count, Engine specify the number 
 *      in totalCount and return BUFFER_TOO_SHORT_ERR without any supList data available.
 *   6. If requested number is larger than total count, Engine specify the number
 *      in totalCount and supList only contains totalCount valid data.
 */
typedef struct VS_XS_FTYPE_SUP_LIST {
    VULONG reqCount;
    VULONG totalCount;
    VS_FILETYPE *supList;
} VS_XS_FTYPE_SUP_LIST;
/* --- VSCFG_XS_FTYPE_SUP_LIST --- */

typedef struct VS_XS_CENSUS_THRESHOLD {
    VULONG size;         /* structure size */
    VULONG prevalence;   /* Census prevalence value read from pattern */
    VULONG maturity;     /* Census maturity value read from pattern */
} VS_XS_CENSUS_THRESHOLD;


#define VS_SCANINFO_VERSION     1

typedef struct VSXSCANINFO {
    VULONG  version;
    VULONG  triggerID;  /* triggerID for where the file comes from. */
    VULONG  ctx4wLen;   /* length of 4w context. */
    VUCHAR  filesha1[20];
    void    *ctx4w;     /* 4w context of When, Where, What and Who of the file. */
                        /* It should be encoded by google protocol buffer. */
} VSXSCANINFO;

/*==========================================================================*/
/**    XScan resource for virus
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     xScanInfo  information used to do XScan.
 *    @param[in]     res        resource to scan
 *    @param[in]     para        user defined parameter passed to ProcFunc
 *    @retval >=0                  ok
 *    @retval NOT_SUPPORTED_ERR    error, file type is not supported.
 *    @retval PARA_ERR             invalid Parameter
 *    @retval <0                   error
 */
TMVSAPI int PASCAL EXPORT VSXScanResource(VSCTYPE vsc,VSXSCANINFO *xScanInfo, RESOURCE *res,void *para);

/*==========================================================================*/
/**    XScan plug-in interface
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     res        resource to scan
 *    void **ProcInfo : IN/OUT : user defined parameter
 *    @param[in]     para        user defined parameter passed to ProcFunc
 *    >=0: ok
 *    NOT_SUPPORTED_ERR: file type is not supported.
 *    PARA_ERR: invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSXScanner(VSCTYPE vsc, RESOURCE *res, void **ProcInfo, void *para);

typedef struct XSCAN_FBDATA {
    VULONG eng_major_ver; /* Engine major version number */
    VULONG eng_minor_ver; /* Engine minor version number */
    VULONG eng_build_number; /* Engine build number */
    VULONG xscan_version; /* Internal engine version of XScan */
    VSHORT file_type; /* File type */
    VSHORT file_sub_type; /* File sub-type */
    VUCHAR feature_version[20]; /* SHA1 of feature set */
} XSCAN_FBDATA;

typedef struct VSAFI_XSCAN_RSP {
    VSAFIH  header;
    const void *xResultHandle; /* xResultHandle for XQuery */
    XSCAN_FBDATA feedback_data; /* Feedback data which provided by Engine */
} VSAFI_XSCAN_RSP;

typedef struct VSAFI_XSCAN_IMPTBL {
    VSAFIH  header;
    const VUCHAR  *data; /* import table raw data encoded by MULTI_SZ */
} VSAFI_XSCAN_IMPTBL;

typedef struct VSAFI_XSCAN_OPCODE {
    VSAFIH  header;
    VULONG  data_len; /* length of opcode raw data */
    const VUCHAR  *data; /* opcode raw data */
} VSAFI_XSCAN_OPCODE;

typedef struct VSAFI_4WCTX_QUERY
{
    VSAFIH  header;
    VULONG  ulDataType; /* Unknown Type: (ulDataType, ulSubType) = (0xFFFFFFFF, 0xFFFFFFFF) */
    VULONG  ulSubType;
    VCHAR*  pFileName;
    VULONG  ulFileNameType;
    VSHANDLE *pVshandle;
    VSXSCANINFO *xscaninfo;
    VULONG info_size;
} VSAFI_4WCTX_QUERY;

#ifdef __cplusplus
}
#endif

#endif /* __XSCAN_H__ */
