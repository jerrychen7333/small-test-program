/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2005 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* vsres.h */
/* Description: */
/* */
/**************************************************************************/

/** @file vsres.h
 *    @brief Virus Scan API (VSAPI)
 */

#define WIN32_DLL 1


/*************************************/
/*    platform dependence(public)    */
/*************************************/
#ifdef WIN32_DLL
    #include <windows.h>
    /******* basic data type *******/
    typedef unsigned char               UINT8;
    typedef unsigned short              UINT16;
    typedef unsigned int                UINT32;/* ref. basetsd.h */
    typedef unsigned __int64            UINT64;/* ref. basetsd.h */
    typedef   signed char               SINT8;
    typedef   signed short              SINT16;
    typedef   signed long               SINT32;
    typedef   signed __int64            SINT64;
    typedef unsigned short              WCHAR;
    typedef void*                       HANDLE;

    /******* constant *******/
    #define VS_MAX_FILENAME_LENGTH      MAX_PATH
#endif

#ifdef UNIX
    /******* basic data type *******/
    typedef unsigned char               UINT8;
    typedef unsigned short              UINT16;
    typedef unsigned int                UINT32;
    typedef unsigned __int64            UINT64;
    typedef   signed char               SINT8;
    typedef   signed short              SINT16;
    typedef   signed long               SINT32;
    typedef   signed __int64            SINT64;
    typedef unsigned short              WCHAR;
    #define HANDLE                /**/  int         /*/ FILE* /**/
#endif

/*************************************/
/*   platform independence(public)   */
/*************************************/
/******* standard error code *******/
#define NO_ERR                        0
#define PARA_ERR                    -99
#define NO_MEM_ERR                  -98
#define WRITE_ERR                   -97
#define READ_ERR                    -96
#define OPEN_W_ERR                  -95
#define OPEN_R_ERR                  -94
#define ACCESS_ERR                  -87
#define NOT_SUPPORTED_ERR           -81
/******* structure *******/


/*************************************/
/*   platform independence(private)  */
/*************************************/
/******* constant *******/

#define VSCONST_1K_MINUS                    0x000003FF
#define VSCONST_1K                          0x00000400
#define VSCONST_4K_MINUS                    0x00000FFF
#define VSCONST_4K                          0x00001000
#define VSCONST_64K_MINUS                   0x0000FFFF
#define VSCONST_64K                         0x00010000
#define VSCONST_1M_MINUS                    0x000FFFFF
#define VSCONST_1M                          0x00100000
#define VSCONST_2G_MINUS                    0x7FFFFFFF
#define VSCONST_2G                  0x0000000080000000
#define VSCONST_4G_MINUS            0x00000000FFFFFFFF
#define VSCONST_4G                  0x0000000100000000
#define VSCONST_16E_MINUS           0xFFFFFFFFFFFFFFFF
#define VSCONST_INITGETSIZE         VSCONST_16E_MINUS
/******* special macro *******/
#define _offsetof(s,m)              ((char*)&((s*)0)->m - (char*)0)
#define _this(p,s,m)                (s*)((char*)(p) - _offsetof(s,m))

/*************************************/
/*    platform dependence(private)   */
/*************************************/
#ifdef WIN32_DLL
    #include <malloc.h>
    #include <string.h>
    #include <stdio.h>
    #include <io.h>
    /******* C function *******/
    #define MALLOC                      malloc  /* ref. malloc.h */
    #define FREE                        free    /* ref. malloc.h */
    #define MEMCPY                      memcpy  /* ref. string.h */
    #define MEMSET                      memset  /* ref. string.h */
    #define STRLEN                      strlen  /* ref. string.h */
    #define STRCPY                      strcpy  /* ref. string.h */
    #define STRCAT                      strcat  /* ref. string.h */
    #define STRCMP                      strcmp  /* ref. string.h */
    #define WCSLEN                      wcslen  /* ref. string.h */
    #define WCSCPY                      wcscpy  /* ref. string.h */
    #define WCSCAT                      wcscat  /* ref. string.h */
    #define WCSLEN                      wcslen  /* ref. string.h */
    #define WCSCMP                      wcscmp  /* ref. string.h */
    #define SPRINTF                     sprintf /* ref. stdio.h */
    #define WSPRINTF                    wsprintfW /* ref. winuser.h */
    #define MKTEMP                      mktemp  /* ref. io.h     */
    #define UNLINK                      unlink  /* ref. io.h     */
    /******* constant *******/
    #define PATH_SEPARATOR              '\\'
#endif

#ifdef UNIX
    #include <malloc.h>
    #include <string.h>
    #include <stdio.h>
    #include <io.h>
    /******* C function *******/
    #define MALLOC                      malloc  /* ref. malloc.h */
    #define FREE                        free    /* ref. malloc.h */
    #define MEMCPY                      memcpy  /* ref. string.h */
    #define MEMSET                      memset  /* ref. string.h */
    #define STRLEN                      strlen  /* ref. string.h */
    #define STRCPY                      strcpy  /* ref. string.h */
    #define STRCMP                      strcmp  /* ref. string.h */
    #define SPRINTF                     sprintf /* ref. stdio.h */
    #define MKTEMP                      mktemp  /* ref. io.h     */
    #define UNLINK                      unlink  /* ref. io.h     */
    /******* constant *******/
    #define PATH_SEPARATOR              '\\'
#endif






#ifndef __VSRES_H__
#define __VSRES_H__

/*************************************/
/*            Application            */
/*************************************/
typedef struct _VSRES VSRES;/* Resource Size Support 0xFFFFFFFF-FFFFFFFF Bytes */

/*
    return VSAPI standard return code
*/
int VSResClose(VSRES**rRes);

/*
    return 0 : success
           1 : success but offset is over then 4G
           0<: VSAPI standard error code
*/
int VSResSeekSet32(VSRES* Res, UINT32 ofs);
int VSResSeekCur32(VSRES* Res, SINT32 dist, UINT32 *rofs);
int VSResSeekEnd32(VSRES* Res, SINT32 dist, UINT32 *rofs);
int VSResSeekSet64(VSRES* Res, UINT64 ofs);
int VSResSeekCur64(VSRES* Res, SINT64 dist, UINT64 *rofs);
int VSResSeekEnd64(VSRES* Res, SINT64 dist, UINT64 *rofs);


/*
    return 0 : success
           1 : success but data size is over then 4G Bytes
           0<: VSAPI standard error code
*/
int VSResGetSize32(VSRES* Res, UINT32 *rsze);
int VSResGetSize64(VSRES* Res, UINT64 *rsze);
int VSResSetSize32(VSRES* Res, UINT32 sze);
int VSResSetSize64(VSRES* Res, UINT64 sze);

/*
    return 0 : success
           1 : success but not all data be read
           0<: VSAPI standard error code
*/
int VSResRead(VSRES* Res, void* buf, SINT32 len, SINT32 *rlen);

/*
    return 0 : success
           1 : success and file size is increase
           0<: VSAPI standard error code
*/
int VSResWrite(VSRES* Res, void* buf, SINT32 len, SINT32 *rlen);


/*
    return VSAPI standard return code
*/
int VSResOpenDirA(const char*  d_name, const char*  filter, const char* alias, VSRES** rRes);
int VSResOpenDirW(const WCHAR* d_name, const WCHAR* filter, const char* alias, VSRES** rRes);

char*  VSResReadDirA(VSRES* Res);
WCHAR* VSResReadDirW(VSRES* Res);


/*************************************/
/*             Resource              */
/*************************************/
#define VS_READ     0x00
#define VS_WRITE    0x01
#define VS_CREATE   0x02

/*
basic file resource :
    return VSAPI standard return code 
*/
int VSResOpenFileA(const char*  f_name, UINT32 mod, const char* alias, VSRES** rRes);
int VSResOpenFileW(const WCHAR* f_name, UINT32 mod, const char* alias, VSRES** rRes);
int VSResOpenFileH(HANDLE hd, UINT32 mod, const char* alias, VSRES** rRes);

/*
OLE stream resource :
    return VSAPI standard return code 
*/
typedef struct _OLE_HANDLE  OLE_HANDLE;
int VSResOpenOleStmA(OLE_HANDLE* stg, char* name, UINT32 mod, const char* alias, VSRES** rRes);
int VSResOpenOleStmW(OLE_HANDLE* stg, char* name, int nlen, UINT32 mod, const char* alias, VSRES** rRes);
int VSResOpenOleStmF(OLE_HANDLE* stg, UINT32 idx, char* name, int nlen, UINT32 mod, const char* alias, VSRES** rRes);
int VSResOpenOleStmH(OLE_HANDLE* stm, UINT32 mod, const char* alias, VSRES** rRes);

/*
memory resource :
    return VSAPI standard return code
*/
int VSResOpenMem(UINT8* mem, UINT32 sze, UINT32 mod, const char* alias, VSRES** rRes);

/*
temporary resource :
    return VSAPI standard return code
*/
int VSResOpenTemp(const char* path, UINT32 seed, UINT32 msize, const char* alias, VSRES** rRes);



/*************************************/
/*            Filtration             */
/*************************************/
/*
unitary filter :
    return VSAPI standard return code
*/
int VSResUnitaryFilterPushIn(VSRES* Res);
int VSResUnitaryFilterPopOut(VSRES* Res);


/*
xor decoder filter :
    return VSAPI standard return code
*/
int VSResXorDecoderPushIn(VSRES* Res, UINT8 key);
int VSResXorDecoderPopOut(VSRES* Res);

/*
cache filter : return VSAPI standard return code
不適合用在stream resource上
*/
int VSResCachePopOut(VSRES* Res);
int VSResCachePushIn(VSRES* Res, UINT32 csize, UINT32 mod);

/*
buffer filter :
    return VSAPI standard return code
*/
int VSResBufferPopOut(VSRES* Res);
int VSResBufferPushIn(VSRES* Res, const char* path, UINT32 seed, UINT32 msize);

/*
base64 filter : (stream)
    return VSAPI standard return code
*/
int VSResBase64PopOut(VSRES* Res);
int VSResBase64PushIn(VSRES* Res, UINT32 mod, SINT32 buffer_size);

#endif /* __VSRES_H__ */
