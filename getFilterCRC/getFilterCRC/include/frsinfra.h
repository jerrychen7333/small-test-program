/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-201 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 *****************************************************************************
 */

#ifndef __FRSINFRA_H__
#define __FRSINFRA_H__

#define VSCFG_MIP2_HASH_COLLECT_FUNC    0xFF00002B      /* 9.850 (VULONG) Get/Set MIP2 Hash collecting callback function */
#define VSCFG_AFI_RULESIGNATURE_FLAG    0xFF00002D      /* 9.853 (VULONG) Retrieve matched rule signatures */

#ifdef __cplusplus
extern "C" {
#endif

/* [it is a hacking here]
   VS_NORMALIZE_MEM_SUPPORTED_FLAG is a VSAPI private definition
   Products don't have to know it.

   However, FRS Infra need the following structure to retrieve CLSH hash.
   So we explicity define the value in product's header
*/
#define VS_NORMALIZE_MEM_SUPPORTED_FLAG 1

#if VS_NORMALIZE_MEM_SUPPORTED_FLAG
    /* 
       The structure VS_MIP2_HASH_COLLECT_DATA is exported to FRS Infra,
       We use #pragma pack to keep the alignment consistent.

       Solaris compiler doesn't support pack(push, 8) and we use the data structure only when in MIP2
       Hence, we restrict the pack in platforms that support VS_NORMALIZE_MEM_SUPPORTED_FLAG only
    */
    #pragma pack(push, 8)
#endif

typedef struct _VS_MIP2_HASH_COLLECT_DATA {
    /* Structure Information */
    int size;              /* size of the VS_MIP2_HASH_COLLECT_DATA, for compatibility */

    /* File Information */
    char *file;
    char *name;
    void *para;
    
    /* Image Information */
    VUINT64 imageBegin;
    VUINT64 imageEnd;

    /* Hash Information */
    VUINT64 hashOffset;
    VULONG hashCRC;
    VULONG hashMaskedInstCount;
    VULONG hashRawInstCount;
} VS_MIP2_HASH_COLLECT_DATA;

#if VS_NORMALIZE_MEM_SUPPORTED_FLAG
    #pragma pack(pop)
#endif

#ifdef PACK_STRUCTURE
#pragma pack(1)
#endif
/* If you need to add new member to VSAFI_RULEMATCHED_FRS_INFO */
/* Remember add it at the bottom of structure */
/* And sync it with VSAFI_RULEMATCHED_INFO in tmvsdef.h */
typedef struct
{
    const char* short_virname;
    const char* long_virname;
    VULONG  atse_rule_aggressive_level; /* 0 is not ATSE rule */
    VULONG  atse_rule_category; /* ATSE_RC_xxx */
    VULONG  pattern_format;
    VULONG  rule_category;
    VULONG  detection_class;
    void* rule_info; /* structure that contains detail rule information */
} VSAFI_RULEMATCHED_FRS_INFO;
#ifdef PACK_STRUCTURE
#pragma pack()
#endif

#define AFI_RULECAT_DEFAULT     0    /* rmi.reserve3 is NULL */
#define AFI_RULECAT_13BYTE      1
#define AFI_RULECAT_SCRC        2 
#define AFI_RULECAT_PECRC       3 
#define AFI_RULECAT_LP          4

typedef struct
{
    VULONG  Ver;  /* Version of structure */
    VULONG  Mode; /* version 0 *//* Virus_Found=1, Virus_Maybe=2 */
} VSAFI_13BYTE_INFO;

typedef struct
{
    VULONG  Ver;      /* Version of structure */
    VULONG  Mode;     /* version 0 *//* Normalize level (0~3) */
    VULONG  Type;     /* version 0 *//* Prepend=0, Append=1 */
    VULONG  crcNum;   /* version 0 */
    VULONG* crcRule;  /* version 0 */
} VSAFI_SCRCRULE_INFO;

typedef struct
{
    VULONG  Ver;      /* Version of structure */
    VULONG  crcNum;   /* version 0 */
    VULONG* crcRule;  /* version 0 */
} VSAFI_PECRCRULE_INFO;

typedef struct
{
    VULONG  Ver;       /* Version of structure */
    VULONG  LeaderID;  /* version 0 */
    VULONG  crcNum;    /* version 0 */
    VULONG* crcRule;   /* version 0 */
} VSAFI_LPRULE_INFO;

typedef void PASCAL EXPORT
VS_MIP2_HASH_COLLECT_FUNC(const VS_MIP2_HASH_COLLECT_DATA *);

#ifdef __cplusplus
}
#endif

#endif /* __FRSINFRA_H__ */


