/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2005 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* tmvsx.h */
/* Description: VSAPI extended function prototype */
/* */
/**************************************************************************/

/** @file tmvsx.h
 *    @brief Virus Scan API (VSAPI)
 */

#ifndef __TMVSX_H__
#define __TMVSX_H__

#ifdef __cplusplus
extern "C" {
#endif

/*==========================================================================*/
/**    Add data type checking function
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     IdFunc    data type indentify func
 *    @param[in,out] dt        return the type id
 *    @retval 0            type added
 *    @retval -1            no type name defined
 *    @retval -2            user define data type table full
 *    @retval -3            user define data type already exists
 *    @retval NO_MEM_ERR    out of memory
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSAddDataType(VSCTYPE vsc,int (*IdFunc)(VSHANDLE *,VUCHAR *,int),VSDTYPE *dt);

/*==========================================================================*/
/**    Check if file in archive need process
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     FileName    file path
 *    @retval 0            no
 *    @retval 1            yes
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSArchFileNeedProcess(VSCTYPE vsc,char *FileName);

/*==========================================================================*/
/**    Function to back up current VSC data. This function
 *    is call prior update the engine
 *    @ingroup VSAPI
 *    @param[out]    buffer        data buffer allocated to hold VSC data
 *    @retval MEM_ERR        not enough memory
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSBackupVSCData(char **buffer);

/*==========================================================================*/
/**    Return pointer to the last field of a given path
 *    @ingroup VSAPI
 *    @param[in]     Path        path string
 *    @retval (char*)0    invlaid path string
 *    @retval others        pointer to the last field of Path
 */
TMVSAPI char *PASCAL EXPORT VSBaseName(char *Path);

/*==========================================================================*/
/**    Calculate 32 bits CRC of a given block of data
 *    @ingroup General
 *    @param[in]     Data        Block of data to calculate CRC
 *    @param[in,out] CRC        starting seed of CRC
 *    @param[in]     DataLen    Data length
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSCalculateCRC(char *Data,VULONG *CRC,unsigned int DataLen);

/*==========================================================================*/
/**    Calculate 32 bits CRC of a given block of data
 *    @ingroup General
 *    @param[in]     Data        Block of data to calculate CRC
 *    @param[in,out] CRC        starting seed of CRC
 *    @param[in]     DataLen    Data length
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSCalculateCRC16(char *Data,VUSHORT *CRC,unsigned int DataLen);

/*==========================================================================*/
/**    Return character type
 *    @ingroup VSAPI
 *    @param[in]     String    data string
 *    @param[in]     column    check on request column's byte.
 *    @retval 0            ANSI character
 *    @retval 1            first byte(leading byte) of a two-byte word
 *    @retval 2            second byte of a two-byte word
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSCharType(char *String,int column);

/*==========================================================================*/
/** Portable version of close()/CloseFile()
 *    @ingroup VSAPI
 *    @param[in]     Fd        resouse handle
 */
TMVSAPI void PASCAL EXPORT VSCloseFile(HANDLE Fd);

/*==========================================================================*/
/**    Close and release resource
 *    @ingroup Resource
 *    @param[in]     handle    resouse handle
 *    @retval 0            Ok
 *    @retval BAD_HANDLE_ERR    invalid VSHANLDE pointer
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSCloseResource(VSHANDLE *handle);

/*==========================================================================*/
/**    Replace all occurances of a byte in string
 *    @ingroup General
 *    @param[in,out] String    Data string need processes
 *    @param[in]     OldChar    old byte which will be replace
 *    @param[in]     NewChar    new byte which OldChar will be replaced with
 *    @retval >=0            How many bytes been replaced
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSConvertCharacter(char *String,char OldChar,char NewChar);

/*==========================================================================*/
/**    Copy a file
 *    @ingroup VSAPI
 *    @param[in]     SrcFile    path of source file
 *    @param[in]     DestFile    path of destination file
 *    @retval 0            ok
 *    @retval -1            source file lseek error
 *    @retval -2            dest file lseek error
 *    @retval -3            failed to get source file size
 *    @retval -4            Invalid copy offset
 *    @retval READ_ERR    read error
 *    @retval WRITE_ERR    write error
 *    @retval OPEN_R_ERR    file open error
 *    @retval OPEN_W_ERR    dest file create error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSCopyFile(char *SrcFile,char *DestFile);

/*==========================================================================*/
/**    Copy data from one file to another
 *    @ingroup VSAPI
 *    @param[in]     SrcHandle    source resouse handle
 *    @param[in]     SrcOffset    offset where to copy data from src file
 *    @param[in]     DestHandle    source resouse handle
 *    @param[in]     DestOffset    offset where to copy data into Dest file
 *    @param[in]     Length        how many byte to copy
 *    @param[out]    CopyLength    how many byte been copy
 *    @retval 0            ok
 *    @retval -1            source file lseek error
 *    @retval -2            dest file lseek error
 *    @retval -3            failed to get source file size
 *    @retval -4            Invalid copy offset
 *    @retval READ_ERR    read error
 *    @retval WRITE_ERR    write error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSCopyFileFD(VSHANDLE *SrcHandle,long SrcOffset,VSHANDLE *DestHandle,long DestOffset,long Length,long *CopyLength);

/*==========================================================================*/
/**    Calculate one byte CRC
 *    @ingroup General
 *    @param DataByte        data byte
 *    @param Seed            CRC seed
 *    @return    16 bits CRC
 */
TMVSAPI VUSHORT PASCAL EXPORT VSCrc16(char DataByte,VUSHORT Seed);

/*==========================================================================*/
/**    Calculate one byte CRC
 *    @ingroup General
 *    @param DataByte        data byte
 *    @param Seed            CRC seed
 *    @return    32 bits CRC
 */
TMVSAPI long PASCAL EXPORT VSCrc32(char DataByte,VULONG Seed);

/*==========================================================================*/
/**    Like mkdir() but will create any non-existance parent directories too
 *    @ingroup VSAPI
 *    @param[in]     DirPath    path of directory
 *    @retval >=0            how many directory created
 *    @retval -1            path already exist and is not a directory
 *    @retval -2            mkdir failed, permision?
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSCreateDirectoryTree(char *DirPath);

/*==========================================================================*/
/**    Check file type of a data block
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Data        pointer to block of data
 *    @param[in]     BufLen    how many data are in block
 *    @param[out]    dt        return the type id
 *    @retval >=0            file type
 *    @retval -2            unknow file type
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDataType(VSCTYPE vsc,VUCHAR *Data,int BufLen,VSDTYPE *dt);

/*==========================================================================*/
/**    Check data type on resourc handle
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     handle    resource pointer
 *    @retval >=0            file type
 *    @retval -2            unknow file type
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDataTypeFD(VSCTYPE vsc,VSHANDLE *handle);

/*==========================================================================*/
/**    Set data type on resourc handle
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     handle     resource pointer
 *    @param[in]     major      resource major data type
 *    @param[in]     minor      resource minor data type
 *    @retval NOT_SUPPORTED_ERR fail to find data type
 *    @retval PARA_ERR          invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDataTypeFD(VSCTYPE vsc, VSHANDLE *handle, VULONG major, VULONG minor);

/*==========================================================================*/
/**    Open a VSIO handle from VSHANDLE
 *    @ingroup VSAPI
 *    @param[in]     vsh    VSHANDLE
 *    @param[in/out] **io   VSIO
 *    @retval 0             success
 *    @retval < 0           failed to open
 *    @retval NULL          failed to open
 */

TMVSAPI int PASCAL EXPORT VSOpenIOFromHandle(VSHANDLE *vsh, struct _VSIO** io);

/*==========================================================================*/
/**    Close VSIO handle which is opened from VSHANDLE
 *    @ingroup VSAPI
 *    @param[in/out] **io   VSIO
 *    @retval 0             success
 *    @retval < 0           failed to close
 */

TMVSAPI int PASCAL EXPORT VSCloseIOFromHandle(struct _VSIO** io);

/*==========================================================================*/
/**    Get normalize memory scanning flag
 *    @ingroup VSAPI
 *    @param[in]     vsc    virus scan context
 *    @retval >=0                current setting
 *    @retval PARA_ERR        invalid parameter
 */

TMVSAPI int PASCAL EXPORT VSGetNormalizeMemScanFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set normalize memory scanning flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetNormalizeMemScanFlag(VSCTYPE vsc, int NewSetting);

/*==========================================================================*/
/**    Get process memory scanning flag
 *    @ingroup VSAPI
 *    @param[in]     vsc    virus scan context
 *    @retval >=0                current setting
 *    @retval PARA_ERR        invalid parameter
 */

TMVSAPI int PASCAL EXPORT VSGetProcessMemScanFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set process memory scanning flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetProcessMemScanFlag(VSCTYPE vsc, int NewSetting);

/*==========================================================================*/
/**    Check if data type is compressed
 *    @ingroup DataType
 *    @param[in]     DType    data type structure pointer
 *    @param[out]    Function    return Decompress function pointer
 *    @retval 0            not compressed
 *    @retval 1            yes, is compressed, decompress function in *Function
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDCIsCompressed(VSDTYPE *DType,void **Function);

/*==========================================================================*/
/**    Decompress resource
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     SrcRes    Compressed Resource
 *    @param[in]     DestPath    where to put decompressed file(s)
 *    @param[in]     ProcFunc    What to do on each file been decompressed
 *    @param[in]     para        user defined parameter passed to ProcFunc
 *    @retval 0            ok
 *    @retval -2            Not compress type or unknow type
 *    @retval -3            No configuration set
 *    @retval -4            program want to break
 *    @retval OPEN_R_ERR    source open error
 *    @retval OPEN_W_ERR    destination open error
 *    @retval READ_ERR    failed to decompress/decode(read error)
 *    @retval WRITE_ERR    failed to decompress/decode(write error)
 *    @retval MAXDECOM_ERR    exceed compression layer
 *    @retval NO_MEM_ERR    out of memory
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDecompress(VSCTYPE vsc,RESOURCE *SrcRes,char *DestPath,VS_PROC_FUNC *ProcFunc,void *para);

/*==========================================================================*/
/**    Decompress file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     SrcFile    Compressed file path
 *    @param[in]     SrcFileName    Compressed file name
 *    @param[in]     SrcType    resource type of SrcFile
 *    @param[in]     DestPath    where to put decompressed file(s)
 *    @param[in]     ProcFunc    What to do on each file been decompressed
 *    @param[in]     para        user defined parameter passed to ProcFunc
 *    @retval 0            ok
 *    @retval -1            failed to decompress(corrupted or invalid file type)
 *    @retval -2            non compressed/encoded file
 *    @retval -3            cfg not initiate
 *    @retval -4            exceed compression layer
 *    @retval DISABLE_ERR    enable flag is off
 *    @retval OPEN_R_ERR    failed to decompress/decode(src file open error)
 *    @retval OPEN_W_ERR    failed to decompress/decode(dest file create error)
 *    @retval READ_ERR    failed to decompress/decode(read error)
 *    @retval WRITE_ERR    failed to decompress/decode(write error)
 *    @retval NO_MEM_ERR    no memory
 *    @retval MAXDECOM_ERR    exceed compression layer
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDecompressFile(VSCTYPE vsc,char *SrcFile,char *SrcFileName,int SrcType,char *DestPath,VS_PROC_FUNC *ProcFunc,void *para);

/*==========================================================================*/
/**    Remove data type checking function
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     dt        type to remove
 *    @retval 0            data type removed
 *    @retval    -1            type is not user deinfed
 *    @retval -2            type is not defined
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDelDataType(VSCTYPE vsc,VSDTYPE *dt);

/*==========================================================================*/
/**    Disable virus pattern
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     VirusNumber    virus number
 *    @param[in]     VirusName    virus name
 *    @retval 0            ok
 *    @retval 1            already disable
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDisablePattern(VSCTYPE vsc,int VirusNumber,char *VirusName);

/*==========================================================================*/
/**    Check if file need process
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     file        file path
 *    @retval 0            no
 *    @retval 1            yes
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFileNeedProcess(VSCTYPE vsc,char *file);

/*==========================================================================*/
/**    Determind what type the file is
 *    @ingroup VSAPI
 *    @param[in]     File        file to check
 *    @param[out]    FileType    contain file type information
 *    @param[out]    inode    contains i-node number for file if not zero
 *    @retval 0            ok
 *    @retval -1            failed to get file type
 *    @retval -2            unknown file type
 *    @retval PARA_ERR    invalid parameters
 */
TMVSAPI int PASCAL EXPORT VSFileType(char *File,VULONG *FileType,long *inode);

/*==========================================================================*/
/**    Find the first match file in a directory
 *    @ingroup VSAPI
 *    @param[in]     path        directory path to search
 *    @param[in]     dir        pointer to the directory handle
 *    @retval 0            ok, and first file info is placed in *dir
 *    @retval -1            path does not exist
 *    @retval -2            path is not a directory
 *    @retval -3            dirpath too long
 *    @retval -4            failed to open
 *    @retval -5            search mask too long
 *    @retval -6            failed to read next block data of directory
 *    @retval -7            no matched file left
 *    @retval -8            matched file found, but failed to get file info
 *    @retval NO_MEM_ERR    out of memory
 *    @retval NO_MEM_ERR    out of memory
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFindFirst(char *path,VSDIR **dir);

/*==========================================================================*/
/**    Find the next matched file in a directory
 *    @ingroup VSAPI
 *    @param[in]     dir        pointer to the directory handle
 *    @retval 0            ok
 *    @retval -1            failed to read next block data of directory
 *    @retval -2            no matched file left
 *    @retval -3            matched file found, but failed to get file info
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFindNext(VSDIR *dir);

/*==========================================================================*/
/**    Output date in desired format
 *    @ingroup General
 *    @param[in]     date        date
 *    @param[out]    FormatBuffer    output buffer
 *    @param[in]     Flag        format control flag
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFormatDate(int *date,char *FormatBuffer,int Flag);

/*==========================================================================*/
/**    Output time(in seconds) in desired format
 *    @ingroup General
 *    @param[in]     Second    time
 *    @param[out]    FormatBuffer    output buffer
 *    @param[in]     Flag        format control flag
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFormatDateTime(VULONG Second,char *FormatBuffer,int Flag);

/*==========================================================================*/
/**    Free pattern list
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFreePatternList(VSCTYPE vsc);

/*==========================================================================*/
/**    Free pattern node
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ptn_handle     handle of pattern to free
 *    @retval 0            ok
 *    @retval -1            invalid pattern handle
 *    @retval -2            pattern node not found
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFreePatternNode(VSCTYPE vsc,VSPTN_HANDLE ptn_handle);

/*==========================================================================*/
/**    Return current character environment type
 *    @ingroup VSAPI
 *    @param[out]    OutValue    buffer to store environment type
 *    @retval 0            ok, and type return
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetCharacterEnvType(int *OutValue);

/*==========================================================================*/
/**    Check if conf had been changed
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval 0            no
 *    @retval 1            yes
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetConfChangeFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return data type's information
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in,out] dt        Data type
 *    @retval 0            ok and data stored
 *    @retval -2            unknow type id
 *    @retval -3            unknow sub type id
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDataTypeInfo(VSCTYPE vsc,VSDTYPE *dt);

/*==========================================================================*/
/**    Get the global debug message file handle
 *    @ingroup General
 *    @param[out]    OutFile     file handle for debug message
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDebug(FILE **OutFile);

/*==========================================================================*/
/**    Return count-file-only flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetCountFileFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return setting of should we keep the file created by decompress module
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetKeepDecompressFileFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Get the log file path
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    LogPath    where is the log directory
 *    @param[in]     cbLength    maximum length for log path
 *    @retval 0            ok
 *    @retval BUFFER_TOO_VSHORT_ERR    buffer legnth too short for log path
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetLogFilePath(VSCTYPE vsc,char *LogPath,int cbLength);

/*==========================================================================*/
/**    Return log enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetLogFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Search and return next pattern node in vsc
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in,out] ptn_handle    current node. next node found will be put into
 *    @retval 0            ok, *ptn_handle is set
 *    @retval -1            current node not found
 *    @retval -2            no more ptn handle exist
 *    @retval PARA_ERR    invalid parameters
 */
TMVSAPI int PASCAL EXPORT VSGetPatternHandle(VSCTYPE vsc,VSPTN_HANDLE *ptn_handle);

/*==========================================================================*/
/**    Return smart decompress  enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetSmartDecompressFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Get user name
 *    @ingroup General
 *    @param[in]     uid        user id number
 *    @param[out]    name        buffer to store user name
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetUserName(int uid,char *name);

/*==========================================================================*/
/**    Get the global debug verbose level value
 *    @ingroup VSAPI
 *    @param[out]    OutValue buffer to store global verbose level
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetVerboseLevel(int *OutValue);

/*==========================================================================*/
/**    Get current volume name
 *    @ingroup General
 *    @param[out]    volume    buffer to store volume name
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetVolume(char *volume);

/*==========================================================================*/
/**    Get logger id and caller id of a VSC
 *    @ingroup VSAPI
 *    @param[in,out] vscinfo     VSC information data structure.
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 *    @note vscinfo->vi_vsc is the target VSC and must be filled before calling
 */
TMVSAPI int PASCAL EXPORT VSGetVSCInfo(VSCINFO *vscinfo);

/*==========================================================================*/
/**    Get VSAPI Version
 *    @ingroup VSAPI
 *    @param[in,out] version     VSAIP version data structure.
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetVersion(VSVERSION *version);

/*==========================================================================*/
/**    Get the debug message file handle
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    OutFile     file handle for debug message
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetVSDebug(VSCTYPE vsc,FILE **OutFile);

/*==========================================================================*/
/**    Get the debug verbose level value
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    OutValue verbose level value to return
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetVSVerboseLevel(VSCTYPE vsc,int *OutValue);

/*==========================================================================*/
/**    Determind if path is a directory
 *    @ingroup General
 *    @param Path            path to check
 *    @retval 1            path is directory
 *    @retval 0            path is not directory(other type, not exist, or permision denyed)
 *    @retval PARA_ERR    invalid parameter, Path == 0 || *Path == 0
 */
TMVSAPI int PASCAL EXPORT VSIsDir(char *Path);

/*==========================================================================*/
/**    Check if path is a full path name
 *    @ingroup General
 *    @param[in]     path        file path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSIsFullPathName(char *path);

/*==========================================================================*/
/**    Check if character is a leading byte of a double-byte word
 *    @ingroup General
 *    @param[in]     c        character to check
 *    @retval 0            ANSI character
 *    @retval 1            first byte(leading byte) of a two-byte word
 */
TMVSAPI int PASCAL EXPORT VSIsTwoByteWord(VUCHAR c);

/*==========================================================================*/
/**    Log an event
 *    @ingroup General
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Logger    Logger ID
 *    @param[in]     Event    Event type key word(4 bytes)
 *    @param[in]     LogString    log data string
 *    @retval -1            Log file has not been opened
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSLog(VSCTYPE vsc,char *Logger,char *Event,char *LogString);

/*==========================================================================*/
/**    Move current I/O pointer
 *    @ingroup Resource
 *    @param[in]     handle    resouse handle
 *    @param[in]     Offset    request offset
 *    @param[in]     RefMode    reference mode(BOF, current, or EOF)
 *    @retval >=0            new offset
 *    @retval -1            invalid offset
 *    @retval -2            invalid Refmode
 *    @retval -3            seek is not supported for this type
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI long PASCAL EXPORT VSLseekResource(VSHANDLE *handle,long Offset,int RefMode);

/*==========================================================================*/
/**    Check if string matches a wildcard pattern
 *    @ingroup General
 *    @param[in]     String    data string to check
 *    @param[in]     Pattern    wildcard pattern
 *    @retval 0            no, it does not match
 *    @retval 1            yes, it matchs
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSMatch(VUCHAR *String,VUCHAR *Pattern);

/*==========================================================================*/
/**    Merge two directorys
 *    @ingroup VSAPI
 *    @param[in]     dir1        base direcotry path to merge
 *    @param[in]     dir2        reference direcotry path to merge
 *    @param[out]    out        buffer to store created direcotry path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSMergeDir(char *dir1,char *dir2,char *out);

/*==========================================================================*/
/**    Like mkdir(), portable verion for all platforms/compilers
 *    @ingroup VSAPI
 *    @param[in]     Path        directory path
 *    @retval others        return from mkdir()
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSMkdir(char *Path);

/*==========================================================================*/
/**    Skip volume name in path
 *    @ingroup General
 *    @param[in]     path        path
 *    @retval !0            pointer to path without volume name
 *    @retval 0            invalid pointer
 */
TMVSAPI char *PASCAL EXPORT VSNoVolumeName(char *path);

/*==========================================================================*/
/**    Like open(), portable version for all platforms/compilers
 *    @ingroup VSAPI
 *    @param[in]     FilePath    file path to open
 *    @param[in]     mode        read/write mode
 *    @retval 0            ok
 *    @retval OPEN_R_ERR    open error
 *    @retval PARA_ERR    invalid parameter
 */
HANDLE PASCAL EXPORT VSOpenFile(char *FilePath,int mode);

/*==========================================================================*/
/**    Open resource
 *    @ingroup Resource
 *    @param[in]     Resource    file path to open
 *    @param[in]     type        what kind is the resource
 *    @param[in]     mode        read/write mode
 *    @param[in]     StartOffset    from where is the accessable data
 *    @param[in]     AccessableSize    how long is the accessable data; 0 if rest of file
 *    @param[out]    Handle    contain handle pointer to the resource if open OK, zero if error
 *    @retval 0            ok
 *    @retval -1            invalid resource type
 *    @retval -2            requested mode is not supported for this type
 *    @retval -3            failed to get file size
 *    @retval OPEN_R_ERR    open error
 *    @retval NO_MEM_ERR    insufficient memory
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSOpenResource(char *Resource,short type,short mode,long StartOffset,long AccessableSize,VSHANDLE **Handle);

/**    Dup resource
 *    @ingroup Resource
 */
TMVSAPI int PASCAL EXPORT VSDupResource(VSHANDLE *,VSHANDLE **);
/*==========================================================================*/
/**    Process directory
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     path        directory path
 *    @param[in]     ControlFlag    control flag
 *    @param[in,out] Counter    process counter
 *    @param[in]     PreService    before process call back function
 *    @param[in]     PostService    after process call back function
 *    @param[in]     Service        process funcion
 *    @param[in]     Para        user defined parameter passed to PreService, Service, and PostService
 *    @retval 0            ok
 *    @retval -1            stop by service function
 *    @retval -2            failed to access target directory
 *    @retval -3            failed to locate currect directory
 *    @retval -4            can not get back to original directory
 *    @retval -5            target path is not a directory
 *    @retval PARA_ERR    invalid parameter
 */

int _VSDupResourceEx(VSHANDLE *vsh, VSHANDLE **rvsh,int flag);

TMVSAPI int PASCAL EXPORT VSProcessDir(VSCTYPE vsc,char *path,long ControlFlag,
    PDCOUNT *Counter,VS_SERVICE *PreService,
    VS_SERVICE *PostService,
    VS_PROC_FUNC *Service,
    void *Para);

/*==========================================================================*/
/**    Process file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     res        resource to process
 *    @param[in]     ProcFunc    user defined process function.
 *    @param[in]     para        user defined parameter passed to ProcFunc
 *    @retval >=0            ok, return code from ProcFunc()
 *    @retval SKIP_ERR    error, but continue
 *    @retval MAXDECOM_ERR    exceed compression layer
 *    @retval BREAK_ERR    error, break
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSProcessFile(VSCTYPE vsc,RESOURCE *res,VS_PROC_FUNC *ProcFunc,void *para);

/*==========================================================================*/
/**    Portable version of read() or ReadFile()
 *    @ingroup VSAPI
 *    @param[in]     Fd        file handle
 *    @param[out]    buf        buffer to put read in data
 *    @param[in]     Len        request read length
 *    @param[out]    ReadLen    how may byte been placed in buf
 *    @retval 0            ok
 *    @retval 1            only partial of requested data been read
 *    @retval READ_ERR    read error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSReadFile(HANDLE Fd,char *buf,VUSHORT Len,VUSHORT *ReadLen);

/*==========================================================================*/
/**    Read log entry
 *    @ingroup General
 *    @param[in]     vsc        virus scan context
 *    @param[in]     LogFile    Log file to open
 *    @param[in]     LogFunc    process log entry function
 *    @param[in]     para        user defined parameter to LogFunc()
 *    @retval 0            ok
 *    @retval -1            no log file name
 *    @retval -2            no log processing function
 *    @retval -3            break by user defined log function
 *    @retval OPEN_R_ERR    fail to open log file
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSReadLog(VSCTYPE vsc,char *LogFile,VS_LOG_FUNC LogFunc,void *para);

/*==========================================================================*/
/**    Read in virus pattern
 *    @ingroup Pattern
 *    @param[in]     ptn    pattern structure data buffer
 *    @retval 0             ok
 *    @retval -1            pattern file corrupted
 *    @retval -2            unknown pattern file verion
 *    @retval -3            crc error
 *    @retval -4            internal error:not read new pattern structure
 *    @retval OPEN_R_ERR    fail to open pattern file
 *    @retval READ_ERR      fail to read pattern file
 *    @retval NO_MEM_ERR    out of memory
 *    @retval PARA_ERR      invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSReadPattern(PTNHANDLE ptn);

/*==========================================================================*/
/**    Read data from resource
 *    @ingroup Resource
 *    @param[in]     handle    resouse handle
 *    @param[out]    buf        buffer to put read in data
 *    @param[in]     Len        request read length
 *    @param[out]    ReadLen    how may byte been placed in buf
 *    @retval 1            not all of requested byte read(EOF ?)
 *    @retval 0            ok
 *    @retval READ_ERR    read error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSReadResource(VSHANDLE *handle,char *buf,VUSHORT Len,VUSHORT *ReadLen);

/*==========================================================================*/
/**    Remove all white characters from string
 *    @ingroup General
 *    @param[in]     String     string to process
 *    @retval >=0            how many bytes been removed
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSRemoveWhiteChar(char *String);

/*==========================================================================*/
/**    Turn off conf change flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSResetConfChangeFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Identify data type
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     file        file name to be exaimed
 *    @param[in]     ResourceType    resource type
 *    @param[in]     ResourceOffset    where does the data start
 *    @param[in]     ResourceSize    how long is the data
 *    @param[out]    dt        return the sub type id
 *    @retval >=0            file type
 *    @retval -2            unknow file type
 *    @retval OPEN_R_ERR    file open error
 *    @retval READ_ERR    read error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSResourceDataType(VSCTYPE vsc,char *file,int ResourceType,long ResourceOffset,long ResourceSize,VSDTYPE *dt);

/*==========================================================================*/
/**    Return resource size
 *    @ingroup Resource
 *    @param[in]     handle    resouse handle
 *    @retval >=0            size
 *    @retval -1            size function is not supported for this type
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI long PASCAL EXPORT VSResourceSize(VSHANDLE *handle);

/*==========================================================================*/
/**    Portable version of lseek
 *    @ingroup VSAPI
 *    @param[in]     Fd        resouse handle
 *    @param[in]     Offset    request offset
 *    @param[in]     RefMode    reference mode(BOF, current, or EOF)
 *    @retval >=0            new offset
 *    @retval -1            invalid offset
 *    @retval -2            invalid Refmode
 *    @retval -3            seek is not supported for this file
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI long PASCAL EXPORT VSSeekFile(HANDLE Fd,long Offset,int RefMode);

/*==========================================================================*/
/**    Set character environment type
 *    @ingroup VSAPI
 *    @param[in]     NewValue    new setting
 *    @retval 0            ok, and type set
 */
TMVSAPI int PASCAL EXPORT VSSetCharacterEnvType(int NewValue);

/*==========================================================================*/
/**    Set file count enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetCountFileFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set data type's info
 *    @ingroup DataType
 *    @param[in]     vsc        virus scan context
 *    @param[in]     dt        Data type
 *    @param[in]     TypeFlag    the type flag
 *    @param[in]     TypeExtFlag    the type extended flag
 *    @retval 0            ok and data stored
 *    @retval -2            unknow type id
 *    @retval -3            unknow sub type id
 *    @retval -4            not allow to set flag
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDataTypeInfo(VSCTYPE vsc,VSDTYPE *dt,long TypeFlag,long TypeExtFlag);

/*==========================================================================*/
/**    Get RedAlert Flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current RedAlert Flag
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetRedAlertFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set RedAlert Flag and Return previous Setting
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval >=0            previous Setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetRedAlertFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set if we should keep files created by decompress module
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetKeepDecompressFileFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set the log file path
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     LogPath    where is the log directory
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetLogFilePath(VSCTYPE vsc,char *LogPath);

/*==========================================================================*/
/**    Set function to generate log file path
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Func        function to generate log file name
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetLogFilePathFunc(VSCTYPE vsc,VS_LOG_FILE_PATH_FUNC *Func);

/*==========================================================================*/
/**    Set log enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetLogFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set the global debug message file handle
 *    @ingroup General
 *    @param[in]     InFile     new global debug message file handle
 *    @retval 0            ok
 */
TMVSAPI int PASCAL EXPORT VSSetDebug(FILE *InFile);

/*==========================================================================*/
/**    Set smart decompress enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetSmartDecompressFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set user defined log function
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     LogFunc    log function
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetUserLogFunc(VSCTYPE vsc,VS_USER_LOG_FUNC *LogFunc);

/*==========================================================================*/
/**    Set the global debug verbose level value
 *    @ingroup VSAPI
 *    @param[in]     NewValue     new global verbose level
 *    @retval 0            ok
 */
TMVSAPI int PASCAL EXPORT VSSetVerboseLevel(int NewValue);

/*==========================================================================*/
/**    Set current volume name
 *    @ingroup General
 *    @param[in]     volume    buffer to store volume name
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetVolume(char *volume);

/*==========================================================================*/
/**    Set logger id and caller id of a VSC
 *    @ingroup VSAPI
 *    @param[in,out] vscinfo     VSC information data structure.
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 *    @note vscinfo->vi_vsc is the target VSC and must be filled before calling
 */
TMVSAPI int PASCAL EXPORT VSSetVSCInfo(VSCINFO *vscinfo);

/*==========================================================================*/
/**    Set the debug message file handle
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     InFile     file handle for debug message
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetVSDebug(VSCTYPE vsc,FILE *InFile);

/*==========================================================================*/
/**    Set the debug verbose level value
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     InValue     verbose level value to set
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetVSVerboseLevel(VSCTYPE vsc,int InValue);

/*==========================================================================*/
/**    Return file size
 *    @ingroup VSAPI
 *    @param[in]     Fd        file handle
 *    @retval >=0            size
 *    @retval -1            file size function is not supported for this type
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI long PASCAL EXPORT VSSizeOfFile(HANDLE Fd);

/*==========================================================================*/
/**    Function to restore VSC data. This function is call after update the engine
 *    @ingroup VSAPI
 *    @param[in]     buffer    data buffer allocated by VSBackupVSCData
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSStoreVSCData(char *buffer);

/*==========================================================================*/
/**    Like stricmp(), caseless compare, some system does not support it
 *    @ingroup General
 *    @param[in]     String1    string to compare
 *    @param[in]     String2    string to compare
 *    @retval >0            String1 is greater than String2
 *    @retval 0            strings are the same
 *    @retval <0            String1 is smaller than String2
 */
TMVSAPI int PASCAL EXPORT VSStricmp(const char *String1,const char *String2);

/*==========================================================================*/
/**    Remove all leading and tailing white characters
 *    @ingroup General
 *    @param[in]     String     string to process
 *    @retval >=0            how many bytes been removed
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSStrip(char *String);

/*==========================================================================*/
/**    Like strnicmp(), caseless compare, some system does not support it
 *    @ingroup General
 *    @param[in]     String1    string to compare
 *    @param[in]     String2    string to compare
 *    @param[in]     Len        compare length
 *    @retval >0            String1 is greater than String2
 *    @retval 0            strings are the same
 *    @retval <0            String1 is smaller than String2
 */
TMVSAPI int PASCAL EXPORT VSStrnicmp(const char *String1,const char *String2,int Len);

/*==========================================================================*/
/**    Swap a 4 byte data from 1-2-3-4 order to 4-3-2-1 order
 *    @ingroup General
 *    @param[in]     LongData    4 byte data to swap
 *    @return swapped LongData
 */
TMVSAPI VULONG PASCAL EXPORT VSSwapLong(VULONG LongData);

/*==========================================================================*/
/**    Swap a 4 byte array
 *    @ingroup General
 *    @param[in]     LongTable    4 byte array to swap
 *    @param[in]     TableSize    how many element in table
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSwapLongTable(VULONG *LongTable,int TableSize);

/*==========================================================================*/
/**    Swap a 2 byte data from 1-2 order to 2-1 order
 *    @ingroup General
 *    @param[in]     ShortData    2 byte data to swap
 *    @return swapped ShortData
 */
TMVSAPI VUSHORT PASCAL EXPORT VSSwapShort(VUSHORT ShortData);

/*==========================================================================*/
/**    Swap a 2 byte array
 *    @ingroup General
 *    @param[in]     ShortTable    2 byte array to swap
 *    @param[in]     TableSize    how many element in table
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSwapShortTable(VUSHORT *ShortTable,int TableSize);

/*==========================================================================*/
/**    Convert all upper case character in string to lower
 *    @ingroup General
 *    @param[in]     String     string to process
 *    @retval >=0            how many bytes been converted
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSToLowerString(char *String);

/*==========================================================================*/
/**    Convert all lower case character in string to upper
 *    @ingroup General
 *    @param[in]     String     string to process
 *    @retval >=0            how many bytes been converted
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSToUpperString(char *String);

/*==========================================================================*/
/**    Scan resource for known virus
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     res        resource for scan
 *    @param[out]    ProcInfo    allocated data for call back
 *    @param[in]     Para        user defined parameters
 *    @retval 0            resource is clean
 *    @retval 1            resource is infected
 *    @retval >1            resource is a archive, return how many file been infected
 *    @retval NO_MEM_ERR    out of memory
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSVirusScan(VSCTYPE vsc,RESOURCE *res,void **ProcInfo,void *Para);

/*==========================================================================*/
/**    Write out boot sector/partition
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     DataBlock    data block
 *    @param[in]     BlockSize    block size
 *    @param[in]     WriteFlag    read which part
 *    @retval 0            ok
 *    @retval -1            not supported
 *    @retval WRITE_ERR    write error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSWriteBP(VSCTYPE vsc,char *DataBlock,int BlockSize,int WriteFlag);

/*==========================================================================*/
/**    Portable version of write() or WriteFile()
 *    @ingroup VSAPI
 *    @param[in]     Fd        resouse handle
 *    @param[in]     buf        buffer contains data to write
 *    @param[in]     Len        request write length
 *    @param[out]    WriteLen    how may byte been written
 *    @retval 0            ok
 *    @retval WRITE_ERR    write error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSWriteFile(HANDLE Fd,char *buf,VUSHORT Len,VUSHORT *WriteLen);

/*==========================================================================*/
/**    Write data to resource
 *    @ingroup Resource
 *    @param[in]     handle    resouse handle
 *    @param[in]     buf        buffer contains data to write
 *    @param[in]     Len        request write length
 *    @param[out]    WriteLen    how may byte been written
 *    @retval 0            ok
 *    @retval WRITE_ERR    write error
 *    @retval BAD_HANDLE_ERR    invalid VSHANLDE pointer
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSWriteResource(VSHANDLE *handle,char *buf,VUSHORT Len,VUSHORT *WriteLen);

/* function added by Roger */

/*======================================================*/
/**    Read boot data
 *    @ingroup VSAPI
 *    @param[in]     nDrive    drive no.  
 *        - 0-25 : read boot sector
 *        - 0x80 - 0x8a : read partition
 *    @param[out]    pData    data
 *    @param[in]     len        size of beffer
 *    @retval 0            OK
 *    @retval PARA_ERR    parameter error
 *    @retval READ_ERR    read error
 */
TMVSAPI int  PASCAL EXPORT VSGetDiskData(int nDrive,char *pData,int len);

/*======================================================*/
/**    Write boot data
 *    @ingroup VSAPI
 *    @param[in]    nDrive    drive no.
 *        - 0-25 : write boot sector,
 *        - 0x80 - 0x8a : write partition
 *    @param[out]    pData    data
 *    @param[in]     len        size of beffer
 *    @retval 0            OK
 *    @retval PARA_ERR    parameter error
 *    @retval READ_ERR    read error
 */
TMVSAPI int  PASCAL EXPORT VSSetDiskData(int nDrive,char *pData,int len);

/*======================================================*/
/**    Set the clean boot data
 *    @ingroup VSAPI
 *    @param[in]     nDrive    drive no.
 *        - 0-25 : write boot sector,
 *        - 0x80 - 0x8a : write partition
 *    @param[out]    pData    data
 *    @param[in]     len        size of beffer
 *    @retval 0            OK
 *    @retval PARA_ERR    parameter error
 *    @retval BUFFER_TOO_VSHORT_ERR    buffer too short
 *    @retval NOT_SUPPORTED_ERR    not supported drive #
 */
TMVSAPI int  PASCAL EXPORT VSCleanBootData(int nDrive,char *pData,int len);

/*======================================================*/
/**    Get the virus name information
 *    @ingroup VSAPI
 *    @obsoleted by VSGetVirusNameInfoEx()
 *    @param[in]     vsc        virus scan context
 *    @param[in]     index    start index
 *    @param[in]     count    # to read
 *    @param[out]    Buffer    virus name information to place
 *    @param[in]     len        buffer length
 *    @return # of virus name information read
 */
TMVSAPI int PASCAL EXPORT VSGetVirusNameInfo(VSCTYPE vsc, VUSHORT index, VUSHORT count, char *Buffer, VULONG len);

/*======================================================*/
/**    Get the virus name information
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     index    start index
 *    @param[in]     count    # to read
 *    @param[out]    Buffer    virus name information to place
 *    @param[in]     len        buffer length
 *    @return # of virus name information read
 */
TMVSAPI int PASCAL EXPORT VSGetVirusNameInfoEx(VSCTYPE vsc, VULONG index, VUSHORT count, char *Buffer, VULONG len);

/*==========================================================================*/
/**    Set function to generate new file name for action rename
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Func        function to generate new file name parameter:
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetRenameFileNameFunc(VSCTYPE vsc,VS_RENAME_FILE_NAME_FUNC *Func);

/*==========================================================================*/
/**    Function to determine the file gonna be updated is newer or not
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     UpdateName    file name to be updated
 *    @retval 1            version is newer
 *    @retval 0            version is same as or older than currently used
 *    @retval PLATFORM_ERR    platform different
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSIsNewerEngine(VSCTYPE vsc, char *UpdateName);

/*==========================================================================*/
/**    Return backup enable/disable flag when take clean action
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetCleanBackupFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set backup enable/disable flag when take clean action
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetCleanBackupFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Remove all temp files used by this vsc
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSRemoveAllTempFile(VSCTYPE vsc);

/**    Check if EZ Drive there
 *    @ingroup VSAPI
 *    @param DriveNo        drive no.
 *    @retval 0            Not installed
 *    @retval 1            Installed
 */
TMVSAPI int PASCAL EXPORT VSIsEZDriveInstalled(int DriveNo);

/*==========================================================================*/
/**    Read in virus pattern from file
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Path       pattern file name
 *    @param[in]     Offset     offset of pattern file starts
 *    @retval 0      ok
 *    @retval -2                unknown pattern file verion
 *    @retval -3                pattern file corrupted
 *    @retval -4                internal error:not read new pattern structure
 *    @retval OPEN_R_ERR        fail to open pattern file
 *    @retval READ_ERR          fail to read pattern file
 *    @retval PTN_UPDATE_ERR    fail to update pattern file
 *    @retval NO_MEM_ERR        out of memory
 *    @retval PARA_ERR          invalid parameter(s)
 *    @retval BAD_VSC_ERR       invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSReadPatternInFile(VSCTYPE vsc,char *Path,long Offset);

/*==========================================================================*/
/**    Delete decode function of an user defined data type
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     dt        data type the function will be removed
 *    @retval 0            set ok
 *    @retval -1            data type is not an user defined data type
 *    @retval -2            user defined-function for data type not found
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSDelUserDecodeFunc(VSCTYPE vsc,VSDTYPE *dt);

/*==========================================================================*/
/**    Set decode function of an user defined data type
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     dt        data type the function will associated with
 *    @param[in]     Func        User defined decode function for this data type
 *    @retval 0            set ok
 *    @retval -1            data type is not an user defined data type
 *    @retval -2            user defined-function table full
 *    @retval -3            user defined-function for data type already exists
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetUserDecodeFunc(VSCTYPE vsc,VSDTYPE *dt,VS_USER_DECODE_FUNC Func);

/**    _VSSimpleEncrypt
 *    @ingroup VSAPI
 */
TMVSAPI void PASCAL EXPORT _VSSimpleEncrypt(VUCHAR *,VUSHORT len);

/*==========================================================================*/
/**    Return encrypt temp file flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetEncryptTempFileFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Scan mac virus
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     file     MAC file to be scanned
 *    @param[out]    virus_name    buffer to store virus name if found
 *    @retval 0            No virus found
 *    @retval 1            virus found
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSScanMacVirus(VSCTYPE vsc,char *file,char *virus_name);

/*==========================================================================*/
/**    Set encrypt temp file flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetEncryptTempFileFlag(VSCTYPE vsc, int NewSetting);
#define _VSSimpleDecrypt _VSSimpleEncrypt

/*==========================================================================*/
/**    Set size of the memory that decompress
 *
 *    module can use to put decompressed data.
 *    File will be decompressed in to memory instead of temp file to speed
 *    up the process, if the file size(after decompressed) is not larger
 *    than the size set by this function
 *
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSize    new size
 *    @retval 0            set ok
 *    @retval NOT_SUPPORTED_ERR    size is not in range of DECOMP_MIN_MEM and DECOMP_MAX_MEM
 *    @retval PARA_ERR    invalid parameter
 *    @note
 *        -# decompress data into memory will only take place when smart decompress flag is on.
 *        -# Each layer of decompression will allocate and use memory of this size separately
 *        -# Valide size are between DECOMP_MIN_MEM and DECOMP_MAX_MEM
 *        -# See m_*.h for default size range for each platforms
 */
TMVSAPI int PASCAL EXPORT VSSetMemoryDecompressSize(VSCTYPE vsc, VULONG NewSize);

/*==========================================================================*/
/**    Set size limit of the Extract File
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSize    new size
 *    @retval 0            set ok
 *    @retval NOT_SUPPORTED_ERR    size is negative.
 *    @retval PARA_ERR    invalid parameter
 *    @note The default size limit is ExtractFile_MAX_SIZE
 */
TMVSAPI int PASCAL EXPORT VSSetExtractFileSizeLimit(VSCTYPE vsc, VULONG NewSize);

/*==========================================================================*/
/**    Set size limit of the Extract File
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSize    new size
 *    @param[in]     major      major type of data type to be configured
 *    @param[in]     minor      minor type of data type to be configured
 *    @retval 0            set ok
 *    @retval NOT_SUPPORTED_ERR    size is negative.
 *    @retval PARA_ERR    invalid parameter (major and minor value can only be VSDT_SWF and VSDT_SWF_CWF for now)
 *    @note The default size limit is ExtractFile_MAX_SIZE
 */
TMVSAPI int PASCAL EXPORT VSSetExtractFileSizeLimitByVSDT(VSCTYPE vsc, VULONG NewSize, VULONG major, VULONG minor);

/*==========================================================================*/
/**    Get size limit of the Extract File
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    SizePtr    current size limit of the Extract File
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetExtractFileSizeLimit(VSCTYPE vsc, VULONG *SizePtr);

/*==========================================================================*/
/**    Get size limit of the Extract File
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    SizePtr    current size limit of the Extract File
 *    @param[in]     major      major type of data type to be configured
 *    @param[in]     minor      minor type of data type to be configured
 *    @retval 0            set ok
 *    @retval NOT_SUPPORTED_ERR    size is negative.
 *    @retval PARA_ERR    invalid parameter (major and minor value can only be VSDT_SWF and VSDT_SWF_CWF for now)
 *    @note The default size limit is ExtractFile_MAX_SIZE
 */
TMVSAPI int PASCAL EXPORT VSGetExtractFileSizeLimitByVSDT(VSCTYPE vsc, VULONG *SizePtr, VULONG major, VULONG minor);

/*==========================================================================*/
/**    Get size of the memory that decompress module can use to put decompressed data.
 *
 *    File will be decompressed in to memory instead of temp file to speed
 *    up the process, if the file size(after decompressed) is not larger
 *    than the size set by this function
 *
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    Size        current size
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 *    @note 
 *        -# decompress data into memory will only take place when smart decompress flag is on.
 *        -# Each layer of decompression will allocate and use memory of this size separately
 *        -# Valide size are between DECOMP_MIN_MEM and DECOMP_MAX_MEM
 *        -# See m_*.h for default size range for each platforms
 */
TMVSAPI int PASCAL EXPORT VSGetMemoryDecompressSize(VSCTYPE vsc, VULONG *Size);

/*==========================================================================*/
/**    Set Heuristic scan Lever
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewLevel    new level 
 *        - high-byte : relative to buffer size (default =16K)
 *        - low-byte  : relative to Fwin32 level
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetHeuristicLevel(VSCTYPE vsc,    VUSHORT NewLevel);

/*==========================================================================*/
/**    Get Heuristic scan Lever
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    Level     current Level
 *        - high-byte : relative to buffer size (default =16K)
 *        - low-byte  : relative to Fwin32 level
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetHeuristicLevel(VSCTYPE vsc,VUSHORT *Level);

/**    VSGetGenericVirusReport
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    RptBuf    Buffer to put report data
 *    @param         RptBufLen    Buffer length
 *    @param         RptDataLen    report-data length
 *    @param[out]    VirusFlag    VULONG array size 6 to put report flags
 *    @param MacroVersion    for caller to decide which description to use
 *    @retval 0            get ok
 *    @retval -1            buffer is too small
 *    @retval -2            No macro report, because of version or type error
 *    @retval -3            No macro report, because of no macro
 *    @retval -4            No macro report, because generic macro scan is turned off
 *    @retval PARA_ERR    invalid parameter
 *    @note Caller may input either RptBuf or VirusFlag or both
 */
TMVSAPI int PASCAL EXPORT VSGetGenericVirusReport(VSCTYPE vsc,VUCHAR *RptBuf,VULONG RptBufLen,VULONG *RptDataLen, VULONG *VirusFlag, VUCHAR *MacroVersion);

/*==========================================================================*/
/**    Get clean zip file flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval 0            no
 *    @retval 1            yes
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetCleanZipFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set clean zip flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetCleanZipFlag(VSCTYPE vsc, int NewSetting);

/*==========================================================================*/
/**    Set clean zip flag
 *    @ingroup VSAPI
 *    @param[in]     ZipName    zip file name
 *    @param[in]     TempZipName    a temp name for backup zip file
 *    @param[in]     ZipupList    structure of information for update
 *    @param[in]     UnicodeFlag
 *    @retval 0            ok
 *    @retval <0            error
 */
TMVSAPI int PASCAL EXPORT VSUpdateZip(char *ZipName, char *TempZipName, ZIPUPLIST *ZipupList, int UnicodeFlag);

/*==========================================================================*/
/**    Check machine is Nec-9801 series or not, in bpm95.dll
 *    @ingroup VSAPI
 *    @return if true return (1) else return (0)
 */
TMVSAPI int PASCAL EXPORT VSCheckNec32(void);

/*==========================================================================*/
/**    Check machine is Fujitsu Fmr series or not, in bpm95.dll
 *    @ingroup VSAPI
 *    @return if true return (1) else return (0)
 */
TMVSAPI int PASCAL EXPORT VSCheckFmr32(void);

/*==========================================================================*/
/**    Read floppy boot sector data from PC98 and FMR
 *    @ingroup VSAPI
 *    @param nDrive        (0: A , 1: B, ...)
 *    @param pData        buffer(size : 1024 bytes)
 *    @param len            buffer size
 *    @return pData : Boot Sector if Success
 *    @retval 0            OK
 *    @retval PARA_ERR    parameter error
 *    @retval READ_ERR    read error
 */
TMVSAPI int PASCAL EXPORT VSReadFlpBootSectorJapen(int nDrive, char *pData, int len);

/*==========================================================================*/
/**    Return extract amg file enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetExtractAmgFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set extract amg file enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetExtractAmgFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Scan win95 memory,only for win95,in bpm95.dll
 *    @ingroup VSAPI
 *    @param pBPMInfo
 *        - pBPMInfo->bpm_VirusNam => Virus name
 *        - pBPMInfo->bpm_VirusNumber => virus number
 *        - pBPMInfo->bpm_VirusType => virus type    
 *        - pBPMInfo->bpm_MemPtr    = always NULL
 *    @retval 0            no virus
 *    @retval 1            found virus in memory, but not remove sucessfully
 *    @retval 2            found virus in memory, and remove successfully
 */
TMVSAPI int PASCAL EXPORT VSScanWin95MemoryVirus(BPMINFO *pBPMInfo);

/*==========================================================================*/
/**    Get default Ext name list buffer size (depend on pattern)
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param SizePtr        [Out] the current size of default Ext name list buffer (minimum size = 1)
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultExtListSize(VSCTYPE vsc, VULONG* SizePtr);

/*==========================================================================*/
/**    Get default Ext name list (depend on pattern)
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    BufPtr    the current default Ext name list; Format: "ARJ;HTML;ZIP;"
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultExtList(VSCTYPE vsc, char* BufPtr);

/*==========================================================================*/
/**    Get encode enable/disable flag when backup files
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetBackupEncodeFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set encode enable/disable flag when backup files
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetBackupEncodeFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Backup a file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     SrcFile    path of source file
 *    @param[in]     DestFile    path of destination file
 *    @retval 0            ok
 *    @retval -1            source file lseek error
 *    @retval -2            dest file lseek error
 *    @retval -3            failed to get source file size
 *    @retval -4            Invalid copy offset
 *    @retval READ_ERR    read error
 *    @retval WRITE_ERR    write error
 *    @retval OPEN_R_ERR    file open error
 *    @retval OPEN_W_ERR    dest file create error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSBackupFile(VSCTYPE vsc, char *SrcFile, char *DestFile);

/*==========================================================================*/
/**Backup a file with encoding
 *    @ingroup VSAPI
 *    @param[in]     SrcFile    path of source file
 *    @param[in]     DestFile    path of destination file
 *    @retval 0            ok
 *    @retval -1            source file lseek error
 *    @retval -2            dest file lseek error
 *    @retval -3            failed to get source file size
 *    @retval -4            Invalid copy offset
 *    @retval READ_ERR    read error
 *    @retval WRITE_ERR    write error
 *    @retval OPEN_R_ERR    file open error
 *    @retval OPEN_W_ERR    dest file create error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSEncBackupFile(char *SrcFile, char *DestFile);

/*==========================================================================*/
/**    Restore a backuped file
 *    @ingroup VSAPI
 *    @param[in]     SrcFile    path of source file
 *    @param[in]     DestFile    path of destination file
 *    @retval 0            ok
 *    @retval -1            source file lseek error
 *    @retval -2            dest file lseek error
 *    @retval -3            failed to get source file size
 *    @retval -4            Invalid copy offset
 *    @retval READ_ERR    read error
 *    @retval WRITE_ERR    write error
 *    @retval OPEN_R_ERR    file open error
 *    @retval OPEN_W_ERR    dest file create error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSRestoreFile(char *SrcFile, char *DestFile);

/*==========================================================================*/
/**    Restore a backuped file
 *    @ingroup VSAPI
 *    @param[in]     FileName    backup file name
 *    @param[out]    BackupInfo    backup file information
 *    @retval 0            ok
 *    @retval -1            source file lseek error
 *    @retval -2            dest file lseek error
 *    @retval -3            failed to get source file size
 *    @retval -4            Invalid copy offset
 *    @retval -5            Not a backup file
 *    @retval READ_ERR    read error
 *    @retval WRITE_ERR    write error
 *    @retval OPEN_R_ERR    file open error
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetBackupFileInfo(char *FileName, VSBackupFileInfo *BackupInfo);

/*==========================================================================*/
/**    Get default exclude Ext name list buffer size (depend on pattern)
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    SizePtr    the current size of default Ext name list buffer (minimum size = 1)
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultExcludeExtListSize(VSCTYPE vsc, VULONG* SizePtr);

/*==========================================================================*/
/**    Get default exclude Ext name list (depend on pattern)
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    BufPtr    the current default Ext name list; Format: "ARJ;HTML;ZIP;"
 *    @retval 0            get ok
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultExcludeExtList(VSCTYPE vsc, char* BufPtr);

/**    Set realtime scan flag
 *    @ingroup Flag
 *    @filteronly
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetRealTimeFlag( VSCTYPE vsc, int NewSetting);


/** VSGetBackupFileInfoUtf8 get a backup file info in utf8 string
  *    @ingroup VSAPI
  *    @param[in]      FileName backup file name
  *    @param[out]   BackupInfo  backup file information retrieved
  *    @retval 0        ok
  *    @retval -1       source file lseek error
  *    @retval -2       dest file lseek error
  *    @retval -3       failed to get source file size
  *    @retval -4       Invalid copy offset
  *    @retval  -5      Not a backup file, backup file is not unicode backup file, treat it as error
  *    @retval  READ_ERR           read error
  *    @retval  WRITE_ERR        write error 
  *    @retval  OPEN_R_ERR      file open error
  *    @retval  PARA_ERR          invalid parameter
  *    @note    this API only works on the linux x86 platform.
  */
#ifdef LINUX
int PASCAL EXPORT VSGetBackupFileInfoUtf8(char *BackupFileName,struct _VSBackupFileInfo *BackupInfo);
#endif

/*==========================================================================*/
/**    Check File Integrity
*    @param[in]     vsc       virus scan context
*    @param[in]     res       resource to be examined
*    @param[in]     dt        current data type to be checked
*    @retval <  0   error
*    @retval == 0   check success
*    @retval >  0   check failed
*/
TMVSAPI int PASCAL EXPORT VSCheckResourceIntegrity(VSCTYPE vsc, RESOURCE *res, const VSDTYPE *dt);

#ifdef __cplusplus
}
#endif

#endif /* __TMVSX_H__ */
