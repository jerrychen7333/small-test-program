#ifndef __VSPDT_H__
#define __VSPDT_H__

/************************/
/* primitive data types */
/************************/

#ifdef KDWINNT
    #include "vspdt_win32.h"
#elif defined(WIN64)
	#include "vspdt_win32.h"
#elif defined(WIN32A)
    #include "vspdt_lli64x.h"
#elif defined(WINCE_32)
    #include "vspdt_lli64x.h"
#elif defined(W32DBG)
    #include "vspdt_win32.h"
#elif defined(WIN32)
    #include "vspdt_win32.h"
#endif

#ifdef AIX
#include "vspdt_aix.h"
#endif

#ifdef AIX64
#include "vspdt_billi.h"
#endif

#ifdef AS400
#include "vspdtas400.h"
#endif

#ifdef BIMINI50
#include "vspdt_bimini50.h"
#endif

#ifdef BSDI
#include "vspdt_lli64.h"
#endif

#ifdef DECUX
#include "vspdt_bii64x.h"
#endif

#ifdef DGUX
#include "vspdt_bllli.h"
#endif

#ifdef DOS_BC
#include "vspdt_lli64.h"
#endif

#ifdef DOS_GCC
#include "vspdt_lli64.h"
#endif

#ifdef DOS_WC
#include "vspdt_lli64.h"
#endif

#ifdef EPOC
    #ifdef __WINS__
        #include "vspdt_lli64x.h" /* For VC++ 6 */
    #else
        #include "vspdt_llllix.h" /* For gcc */
    #endif
#endif

#ifdef FBSD
    #ifdef __amd64
        #include "vspdt_lilli.h"
    #else
        #include "vspdt_lllli.h"
    #endif
#endif

#ifdef HP10
#include "vspdt_bllli.h"
#endif

#ifdef HP11IA64
#include "vspdt_billi.h"
#endif

#ifdef LINUX
#include "vspdt_lllli.h"
#endif

#ifdef LINUXARM
#include "vspdt_lill.h"
#endif

#ifdef ANDROID
#include "vspdt_lill.h"
#endif

#if defined(LNXAMD64) || defined(LNXAMD64LITE)
#include "vspdt_lilx.h"
#endif

#ifdef LNXIA64
#include "vspdt_lilx.h"
#endif

#ifdef LNXMP
#include "vspdt_llllix.h"
#endif

#ifdef LNXMP4926
#include "vspdt_llllix.h"
#endif

#ifdef LNXXSCALE
#include "vspdt_blllx.h"
#endif

#ifdef MAC
#include "vspdt_bli64.h"
#endif

#ifdef MACOSX
    #ifdef __BIG_ENDIAN__
        #include "vspdt_blllix.h"
    #else
        #include "vspdt_lllli.h"
    #endif
#endif

#ifdef MACOSX64
    #ifdef __BIG_ENDIAN__
        #include "vspdt_billix.h"
    #else
        #include "vspdt_lilli.h"
    #endif
#endif

#ifdef MAEMO
#include "vspdt_lllli.h"
#endif

#ifdef NEC
#include "vspdt_bli64.h"
#endif

#ifdef NLM
#include "vspdt_lli64.h"
#endif

#ifdef OS2
#include "vspdt_lli64.h"
#endif

#ifdef OS390
#include "vspdt_os390.h"
#endif

#ifdef PALM
#include "vspdt_lli64.h"
#endif

#ifdef SCO5
#include "vspdt_lllli.h"
#endif

#ifdef SGI
#include "vspdt_bli64.h"
#endif

#ifdef SNI
#include "vspdt_bli64.h"
#endif

#ifdef SOL
#include "vspdt_sol.h"
#endif

#ifdef SOLSPC64
#include "vspdt_solspc64.h"
#endif

#ifdef SOL86
#include "vspdt_sol86.h"
#endif

#ifdef SOL64
#include "vspdt_sol64.h"
#endif

#ifdef SOLBSD
#include "vspdt_bli64.h"
#endif

#ifdef SUN4
#include "vspdt_bli64.h"
#endif

#ifdef UNIXWARE
#include "vspdt_lllli.h"
#endif

#ifdef VXD95
#include "vspdt_win32.h"
#endif

#ifdef WIN16
#include "vspdt_lli64.h"
#endif

#ifdef ZLINUX
#include "vspdt_bllli.h"
#endif

#ifdef ZLNX64
#include "vspdt_billi.h"
#endif

#ifndef __VSPDT_DEFINED
Error, No machine defined
#endif


/* boolean */
#define VUBOOL                      VUINT32
#define V_FALSE                     (0)
#define V_TRUE                      (!V_FALSE)

#ifndef NULL
#define NULL                        ((void*)0)
#endif

#ifdef VS_EBCDIC
/* do something */
#endif

/*************************************/
/* load & store (machine dependence) */
/*************************************/
/* Little-Endian-Memory */
#define VS_I8TOLM(m,i)              _VS_I8TOLM(((VUINT8*)m),((VUINT8)(i)))
#define VS_I16TOLM(m,i)             _VS_I16TOLM(((VUINT8*)m),((VUINT16)(i)))
#define VS_I24TOLM(m,i)             _VS_I24TOLM(((VUINT8*)m),((VUINT32)(i)))
#define VS_I32TOLM(m,i)             _VS_I32TOLM(((VUINT8*)m),((VUINT32)(i)))
#define VS_I64TOLM(m,i)             _VS_I64TOLM(((VUINT8*)m),((VUINT64)(i)))

#define VS_LMTOI8(m)                _VS_LMTOI8(((VUINT8*)m))
#define VS_LMTOI16(m)               _VS_LMTOI16(((VUINT8*)m))
#define VS_LMTOI24(m)               _VS_LMTOI24(((VUINT8*)m))
#define VS_LMTOI32(m)               _VS_LMTOI32(((VUINT8*)m))
#define VS_LMTOI64(m)               _VS_LMTOI64(((VUINT8*)m))


#define _VS_I24TOLM(m,i) \
{ \
    m[0]=(VUINT8)i; \
    m[1]=(VUINT8)(i>>8); \
    m[2]=(VUINT8)(i>>16); \
}

#define _VS_LMTOI24(m) \
    ( \
    (((VUINT32)m[0])) + \
    (((VUINT32)m[1])<<8) + \
    (((VUINT32)m[2])<<16) \
    )

#if (VS_BYTE_ORDER == 0) && (BUS_ERROR_TRAP == V_FALSE)
    #define _VS_I8TOLM(m,i)             (*(VUINT8*)m = i)
    #define _VS_I16TOLM(m,i)            (*(VUINT16*)m = i)
    #define _VS_I32TOLM(m,i)            (*(VUINT32*)m = i)
    #define _VS_I64TOLM(m,i)            (*(VUINT64*)m = i)

    #define _VS_LMTOI8(m)               (*(VUINT8*)m)
    #define _VS_LMTOI16(m)              (*(VUINT16*)m)
    #define _VS_LMTOI32(m)              (*(VUINT32*)m)
    #define _VS_LMTOI64(m)              (*(VUINT64*)m)

#else
    #define _VS_I8TOLM(m,i) \
    { \
        m[0]=(VUINT8)i; \
    }
    #define _VS_I16TOLM(m,i) \
    { \
        m[0]=(VUINT8)i; \
        m[1]=(VUINT8)(i>>8); \
    }
    #define _VS_I32TOLM(m,i) \
    { \
        m[0]=(VUINT8)i; \
        m[1]=(VUINT8)(i>>8); \
        m[2]=(VUINT8)(i>>16); \
        m[3]=(VUINT8)(i>>24); \
    }
    #define _VS_I64TOLM(m,i) \
    { \
        m[0] = VS_SHR6432(i, 0); \
        m[1] = VS_SHR6432(i, 8); \
        m[2] = VS_SHR6432(i, 16); \
        m[3] = VS_SHR6432(i, 24); \
        m[4] = VS_SHR6432(i, 32); \
        m[5] = VS_SHR6432(i, 40); \
        m[6] = VS_SHR6432(i, 48); \
        m[7] = VS_SHR6432(i, 56); \
    }

    #define _VS_LMTOI8(m) \
    ( \
        (VUINT8)m[0] \
    )
    #define _VS_LMTOI16(m) \
    ( \
        (((VUINT16)m[0])) + \
        (((VUINT16)m[1])<<8) \
    )
    #define _VS_LMTOI32(m) \
    ( \
        (((VUINT32)m[0])) + \
        (((VUINT32)m[1])<<8) + \
        (((VUINT32)m[2])<<16) + \
        (((VUINT32)m[3])<<24) \
    )
    #define _VS_LMTOI64(m) \
    ( \
        VS_SHL6432(m[0], 0) + \
        VS_SHL6432(m[1], 8) + \
        VS_SHL6432(m[2], 16) + \
        VS_SHL6432(m[3], 24) + \
        VS_SHL6432(m[4], 32) + \
        VS_SHL6432(m[5], 40) + \
        VS_SHL6432(m[6], 48) + \
        VS_SHL6432(m[7], 56) \
    )

#endif

/* Big-Endian-Memory */
#define VS_I8TOBM(m,i)              _VS_I8TOBM(((VUINT8*)m),((VUINT8)(i)))
#define VS_I16TOBM(m,i)             _VS_I16TOBM(((VUINT8*)m),((VUINT16)(i)))
#define VS_I24TOBM(m,i)             _VS_I24TOBM(((VUINT8*)m),((VUINT32)(i)))
#define VS_I32TOBM(m,i)             _VS_I32TOBM(((VUINT8*)m),((VUINT32)(i)))
#define VS_I64TOBM(m,i)             _VS_I64TOBM(((VUINT8*)m),((VUINT64)(i)))

#define VS_BMTOI8(m)                _VS_BMTOI8(((VUINT8*)m))
#define VS_BMTOI16(m)               _VS_BMTOI16(((VUINT8*)m))
#define VS_BMTOI24(m)               _VS_BMTOI24(((VUINT8*)m))
#define VS_BMTOI32(m)               _VS_BMTOI32(((VUINT8*)m))
#define VS_BMTOI64(m)               _VS_BMTOI64(((VUINT8*)m))

#define _VS_I24TOBM(m,i) \
    { \
        m[1]=(VUINT8)(i>>16); \
        m[2]=(VUINT8)(i>>8); \
        m[3]=(VUINT8)i; \
    }

#define _VS_BMTOI24(m) \
    ( \
        (((VUINT32)m[0])<<16) + \
        (((VUINT32)m[1])<<8) + \
        (((VUINT32)m[2])) \
    )

#if (VS_BYTE_ORDER == 1) && (BUS_ERROR_TRAP == V_FALSE)
    #define _VS_I8TOBM(m,i)             (*(VUINT8*)m = i)
    #define _VS_I16TOBM(m,i)            (*(VUINT16*)m = i)
    #define _VS_I32TOBM(m,i)            (*(VUINT32*)m = i)
    #define _VS_I64TOBM(m,i)            (*(VUINT64*)m = i)

    #define _VS_BMTOI8(m)               (*(VUINT8*)m)
    #define _VS_BMTOI16(m)              (*(VUINT16*)m)
    #define _VS_BMTOI32(m)              (*(VUINT32*)m)
    #define _VS_BMTOI64(m)              (*(VUINT64*)m)
#else
    #define _VS_I8TOBM(m,i) \
    { \
        m[0]=(VUINT8)i; \
    }
    #define _VS_I16TOBM(m,i) \
    { \
        m[0]=(VUINT8)(i>>8); \
        m[1]=(VUINT8)i; \
    }
    #define _VS_I32TOBM(m,i) \
    { \
        m[0]=(VUINT8)(i>>24); \
        m[1]=(VUINT8)(i>>16); \
        m[2]=(VUINT8)(i>>8); \
        m[3]=(VUINT8)i; \
    }
    #define _VS_I64TOBM(m,i) \
    { \
        m[0]=(VUINT8)(VS_SHR6432(i, 56)); \
        m[1]=(VUINT8)(VS_SHR6432(i, 48)); \
        m[2]=(VUINT8)(VS_SHR6432(i, 40)); \
        m[3]=(VUINT8)(VS_SHR6432(i, 32)); \
        m[4]=(VUINT8)(VS_SHR6432(i, 24)); \
        m[5]=(VUINT8)(VS_SHR6432(i, 16)); \
        m[6]=(VUINT8)(VS_SHR6432(i, 8)); \
        m[7]=(VUINT8)(VS_SHR6432(i, 0)); \
    }

    #define _VS_BMTOI8(m) \
    ( \
        (VUINT8)m[0] \
    )
    #define _VS_BMTOI16(m) \
    ( \
        (((VUINT16)m[0])<<8) + \
        (((VUINT16)m[1])) \
    )
    #define _VS_BMTOI32(m) \
    ( \
        (((VUINT32)m[0])<<24) + \
        (((VUINT32)m[1])<<16) + \
        (((VUINT32)m[2])<<8) + \
        (((VUINT32)m[3])) \
    )
    #define _VS_BMTOI64(m) \
    ( \
        VS_SHL6432(m[0], 56) + \
        VS_SHL6432(m[1], 48) + \
        VS_SHL6432(m[2], 40) + \
        VS_SHL6432(m[3], 32) + \
        VS_SHL6432(m[4], 24) + \
        VS_SHL6432(m[5], 16) + \
        VS_SHL6432(m[6], 8) + \
        VS_SHL6432(m[7], 0) \
    )
#endif


/* Define a I64 value */
#ifndef VS_DEF_I64
#define VS_DEF_I64(h,l)             (h ## l)
#endif

/* Sign Extension */
#define VS_S8TOI16(s8)              ((VUINT16)((VSINT16)((VSINT8)s8)))
#define VS_S8TOI32(s8)              ((VUINT32)((VSINT32)((VSINT8)s8)))
#define VS_S16TOI32(s16)            ((VUINT32)((VSINT32)((VSINT16)s16)))
#define VS_S32TOI32(s32)            ((VUINT32)((VSINT32)((VSINT32)s32)))



/* vsapi define */
#ifndef VS_EXPORT_API
#define VS_EXPORT_API
#endif

#ifndef VWCHAR
typedef unsigned short              VWCHAR;     /* wide character */
#endif

#ifndef __VSAPI_TYPE_DEFINED

typedef _VUINT64    VUINT64;     /* 8 bytes unsigned integer */
typedef char        VCHAR;      /* ansi character */
#define VUCHAR      VUINT8      /* 1 byte unsigned integer */
#define VUSHORT     VUINT16     /* 2 bytes unsigned integer */
#define VULONG      VUINT32     /* 4 bytes unsigned integer */

#define VSCHAR      VSINT8      /* 1 byte signed integer */
#define VSHORT      VSINT16     /* 2 bytes signed integer */
#define VLONG       VSINT32     /* 4 bytes signed integer */
#define VINT64      VSINT64     /* 8 bytes signed integer */

#define VSStringToLong(cp)          VS_LMTOI32(cp)
#define VSStringToShort(cp)         VS_LMTOI16(cp)
#define VSStringToLongLong(cp)      VS_LMTOI64(cp)

#define VSLongToString(cp, x)       VS_I32TOLM(cp,x)
#define VSShortToString(cp, x)      VS_I16TOLM(cp,x)
#define VSLongLongToString(cp, x)   VS_I64TOLM(cp,x)
#endif

#include "vs_rc.h"


#endif

