/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 *****************************************************************************/

/**************************************************************************
    m_nlm.h 
    Description: 
	    Machine dependent code definition 
 **************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

#include <E32DEF.h>

#pragma pack(1)

/* platform dependent environment */
typedef char                    VCHAR;/* 1 byte */
typedef long                    VLONG;	/* 4 byte */
typedef unsigned long           VULONG; /* 4 byte */

#ifdef __WINS__
//	For VC++ 6
typedef __int64                 VINT64;  /* 8 byte */
typedef unsigned __int64        VUINT64; /* 8 byte */

#else
//	For gcc
typedef long long int           VINT64;  /* 8 byte */
typedef unsigned long long int  VUINT64; /* 8 byte */

#endif //__WINS__

void    VSLongLongToString(char *cp,VUINT64 v);
void    VSShortToString(char *cp,VUSHORT v);
void    VSLongToString(char *cp,VULONG v);
VUINT64 VSStringToLongLong(char *);
VULONG  VSStringToLong(unsigned char *cp);
VUSHORT VSStringToShort(unsigned char *cp);

#define VSCTYPE	                long
#define VSPTN_HANDLE            long
#define DEFAULT_MOVE_DIR        "SYS:VIRUS"
#define DEFAULT_PATTERN_DIR     "SYS:PUBLIC"
#define DEFAULT_PATTERN_FILE    "LPT$VPN."
#define DEFAULT_TEMP_DIR        "SYS:TMP"
#define EXPORT
#define PASCAL
#define TMVSAPI					EXPORT_C			

#define HANDLE			        int
#define MAX_PATH_LEN		    1024 

/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM          102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM          204800
