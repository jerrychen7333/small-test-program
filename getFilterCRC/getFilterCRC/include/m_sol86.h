/* $Date: 2001/2/19 PM 05:03:53$ */
/*
 * $Log: 
 *  8    NewScanEngine1.7         2001/2/19 PM 05:03:53Wayne_chu       Synchronize
 *       cheetah and cheeath_rel
 *  7    NewScanEngine1.6         2001/1/19 AM 11:57:55Wayne_chu       add
 *       VSGetBackupFileInfo and fix the problem of convert between Unicode and
 *       Ansicode.
 *  6    NewScanEngine1.5         1999/12/13 PM 06:27:54Joanna_tsai     for RAR,
 *       first using 0x100000, if fail to malloc such big space
 *       then using 0x10000
 *       move the maxwinsize definition to rar files
 *  5    NewScanEngine1.4         1999/11/2 PM 02:39:38Joanna_tsai    
 *       RAR_MAXWINSIZE should be 0x100000
 *       some platform like win16 cannot support it, so declare it as 0x10000
 *  4    NewScanEngine1.3         1999/5/24 PM 01:28:00Viking_ho       VCHAR should
 *       be defined separately by m_xxx.h
 *  3    NewScanEngine1.2         1998/11/9 PM 02:00:44Lily_ho         
 *  2    NewScanEngine1.1         1998/4/24 AM 11:38:12Lily_ho         
 *  1    NewScanEngine1.0         1998/4/23 AM 09:40:02Lily_ho         
 * $
 * 
 * 5     11/02/99 2:39p Joanna
 * RAR_MAXWINSIZE should be 0x100000
 * some platform like win16 cannot support it, so declare it as 0x10000
 * 
 * 4     99/05/24 1:27p Viking
 * VCHAR should be defined separately by m_xxx.h
 * 
 * 3     11/09/98 2:00p Lily
 * 
 * 8     11/06/98 4:08p Cliff_liang
 * define VSCTYPE(=long) 
 * 
 * 20    4/22/98 3:28p Cliff
 * Add definition of DECOMP_MIN_MEM and DECOMP_MAX_MEM
 * 
 * 19    3/23/98 2:24p Cliff
 * VULONG and VLONG should be defined separately by m_xxx.h
 * 
 * 18    3/20/98 2:59p Cliff
 * Add proejct name and Trend Copy right declaration.
 *
 * 16	 11/03/97 12:22p Cliff
 * Move internal use data to header
 *
 * 14	 10/30/97 11:44a Cliff
 * Intel(or Alpha) CPU should undef SWAPBYTE
 *
 * 12	 10/20/97 11:39a Cliff
 * redefine MACOPEN
 *
 * 11	 10/16/97 3:33p Cliff
 * Add MACCLOSE, MACOPEN,MACSEEK, and MACREAD macro
 *
 * 6	 9/23/97 3:05p Cliff
 * Remove VSRotateRight definition because it was used only by getoutvr.c
 *
 * 5	 8/15/97 11:59a Cliff
 * Rearrange the sequence
 *
 * 4	 7/31/97 12:46p Cliff
 * Define UNIX only if not defined.
 *
 * 3	 7/29/97 4:26p Cliff
 * Redefine CHSIZE
 *
 * 2	 7/24/97 12:44p Cliff
 * Remove ALTERNATIVE
 * Add vdd_off
 *
 * 2	 7/03/97 11:03a Rogerc
 * merged with Cliff's updated on 0702.
 *
 * 6	 4/29/97 1:48p Roger
 * after merge with Cliff's source code on 24/4.
 *
 * 3	 3/26/97 2:05p Roger
 * sync the source codes from Cliff 03/21/97.
 *
 * Revision 1.0  1996/07/31 14:38:16  cliff
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_sol86.h */
/* Description: */
/*	Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: SUN Solaris running on x86 */
typedef char          VCHAR;/* 1 byte */
typedef long			VLONG;	/* 4 byte */
typedef unsigned long	VULONG; /* 4 byte */
typedef long long int  	            VINT64;  /* 8 byte */
typedef unsigned long long int		VUINT64; /* 8 byte */

#define VSCTYPE long
#define VSPTN_HANDLE long
#define DEFAULT_MOVE_DIR	"/virus"
#define DEFAULT_PATTERN_DIR "/etc/iscan"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR	"/tmp"
#define EXPORT
#define TMVSAPI
#define HANDLE			int
#define MAX_PATH_LEN		1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PASCAL

#define VSStringToLongLong(cp)	(*(VUINT64 *)(cp))
#define VSStringToLong(cp) (*(VULONG *)(cp))
#define VSStringToShort(cp) (*(VUSHORT *)(cp))
#define VSLongLongToString(cp,x)	(*(VUINT64 *)(cp) = x)
#define VSLongToString(cp,x) (*(VULONG *)(cp) = x)
#define VSShortToString(cp,x) (*(VUSHORT *)(cp) = x)
