/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2005 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* vsres_fr.h */
/* Description: */
/* */
/**************************************************************************/

/** @file vsres_fr.h
 *    @brief Virus Scan API (VSAPI)
 */

/*

        Application Layer(Ex. VSResRead)
             |    |    |
             |    |  Filtration Layer(Ex. decompress.read)
             |    |    |
             |  Filtration Layer(Ex. cache.read)
             |    |    |
        Abstraction Layer(Ex. winbase.read)
             |    |    |
          System Layer(Ex. ReadFile)
             |    |    |
            Resource(Ex.c:\sample.zip)

在這裡第一次嚐試 :
  @ 跨平台的offsetof
  @ public/friend/private的包裝方式
  @ pointer與reference不同的 : reference要使用()
    STRUCTURE* ptr;   ptr->member = 0;
    STRUCTURE *ref; (*ref).member = 0;
  @ free pointer of 1st-member等於free structure

規則
  @各層向下保證參數在有效的定義域內,但不保證是否超出存取範圍

Application Layer : 這一層的HANDLE為一個private的結構pointer--VSRES*
  @ 對上提供方便的介面(有彈性的資料輸出入)
    對下減少實際io的次數
  @ 主要參數VSRES,為private的資料結構

Filtration Layer : 這一層的HANDLE為一個friend的結構pointer--VSRES_IOF*
  @ 在ApplicationLayer與AbstractionLayer間提供一個通透的轉換功能
    如cache,buffer,decode,encode,decompress...
  @ 這一層最少需提供pushin與close介面,未提供的介面被call時自動轉call下一層所對應的介面
  @ 主要參數VSRES_IOF,為public的資料結構,可以運用_this macro來找到這一層的private的Handle(註1)

Abstraction Layer : 這一層的HANDLE為一個friend的結構pointer--VSRES_IOF*
  @ 對上提供統一的介面來操作SystemLayer的Resource, 
    SystemLayer發生error時才return error code,否則應視為成功return 0
  @ 這一層最少需提供open與close介面,未提供的介面被call時自動return error
  @ 主要參數VSRES_IOF,為public的資料結構,可以運用_this macro來取得這一層的private的Handle(註1)

System Layer : 這一層的HANDLE為系統所提供(或是自已定義)的資料型別
  @ 由系統所提供(或是自已定義)用來操作Resource的介面

註1: _this macro是利用_offsetof macro來取得該層的private的Handle,
     若統無法支援_offsetof時可利用VSRES_IOF.para來存放該層的private的Handle
     或將VSRES_IOF放置於該層的private的Handle的第一個member

*/

#include "vsres.h"

#ifndef __VSRES_FR_H__
#define __VSRES_FR_H__


typedef struct _VSRES_IOF {
    int  (*close)(struct _VSRES_IOF* iof);
    int  (*seek) (struct _VSRES_IOF* iof, UINT64 ofs);
    int  (*size) (struct _VSRES_IOF* iof, UINT64 *rsze);
    int  (*read) (struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);
    int  (*write)(struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);
    void*   para;
    struct  _VSRES_IOF*  next;
} VSRES_IOF;

int _res_seek_success(struct _VSRES_IOF* iof, UINT64 ofs);
int _res_size_success(struct _VSRES_IOF* iof, UINT64 *rsze);
int _res_read_success(struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);
int _res_write_success(struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);

int _res_seek_error(struct _VSRES_IOF* iof, UINT64 ofs);
int _res_size_error(struct _VSRES_IOF* iof, UINT64 *rsze);
int _res_read_error(struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);
int _res_write_error(struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);

int _res_seek_bypass(struct _VSRES_IOF* iof, UINT64 ofs);
int _res_size_bypass(struct _VSRES_IOF* iof, UINT64 *rsze);
int _res_read_bypass(struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);
int _res_write_bypass(struct _VSRES_IOF* iof, void* buf, SINT32 len, SINT32 *rlen);


int VSNewRes(VSRES_IOF* iof, const char* alias, VSRES** rRes);
int VSResSeekForce(VSRES* Res, UINT64 ofs);
int VSResFilterPopOut(VSRES* Res, int (*close)(struct _VSRES_IOF* iof), VSRES_IOF** riof);
int VSResFilterPushIn(VSRES* Res, VSRES_IOF*   iof, UINT64* rofs);


#define VSRES_MAGICLEN_DIRA 0x80000000
#define VSRES_MAGICLEN_DIRW 0x80000001



#endif /* __VSRES_FR_H__ */
