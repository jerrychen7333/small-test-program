/*
 *****************************************************************************
 *
 *    (C) Copyright 1989-2005 Trend Micro, Inc.
 *    All Rights Reserved.
 *
 *    This program is an unpublished copyrighted work which is proprietary
 *    to Trend Micro, Inc. and contains confidential information that is not
 *    to be reproduced or disclosed to any other person or entity without
 *    prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *    WARNING:  Unauthorized reproduction of this program as well as
 *    unauthorized preparation of derivative works based upon the
 *    program or distribution of copies by sale, rental, lease or
 *    lending are violations of federal copyright laws and state trade
 *    secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* tmvs.h */
/* Description: */
/* */
/**************************************************************************/

/** @file tmvs.h
 *    @brief Virus Scan API (VSAPI)
 */

#ifndef __TMVS_H__
#define __TMVS_H__

#include "tmvsdef.h"

#ifdef __cplusplus
extern "C" {
#endif

/**    @mainpage Welcome to Trend Micro VSAPI Antivirus Development Kit

    <img src="tav00000.gif">
    This section consists of the following topics:
    <ul>
        <li>@ref Introduction
        <li>@ref VSAPI
    </ul>

    @section Introduction
    <p>Trend Micro, Inc. has been a global leader in network antivirus solutions since its founding in 1988. Trend Micro products currently protect the flow of information for over a third of the Fortune 500 companies, and for millions of customers around the world.
    <p>Trend Micro's innovative antivirus solutions include InterScan VirusWall, ScanMail for Exchange, ServerProtect, OfficeScan and PC-cillin, to name but a few.
    <p>At the heart of all Trend Micro products lays our proprietary scan engine, capable of quickly detecting all virus known to be "in the wild", or actively circulating, as well as file, Distributed Denial of Service (dDos) attacks, and mass mailing worms/viruses such as Nimda, CodeRed, ILOVEYOU, and KLEZ.E.
    <p>Data is checked for viruses using Trend Micro's 32-bit, multi-threading scan engine and a process called pattern matching. In addition, whereas most antivirus companies are limited to only one or two types of heuristic scanning, the Trend Micro scan engine employs half a sophisticated algorithms to allow the quick identification of previously unknown Macro, Script, Boot, DOS, and polymorphic viruses.
    <p>Likewise, whereas many antivirus companies support only Windows, the Trend Micro scan engine supports a number of operating systems, including all Windows, DOS, and a broad range of UNIX flavors.

    @section VSAPI
    <p>To interact with the scan engine, Trend Micro has developed a comprehensive library of programming functions called the <b>Virus Application Programming Language</b>, or VSAPI for short. VSAPI allows 3rd party developers (who have entered into a licensing agreement with Trend Micro), to build their own custom applications around the scan engine, virus pattern file, and also access web-based virus information services such as Trend Micro's on-line Virus Encyclopedia, which contains detailed description and analysis of some 5,000 viruses.
    <p>VSAPI, and the Trend Micro scan engine, allows developers to quickly create an antivirus application that controls:
        - Which network protocols, data-streams, or data sources to monitor and scan (i.e., files, email, Web traffic)
        - Which files to pass to the engine for scanning
        - What action to take upon detecting a virus
        - Who to notify upon detecting a virus (and how: email, SNMP trap, log, etc.)
        - How to update the virus pattern file automatically
        - How to write virus or other events to log
*/

/**    @defgroup Config        Configurations                                    */

/**    @defgroup Layer            Scan Layer
    @ingroup Config */

/**    @defgroup ExtNameList    Extension Name Table
    @ingroup Config
    <p>VSAPI has two name filters; one is for compressed files and the other one is for uncompressed files. 
    They filter out some files that do not need to be scanned, for the purpose of speeding up scanning. 
    The name filter (for normal files) is the default extension list of VSAPI. To control the behaviors 
    of the name filters, VSAPI has four significant extension-name tables in each VSC configuration: 
    - ProcessExtName
    - ProcessExcludeExtName
    - ArchProcessExtName
    - ArchProcessExcludeExtName
    <p>There are also two flags; ProcessAllFileFlag and ProcessAllFileInArcFlag, which are related to 
    the above-mentioned four tables. The diagram below shows the process flow:
 */

/**    @defgroup Flag            Flag
    @ingroup Config */

/**    @defgroup SetCallback    Callback Function
    @ingroup Config */
/**    @defgroup Callback        Callbacks
    @ingroup SetCallback */

/**    @defgroup DataType        Data Type                                        */
/**    @defgroup Pattern        Pattern File Processing                            */
/**    @defgroup General        General Purpose                                    */

/** @defgroup VSAPI            VSAPI - Not Classified                             */

/**    @defgroup Setting        Configurations - Settings/Path
    @ingroup VSAPI */

/**    @defgroup FileProc        File Processing
    @ingroup VSAPI */

/**    @defgroup Scan            Scan API
    @ingroup VSAPI */

/**    @defgroup Resource        Resource API
    @ingroup VSAPI */

/*==========================================================================*/
/** Take action on file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     action    override action setting
 *    @param[in]     file        file path
 *    @param[out]    NewPath    buffer to store new path if removed or moved
 *    @retval 0                action successed
 *    @retval -1                action failed
 *    @retval -2                failed to create uniq name for new file
 *    @retval -3                faild to delet original file
 */
TMVSAPI int PASCAL EXPORT VSActOnFile(VSCTYPE vsc,short action,char *file,char *NewPath);

/*==========================================================================*/
/* VSBackupResource() : Backup from resource */
/* Parameters: */
/*    VSCTYPE vsc: IN: virus scan context */
/*    VSHANDLE *pSrcResource: IN : resource to be backup */
/*    VSHANDLE *pDestResource: IN : resource for backup */
/*    VSBackupFileInfo: IN: backup information for backup header */
/* Return: */
/*    0: backup successes */
/*    OPEN_W_ERR: Clean file create failed */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    BUFFER_TOO_SHORT_ERR: insufficient buffer */
/*    PARA_ERR: invalid parameter */
/*    BREAK_ERR: VSAPI is forced to stop */
int PASCAL EXPORT VSBackupResource(VSCTYPE vsc, struct _VSHANDLE *pSrcResource, struct _VSHANDLE *pDestResource, VSBackupFileInfo *pBackupInfo);

/*==========================================================================*/
/** Add ext name into process list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewName    new ext name
 *    @retval >0                Add Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSAddProcessExtName(VSCTYPE vsc,char *NewName);

/*==========================================================================*/
/** Add ext name into process exclude list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewName    new ext name
 *    @retval >0                add Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSAddProcessExcludeExtName(VSCTYPE vsc,char *NewName);

/*==========================================================================*/
/** Add ext name into archive process list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewName    new ext name
 *    @retval >0                Add Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid Parameter 
 */
TMVSAPI int PASCAL EXPORT VSAddArchProcessExtName(VSCTYPE vsc,char *NewName);

/*==========================================================================*/
/** Add ext name into archive process exclude list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewName    new ext name
 *    @retval >0                Add Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSAddArchProcessExcludeExtName(VSCTYPE vsc,char *NewName);

/*==========================================================================*/
/**    Add a spyware name to exception list, by Solomon Lo
 *    @ingroup VSAPI
 *    @param[in]     vsc           virus scan context
 *    @param[in]     strSpyName    Pointer to name which add into
 *    @retval PARA_ERR             invalid parameter
 *    @retval BAD_VSC_ERR          invalid VSC handle
 *    @retval >=0                  How many spyware name in table
 *    @retval -2                   Spyware name has existed in the exception list
 *    @retval -3                   Spyware name is not spyware name in current pattern
 */
TMVSAPI int PASCAL EXPORT VSAddSpywareExcludeName(VSCTYPE vsc, const char* strSpyName);

/*==========================================================================*/
/** Clean a infected file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     name        file path to be cleaned
 *    @param[out]    bakname    full path of backuped file
 *    @param[in]     len        length of buffer for backuped file
 *    @param[out]    bakrc    
 *        - 0 -- backuped ok
 *        - other -- backup failed
 *    @retval 0                ok
 *    @retval -1                unable to clean
 *    @retval OPEN_W_ERR        Clean file create failed
 *    @retval READ_ERR        read error
 *    @retval WRITE_ERR        write error
 *    @retval PARA_ERR        invalid Parameter 
 */
TMVSAPI int PASCAL EXPORT VSCleanVirus(VSCTYPE vsc,VCHAR *name,VCHAR *bakname,int len,int *bakrc);

/*==========================================================================*/
/** Clean a infected file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context 
 *    @param[in]     name        file path to be cleaned 
 *    @param[out]    bakname    full path of backuped file 
 *    @param[in]     len        length of buffer for backuped file 
 *    @param[out]    bakrc    
 *        - 0 -- backuped ok
 *        - other -- backup failed 
 *    @retval 0                ok 
 *    @retval -1                unable to clean 
 *  @retval -20                no patern 
 *    @retval OPEN_W_ERR        Clean file create failed 
 *    @retval READ_ERR        read error 
 *    @retval WRITE_ERR        write error 
 *    @retval PARA_ERR        invalid Parameter 
 */
TMVSAPI int PASCAL EXPORT VSCleanEncryptedVirus(VSCTYPE vsc,VCHAR *name,VCHAR *bakname,int len,int *bakrc);

/*==========================================================================*/
/**    Remove a spyware name from exception list, by Solomon Lo
 *    @ingroup VSAPI
 *    @param[in]     vsc            virus scan context
 *    @param[in]     strSpyName    Pointer to name which add into
 *    @retval PARA_ERR        invalid parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 *    @retval >=0                how many spyware name left in table
 *    @retval -1                The specified name doesn't exist
 */
TMVSAPI int PASCAL EXPORT VSDeleteSpywareExcludeName(VSCTYPE vsc, const char* strSpyName);

/*==========================================================================*/
/** Clear all archive exclude ext name list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSClearArchProcessExcludeExtNameTable(VSCTYPE vsc);

/*==========================================================================*/
/** Clear all archive ext name table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSClearArchProcessExtNameTable(VSCTYPE vsc);

/*==========================================================================*/
/** Clear all exclude ext name list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid Parameter 
 */
TMVSAPI int PASCAL EXPORT VSClearProcessExcludeExtNameTable(VSCTYPE vsc);

/*==========================================================================*/
/**    Clear all ext name table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSClearProcessExtNameTable(VSCTYPE vsc);

/*==========================================================================*/
/**    Clear all spyware names in the exception list, by Solomon Lo
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSClearSpywareExcludeNameTable(VSCTYPE vsc);

/*==========================================================================*/
/**    Remove ext name from arch process exclude list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    ext name want to remove
 *    @retval >=0                Delete Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid Parameter 
 */
TMVSAPI int PASCAL EXPORT VSDeleteArchProcessExcludeExtName(VSCTYPE vsc,char *ExtName);

/*==========================================================================*/
/** Remove ext name from arch process list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    ext name want to remove
 *    @retval >=0                Delete Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSDeleteArchProcessExtName(VSCTYPE vsc,char *ExtName);

/*==========================================================================*/
/**    Remove ext name from process exclude list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    ext name want to remove
 *    @retval >=0                Add Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSDeleteProcessExcludeExtName(VSCTYPE vsc,char *ExtName);

/*==========================================================================*/
/**    Remove ext name from process list table
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    ext name want to remove
 *    @retval >=0                Add Ok, return how many ext name in the list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSDeleteProcessExtName(VSCTYPE vsc,char *ExtName);

/*==========================================================================*/
/**    Search for the newest pattern file
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     dirpath    where to search
 *    @param[in]     ptn        pattern file name
 *    @param[out]    found    pattern file name found
 *    @retval 0                ok
 *    @retval -1                no pattern file found
 *    @retval -2                failed to access pattern directory
 *    @retval -3                failed to locate currect directory
 *    @retval -4                can not get back to original directory
 *    @retval -5                pattern path is not a directory
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSFindNewestPattern(VSCTYPE vsc,char *dirpath,char *ptn,char *found);

/*==========================================================================*/
/** Return name in Arch process ext exclude list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     index    which name to return
 *    @param[out]    buf        buffer to keep name
 *    @retval 0                name copied
 *    @retval -1                index < 0 or index > number of names in list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetArchProcessExcludeExtName(VSCTYPE vsc,int index,char *buf);

/*==========================================================================*/
/**    Return name in Arch process ext list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     index    which name to return
 *    @param[out]    buf        buffer to keep name
 *    @retval 0                name copied
 *    @retval -1                index < 0 or index > number of names in list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetArchProcessExtName(VSCTYPE vsc,int index,char *buf);

/*==========================================================================*/
/**    Return how many name in archive ext exclude list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval >=0                number of names in list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetArchProcessExcludeExtNumber(VSCTYPE vsc);

/*==========================================================================*/
/**    Return how many name in archive ext list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval >=0                number of names in list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetArchProcessExtNumber(VSCTYPE vsc);

/*==========================================================================*/
/**    Return supported max archive ext name number
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval >0                maximum number of names can be stored
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetArchProcessExtTableSize(VSCTYPE vsc);

/*==========================================================================*/
/**    Return current pattern file version in memory
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[out]    Version    pattern file version number
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetCurrentPatternFileVersion(VSCTYPE vsc,int *Version);

/*==========================================================================*/
/**    Return decode enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        pointer to current VSC context
 *    @retval >=0                current setting
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDecodeFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return supported decompress layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @retval >0                current setting
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDecompressLayer(VSCTYPE vsc);

/*==========================================================================*/
/**    Return how many virus patterns in memory
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @retval >0                virus patterns in use
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDetectableVirusNumber(VSCTYPE vsc);

/*==========================================================================*/
/**    Get defult virus hospital directory path
 *    @ingroup VSAPI
 *    @param[out]    path        buffer to store path
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultHospitalPath(char *path);

/*==========================================================================*/
/**    Get defult pattern file name
 *    @ingroup Pattern
 *    @param[out]    file        buffer to store file
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultPatternFile(char *file);

/*==========================================================================*/
/**    Get defult pattern file path
 *    @ingroup Pattern
 *    @param[out]    path        buffer to store path
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultPatternPath(char *path);

/*==========================================================================*/
/**    Get defult temp directory path
 *    @ingroup VSAPI
 *    @param[out]    path        buffer to store file
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetDefaultTempPath(char *path);

TMVSAPI int PASCAL EXPORT VSGetDiskBootData(int nDrive,char *pData,int len);

/*==========================================================================*/
/**    Return expand enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        pointer to current VSC context
 *    @retval >=0                current setting
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetExpandLiteFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return extract enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        pointer to current VSC context
 *    @retval >=0                current setting
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetExtractArchiveFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Get current extract directory path
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    path    where to store extracted files
 *    @param[in]     cbLength    maximum length for log path
 *    @retval 0            ok
 *    @retval BUFFER_TOO_VSHORT_ERR buffer legnth too short for log path
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetExtractPath(VSCTYPE vsc,char *path,int cbLength);

/*==========================================================================*/
/**    Return generic macro scan enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        pointer to current VSC context
 *    @retval >=0                current setting
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanGenericMacroFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return pattern information
 *    @ingroup Pattern
 *    @obsoleted by VSGetVirusPatternInfoEx(), VSGetVirusPatternInformation()
 *    @param[in]     ptn_handle    pattern handle
 *    @param[out]    pVSPI        pattern information structure
 *    @retval 0:                ok, information is stored
 *    @retval PARA_ERR        invalid parameters
 */
TMVSAPI int PASCAL EXPORT VSGetVirusPatternInfo(VSPTN_HANDLE ptn_handle,VSPatternInfo *pVSPI);

/*==========================================================================*/
/**    Get current pattern directory path
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[out]    patternpath    store pattern directory path
 *    @param[in]     cbLength    maximum length for log path
 *    @retval 0            ok
 *    @retval BUFFER_TOO_VSHORT_ERR buffer legnth too short for log path
 *    @retval PARA_ERR        invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetPatternPath(VSCTYPE vsc,char *patternpath,int cbLength);

/*==========================================================================*/
/**    Return name in process ext exclude list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     index    which name to return
 *    @param[out]    buf        buffer to kepp name
 *    @retval 0                name copied
 *    @retval -1                index < 0 or index > number of names in list
 *    @retval PARA_ERR        invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessExcludeExtName(VSCTYPE vsc,int index,char *buf);

/*==========================================================================*/
/**    Get the spyware name at the specified index posotion, by Solomon Lo
 *    @ingroup ExtNameList
 *    @param[in]        vsc        virus scan context
 *    @param[in]        index    which name to return
 *    @param[out]        buf        buffer to keep name
 *    @retval PARA_ERR        invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 *    @retval 0                success
 *    @retval -1                index out of range
 *
 *    Note:
 *    The buffer size (counting the terminating null character) must be 
 *    greater than or equal to 17 bytes.
 */
TMVSAPI int PASCAL EXPORT VSGetSpywareExcludeName(VSCTYPE vsc, int index, char *buf);

/*==========================================================================*/
/**    Get the long spyware name at the specified index posotion
 *    @ingroup ExtNameList
 *    @param[in]        vsc     virus scan context
 *    @param[in]        index   which name to return
 *    @param[in,out]    len     pointer to buffer size in bytes
 *    @param[out]       buf     buffer to keep name
 *    @retval 0             success
 *    @retval -1            index out of range
 *    @retval BAD_VSC_ERR   invalid VSC handle
 *    @retval BUFFER_TOO_SHORT_ERR    string buffer too short
 *    @retval NO_MEM_ERR    out of memory
 *    @retval PARA_ERR      invalid Parameter
 *
 *    Note:
 *    When the return value is BUFFER_TOO_SHORT_ERR, get needed buffer 
 *    size (counting the terminating null character) from len.
 *    To get needed buffer size only, just set *len to zero and buf to NULL.
 */
TMVSAPI int PASCAL EXPORT VSGetSpywareExcludeNameEx(VSCTYPE vsc, int index, int *len, char *buf);

/*==========================================================================*/
/**    Return name in process ext list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     index    which name to return
 *    @param[out]    buf        buffer to kepp name
 *    @retval 0            name copied
 *    @retval -1            index < 0 or index > number of names in list
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessExtName(VSCTYPE vsc,int index,char *buf);

/*==========================================================================*/
/**    Return how many name in ext exclude list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            number of names in list
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessExcludeExtNumber(VSCTYPE vsc);

/*==========================================================================*/
/**    Return how many spyware name in exception list, by Solomon Lo
 *    @ingroup ExtNameList
 *    @param[in]        vsc        virus scan context
 *    @retval >=0                how many spyware name in table
 *    @retval PARA_ERR        invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSGetSpywareExcludeNumber(VSCTYPE vsc);

/*==========================================================================*/
/**    Return how many name in ext list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            number of names in list
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessExtNumber(VSCTYPE vsc);

/*==========================================================================*/
/**    Return supported max ext name number
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval >0            maximum number of names can be stored
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessExtTableSize(VSCTYPE vsc);

/*==========================================================================*/
/**    Return if process all files
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessAllFileFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return if process all file in archive
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessAllFileInArcFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return process all sub dir flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetProcessAllSubDirFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return Boot/Partition need check flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanBPFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return scan macro function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanMacroFlag(VSCTYPE vsc);



/*==========================================================================*/
/**    Return strip macro function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetStripMacroFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return memory need check flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanMemoryFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return current Scan task data structure pointer
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    stp        return pointer to current SCANTASK
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanTask(VSCTYPE vsc,SCANTASK **stp);

/*==========================================================================*/
/**    Copy current scan task status into buffer
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    st        buffer to keep data
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanTaskStatus(VSCTYPE vsc,SCANTASK *st);

/*==========================================================================*/
/**    Return softmice function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetSoftMiceFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Get temp directory path
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    TempPath    temp directory path
 *    @param[in]     cbLength    maximum length for log path
 *    @retval 0            ok
 *    @retval BUFFER_TOO_VSHORT_ERR buffer legnth too short for log path
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetTempPath(VSCTYPE vsc,char *TempPath,int cbLength);

/*==========================================================================*/
/**    Get current setting of what to do to a infected file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @retval >0            old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetVirusAction(VSCTYPE vsc);

/*==========================================================================*/
/**    Get virus hospital directory path
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    path        buffer to store hospital directory path
 *    @param[in]     cbLength    maximum length for log path
 *    @retval 0            ok
 *    @retval BUFFER_TOO_VSHORT_ERR buffer legnth too short for log path
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetVirusHospitalPath(VSCTYPE vsc,char *path,int cbLength);

/*==========================================================================*/
/**    Init the virus scan system
 *    @ingroup VSAPI
 *    @param[in]     CallerID    Identification for calling thread/process
 *    @param[in]     LogID    string ID for log
 *    @param[in]     OldCfgSection use same conf of this session if non 0
 *    @param[out]    NewSection    return the handle to new session
 *    @retval 0            ok
 *    @retval -1            read pattern error
 *    @retval -2            Too many VSC section
 *    @retval -20            pattern file not found
 *    @retval READ_ERR    pattern file read error
 *    @retval NO_MEM_ERR    out of memory
 *    @retval PARA_ERR    parameter error
 */
TMVSAPI int PASCAL EXPORT VSInit(long CallerID,char *LogID,VSCTYPE OldCfgSection,VSCTYPE *NewSection);

/*==========================================================================*/
/**    Terminate VS session
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSQuit(VSCTYPE vsc);

/*==========================================================================*/
/**    Read in boot sector/partition
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    DataBlock    data block
 *    @param[in]     BlockSize    block size
 *    @param[in]     ReadFlag    read which part
 *    @retval 0            ok
 *    @retval -1            not supported
 *    @retval READ_ERR    read error
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSReadBP(VSCTYPE vsc,char *DataBlock,int BlockSize,int ReadFlag);

/*==========================================================================*/
/**    Read hard disk physical sector
 *    @ingroup VSAPI
 *    @param[in]     nDrive    drive no. 0x80 - 0x8a : read partition
 *    @param[out]    pData    data
 *    @param[in]     len
 *    @param[in]     number
 *    @retval 0            OK
 *    @retval PARA_ERR    parameter error
 *    @retval READ_ERR    read error
 */
TMVSAPI int PASCAL EXPORT VSReadPhysicalSector(int nDrive,char *pData,int len,int number);

/*==========================================================================*/
/**    Read in virus pattern
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     OldSection    reference section
 *    @param[in]     ptnfile    pattern file name
 *    @param[out]    ptn_handle    buffer to keep new pattern data handle
 *    @retval 0      ok
 *    @retval -1     old session has corrupted pattern structure
 *    @retval -2     old session has no pattern structure
 *    @retval -3     invalid pattern file
 *    @retval -4     pattern not found
 *    @retval -5     fail to get current working directory
 *    @retval -6     fail to change working directory back to the original one
 *    @retval OPEN_R_ERR        fail to open pattern file
 *    @retval READ_ERR          fail to read pattern file
 *	  @retval PTN_UPDATE_ERR    fail to update pattern file
 *    @retval NO_MEM_ERR        out of memory
 *    @retval PARA_ERR          invalid parameter(s)
 *    @retval BAD_VSC_ERR       invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSReadVirusPattern(VSCTYPE vsc,VSCTYPE OldSection,char *ptnfile,VSPTN_HANDLE *ptn_handle);

/*==========================================================================*/
/**    Reset scan statistic counter
 *    @ingroup VSAPI
 *    @param[in]     st        virus scan task
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSResetScanCounter(SCANTASK *st);

/*==========================================================================*/
/**    Scan Boot/Partition data block
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     DataBlock    data block
 *    @param[in]     BlockSize    block size
 *    @param[out]    pBPMInfo        contains virus informations
 *    @retval 0            ok
 *    @retval -1            virus found
 *    @retval -2            no boot virus pattern in memory
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSScanBP(VSCTYPE vsc,char *DataBlock,int BlockSize,BPMINFO *pBPMInfo);

/*==========================================================================*/
/**    Scan data block for known virus
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     data        data buffer to scan
 *    @param[in]     len        data buffer length
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSScanDataBlock(VSCTYPE vsc,char *data,VUSHORT len);

/*==========================================================================*/
/**    Scan directory for virus
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     dirpath    directory path to scan
 *    @param[in]     BeforeScan    before scan call back function
 *    @param[in]     AfterScan    after scan call back function
 *    @param[in]     para        user defined parameter passed to BeforeScan and AfterScan
 *    @retval 0            ok
 *    @retval -1            stop by service function
 *    @retval -2            failed to access target directory
 *    @retval -3            failed to locate currect directory
 *    @retval -4            can not get back to original directory
 *    @retval -5            target path is not a directory
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSScanDir(VSCTYPE vsc,char *dirpath,VS_SERVICE *BeforeScan,VS_SERVICE *AfterScan,void *para);

/*==========================================================================*/
/**    Scan a file for known virus
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     file        file for scan
 *    @param[out]    VirusNumber    virus number if infected
 *    @param[out]    VirusName    virus name if infected
 *    @retval 0            file is clean
 *    @retval >0            file is infected, return virus number
 *    @retval -1            failed to scan
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSScanFile(VSCTYPE vsc,char *file,int *VirusNumber,char *VirusName);

/*==========================================================================*/
/**    Scan a handle for known virus
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     handle    resource handle for scan
 *    @param[out]    VirusNumber    virus number if infected
 *    @param[out]    VirusName    virus name if infected
 *    @retval 0            file is clean
 *    @retval >0            file is infected, return virus number
 *    @retval -1            failed to scan
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSScanFileFD(VSCTYPE vsc,VSHANDLE *handle,int *VirusNumber,char *VirusName);

/*==========================================================================*/
/**    Scan memory
 *    @ingroup Scan
 *    @param[in]     vsc            virus scan context
 *    @param[in]     BreakSize    where to call call-back function
 *    @param[out]    pBPMInfo        contain virus informations
 *    @param[in]     BreakFunc    call back function
 *    @param[in,out] para            application's informations
 *    @retval 0            ok
 *    @retval 1            found virus
 *    @retval -1            break by call back
 *    @retval PARA_ERR    invalid Parameter
 *    @see VS_BREAK_FUNC
 */
TMVSAPI int PASCAL EXPORT VSScanMemory(VSCTYPE vsc,int BreakSize,BPMINFO *pBPMInfo,VS_BREAK_FUNC BreakFunc,void *para);

/*==========================================================================*/
/**    Scan resource for virus
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     res        resource to scan
 *    @param[in]     para        user defined parameter passed to ProcFunc
 *    @retval >=0            ok, return code from ProcFunc()
 *    @retval SKIP_ERR    error, but continue
 *    @retval BREAK_ERR    error, break
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSScanResource(VSCTYPE vsc,RESOURCE *res,void *para);

/*==========================================================================*/
/**    Set decode enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDecodeFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Change supported decompress layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @param[in]     DLayer    new setting
 *    @retval >0            Old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDecompressLayer(VSCTYPE vsc,int DLayer);

/*==========================================================================*/
/**    Set default virus hospital direcotry path
 *    @ingroup VSAPI
 *    @param[in]     path        new default virus hospital directory path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDefaultHospitalPath(char *path);

/*==========================================================================*/
/* VSSetDefaultPatternFile() : set default pattern file name
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     filename    default pattern file name
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDefaultPatternFile(char *filename);

/*==========================================================================*/
/**    Set default pattern directory path
 *    @ingroup Pattern
 *    @param[in]     path        new default pattern directory path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDefaultPatternPath(char *path);

/*==========================================================================*/
/**    Set defualt temp directory path
 *    @ingroup VSAPI
 *    @param[in]     path        new default temp directory path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetDefaultTempPath(char *path);

/*==========================================================================*/
/**    Set expand enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetExpandLiteFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set extract enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok,return old setting
 *    @retval -1            corrupt handle
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetExtractArchiveFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set extract directory path
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     path        where to store extracted files
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetExtractPath(VSCTYPE vsc,char *path);

/*==========================================================================*/
/**    Set current pattern directory path
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     patternpath    new pattern directory path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetPatternPath(VSCTYPE vsc,char *patternpath);

/*==========================================================================*/
/**    Set the function to call before extract file from archive
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Function    function to call
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetPreExtractArchFunc(VSCTYPE vsc,VS_PRE_EXTRACT_ARCH_FUNC *Function);

/*==========================================================================*/
/**    Set if process all files
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetProcessAllFileFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set process all file in archive flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetProcessAllFileInArcFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set process all sub dir flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetProcessAllSubDirFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set user defined call back function to VSProcessFile()
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Function    process function
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetProcessFileCallBackFunc(VSCTYPE vsc,VS_PROCESS_FILE_CALLBACK_FUNC *Function);

TMVSAPI int PASCAL EXPORT VSSetAdvFileInfoCallBackFunc(VSCTYPE vsc,VS_ADV_FILE_INFO_CALLBACK_FUNC *Function); /*[AFI_PE]*/

/*==========================================================================*/
/**    Set user defined feedback call back function to VSProcessFile()
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Function    process function
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetFeedbackCallBackFunc(VSCTYPE vsc,VS_FEEDBACK_CALLBACK_FUNC *Function);

/*==========================================================================*/
/**    Set user defined call back function for OLE Embed File
 *    @ingroup SetCallback
 *    @param[in]     vsc      virus scan context
 *    @param[in]     Function User CallBack function
 *    @param[in]     Para     User CallBack Para
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetOleEmbedCallBackFunc(VSCTYPE vsc, VS_OLE_EMBED_CALLBACK_FUNC *Function);

/*==========================================================================*/
/**    Set user defined function to ask for action on infected file. This will be called when action set to VC_ACT_ASK
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Function    process function
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetAskActionFunc(VSCTYPE vsc,VS_ASK_ACTION_FUNC *Function);

/*==========================================================================*/
/**    Set the function to call after extract file from archive
 *    @ingroup SetCallback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Function    function to call
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetPostExtractArchFunc(VSCTYPE vsc,VS_POST_EXTRACT_ARCH_FUNC *Function);

/*==========================================================================*/
/**    Set Boot/Partition need check flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetScanBPFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set scan generic macro function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetScanGenericMacroFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set scan macro function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetScanMacroFlag(VSCTYPE vsc,int NewSetting);


/*==========================================================================*/
/**    Set strip macro function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetStripMacroFlag(VSCTYPE vsc, int NewSetting);

/*==========================================================================*/
/**    Set memory need check flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetScanMemoryFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set softmice function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetSoftMiceFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Set temp directory path
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     TempPath    new temp directory path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetTempPath(VSCTYPE vsc,char *TempPath);

/*==========================================================================*/
/**    Set what to do to a infected file
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     action    what to do
 *    @retval >0: old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetVirusAction(VSCTYPE vsc,int action);

/*==========================================================================*/
/**    Set where to move virus file to
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Path        hospital Path
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetVirusHospitalPath(VSCTYPE vsc,char *Path);


/*==========================================================================*/
/**    Scan a file for known virus after the file name pass the file name filter
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     file        file for scan
 *    @param[in]     para        user defined parameter
 *    @retval 0            file is clean, or the file failed the file name filter and has not been scanned
 *    @retval >0            file is infected, return virus number
 *    @retval -1            failed to scan
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSVirusScanFile(VSCTYPE vsc,char *file,void *para);

/*==========================================================================*/
/**    Scan a file for known virus with out applying the file name filter
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     file        file for scan
 *    @param[in]     para        user defined parameter
 *    @retval 0            file is clean
 *    @retval >0            file is infected, return virus number
 *    @retval -1            failed to scan
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSVirusScanFileWithoutFNFilter(VSCTYPE vsc,char *file,void *para);

/* function added by Roger */

/*======================================================*/
/**    Scan boot virus for drive
 *    @ingroup Scan
 *    @param[in]     vsc        virus scan context
 *    @param[in]     nDrive    drive no.
 *        - 0-25 : write boot sector
 *        - 0x80 - 0x8a : write partition
 *    @param pBPMInfo        informations about virus
 *    @retval 0            OK
 *    @retval PARA_ERR    parameter error
 */
TMVSAPI int  PASCAL EXPORT VSScanBootVirus(VSCTYPE vsc,int nDrive,BPMINFO *pBPMInfo);

/*======================================================*/
/**    Clean boot virus in drive
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     nDrive    drive no.
 *        - 0-25 : write boot sector
 *        - 0x80 - 0x8a : write partition
 *    @param pBPMInfo        informations about virus
 *    @retval 0            OK
 *    @retval PARA_ERR    parameter error
 */
TMVSAPI int  PASCAL EXPORT VSCleanBootVirus(VSCTYPE vsc,int nDrive,BPMINFO *pBPMInfo);

/*==========================================================================*/
/**    Return scan java function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanJavaFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set scan java function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetScanJavaFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Get a bunch of virus information
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param Index        start index in the virus info group
 *    @param pSI            signature data to store
 *    @param Total        number of virus info need to get
 *    @retval >=0            number of signature got
 *    @retval PARA_ERR    parameters error
 */
TMVSAPI int PASCAL EXPORT VSGetVirusInfo(VSCTYPE vsc, int Index, VSVirusInfo pSI[],int Total);

/*==========================================================================*/
/**    Get Virus Info. by Virus Name
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     VirusName    virus name
 *    @param[in]     Command    function command (See. VSVPN_?)
 *    @param[in]     in        input extra data buffer (See. VSVPN_?)
 *    @param[out]    out        output data buffer (See. VSVPN_?)
 *    @retval >=0            Number of Data can be Get
 *  @retval <0            VSAPI Standard Error
 */
TMVSAPI int PASCAL EXPORT VSGetVirusPropertyByName(VSCTYPE vsc, char* VirusName, VULONG Command, void* in, void* out);

/**    Get Pattern Info
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Command    function command (See. VSPP_?)
 *    @param[in]     in        input extra data buffer (See. VSVPN_?)
 *    @param[out]    out         output data buffer (See. VSPP_?)
 *    @retval >=0            Number of Data can be Get
 *    @retval <0            VSAPI Standard Error
 */
TMVSAPI int PASCAL EXPORT VSGetPatternProperty(VSCTYPE vsc, VULONG Command, void* in, void* out);

/*==========================================================================*/
/**    Return scan lite compressed file function enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScanLiteFileFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return set scan lite compressed file function flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @return                old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetScanLiteFileFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Search the specific extension name in process exclude list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    extension name to check
 *    @retval >=0            index found
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSearchProcessExcludeExtName(VSCTYPE vsc, char *ExtName);

/*==========================================================================*/
/**    Check whether the spyware name exists in the exception list, by Solomon Lo
 *    @ingroup ExtNameList
 *    @param[in]     vsc            virus scan context
 *    @param[in]     strSpyName    spyware name to check
 *    @retval PARA_ERR        invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 *    @retval >=0                found, return index
 *    @retval -1                not found
 */
TMVSAPI int PASCAL EXPORT VSSearchSpywareExcludeName(VSCTYPE vsc, char *strSpyName);

/*==========================================================================*/
/**    Search the specific extension name in process list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    extension name to check
 *    @retval >=0            index found
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSearchProcessExtName(VSCTYPE vsc, char *ExtName);

/*==========================================================================*/
/**    Search the specific extension name in archive process exclude list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    extension name to check
 *    @retval >=0            index found
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSearchArchProcessExcludeExtName(VSCTYPE vsc, char *ExtName);

/*==========================================================================*/
/**    Search the specific extension name in archive process list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @param[in]     ExtName    extension name to check
 *    @retval >=0            index found
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSearchArchProcessExtName(VSCTYPE vsc, char *ExtName);

/**    Get the virus information
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in,out] VirusID    virus number
 *    @param[in,out] index    index of name table
 *    @param[in,out] VirusName virus name
 *    @retval 0            OK
 *    @retval -1            search not found
 */
TMVSAPI int PASCAL EXPORT _VSIScanGetVirusInfo(VSCTYPE vsc, VUSHORT *VirusID, VUSHORT *index, char *VirusName);

/**    Get the virus information
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in,out] VirusID     virus number
 *    @param[in,out] index     index of name table
 *    @param[in,out] VirusName virus name
 *    @retval 0            OK
 *    @retval -1            search not found
 */
TMVSAPI int PASCAL EXPORT _VSIScanGetVirusInfoEx(VSCTYPE vsc, VUSHORT *VirusID, VULONG *index, char *VirusName);

/**    Get a bunch of virus information
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Index
 *    @param         pSI        signature data to store
 *    @param[in]     Total
 *    @retval >=0            number of signature got
 *    @retval PARA_ERR    parameters error
 */
TMVSAPI int PASCAL EXPORT _VSIScanEnumSignature(VSCTYPE vsc, int Index, char *pSI,int Total);

/**    Get the virus information
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Index    index of name table
 *    @param Flag            On/Off flag
 *    @retval 0            OK
 *    @retval -1            out of index
 */
TMVSAPI int PASCAL EXPORT _VSIScanEnableSignature(VSCTYPE vsc, VUSHORT Index, VSHORT Flag);

/*==========================================================================*/
/**    Change supported OLE layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @param[in]     DLayer    new setting
 *    @retval >0            Old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetOleEmbedScanLayer(VSCTYPE vsc,int DLayer);

/*==========================================================================*/
/**    Return supported Ole layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @retval >0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetOleEmbedScanLayer(VSCTYPE vsc);

/*==========================================================================*/
/**    Change supported RTF layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @param[in]     DLayer    new setting
 *    @retval >0            Old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetRTFScanLayer(VSCTYPE vsc,int DLayer);

/*==========================================================================*/
/**    Return supported RTF layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @retval >0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetRTFScanLayer(VSCTYPE vsc);

/*==========================================================================*/
/**    Change supported Msg layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @param[in]     DLayer    new setting
 *    @retval >0            Old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetMsgScanLayer(VSCTYPE vsc,int DLayer);

/*==========================================================================*/
/**    Return supported Msg layer number
 *    @ingroup Layer
 *    @param[in]     vsc        virus scan context
 *    @retval >0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetMsgScanLayer(VSCTYPE vsc);

/*==========================================================================*/
/**    Set the count of file to be decompressed
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewCount    new setting to set the File count limit
 *    @param[out]    OldCount
 *    @retval 0            Successful
 *    @retval PARA_ERR    invalid parameter
 *    @retval BAD_VSC_ERR    invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSSetExtractFileCountLimit(VSCTYPE vsc, VULONG NewCount, VULONG *OldCount);

/*==========================================================================*/
/**    Return current limit of the count of files to be decompressed
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    limit
 *    @retval 0            Successful
 *    @retval PARA_ERR    invalid parameter
 *  @retval BAD_VSC_ERR    invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSGetExtractFileCountLimit(VSCTYPE vsc, VULONG *limit);

/*==========================================================================*/
/**    Set the decompress ratio limit of file to be decompressed
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewRatio    new setting to set the decompress ratio limit
 *    @param[out]    OldRatio
 *    @retval 0            Successful
 *    @retval PARA_ERR    invalid parameter
 *    @retval BAD_VSC_ERR    invalid VSC handle
 *    @note ratio setting == 100 means compress ratio = 1-(1/100) = 99%
 */
TMVSAPI int PASCAL EXPORT VSSetExtractFileRatioLimit(VSCTYPE vsc, VULONG NewRatio, VULONG *OldRatio);

/*==========================================================================*/
/**    Return current decompress file ratio limit
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    limit
 *    @retval 0            Successful
 *    @retval PARA_ERR    invalid parameter
 *    @retval BAD_VSC_ERR    invalid VSC handle
 *    @note ratio setting == 100 means compress ratio = 1-(1/100) = 99%
 */
TMVSAPI int PASCAL EXPORT VSGetExtractFileRatioLimit(VSCTYPE vsc,VULONG *limit);

/*==========================================================================*/
/**    Validate pattern
 *    @ingroup Pattern
 *    @param[in]     FileName    file name of pattern
 *    @retval >0            pattern version
 *    @retval PATTERN_CRC_ERR    something wrong with pattern
 *    @retval PATTERN_VERSION_ERR    pattern number incorrect
 *    @retval OLD_PATTERN_ERR    pattern too old error
 *    @retval OPEN_R_ERR    pattern file openning error
 *    @retval READ_ERR    pattern file reading error
 *    @retval PARA_ERR    pattern file name is null
 */
TMVSAPI int PASCAL EXPORT VSCheckPatternFile(char *FileName);

/*==========================================================================*/
/**    Get pattern list
 *    @ingroup Pattern
 *    @param[in]     Path        the path that stores patterns
 *    @param[in]     FileNameMask    pattern file name mask
 *    @param[in]     PatternList    pattern list
 *    @param[in,out] ElementNum    specify and return the buffer size (total size / sizeof(PATTERN_INFO))
 *    @param[in]     CheckPattern    check pattern file. 0: do not check pattern
 *    @retval >0            
 *        - if ElementNum = 0, return pattern count.
 *        - if ElementNum != 0, return 0 if get pattern list ok
 *    @retval PATTERN_NOT_FOUND_ERR    no pattern found
 *    @retval PARA_ERR    parameter error
 */
TMVSAPI int PASCAL EXPORT VSGetPatternList(char *Path, char *FileNameMask, VSPtnCheckInfo *PatternList, int *ElementNum, int CheckPattern);

/*==========================================================================*/
/**    Get last pattern
 *    @ingroup Pattern
 *    @param[in]     Path            the path that stores patterns
 *    @param[in]     FileNameMask    pattern file name mask
 *    @param[out]    PatternInfo    the last pattern information
 *    @param[in]     CheckPattern    check pattern file. 0: do not check pattern
 *    @retval 0            pattern is legal
 *    @retval PATTERN_NOT_FOUND_ERR    no pattern found
 *    @retval PARA_ERR    parameter error
 */
TMVSAPI int PASCAL EXPORT VSGetLastPattern(char *Path, char *FileNameMask, VSPtnCheckInfo *PatternInfo, int CheckPattern);

/*==========================================================================*/
/**    Delete unused pattern files
 *    @ingroup Pattern
 *    @param[in]     Path            the path that stores patterns
 *    @param[in]     FileNameMask    pattern file name mask
 *    @param[in]     KeepNumber    how many pattern files to keep
 *    @retval 0            delete pattern files ok
 *    @retval PATTERN_NOT_FOUND_ERR    no pattern found
 *    @retval NO_MEM_ERR    memory allocate error
 *    @retval PARA_ERR    pattern file name is null
 *    @retval REMOVE_PATTERN_ERR    can not remove pattern files
 */
TMVSAPI int PASCAL EXPORT VSDeleteUnusedPattern(char *Path, char *FileNameMask, int KeepNumber);

/*==========================================================================*/
/**    Convert pattern version to string
 *    @ingroup Pattern
 *    @param[in]     Version    pattern version
 *    @param[out]    String    get the string of pattern version
 *    @param[in]     Len        string buffer length
 *    @retval 0            convert ok
 *    @retval BUFFER_TOO_SHORT_ERR    string buffer too short
 *    @retval PARA_ERR    parameter error
 */
TMVSAPI int PASCAL EXPORT VSPatternVersionToString(int Version, char *String, int Len);

/*==========================================================================*/
/**    Convert string to pattern version
 *    @ingroup Pattern
 *    @param[in]     String    the string of pattern version
 *    @retval >0            pattern version
 *    @retval -1            string format error
 *    @retval PARA_ERR    parameter error
 */
TMVSAPI int PASCAL EXPORT VSStringToPatternVersion(char *String);

/*==========================================================================*/
/**    Set ScriptTrap enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting
 *    @retval 0,1            set ok, return old setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSSetScriptTrapFlag(VSCTYPE vsc,int NewSetting);

/*==========================================================================*/
/**    Return ScriptTrap enable/disable flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            current setting
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetScriptTrapFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Set actions to be encoded
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     EncodeAction    new setting to define the actions to be encoded
 *    @retval >=0            the original setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetEncodeAction(VSCTYPE vsc, VUSHORT EncodeAction);

/*==========================================================================*/
/**    Return ScriptTrap enable/disable flag
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            the current setting
 *    @retval PARA_ERR    invalid parameter
 */
VUSHORT PASCAL EXPORT VSGetEncodeAction(VSCTYPE vsc);

/*==========================================================================*/
/**    Set Active Scan flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @param[in]     NewSetting    new setting to turn on or turn off Active Scan
 *    @retval >=0            the original setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSSetActiveScanFlag(VSCTYPE vsc, int NewSetting);

/*==========================================================================*/
/**    Return Active Scan flag
 *    @ingroup Flag
 *    @param[in]     vsc        virus scan context
 *    @retval >=0            the current setting
 *    @retval PARA_ERR    invalid parameter
 */
TMVSAPI int PASCAL EXPORT VSGetActiveScanFlag(VSCTYPE vsc);

/*==========================================================================*/
/**    Return extra pattern information
 *    @ingroup Pattern
 *    @obsoleted by VSGetVirusPatternInformation()
 *    @param[in]     ptn_handle    pattern handle
 *    @param[out]    pVSPIEx    extra pattern information structure
 *    @retval 0            ok, information is stored
 *    @retval PARA_ERR    invalid parameters
 */
TMVSAPI int PASCAL EXPORT VSGetVirusPatternInfoEx(VSPTN_HANDLE ptn_handle, VSPatternInfoEx *pVSPIEx);

/*==========================================================================*/
/**    Return extra pattern information
 *    @ingroup Pattern
 *    @param[in]     ptn_handle pattern handle
 *    @param[out]    pVSPIEx    extra pattern information structure
 *    @retval 0            ok, information is stored
 *    @retval PARA_ERR    invalid parameters
 *    @note VSPatternInformation::dwSize should be initialed to sizeof(VSPatternInformation), before every call.
 */
TMVSAPI int PASCAL EXPORT VSGetVirusPatternInformation(VSPTN_HANDLE ptn_handle, VSPatternInformation *pVSPIEx);


/*==========================================================================*/
/**    Return extra current pattern version from vsc
 *    @ingroup Pattern
 *    @param[in]     vsc        virus scan context
 *    @param[out]    InternalVer    extra pattern file version number
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 */
TMVSAPI int PASCAL EXPORT VSGetCurrentPatternFileInternalVersion(VSCTYPE vsc, unsigned long *InternalVer);

/*==========================================================================*/
/**    Return extra current pattern version from a file version in memory
 *    @ingroup Pattern
 *    @param[in]     FileName    pattern file name
 *    @param[out]    InternalVer    extra pattern file version number
 *    @param[out]    PtnVer    pattern version of pattern extension name
 *    @retval 0            ok
 *    @retval -1            pattern contain no version info
 *    @retval -2            pattern version mismatch
 *    @retval -3            section corrupted
 *    @retval NO_MEM_ERR    memory not enough
 *    @retval ACCESS_ERR    I/O error
 *    @retval OPEN_R_ERR    can not open pattern file
 *    @retval    PARA_ERR    invalid Parameter or invalid pattern file name
 */
TMVSAPI int PASCAL EXPORT VSGetPatternInternalVersion(char *FileName, unsigned long *InternalVer, unsigned short *PtnVer);


/*==========================================================================*/
/**    Get multi-pattern files list
 *    @ingroup Pattern
 *    @param[in]     patternName    pattern file name (recommand: full path)
 *    @param[in]     offset        pattern offset (usual : 0)
 *    @param[out]    patternList    pattern lists
 *    @param[in]     verify        verify flag (0 or 1)
 *    @retval 0            ok
 *    @retval -1            pattern contain no version info
 *    @retval -2            pattern version mismatch
 *    @retval -3            section corrupted
 *    @retval NO_MEM_ERR    memory not enough
 *    @retval ACCESS_ERR    I/O error
 *    @retval OPEN_R_ERR    can not open pattern file
 *    @retval PARA_ERR    invalid Parameter or invalid pattern file name
 *    @note    PATTERNFILE_LIST::listSize should be initialed to sizeof(PATTERNFILE_LIST), before every call.
 *    @note    PATTERNFILE_LIST::itemSize should be initialed to sizeof(PATTERNFILE_ITEM), before every call.
 */
TMVSAPI int PASCAL EXPORT VSGetMultiPatternFilesInfo(VUCHAR *patternName,long offset, PATTERNFILE_LIST *patternList,int verify);



/**    To get the Config Value
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     CfgID    Config ID (See. VSCFG_?)
 *    @param[out]    Value    receive Config Value
 *    @retval 0            Successful
 *    @retval PARA_ERR    invalid parameter
 *    @retval BAD_VSC_ERR    invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSGetConfig(VSCTYPE vsc , VULONG CfgID, VULONG *Value);

/**    To set the Config Value
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     CfgID    Config ID (See. VSCFG_?)
 *    @param[in]     NewValue    New Config Value
 *    @param[out]    OldValue    (Not necessary)receive Old Config Value
 *    @retval 0            Successful
 *    @retval PARA_ERR    invalid parameter
 *    @retval BAD_VSC_ERR    invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSSetConfig(VSCTYPE vsc , VULONG CfgID, VULONG NewValue, VULONG* OldValue);

/**    To get the Config Value
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     CfgID      Config ID (See. VSCFG_?)
 *    @param[out]    CfgData    point to a Config Data Structure
 *    @return                   VSAPI standard return code
 */
TMVSAPI int PASCAL EXPORT VSGetConfigEx(VSCTYPE vsc , VULONG CfgID, void* CfgData);

/**    To set the Config Value
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     CfgID      Config ID (See. VSCFG_?)
 *    @param[in]     NewCfgData point to New Config Data Structure
 *    @param[out]    OldCfgData (Not necessary)receive Old Config Data
 *    @return                   VSAPI standard return code
 */
TMVSAPI int PASCAL EXPORT VSSetConfigEx(VSCTYPE vsc , VULONG CfgID, void* NewCfgData, void* OldCfgData);

/*==========================================================================*/
/**    To load control pattern (CPR)
 *    @ingroup pattern
 *    @param[in]    vsc                 virus scan context
 *    @param[in]    Command             command ID (see VSCPR_?) 
 *    @param[in]    PtnInfo             CPR pattern information
 *    @retval       0                   successful
 *    @retval       -1                  pattern file corrupt
 *    @retval       OPEN_R_ERR          fail to open control pattern file
 *    @retval       READ_ERR            fail to read control pattern file
 *    @retval       NO_MEM_ERR          out of memory
 *    @retval       PARA_ERR            invalid parameter(s)
 *    @retval       BAD_VSC_ERR         invalid VSC handle
 *    @retval       DISABLE_ERR         disable to load control pattern because no master pattern is loaded
 *    @retval       PTN_UPDATE_ERR      fail to update pattern and no valid pattern available
 *    @retval       NOT_SUPPORTED_ERR   this command is not supported
*/
TMVSAPI int PASCAL EXPORT VSReadControlPattern(VSCTYPE vsc, VULONG Command, VSPtnCheckInfoEx *PtnInfo);

/*==========================================================================*/
/**    To scan the target buffer with shell code scanner
*    @ingroup VSAPI
*    @param[in]    pDataBuf            target raw binary data to scan. 
*    @param[in]    nDataLen            total raw binary data length to scan. 
*    @retval       0                   no shell code content in the raw binary data.
*    @retval       1                   found shell code content.
*    Note:
*    1. The length of raw binary data must be greater than or equal to 64 bytes.
*    2. No need to pad the terminate char (NULL) to the input data buffer.
*/
TMVSAPI int PASCAL EXPORT VSScanShellCode(VSCTYPE vsc, const VUCHAR *pDataBuf,int nDataLen);

/*==========================================================================*/
/** get virus detection info
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[out]    pDetectionInfo    detection information
 *        - size : size of struct VIRUS_DETECTION_INFO
 *        - AggressiveScan 0 : not detected by AggressiveScan
 *        - AggressiveScan !=0 : detected by AggressiveScan
 *    @retval 0            sucessfully
 *    @retval PARA_ERR     invalid parameter
 *    @retval BAD_VSC_ERR  bad vsc
 *    @note This API should be called in VS_PROCESS_FILE_CALLBACK_FUNC.
 */
struct VIRUS_DETECTION_INFO {
    int           size;
    unsigned int AggressiveScan;
};
TMVSAPI int PASCAL EXPORT VSGetVirusDetectionInfo(VSCTYPE vsc, struct VIRUS_DETECTION_INFO *pDetectionInfo);

/*==========================================================================*/
/**    Add a PackPolicy name to exception list
 *    @ingroup VSAPI
 *    @param[in]     vsc           virus scan context
 *    @param[in]     strPackPolicyName    Pointer to name which add into
 *    @retval PARA_ERR             invalid parameter
 *    @retval BAD_VSC_ERR          invalid VSC handle
 *    @retval >=0                  How many spyware name in table
 *    @retval -2                   PackPolicy name has existed in the exception list
 *    @retval -3                   PackPolicy name is not PackPolicy name in current pattern
 */
TMVSAPI int PASCAL EXPORT VSAddPackPolicyExcludeName(VSCTYPE vsc, const char* strPackPolicyName);

/*==========================================================================*/
/**    Remove a PackPolicy name from exception list
 *    @ingroup VSAPI
 *    @param[in]     vsc            virus scan context
 *    @param[in]     strPackPolicyName    Pointer to name which add into
 *    @retval PARA_ERR        invalid parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 *    @retval >=0                how many PackPolicy name left in table
 *    @retval -1                The specified name doesn't exist
 */
TMVSAPI int PASCAL EXPORT VSDeletePackPolicyExcludeName(VSCTYPE vsc, const char* strPackPolicyName);

/*==========================================================================*/
/**    Clear all PackPolicy names in the exception list
 *    @ingroup ExtNameList
 *    @param[in]     vsc        virus scan context
 *    @retval 0                ok
 *    @retval PARA_ERR        invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSClearPackPolicyExcludeNameTable(VSCTYPE vsc);

/*==========================================================================*/
/**    Check whether the PackPolicy name exists in the exception list
 *    @ingroup ExtNameList
 *    @param[in]     vsc            virus scan context
 *    @param[in]     strPackPolicyName    PackPolicy name to check
 *    @retval PARA_ERR        invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 *    @retval >=0                found, return index
 *    @retval -1                not found
 */
TMVSAPI int PASCAL EXPORT VSSearchPackPolicyExcludeName(VSCTYPE vsc, char *strPackPolicyName);

/*==========================================================================*/
/**    Load user defined custom defense list
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     BlockSize total byte count of memory block which caller passed in
 *    @param[in]     BlackList memory chunk of custom defense list
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 *    @retval NO_MEM_ERR   memory allocation failed
 *    @retval PATTERN_NOT_FOUND_ERR   can't find master pattern
 *    @retval SKIP_ERR                SHA1 signature of current list is the same with old one (skip this list)
 *    @retval READ_ERR                read pattern error
 *    @retval NOT_SUPPORTED_ERR       method used for match is not supported by current engine
 */
TMVSAPI int PASCAL EXPORT VSLoadCustomDefenseList(VSCTYPE vsc, VULONG BlockSize, void *BlackList);

/*==========================================================================*/
/**    Reset all user defined custom defense list
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 */
TMVSAPI int PASCAL EXPORT VSResetCustomDefenseList(VSCTYPE vsc);

/*==========================================================================*/
/**    Get custom defense method data for pattern generate
 *    @ingroup VSAPI
 *    @param[in]     vsc        virus scan context
 *    @param[in]     handle     resource handle
 *    @param[in]     MethodID   custom defense method identity
 *    @param[out]    Data       custom defense method data for file scan
 *    @retval 0            ok
 *    @retval PARA_ERR    invalid Parameter
 *    @retval BAD_VSC_ERR        invalid VSC handle
 *    @retval NOT_SUPPORTED_ERR   method not support
 *    @retval NO_MEM_ERR   memory allocation failed
 *    @retval READ_ERR                read resource error
 */
TMVSAPI int PASCAL EXPORT VSGetCustomDefenseMethodData(VSCTYPE vsc, VSHANDLE *handle,  VUSHORT MethodID, void *Data);

#ifdef __cplusplus
}
#endif

#include "tmvsx.h"

#endif /* __TMVS_H__ */
