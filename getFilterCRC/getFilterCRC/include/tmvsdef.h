/*
 *****************************************************************************
 *
 *  (C) Copyright 1989-2014 Trend Micro, Inc.
 *  All Rights Reserved.
 *
 *  This program is an unpublished copyrighted work which is proprietary
 *  to Trend Micro, Inc. and contains confidential information that is not
 *  to be reproduced or disclosed to any other person or entity without
 *  prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *  WARNING:  Unauthorized reproduction of this program as well as
 *  unauthorized preparation of derivative works based upon the
 *  program or distribution of copies by sale, rental, lease or
 *  lending are violations of federal copyright laws and state trade
 *  secret laws, punishable by civil and criminal penalties.
 *
 *****************************************************************************
 */

#ifndef __TMVSDEF_H__
#define __TMVSDEF_H__

#ifndef VXD95
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/******* special macro *******/
#define _offsetof(s,m)                      ((char*)&((s*)0)->m - (char*)0)
#define _this(p,s,m)                        (s*)((char*)(p) - _offsetof(s,m))
#define VS_STRUCTSIZE_REQUIREMENT(s,m)      (_offsetof(s,m) + sizeof(((s*)0)->m))



typedef signed char    VSCHAR; /* 1 byte */
typedef unsigned char  VUCHAR; /* 1 byte */
typedef short          VSHORT; /* 2 byte */
typedef unsigned short VUSHORT;/* 2 byte */
extern int VSCharacterEnvType;

/*==========================================================================*/
/* Verbose(Debug) level */
#define VS_VERBOSE_NONE            0
#define VS_VERBOSE_ERR             1
#define VS_VERBOSE_WARN            2
#define VS_VERBOSE_INFO            3
#define VS_VERBOSE_MAX             4

#define VS_DEBUG_SCAN              101
#define VS_DEBUG_CLEAN             102
#define VS_DEBUG_PPT_DECOM_TO_FILE 103
#define VS_DEBUG_HEUR_RULE         104
#define VS_DEBUG_DATATYPE          105
#define VS_DEBUG_CLEANINSTCNT      106
#define VS_DEBUG_HEUR              109

/* 110 ~ 119 reserved for softmice */
#define VS_DEBUG_SMICE_DECRYPT     110
#define VS_DEBUG_SMICE_SCATTERED   111
#define VS_DEBUG_SMICE_REG         115
#define VS_DEBUG_SMICE_INS         116
#define VS_DEBUG_SMICE_PTP         117
#define VS_DEBUG_SMICE_INSCRC      118
#define VS_DEBUG_SMICE_APIINFO     119

/* 120 ~ 129 reserved for script based pattern */
#define VS_DEBUG_SCRBASE         120
#define VS_DEBUG_DETECT_ALL      121
#define VS_DEBUG_SCR_OP_COUNT    122
#define VS_DEBUG_SCR_INDEX       123

/* 124 ~ 126 reserved for the lite script engine */
#define VS_DEBUG_SCR_FTYPE       124
#define VS_DEBUG_SCR_LITE        125
#define VS_DEBUG_SCR_LITE_OP     126

#define VS_DEBUG_SCR_HARDCODE_PTN_CFG 127

#define VS_DEBUG_SCR_CRC_INFO    128
#define VS_DEBUG_RSRC_CACHE      129

/* 130 ~ 139 reserved for boot */
#define VS_DEBUG_BOOT            130

/* 150 ~ 159 reserved for script trap */
#define VS_DEBUG_STRAP           150

/* 160 ~ 169 reserved for pattern rollover */
#define VS_DEBUG_ROLLOVER        160

/* 170 ~ 179 reserved for OLE */
#define VS_DEBUG_OLE_ENG         170
#define VS_DEBUG_OLEEXP_INFO     171

/* 180 ~ 189 reserved for PE CRC */
#define VS_DEBUG_WIN32CRC        180
#define VS_DEBUG_WIN32CRC_NOSIZE 181
#define VS_DEBUG_WIN32CRC_MATCH  182
#define VS_DEBUG_WIN32CRC_MISS   183

/* 200 ~ 209 reserved for pattern dump */
#define    VS_DEBUG_DUMPPTN      200
#define VS_DEBUG_DUMPPTN_NAMETAB 201
#define VS_DEBUG_DUMPPTN_IDXTAB  202
#define VS_DEBUG_DUMPPTN_HASHTAB 203

/* 210 ~ 229 reserved for Script CRC */
#define VS_DEBUG_SCRC            210
#define VS_DEBUG_SC_CRC1         211
#define VS_DEBUG_SC_CRC2         212
#define VS_DEBUG_SC_CRC3         213
#define VS_DEBUG_VBSSCRC4        214
#define VS_DEBUG_JVSSCRC4        215

#define VS_DEBUG_SC_CRC1_APPEND  216
#define VS_DEBUG_SC_CRC2_APPEND  217
#define VS_DEBUG_SC_CRC3_APPEND  218

#define VS_DEBUG_HTML_SC_CRC1_APPEND    216
#define VS_DEBUG_HTML_SC_CRC2_APPEND    217
#define VS_DEBUG_HTML_SC_CRC3_APPEND    218



#define VS_DEBUG_HTMLSCRC       220 
#define VS_DEBUG_HTML_SC_CRC1   221
#define VS_DEBUG_HTML_SC_CRC2   222
#define VS_DEBUG_HTML_SC_CRC3   223

#define VS_DEBUG_HTML_VBSSCRC4  224
#define VS_DEBUG_HTML_JVSSCRC4  225

/* 230 ~ 239 reserved for Generic Unpack */
#define VS_DEBUG_GU_INSTCNT_UP  230
#define VS_DEBUG_GU_INSTCNT_PT  231
#define VS_DEBUG_GU_UNPACK      232
#define VS_DEBUG_GU_APIPTN      234
#define VS_DEBUG_GU_INFO        235

/* 240 ~ 249 reserved for ShellCode Scan */
#define VS_DEBUG_SHELLCODE_SCAN  240
#define VS_DEBUG_SHELLCODE_PERF  241

/* 250 ~ 259 reserved for PDF filetype scan */
#define VS_DEBUG_PDF               250
#define VS_DEBUG_PDF_DUMP_OBJ      251
#define VS_DEBUG_PDF_DUMP_XREF     252
#define VS_DEBUG_PDF_DUMP_TARGET   253
#define VS_DEBUG_PDF_QUERY_OBJ     254
#define VS_DEBUG_PDF_PARSE_TRACE   255
#define VS_DEBUG_PDF_DUMP_CONTENTS 256

/* 260 ~ 269 reserved for SWF filetype scan */
#define VS_DEBUG_SWF               260
#define VS_DEBUG_SWF_DUMP_AS2      261
#define VS_DEBUG_SWF_DUMP_AS3      262

/* 270 reserved for Dynamic Pattern Search and Data Stream */
#define VS_DEBUG_DS                270
/* 271 reserved for Dynamic Pattern Search and Data Stream */
#define VS_DEBUG_GFB               271

/* 280 ~ 289 reserved for DEX filetype scan (Reserved for VSAPI 9.718) */
#define VS_DEBUG_DEX               280
#define VS_DEBUG_DEX_TABLES        281
#define VS_DEBUG_DEX_DISASM        282

/* 300 ~ 399 reserved for Corrupt Information */
#define VS_DEBUG_CORRUPT_INFO      300

/* 400 ~ 409 reserved for NPF related information */
#define VS_DEBUG_NPF_VINFO         400
/* 410 ~ 419 reserved for performance query */
#define VS_DEBUG_PERF              410

#define VS_DEBUG_FILE_PERF         411
#define VS_DEBUG_FILE_TIME_PERF    412
#define VS_DEBUG_FILE_NUM_PERF     413

#define VS_DEBUG_PATTERN_PERF      414
#define VS_DEBUG_PATTERN_TIME_PERF 415
#define VS_DEBUG_PATTERN_NUM_PERF  416

#define VS_DEBUG_LEADER_GEN        420
#define VS_DEBUG_LEADER_SCRIPT     421

#define VS_DEBUG_AGGRESSIVE        430

/* 450 ~ 459 reserved for Profile information */
#define VS_DEBUG_PROFILE      450

#define VS_DEBUG_SFX               500

#define VS_DEBUG_PAGING            510
#define VS_DEBUG_ICRC              520

/* 530 ~ 539 reserved for AFI information */
#define VS_DEBUG_AFI_PE            530 /*[AFI_PE]*/

#define VS_DEBUG_JAVA_CLASS        540 /* Java Class File */

/* 550 ~ 559 reserved for NetApp cache information */
#define VS_DEBUG_DUMP_FIRSTBLOCK   550

/* 600 ~ 699 reserved for file related dump, file type process dump(search "VS_DEBUG_FILE_TYPE_DUMP+" ) */
#define VS_DEBUG_FILE_TYPE_DUMP    600
#define VS_DEBUG_DUMP_FILECONTENT  601 /* Dump partial file content */
#define VS_DEBUG_DUMP_TEMPFILE     602 /* Use to keep temp file */


#define VS_DEBUG_7ZIP              700
#define VS_DEBUG_CPIO              701

#define VS_DEBUG_XAR               710
#define VS_DEBUG_FATBINARY         711
#define VS_DEBUG_MACHO             712

#define VS_DEBUG_SIS               721
#define VS_DEBUG_SMIME             730
 
#define VS_DEBUG_AXML              740 /* Reserved for VSAPI 9.718 */

/* 760 ~ 770 reserved for CLSH scan */
#define VS_DEBUG_CLSH_SCAN          761
#define VS_DEBUG_MIP2_GEN           762 /* For MIP2 file information dump */


/* (Reserved for VSAPI 9.718) */
/*#define VS_DEBUG_ESBC              771*/
#define VS_DEBUG_ESBC_TRACE        772
#define VS_DEBUG_ESBC_TRACE_SPE    772

/* 800 ~ 809 reserved for Parse XML */
#define VS_DEBUG_XML                800

#define VS_DEBUG_XSCAN              820 /* Reserve for XScan */

/* 1000 ~ 1999 define in tmvsdbg.h */



/*==========================================================================*/
/* general error code */
#define VS_RC_NO_ERROR                0
#define PARA_ERR                    -99
#define NO_MEM_ERR                  -98
#define WRITE_ERR                   -97
#define READ_ERR                    -96
#define OPEN_W_ERR                  -95
#define OPEN_R_ERR                  -94
#define DISABLE_ERR                 -93 /* decode/decompress is disabled */
#define PSW_ZIP_ERR                 -92 /* password protected compressed file */
#define NO_SUPP_ERR                 -91 /* un-supported compression method or file version */
#define BAD_VSC_ERR                 -90
#define SKIP_ERR                    -89
#define BREAK_ERR                   -88
#define ACCESS_ERR                  -87
#define BAD_HANDLE_ERR              -86
#define BUFFER_TOO_SHORT_ERR        -85
#define BACKUP_NAME_FULL_ERR        -84
#define NEWPTN_STRUC_NOT_FOUND      -83 /* no new pattern structure found */
#define BAD_ZIP_ERR                 -82 /* corrupted compression file */
#define NOT_SUPPORTED_ERR           -81 /* function is not supoprted */
#define PLATFORM_ERR                -80 /* update engine from different platform file */
#define FILE_EXIST_ERR              -79 /* file exist when take action rename */
#define MAXDECOM_ERR                -78 /* exceed decompression layer */
#define DISK_FULL_ERR               -77 /* fail to write because disk is full */
#define EXTRACT_TOO_BIG_ERR         -76 /* Extract file size > cf_ExtractFileSizeLimit */
#define NEED_SCAN_COM               -75 /* This file muse scan .COM file type */
#define PSW_OFFICE_ERR              -74 /* password protected office file */
#define BAD_FILE_ERR                -73
#define SEEK_ERR                    -72
#define ZIP_RATIO_ERR               -71 /* ZIP RATIO exceed limit*/
#define ZIP_CRC_EQU_SKIP_ERR        -70 /* Files in ZIP file have the same CRC(they should be the same)*/
#define ZIP_FILE_COUNT_ERR          -69 /* The file count in ZIP file exceed the cf_ExtractFileCountLimit*/
#define DTYPE_NEED_MORE_DATA_ERR    -68 /* need more data for data type checking */    
#define PTN_UPDATE_ERR              -67 /* Fail to update pattern and no valid pattern available for VSC */
#define ICRC_QUERY_ERR              -66 /* ICRC query string error */
#define ZIP_64_UNSUPPORTED_ERR      -65 /* ZIP64 decompression is not supported */
#define UNKNOWN_LITE_OPCODE         -9999 /* unknown lite script engine operator */

/* what to do when found virus */
#define VC_ACT_UNCHANGE      0
#define VC_ACT_RENAME        1
#define VC_ACT_MOVE          2
#define VC_ACT_CLEAN         3
#define VC_ACT_DELETE        4
#define VC_ACT_ASK           9
#define VC_ACT_DO_NOTHING   25

#ifndef TRUE
    #define FALSE 0
    #define TRUE 1
#endif

#define Virus_Found     1
#define Virus_Maybe     2
#define Virus_CDF       4 /* flag used to workaround ProcessAgain issue when scan compressed file */
#define FoundStatus     0x0F
#define FoundVirusType  0xF0
#define FoundNormal     0x10
#define FoundWord       0x20
#define FoundSoftmice   0x40
#define FoundBoot       0x80

#define BootVirusType   0x80
#define FileVirusType   0x40
#define SoftMiceType    0x08
#define MemOsType       0x04
#define MemMcbType      0x02
#define MemHighestType  0x01
/* Remove Info.*/
#define RemoveType      0x10
#define OverWriteType   0x20
#define DecryptType     0x30


/*==========================================================================*/
/* include different header file based on platform/compiler we are running */

#ifdef DOS_BC
#include "m_dosbc.h"
#endif

#ifdef DOS_GCC
#include "m_dosgcc.h"
#endif

#ifdef DOS_WC
#include "m_doswc.h"
#endif

#ifdef HP10
#include "m_hp10.h"
#endif

#ifdef HP11IA64
#include "m_hp11ia64.h"
#endif

#ifdef WIN16
#include <windows.h>
#include "m_win16.h"
#endif

/* Using "#else" is because <windows.h> will define WIN32 */
#ifndef KDWINNT
#ifdef WIN64
    #include "basetsd.h"
    #include <windows.h>
    #include "m_win64.h"
#else 
    #ifdef WIN32A
        #include <windows.h>
        #include "m_win32a.h"
    #else
        #ifdef W32DBG
            #include <windows.h>
            #include "m_w32dbg.h"
        #else
            #ifdef WINCE_32
                #include <windows.h>
                #include "m_wince.h"
                #pragma pack(4)
            #else
                #ifdef WIN32
                    #include <windows.h>
                    #include "m_win32.h"
                #endif
            #endif
        #endif
    #endif
#endif
#endif

#ifdef SCO5
#include "m_sco5.h"
#endif

#ifdef UNIXWARE
#include "m_uw.h"
#endif

#ifdef SOL
#include "m_sol.h"
#endif

#ifdef SOLSPC64
#include "m_solspc64.h"
#endif

#ifdef SUN4
#include "m_sun4.h"
#endif

#ifdef SOLBSD
#include "m_solbsd.h"
#endif

#ifdef SOL86
#include "m_sol86.h"
#endif

#ifdef SOL64
#include "m_sol64.h"
#endif

#ifdef NLM
#include "m_nlm.h"
#endif

#ifdef NEC
#include "m_nec.h"
#endif

#ifdef SNI
#include "m_sni.h"
#endif

#ifdef OS2
#include "m_os2.h"
#endif

#ifdef SGI
#include "m_sgi.h"
#endif

#ifdef LINUX
#include "m_linux.h"
#endif

#ifdef ANDROID
#include "m_android.h"
#endif            

#ifdef MAEMO
#include "m_maemo.h"
#endif      

#if defined(LNXAMD64) || defined(LNXAMD64LITE)
#include "m_lnxamd64.h"
#endif

#ifdef LNXIA64
#include "m_lnxia64.h"
#endif      

#ifdef ZLINUX
#include "m_zlinux.h"
#endif

#ifdef ZLNX64
#include "m_zlnx64.h"
#endif

#ifdef LNXMP
#include "m_lnxmp.h"
#endif 

#ifdef LINUXARM
#include "m_linuxarm.h"
#endif

#ifdef LNXXSCALE
#include "m_lnxxs.h"
#endif

#ifdef LNXMP4926
#include "m_lnxmp4926.h"
#endif 

#ifdef BIMINI50
#include "m_bimini50.h"
#endif 

#ifdef FBSD
#include "m_fbsd.h"
#endif

#ifdef AIX64
    #include "m_aix64.h"
#elif defined(AIX)
    #include "m_aix.h"
#endif

#ifdef OS390
#include "m_os390.h"
#endif

#ifdef AS400
#include "m_as400.h"
#endif

#ifdef BSDI
#include "m_bsdi.h"
#endif

#ifdef DECUX
#include "m_dec_ux.h"
#endif

#ifdef VXD95

    /* FILTER 6.0 always include the vtoolsc.h, not vtoolscp.h */
#ifdef NO_FRAMEWORK
    #include <vtoolsc.h>
#else
    #ifdef __cplusplus
    #include <vtoolscp.h>
    #else
    #include <vtoolsc.h>
    #endif
#endif
#include "m_vxd95.h"
#endif

/* IA64 Porting 2003/6/27 */
#ifdef KDWINNT
    /* 02-12-2001    Jason Lin, for ifskit */
    #include "basetsd.h"

    #ifdef USE_IFS
    #include <ntifs.h>
    #else
    #include <ntddk.h>
    #endif
    
    #ifdef _WIN64
        #include "m_kdnt64.h"
    #else
#include "m_kdnt.h"
#endif
#endif

#ifdef EPOC
#include "m_epoc.h"
#endif

#ifdef PALM
#include "m_palm.h"
#endif

#ifdef DGUX
#include "m_dgux.h"
#endif

/* added by S.Kondo 1999.4.7 */
#ifdef MAC
#include "m_mac.h"
#endif

#ifdef MACOSX
#include "m_macosx.h"
#endif

#ifdef MACOSX64
#include "m_macosx64.h"
#endif
#ifdef MACOSX86_64
#include "m_macosx86_64.h"
#endif

#ifndef __TMVS_MACHINE
Error, No machine defined
#endif

#define DEFAULT_LOG_FILE    "vslog.log"

#define __VSAPI_TYPE_DEFINED

/*==========================================================================*/
/* Scan task status structure */
typedef struct {
    VULONG st_Magic;
    /* scan task result */
    VULONG st_StartTime;            /* when the task of check started */
    VULONG st_ScanFileCount;        /* total file passed to proc function */
    VULONG st_ScanDirCount;         /* total directory checked */
    VULONG st_SearchFileCount;        /* total file checked */
    VULONG st_ScanVirusInstanceCount;/* virus instanance count */
    VULONG st_ScanVirusFileCount;    /* file contain virus */
    VULONG st_ScanVirusCleanCount;    /* virus instanance count */
    VULONG st_ScanVirusDeleteCount; /* file contain virus */
} SCANTASK;
#define SCANTASK_MAGIC    0xBEA8BEA8

/*==========================================================================*/
/* Process Directory Counter structure */
typedef struct {
    VULONG ProcDirCount;
    VULONG TotalDirCount;
    VULONG ProcFileCount;
    VULONG TotalFileCount;
} PDCOUNT;

/*==========================================================================*/
/* date/time format function control flag */
#define VS_DTFM_TC    0x0000    /* time counter */
#define VS_DTFM_DO    0x0001    /* date/time, date only */
#define VS_DTFM_TO    0x0002    /* date/time, time only */
#define VS_DTFM_DT    (VS_DTFM_DO|VS_DTFM_TO) /* both date and time */
#define VS_DTFM_FMASK 0x0003

/* numerical date format */
#define VS_DTFM_US      0x0004    /* MM/DD/YY */
#define VS_DTFM_EU      0x0008    /* DD/MM/YY */
#define VS_DTFM_IN      0x000C    /* YY/MM/DD */
#define VS_DTFM_MASK    0x000C
#define VS_DTFM_H12     0x0010    /* 24/12 hour */
#define VS_DTFM_Y4      0x0020    /* 4 digit year */
#define VS_DTFM_GM      0x0040    /* GM time */

/* numerical date or string date */
#define VS_DTFM_STR 0x0080    /* "Jan 1, 1997" or "01/01/1997" */

#define VS_DT_YEAR      0
#define VS_DT_MONTH     1
#define VS_DT_MDAY      2
#define VS_DT_HOUR      3
#define VS_DT_MIN       4
#define VS_DT_SEC       5
#define VS_DT_WDAY      6
#define VS_DT           7

/*==========================================================================*/
/* VS directory/file struture */
#define MAX_SEARCH_MASK_LEN 32
#define MAX_FILE_NAME_LEN    256

typedef struct VS_DIR {
    VULONG td_Magic;
    char td_DirPath[MAX_PATH_LEN]; /* search directory */
    char td_Mask[MAX_SEARCH_MASK_LEN]; /* search pattern */
    char td_File[MAX_FILE_NAME_LEN]; /* matched file */
    long td_CurrentOffset; /* current offset in directory file */
    VULONG td_FileType; /* file type */
    VULONG td_inode; /* inode number */
    HANDLE td_DirFd; /* file descriptor of directory */
    void *td_dirent; /* used only if system support */
                    /* opendir(),readdir(),and closedir() */
                    /* include NLM, and Solaris */
                    /* Borland C also supports it */
} VSDIR;

#define VSFT_FIFO           0x00000001    /* fifo file, named pipe */
#define VSFT_CHAR           0x00000002    /* character device */
#define VSFT_DIR            0x00000004    /* directory */
#define VSFT_REGULAR        0x00000008    /* regular file */
#define VSFT_BLOCK          0x00000010    /* block device */
#define VSFT_SOCKET         0x00000020    /* socket */
#define VSFT_SYMLINK        0x00010000    /* symbolic link device */
#define VSFT_SYSTEM         0x00100000    /* dos:system file */
#define VSFT_VOL_LABEL      0x00200000    /* dos:volume label */
#define VSFT_HIDDEN         0x00400000    /* dos:hidden */
#define VSFT_READONLY       0x00800000    /* dos:read only */
#define VSFT_MASK           0x0000ffff
#define VSFTEF_MASK         0xff000000
#define VSFTUF_MASK         0x00ffffff
#define VSFTEF_WINNORMAL    0x80000000
#define VSFTEF_FORCESCAN    0x40000000 /* force scan */
#define VSFTEF_NO_NEED_SCAN 0x20000000 /* the data type doesn't need scan */
/* use following definition to check if the data type needs scan */
/* if (IS_NO_NEED_SCAN(vsdtype.vsd_TypeExtFlag))  =>  no need scan */
#define VSFTEF_MASK_NO_NEED_SCAN    0x20000000
#define IS_NO_NEED_SCAN(vsd_TypeExtFlag) ((VSFTEF_MASK_NO_NEED_SCAN & (vsd_TypeExtFlag)) == VSFTEF_NO_NEED_SCAN)

#define VS_TD_MAGIC 0x13579246

/*==========================================================================*/
/* Data type */
typedef struct {
    short vsd_Type;
    short vsd_SubType;
    VULONG vsd_TypeExtFlag;
    VULONG vsd_TypeFlag;
    char vsd_TypeName[40];
} VSDTYPE;

/*==========================================================================*/
/* circular cache buffer used for STREAM */
typedef struct {
    long strm_Length;    /* data length in cache buffer */
    long strm_BOffset;    /* data offset in buffer */
    long strm_DOffset;    /* data offset in stream */
    char strm_CacheBuffer[1];    /* circular cache buffer */
} STRMCACHE;

/*==========================================================================*/
/* resource descriptor structure */
typedef struct _VSHANDLE {
    long    vsh_Magic;
    long    vsh_AccessOffset;
    long    vsh_AccessSize;
    long    vsh_CurrentOffset;
    short    vsh_ResourceType;
        /* VS_RT_FILE, VS_RT_FILED,VS_RT_MEM, VS_RT_STRM or VS_RT_PLUGIN */
    short    vsh_Mode; /* VS_READ, VS_WRITE, or VS_CREATE */
    short    vsh_HandleFlag;
    short    vsh_FirstBlockLength;
    char    *vsh_FirstBlock;
    char    *vsh_MemoryPointer;
    VSDTYPE vsh_DType;
    HANDLE    vsh_FileHandle;
    void    *vsh_Info;    /* user defined extended information */
} VSHANDLE;
#define VSH_MAGIC    0xBEBEA8A8
#define VSH_MEMORY_NEED_FREE    0x0001
#define VSH_FREE_SIZE    0x0002

/* resource structure */
typedef struct {
    char *r_File;    /* Resource pointer, file, memory segment, or a pipe */
    char *r_Name;    /* name of this resource(for report) (a system string, may be ASCII-based, EBCDIC-based, etc.) */
    VSHANDLE *r_Handle; /* open handle, 0 if not open yet */
    int r_Type; /* resource type */
} RESOURCE;


typedef long * PTNHANDLE;
typedef long * PVSHANDLE;

/* resource type:vsh_ResourceType */
#define VS_RT_FILE  0x00    /* file */
#define VS_RT_FILED 0x10    /* file descriptor */
#define VS_RT_MEM   0x20    /* memory */
#define VS_RT_STRM  0x40    /* stream */
#define VS_RT_OLE   0x80    /* ole structure resource */
#define VS_RT_PLUGIN   0x90    /* plugin structure resource */
#define VS_RT_TEMP  0xA0    /* temp file */
#define VS_RT_MASK  0xf0    /* resource type mask */

/** Reads from an open file. VSReadFile provides a common interface for reading 
*  data from files regardless of platforms or systems. All parameters will be 
*  set by VSAPI when calling this function
*
*    @param[in]    Fd          native plugin handle
*    @param[out]   buf         buffer for read
*    @param[in]    Len         request read length
*    @param[out]   ReadLen     read length
*
*    @retval VS_RC_NO_ERROR    ok
*    @retval others            error
*/
typedef int VSIOPlugInReadFunc (void* Fd, char *buf, VULONG Len, VULONG *ReadLen);

/** Writes data to an open file. VSWriteFile provides a common interface for 
*  writing data to a file regardless of the platform or system used
*
*    @param[in]    Fd          native plugin handle
*    @param[out]   buf         buffer for write
*    @param[in]    Len         request write length
*    @param[out]   WriteLen    write length
*
*    @retval VS_RC_NO_ERROR    ok
*    @retval others            error
*/
typedef int VSIOPlugInWriteFunc (void* Fd, char *buf, VULONG Len, VULONG *WriteLen);

/**  Move the read/write pointer of an open file.
*
*    @param[in]    Offset      request seek offset
*    @param[out]   RetOffset   seeked offset
*    @param[in]    RefMode     Reference for Offset:
*                              0: Beginning of file SEEK_SET
*                              1: Current location SEEK_CUR
*                              2: End of file SEEK_END
*
*    @retval VS_RC_NO_ERROR    ok
*    @retval others            error
*/
typedef int VSIOPlugInSeekFunc ( void* Fd, VLONG Offset, VLONG* RetOffset, int RefMode);

/**  Return the size of an opened file.
*
*    @param[in]    Fd          native plugin handle
*    @param[out]   RetSize     size of the handle
*
*    @retval VS_RC_NO_ERROR    ok
*    @retval others            error
*/
typedef int VSIOPlugInGetSizeFunc(void* Fd, VLONG* RetSize);

/** 
*
*    @param[in]    Fd          native plugin handle
*    @param[in]    size        size of the handle
*
*    @retval VS_RC_NO_ERROR    ok
*    @retval others            error
*/
typedef int VSIOPlugInSetSizeFunc(void* Fd, VLONG size);
/** 
*
*    @param[in]    Fd          native plugin handle [destination]
*    @param[out]   szSrc       file path [source]
*
*    @retval VS_RC_NO_ERROR    ok
*    @retval others            error
*/
typedef int VSIOPlugInReplaceFunc(void* Fd, void* SrcFd);

/** 
*
*    @param[in]       Fd        native plugin handle [destination]
*    @param[in]       attr      information of file
                                0x1 (VS_FATTR_TIME): Time information of file (fill struct _FILETIMEDATA)
*    @param[in/out]   out       pointer of data to store
*
*    @retval VS_RC_NO_ERROR    ok
*    @retval others            error
*/
typedef int VSIOPlugInGetAttrFunc(void* Fd, int attr, void* out);

/* Attribute identifier for VSIOPlugInGetAttrFunc */
#define VS_FATTR_TIME 0x1
/* this FILETIMEDATA structure will provide to product */ 
typedef struct _FILETIMEDATA {
    VULONG size;
    VULONG status;
    VINT64 CreateTime;
    VINT64 LastAccessTime;
    VINT64 LastWriteTime;
} FILETIMEDATA;
#define VALIDCREATETIME (1)
#define VALIDLACCESSTIME (1<<1)
#define VALIDLWRITETIME (1<<2)

typedef struct  { 
    VULONG                  nLength;    /* size of struct */
    void*                   pHandle;    /* handle of io */
    VSIOPlugInGetSizeFunc*  cf_GetSize; /* get size func */ 
    VSIOPlugInSeekFunc*     cf_Seek;    /* seek func */ 
    VSIOPlugInReadFunc*     cf_Read;    /* read func */ 
    VSIOPlugInWriteFunc*    cf_Write;   /* write func */ 
    VSIOPlugInSetSizeFunc*  cf_SetSize; /* set size func */ 
    /* added in 9.0 for RESOURCE clean support */
    VSIOPlugInReplaceFunc*  cf_Replace; /* replace_from_path func */
    /* added in 9.8 for get file attribute support */
    VSIOPlugInGetAttrFunc*  cf_GetAttr; /* get file attribute */
}VSIOPlugIn;

#define VS_IO_PLUGIN_MIN_SIZE_FOR_REPLACE VS_OFFSETOF(VSIOPlugIn, cf_Replace)
#define VS_IO_PLUGIN_MIN_SIZE_FOR_GETATTR VS_OFFSETOF(VSIOPlugIn, cf_GetAttr)

#define VS_RTF_ENC          0x01  /* VSAPI simple encryption */
#define VS_RTF_UNI          0x02  /* filename is unicode */
#define VS_RTF_COMPRESSED   0x04  /* for nlm Compressed file */
#define VS_RTF_USERDEC      0x08  /* for user decode */
#define VS_RTF_MASK         0x0f  /* resource type flag mask */


#define VSIsValidRType(x) ( ((x)&VS_RT_MASK)==VS_RT_FILE || \
                            ((x)&VS_RT_MASK)==VS_RT_FILED || \
                            ((x)&VS_RT_MASK)==VS_RT_MEM || \
                            ((x)&VS_RT_MASK)==VS_RT_STRM || \
                            ((x)&VS_RT_MASK)==VS_RT_OLE || \
                            ((x)&VS_RT_MASK)==VS_RT_PLUGIN)
#define VSIsValidVSHandle(x) ((x)&&(x)->vsh_Magic==VSH_MAGIC&&VSIsValidRType((x)->vsh_ResourceType))

/* h_Mode */
#define VS_READ         0x00
#define VS_WRITE        0x01
#define VS_CREATE       0x02
#define VS_COMPRESSED   0x04 /* for nlm compressed file */
#define VS_LOCKED       0x08 /* for locked file */
/*      0x10 is reserved for internal use  */
#define VS_NO_OPLOCK    0x20 /*  for winntkd only */

/* lily add to record dup file */
/* 7/25/00 tracker#1876 */
#define VS_DUP_FILE         0x08
#define VS_MODE_MASK        0x0f
#define VS_SIMPLE_ENCRYPT   0x10    /* apply simple encryption to prevent */
                    /* scan false alarm on temp file */

/*==========================================================================*/
/* Data Type */
/** @addtogroup DataType
 *    @{ */

#define VSDT_UNINIT     -1        /**  Uninitialized type */  
#define VSDT_UNKNOWN    -2        /**< Unknown type */
#define VSDT_DIR        0
#define VSDT_WINWORD    1         /**< Word for Windows */
#define VSDT_PPT        2         /**< Windows PowerPoint */
#define VSDT_FON        3         /**< Windows Font */
#define VSDT_EXCELL     4         /**< Excel for Windows */
#define VSDT_COM        5         /**< COM : see sub type */
#define VSDT_ICO        6         /**< Windows Icon */
#define VSDT_EXE        7         /**< EXE : see sub type */
#define VSDT_GKS        8         /**< SUN GKS */
#define VSDT_MSCOMP     9         /**< MSCOMP */
#define VSDT_PCX        10        /**< PCX */
#define VSDT_CPIO       11        /**< unix cpio archive */
#define VSDT_PPM        12        /**< PPM image */
#define VSDT_LHA        13        /**< LHA */
#define VSDT_AR         14        /**< unix ar archive */
#define VSDT_ARC        15        /**< ARC */
#define VSDT_WRT        16        /**< Windows Write : see sub type */
#define VSDT_CAL        17        /**< Windows Calendar */
#define VSDT_ASCII      18        /**< ASCII text */
#define VSDT_ELF        19        /**< ELF */
#define VSDT_TAR        20        /**< TAR */
#define VSDT_TD0        21        /**< TeleDisk Image */
#define VSDT_FLI        22        /**< AutoDesk Animator(FLI or FLC) */
#define VSDT_EMPTY      23        /**< empty file(size 0) */
#define VSDT_WIN_LNK    24        /**< NT/95 shortcut(*.lnk) */
#define VSDT_RAR        25        /**< RAR */
#define VSDT_MDB        26        /**< Microsoft Access(MDB) */
#define VSDT_MAC        27        /**< MAC */
#define VSDT_TEXT       28        /**< VBScript, HTML, JavaScript */
#define VSDT_SBFT       29        /**< Script File Type match */
#define VSDT_PROJECT    30        /**< Project for Windows */
#define VSDT_ASF        31        /**< Advanced Streaming Format */
#define VSDT_QTM        32        /**< Quick Time Media */
#define VSDT_MPG        33        /**< MPEG */
#define VSDT_PNG        34        /**< Portable Network Graphics */
#define VSDT_PSP        35        /**< Pain Shop Pro */
#define VSDT_TGA        36        /**< Targa Image */
#define VSDT_PICT       37        /**< Macintosh Bitmap */
/* 38-115: reserve for vsapi 5.41 (Reyer: 6/13/2001) */
/*       : some removed for 6.0 (Murphy: 10/17/2001) */
/* following types from Cyril (Reyer, 6/6/2001) */
#define VSDT_AFC        38        /**< Apple Sound */
#define VSDT_AI         39        /**< Encapsulated Postscript */
#define VSDT_AIF        40        /**< Audio InterChange File Format from Apple/SGI */
#define VSDT_ANI        41        /**< Animated Cursor */ /**< USELESS since moving to VSDT_RIFF_ANI */
#define VSDT_ATM        42        /**< TerraGen ATMosphere */
#define VSDT_AVS        43        /**< Nullsoft AVS Files */
#define VSDT_BW         44        /**< SGI Image */
#define VSDT_C4D        45        /**< Cinema 4D */
#define VSDT_CDA        46        /**< BAR CDA Music Track File Format */ /**< USELESS since moving to VSDT_RIFF_CDA */
#define VSDT_CGM        47        /**< Computer Graphics Metafiles */
#define VSDT_CHL        48        /**< CHL File */
#define VSDT_CMX        49        /**< Corel Presentation Exchange */ /**< USELESS since moving to VSDT_RIFF_CMX */
#define VSDT_COB        51        /**< Caligari TrueSpace File */
#define VSDT_COBJ       52        /**< Visual C Obj File */
#define VSDT_CST        53        /**< Macromedia Director Cast */
#define VSDT_DCR        54        /**< Macromedia Director Shockwave Movie */
#define VSDT_DWD        57        /**< Diamondware Dgitized Sound */
#define VSDT_DWG        58        /**< AutoCAD DWG */
#define VSDT_FH9        61        /**< Free Hand Document */
#define VSDT_HRC        65        /**< SoftImage */
#define VSDT_IFF        66        /**< Amiga 8SVX Audio InterChange File Format */
#define VSDT_IIMG       68        /**< Interleaf Image */
#define VSDT_IMG        69        /**< GEM Image */
#define VSDT_IOB        70        /**< Imagine 3D Object */
#define VSDT_ISU        71        /**< Uninstall Scripts */
#define VSDT_IVC        72        /**< InterVoice Files */
#define VSDT_LWO        74        /**< LightWave 3D Object */
#define VSDT_MAT        75        /**< Matlab Sound */
#define VSDT_MAUD       76        /**< MAUD Sample Format */
#define VSDT_MIF        78        /**< Magick Image File Format */
#define VSDT_MMC        79        /**< Media Catalog */
#define VSDT_MNG        80        /**< Multiple-image Network Graphics */
#define VSDT_NEO        81        /**< Atari Neochrome */
#define VSDT_PAT        83        /**< Gravis Patch Files */
#define VSDT_PDB        84        /**< PalmPilot Image */
#define VSDT_PFB        85        /**< Adobe Font File */
#define VSDT_PIF        86        /**< Shortcut to Microsoft Program */
#define VSDT_RA         87        /**< Real Audio */
#define VSDT_RLA        90        /**< WaveFront RLA */
#define VSDT_SCENE      92        /**< Sculpt 3D/4D Scene */
#define VSDT_SCM        94        /**< Lotus ScreenCam Movie */
#define VSDT_SDS        95        /**< MIDI Sample Sound */
#define VSDT_SF         96        /**< IRCAM */
#define VSDT_SFR        97        /**< Sonic Foundry File */
#define VSDT_SIR        98        /**< Solitaire Image Recorder */
#define VSDT_SMP        99        /**< SampleVision Sound */
#define VSDT_SNDT       100        /**< Sndtool Sound File */
#define VSDT_SRF        101        /**< TerraGen Surface */
#define VSDT_TER        102        /**< TerraGen Terrain */
#define VSDT_TGW        103        /**< TerraGen World */
#define VSDT_TXW        104        /**< Yamaha tx-16w */
#define VSDT_V8         106        /**< Convox V8 File */
#define VSDT_VID        107        /**< Bitmap Image YUV12 */
#define VSDT_WBC        109        /**< Webshots Collection */
#define VSDT_WMF        110        /**< Windows Metafile */
#define VSDT_WVE        112        /**< Psion Audio Files */

#define VSDT_MACBIN     115        /**< Macintosh MacBinary */
#define VSDT_MBX        116        /**< Mail Box (MicroSoft Outlook 4.x or Unix-based) */

#define VSDT_USRDEF     117        /**< Script User-Defined Type match (Reyer, 10/19/01) */
#define VSDT_CUSDEF     118        /**< Script Customer-Defined Type match (Reyer, 11/26/01) */

#define VSDT_GMS        119        /**< Corel Global Macro (aldous, 02/22/2002) */
#define VSDT_CPT        120        /**< Corel PhotoPaint (aldous, 04/04/2002) */
#define VSDT_BZIP2      121        /**< GNU BZIP2 */
#define VSDT_WORDPRO    122        /**< WordPro */
#define VSDT_MSI        123        /**< Windows Installer */
#define VSDT_JGF        124        /**< JP Government file */
#define VSDT_ACE        125        /**< ACE compression file*/
#define VSDT_EPOC       126        /**< EPOC file */

#define VSDT_PROCESS    127        /**< Windows Process Memory */

#define VSDT_SIS        128        /**< EPOC sis file (sis)*/
#define VSDT_DEX        129        /**< Dalvik VM DEX file (VSAPI 9.700)*/

#define VSDT_LZMA       200        /**< LAMA file */
#define VSDT_XZ         201        /**< .xz file */
#define VSDT_7ZIP       202        /**< 7-ZIP file */
#define VSDT_MSEMF      203        /**< MS Enhanced Metafile format (VSAPI 9.700)*/
#define VSDT_BPL        204        /**< Binary Property List */
#define VSDT_UHA        205        /**< UHarc Compressed Archive */
#define VSDT_HWP        206        /**< Hangul Document (Korean) (ATSE 9.740)*/
#define VSDT_WIM        207        /**< Microsoft Windows Imaging Format file */

/* Sub file type -PKZIP*/
#define VSDT_PKZIP_APPEND   0xf000    /**< PKZIP file append garbage date from head or tail */
/**************************************************************************/
/* Extented type : 4 byte magic */
#define VSDT_CORE       4000    /**< unix core file */
#define VSDT_GRP        4001    /**< Windows Group */
#define VSDT_JPG        4002    /**< JPEG */
#define VSDT_PKZIP      4003    /**< PKZIP */
#define VSDT_SND        4004    /**< Audio */
#define VSDT_JAVA       4005    /**< JAVA Aplet */
#define VSDT_PA_EXE     4006    /**< PA-RISC executable */
#define VSDT_PA_DEXE    4007    /**< PA-RISC demand-load executable */
#define VSDT_PA_SEXE    4008    /**< PA-RISC shared executable */
#define VSDT_PA_DLIB    4009    /**< PA-RISC dynamic load library */
#define VSDT_PA_SLIB    4010    /**< PA-RISC shared library */
#define VSDT_C_LISP     4011    /**< Compiled LISP */
#define VSDT_HP_FONT    4012    /**< HP-WINDOWS font */
#define VSDT_MMDF       4013    /**< MMDF mail box */
#define VSDT_S800_EXE   4014    /**< HP s800 executable */
#define VSDT_S800_SEXE  4015    /**< HP s800 shared executable */
#define VSDT_S800_DEXE  4016    /**< HP s800 demand-load executable */
#define VSDT_S800_SLIB  4017    /**< HP s800 shared library */
#define VSDT_S800_DLIB  4018    /**< HP s800 dynamic load library */
#define VSDT_PA_ROBJ    4019    /**< PA-RISC relocatable object */
#define VSDT_RIFF       4020    /**< Microsoft RIFF */
#define VSDT_MSP1       4021    /**< Microsoft Paint v1.x */
#define VSDT_MSP2       4022    /**< Microsoft Paint v2.x */
#define VSDT_CMF        4023    /**< Creative Lab CMF */
#define VSDT_TIFF       4024    /**< TIFF */
#define VSDT_WP         4025    /**< WordPerfect */
#define VSDT_RAS        4026    /**< Sun Raster(RAS) */
#define VSDT_PSD        4027    /**< Adobe PhotoShop(PSD) */
#define VSDT_MIDI       4028    /**< MIDI */
#define VSDT_DWORD      4029    /**< MS word/DOS 4.0/5.0 */
#define VSDT_MSCF       4030    /**< MS Cabinet */
#define VSDT_MP3        4031    /**< MP3 */
#define VSDT_MSFT       4032    /**< MSFT(TLB,HTA) */
#define VSDT_HLP        4033    /**< HLP */
#define VSDT_BND        4034    /**< BND */
#define VSDT_BAK        4035    /**< Trend backup file */
#define VSDT_RMF        4036    /**< Real Media */
#define VSDT_TTC        4037    /**< True Type Collection */
#define VSDT_SWF        4038    /**< Macromedia Flash */
#define VSDT_CHM        4039    /**< Compiled HTML (CHM) */
#define VSDT_CDR        4040    /**< Corel Draw file*/
#define VSDT_SAVF       4041    /**< IBM AS400 saving file*/
#define VSDT_NSF        4042    /**< Lotus Notes Database*/
#define VSDT_EPS        4043    /**< Encapsulated Postcript (EPS)*/
#define VSDT_QXD        4044    /**< QuarkXPress Document (QXD)*/
#define VSDT_OFFICE12   4045    /**< Office 12 , added by Lucy Office12*/
#define VSDT_MDI        4046    /**< Microsoft Document Imaging */
#define VSDT_FLV        4047    /**< Macromedia Flash FLV Video */
#define VSDT_OPENDOC    4048    /**< Open Document */
#define VSDT_JAR        4049    /**< Java Archive (JAR) file (VSAPI 9.700)*/
#define VSDT_APK        4050    /**< Android Application Package file (APK) (VSAPI 9.700)*/
#define VSDT_FATBINARY  4051    /**< Fat binary file */
#define VSDT_MACHO      4052    /**< Mach object file format (Mach-O) */
#define VSDT_AXML       4053    /**< Reserved for VSAPI 9.718 - Binary XML (AndroidManifest.xml) (VSAPI 9.718)*/
#define VSDT_PST        4054    /**< PST Outlook PST File */
#define VSDT_ONENOTE    4055    /**< MS OneNote File */
#define VSDT_XAR        4056    /**< Extensible Archiver (XAR) */
#define VSDT_DMG        4057    /**< Apple Disk Image */
/**************************************************************************/
/* Extented type : string mark */
#define VSDT_UUCODE     6000    /**< UUENCODE */
#define VSDT_ADB        6001    /**< Adobe Font */
#define VSDT_BINHEX     6002    /**< BINHEX */
#define VSDT_CRD        6003    /**< Windows Cardfile */
#define VSDT_FM         6004    /**< Frame Maker */
#define VSDT_GIF        6005    /**< GIF */
#define VSDT_NLM        6006    /**< Netware Loadable Module */
#define VSDT_PS         6007    /**< Postscript */
#define VSDT_RTF        6008    /**< Microsoft RTF */
#define VSDT_MIME       6010    /**< Mime base 64 */
#define VSDT_NWPDF      6011    /**< Novell system PrinfDef Device Definition */
#define VSDT_NWHLP      6012    /**< Novell Help Librarian data file */
#define VSDT_NWUNI      6013    /**< NetWare Unicode Rule Table file */
#define VSDT_VOC        6014    /**< Creative Voice Format(VOC) */
#define VSDT_PDF        6015    /**< Adobe Portable Document Format file */
#define VSDT_MSO        6016    /**< Macors in MS Office compressed by ActiveMime */
#define VSDT_SIT        6017    /**< Aladdin StuffIt Archive */
#define VSDT_YCODE      6018    /**< YEncode */
#define VSDT_LNK        6019    /**< Microsoft Shell Link */

/**************************************************************************/
/* Extented type : 2 byte magic */
#define VSDT_ARJ        2000    /**< ARJ */
#define VSDT_BMP        2001    /**< Windows BMP */
#define VSDT_CLP        2002    /**< Windows Clipboard */
#define VSDT_GZIP       2003    /**< GNU ZIP */
#define VSDT_LZW        2004    /**< LZW */
#define VSDT_TERMINFO   2005    /**< Compiled Terminfo entry */
/* lily add Fujitsu AMG compressed type */
#define VSDT_AMG        1000    /**< Fujitsu AMG compressed type */
/**************************************************************************/
/* Reserved for ATSE */
#define VSDT_TNEF       1001    /**< Outlook Item(TNEF) */
#define VSDT_ZLIB       1002    /**< ZLib format */
#define VSDT_OFFICEXML  1003    /**< Office XML */
#define VSDT_HANCOM14   1004    /**< Hancom Office 2014 */

/**    @} */
/*==========================================================================*/

/**************************************************************************/
/* definitions for sub type */

#define VSDT_WINWORD_OFFICE     0        /* MS Office or Unknown OLE */
#define VSDT_WINWORD_WINWORD20  1        /* Winword 2.0 */
#define VSDT_WINWORD_WINWORD10  2        /* Winword 1.0 */
#define VSDT_WINWORD_HWP        3        /* Hangul Word Processor (Korean) */
#define VSDT_WINWORD_ICHITARO   4        /* Ichitaro (Japan) */
#define VSDT_WINWORD_JUNGUM     5        /* JungUm Global (Korean) (ATSE 9.740) */
#define VSDT_WINWORD_OUTLOOK    6        /* Outlook Item (.msg) (ATSE 9.755) */
#define VSDT_WINWORD_PUBLISHER  7        /* MS Publisher (.pub) (ATSE 9.862) */

/* sub type EXCELL */
#define VSDT_EXCELL_OFFICE      0        /* MS Office */
#define VSDT_EXCELL_HANCELL     1        /* Hancom Hancell (ATSE 9.740) */

/* sub type OFFICE12 */ /* added by Lucy2*/
#define VSDT_OFFICE12_UNKNOWN 0
#define VSDT_OFFICE12_WORD    1
#define VSDT_OFFICE12_EXCEL   2
#define VSDT_OFFICE12_PPT     3
#define VSDT_OFFICE12_XPS     4 /*(VSAPI 9.700)*/

/* sub type HANCOM14 */
#define VSDT_HANCOM14_WORD  0            /* Hwp Standard OWPML Document */

/* sub type OFFICEXML */
#define VSDT_OFFICEXML_UNKNOWN   0       
#define VSDT_OFFICEXML_WORD    	 1       /* Word 2003 XML Document */
#define VSDT_OFFICEXML_EXCEL   	 2       /* XML Spreadsheet 2003 */
#define VSDT_OFFICEXML_PPT     	 3       /* PowerPoint XML Presentation */

/* sub type FLI */
#define VSDT_FLI_FLI    0    /* .FLI: AutoDesk Animator */
#define VSDT_FLI_FLC    1    /* .FLC: AutoDesk 3D studio */
#define VSDT_FLI_FLIC   2    /* .FLIC:AutoDesk Animator Pro  */ 

/* sub type - WRT */
#define VSDT_WRT_WIN    0    /* Windows Write */
#define VSDT_WRT_DOS    1    /* Word for DOS */

/* Sub file type - EXE */
#define VSDT_EXE_DOS    0    /* DOS EXE */
#define VSDT_EXE_W16    1    /* WIN16 EXE */
#define VSDT_EXE_W32    2    /* WIN32 EXE */
#define VSDT_EXE_OS2    3    /* OS2 EXE */
#define VSDT_DLL_W16    4    /* WIN16 DLL */
#define VSDT_DLL_W32    5    /* Win32 DLL */
#define VSDT_VXD        6    /* Windows VxD */
#define VSDT_VXD_OS2    7    /* OS/2 2.x VxD */
#define VSDT_EXE_MIPS   8    /* NT/MIPS EXE */
#define VSDT_EXE_PKLITE 9    /* PKLITE EXE */
#define VSDT_EXE_LZEXE  10    /* LZEXE */
#define VSDT_EXE_DIET   11    /* DIET EXE */
#define VSDT_EXE_ZIP    12    /* PKZIP EXE */
#define VSDT_EXE_ARJ    13    /* ARJ EXE */
#define VSDT_EXE_LZH    14    /* LZH EXE */
#define VSDT_EXE_LZH_MK 15    /* LZH EXE used by ZipMail */
#define VSDT_EXE_ASPACK 16    /* ASPACK */
#define VSDT_EXE_UPX    17    /* UPX EXE */
#define VSDT_EXE_MSIL   18    /* MSIL */
#define VSDT_EXE_ASPACK2 19    /* ASPACK 2.x */
#define VSDT_EXE_WWPACK 20    /* WWPACK */
#define VSDT_EXE_PETITE 21    /* PETITE */
#define VSDT_EXE_PEPACK 22    /* PEPACK */
#define VSDT_EXE_MEW11  23    /* MEW 1.1 */
#define VSDT_EXE_MEW05  24    /* MEW 0.5 */
#define VSDT_EXE_MEW10  25    /* MEW 1.0 */
#define VSDT_EXE_AMD64  26    /* AMD64 EXE */
#define VSDT_DLL_AMD64  27    /* AMD64 DLL */
#define VSDT_EXE_ARM	28	/* ARM EXE*/
#define VSDT_EXE_THUMB	29	/* THUNB EXE*/
#define VSDT_EXE_MISC   30	/* Miscellaneous EXE*/

/* Sub file type - COM */
#define VSDT_COM_DOS    0    /* DOS COM */
#define VSDT_COM_PKLITE 1    /* PKLITE COM */
#define VSDT_COM_DIET   2    /* DIET COM */
#define VSDT_COM_LZH    3    /* LZH COM */

/* Sub file type - ELF */
#define VSDT_ELF_ELF    0
#define VSDT_ELF_REL    1
#define VSDT_ELF_EXE    2
#define VSDT_ELF_LIB    3
#define VSDT_ELF_CORE   4

/* Sub file type - EPOC */
#define VSDT_EPOC_BIN 0
#define VSDT_EPOC_EXE 1
#define VSDT_EPOC_LIB 2
#define VSDT_EPOC_COMPRESSED 3

/* Sub file type - SIS */
#define VSDT_SIS_2ND    0
#define VSDT_SIS_3RD    1

/* sub type - PDF */
#define VSDT_PDF_1      0
#define VSDT_PDF_1_0    1
#define VSDT_PDF_1_1    2
#define VSDT_PDF_1_2    3
#define VSDT_PDF_1_3    4
#define VSDT_PDF_1_4    5

/* sub type - FM */
#define VSDT_FM_DOC     0    /* Frame Maker documentation file */
#define VSDT_FM_MIF     1    /* Frame Maker MIF file */
#define VSDT_FM_MML     2    /* Frame Maker MML file */
#define VSDT_FM_BOOK    3    /* Frame Maker Book file */
#define VSDT_FM_DICT    4    /* Frame Maker dictionary file */
#define VSDT_FM_FONT    5    /* Frame Maker font file */
#define VSDT_FM_IPL     6    /* Frame Maker IPL */

/* sub type - ADB */
#define VSDT_ADB_FNTM   0    /* Adobe font metrics */
#define VSDT_ADB_FNTB   1    /* Adobe font bits */

/* sub type - RIFF */
#define VSDT_RIFF_AVI   0    /* .AVI */
#define VSDT_RIFF_WAV   1    /* .WAV */
#define VSDT_RIFF_BND   2    /* .BND */
#define VSDT_RIFF_RMI   3    /* .RMI */
#define VSDT_RIFF_RDI   4    /* .RDI */
#define VSDT_RIFF_CDA   5    /* .CDA */
#define VSDT_RIFF_ANI   6    /* .ANI */
#define VSDT_RIFF_CMX   7    /* .CMX */

/* sub type - LZW */
#define VSDT_LZW_LZW    0    /* compressed 16 bits */
#define VSDT_LZW_PCK    1    /* packed data */
#define VSDT_LZW_CMP    2    /* compacked data */
#define VSDT_LZW_LZH    3    /* SCO compressed -H */

/* lily add to test sub type - vsdttext */
#define VSDT_TEXT_SCRIPT    0
#define VSDT_TEXT_HTML      1
#define VSDT_TEXT_PRC       2    /* special for PALM 10/9 */
#define VSDT_TEXT_ASP       3
#define VSDT_TEXT_GENERAL   4
#define VSDT_TEXT_AS        5    /* for ActiveScan-added type */
#define VSDT_TEXT_XDP       6    /* XML Data Package is created by Adobe Systems in 2003. */

/* sub type for MacBinary */
#define VSDT_MACBIN_I       0
#define VSDT_MACBIN_II      1
#define VSDT_MACBIN_III     2

/* sub type for DWG */
#define VSDT_DWG_AUTOCAD    0    /* AutoCAD DWG */
#define VSDT_DWG_R2000      1    /* AutoCAD R2000 */

/* sub type for MBX */
#define VSDT_MBX_OUTLOOK4   0
#define VSDT_MBX_UNIX       1
#define VSDT_MBX_FOXMAIL    2

/* sub type for MDB */
#define VSDT_MDB_ORIGINAL   0    /* MS Access(MDB) */
#define VSDT_MDB_2K         1    /* MS Access 2000/XP (aldous, 03/01/2002) */
#define VSDT_MDB_20         2    /* MS Access(MDB)2.0 */
#define VSDT_MDB_2007       3    /* MS Access 2007*/  /*added by Lucy3 */

/* sub type for VSDT_MSO */
#define VSDT_MSO_FILE       0 /* Outlook MSO File  */
#define VSDT_MSO_DATA       1 /* Exchange MSO DATA */

/* sub type for VSDT_OPENDOC */
#define VSDT_OPENDOC_UNKNOWN      0
#define VSDT_OPENDOC_TEXT         1
#define VSDT_OPENDOC_GRAPHICS     2
#define VSDT_OPENDOC_PRESENTATION 3
#define VSDT_OPENDOC_SPREADSHEET  4
#define VSDT_OPENDOC_FORMULA      5
#define VSDT_OPENDOC_DATABASE     6

/* sub type for SIT */
#define VSDT_SIT5           0
#define VSDT_SITX           1

/* sub type for SWF */
#define VSDT_SWF_FWS        0
#define VSDT_SWF_CWS        1
#define VSDT_SWF_ZWS        2

/* sub type for Mach-O */
#define VSDT_MACHO_UNKNOWN  0
#define VSDT_MACHO_X86      1
#define VSDT_MACHO_X64      2

/* sub type for OneNote */
#define VSDT_ONENOTE_ONE            0
#define VSDT_ONENOTE_ONETOC2        1

/*==========================================================================*/

/* Layer Control */
#ifndef KDWINNT
#define VS_MAX_ABSLAYER     20 /* maximum absolute layer supported */
#define VS_MAX_DECOMP       20 /* maximum decompression layer supported */
#define VS_MAX_OLE_LAYER    20 /* maximum OLE Insert layer supported */
#else
#define VS_MAX_ABSLAYER     15 /* for NTKD, maximum absolute layer supported */
#define VS_MAX_DECOMP        6 /* for NTKD, maximum decompression layer supported */
#define VS_MAX_OLE_LAYER    15 /* for NTKD, maximum OLE Insert layer supported */
#endif

#define VS_MAX_PACK_LAYER   17
#define VS_DEF_PACK_LAYER   17

#define VS_MAX_UNPACKSIZE_LIMIT 0x7FFFFFFF  /*  2G Bytes */
#define VS_DEF_UNPACKSIZE_LIMIT 0x01000000  /* 16M Bytes */

#define VS_DEF_OLEEMBEDSCANLAYER VS_MAX_OLE_LAYER

#define VS_MAX_RTF_LAYER         10 /* Useless */
#define VS_DEF_RTFSCANLAYER       5 /* Useless */
#define VS_MAX_MSG_LAYER         10 /* Useless */
#define VS_DEF_MSGSCANLAYER       5 /* Useless */





#define VS_HEURISTICLEVEL 2 /* default heuristiclevel */
/* for heuristic report */
#define    VS_HEUR_WORD95          1
#define    VS_HEUR_VBA5            2
#define    VS_HEUR_VBA3            3
#define    MAX_HEUR_RESULT_FLAG    6

/* define the switching flag for office exploit feature */
#define VS_HEUR_OLEEXPlOIT_DISABLE          0x00000000
#define VS_HEUR_OLEEXPlOIT_PTNSCAN          0x00000001
#define VS_HEUR_OLEEXPlOIT_SCODESCAN        0x00000002
#define VS_HEUR_OLEEXPlOIT_HEURISTIC        0x00000004
#define VS_HEUR_OLEEXPLOIT_CVE20120158      0x00000008
#define VS_HEUR_OLEEXPLOIT_EXEPAYLOAD       0x00000010
#define VS_HEUR_OLEEXPLOIT_CVE20060022      0x00000020
#define VS_HEUR_OLEEXPLOIT_CVE20090566      0x00000040  /* ATSE 9.740 */
#define VS_HEUR_OLE_VBE6_LOAD_DLL           0x00000080  /* ATSE 9.740 */
#define VS_HEUR_OLEEXPLOIT_ENCRYPTEDOBJ     0x00000100  /* ATSE 9.740 */
#define VS_HEUR_OLEEXPLOIT_MSCOMCTL_A       0x00000200  /* ATSE 9.740 */
#define VS_ENTROPY_STATISTIC_SIZE           256
/* define the switching flag for pdf exploit feature */
#define VS_HEUR_PDFEXPLOIT_DISABLE                  0x00000000
#define VS_HEUR_PDFEXPLOIT_JS_INBADPDF              0x00000001
#define VS_HEUR_PDFEXPLOIT_POLYJS1                  0x00000002
#define VS_HEUR_PDFEXPLOIT_POLYJS2                  0x00000004
#define VS_HEUR_PDFEXPLOIT_EXTJSFAIL                0x00000008
#define VS_HEUR_PDFEXPLOIT_BADLAUNCH                0x00000010
#define VS_HEUR_PDFEXPLOIT_CVE20090658              0x00000020
#define VS_HEUR_PDFEXPLOIT_LARGEPAYLOAD             0x00000040
#define VS_HEUR_PDFEXPLOIT_OBFUSCATED               0x00000080
#define VS_HEUR_PDFEXPLOIT_CVE20100188              0x00000100
#define VS_HEUR_PDFEXPLOIT_HARDCODEPTN              0x00000200
/* new */
#define VS_HEUR_PDFEXPLOIT_CVE20093459              0x00000400
#define VS_HEUR_PDFEXPLOIT_CVE20102883              0x00000800
#define VS_HEUR_PDFEXPLOIT_CVE20124149              0x00001000
#define VS_HEUR_PDFEXPLOIT_CVE20124155              0x00002000
#define VS_HEUR_PDFEXPLOIT_CVE20124157              0x00004000
#define VS_HEUR_PDFEXPLOIT_EXTSTREMFAIL_FOUND       0x00008000
#define VS_HEUR_PDFEXPLOIT_ABNORMAL_FILTER_COUNT    0x00010000
#define VS_HEUR_PDFEXPLOIT_ABNORMAL_ASCII_SPACING   0x00020000
#define VS_HEUR_PDFEXPLOIT_OBFUSCATED_XFA           0x00040000
#define VS_HEUR_PDFEXPLOIT_UNREFERENCE_JS           0x00080000
#define VS_HEUR_PDFEXPLOIT_UNREFERENCE_FONTFILE     0x00100000
#define VS_HEUR_PDFEXPLOIT_SUSPICIOUS_JS_IN_XFA     0x00200000
#define VS_HEUR_PDFEXPLOIT_JS_HDJSFN_FOUND          0x00400000
#define VS_HEUR_PDFEXPLOIT_JS_USE_INFO              0x00800000
#define VS_HEUR_PDFEXPLOIT_JS_USE_ANNOTS            0x01000000

/* define the switching flag for swf heuristic detection */
#define VS_HEUR_SWFEXPlOIT_DISABLE          0x00000000
#define VS_HEUR_SWFEXPlOIT_AS2EXEC          0x00000001
#define VS_HEUR_SWFEXPlOIT_AS2DROP          0x00000002
#define VS_HEUR_SWFEXPlOIT_CVE20091862      0x00000004
#define VS_HEUR_SWFEXPlOIT_JITSPRAY         0x00000008
#define VS_HEUR_SWFEXPlOIT_CVE20070071      0x00000010
#define VS_HEUR_SWFEXPlOIT_W                0x00000020
#define VS_HEUR_SWFEXPlOIT_CVE20120779      0x00000040
#define VS_HEUR_SWFEXPlOIT_CVE20122037      0x00000080
#define VS_HEUR_SWFEXPlOIT_CVE20121535      0x00000100
#define VS_HEUR_SWFEXPlOIT_CVE20130634      0x00000200  /* ATSE 9.740 */

/* define the switching flag for rtf heuristic detection */
#define VS_HEUR_RTFEXPlOIT_CVE20103333      0x00000001
#define VS_HEUR_RTFEXPlOIT_CVE20101901      0x00000002
#define VS_HEUR_RTFEXPlOIT_PAYLOAD          0x00000004  /* ATSE 9.740 */
#define VS_HEUR_RTFEXPlOIT_MALFORMED_A      0x00000008  /* ATSE 9.740 */
#define VS_HEUR_RTFEXPlOIT_CVE20122539      0x00000010  /* ATSE 9.740 */

/* define the switching flag for lnk heuristic detection */
#define VS_HEUR_LNKEXPlOIT_BADEXEC          0x00000001

/* define the switching flag for RTLO heuristic detection */
#define VS_HEUR_RTLO_PE                     0x00000001
#define VS_HEUR_RTLO_NAME                   0x00000002
#define VS_HEUR_NAME_DOUBLE_EXT             0x00000004  /* ATSE 9.740 */
#define VS_HEUR_NAME_COVER_HIDE             0x00000008  /* ATSE 9.740 */

/* define the switching flag for MISC heuristic detection */
#define VS_HEUR_MISC_MIDIEXPLOIT_CVE20120003 0x00000001
#define VS_HEUR_MISC_HLP_HSR_Y               0x00000002
#define VS_HEUR_MISC_HLP_DEC_O               0x00000004
#define VS_HEUR_MISC_HLP_DYN_T               0x00000008
#define VS_HEUR_MISC_FLVEXPLOIT_CVE20130638  0x00000010 /* ATSE 9.740 */
#define VS_HEUR_MISC_PNGEXPLOIT_CVE20131331  0x00000020 /* ATSE 9.740 */
#define VS_HEUR_MISC_JPG_A                   0x00000040 /* ATSE 9.740 */

/* define the switching flag for TEXT heuristic detection */
#define VS_HEUR_HTJS_CVE20121889             0x00000001
#define VS_HEUR_HTJS_HDJSFN                  0x00000002
#define VS_HEUR_HTJS_PACRYP                  0x00000004
#define VS_HEUR_XFA_SUSPICIOUS_JS_INIT_EVENT 0x00000008

/* define the switching flag for JAVA heuristic detection */
#define VS_HEUR_JAVA_EXEC                   0x00000001
#define VS_HEUR_JAVA_CVE20113544            0x00000002
#define VS_HEUR_JAVA_CVE20121723            0x00000004
#define VS_HEUR_JAVA_CVE20124681            0x00000008
#define VS_HEUR_JAVA_CVE20130422            0x00000010  /* ATSE 9.740 */
#define VS_HEUR_JAVA_HDFN                   0x00000020  /* ATSE 9.740 */
#define VS_HEUR_JAVA_TC                     0x00000040  /* ATSE 9.740 */
#define VS_HEUR_JAVA_CVE20130431            0x00000080  /* ATSE 9.740 */
#define VS_HEUR_JAVA_CMM_SPRAY              0x00000100  /* ATSE 9.740 */

/* define max extract file size limit*/
#define ExtractFile_MAX_SIZE 0x7FFFFFFF


/* prototype definition of functions as parameters */

#define VS_UNIVERSAL_CB_SCAN_INFO   1 /* reserved for TDA */

typedef struct _VS_UNIVERSAL_CALLBACK_HANDLE {
    int     ucb_size;               /* size of VS_UNIVERSAL_CALLBACK_HANDLE */
    int     (*ucb_func)(VSCTYPE vsc, void* ucb_user_para, VULONG ucb_mode, void* ucb_info);
    void*   ucb_user_para;          
    /* any future expansion should be added here */
} VS_UNIVERSAL_CALLBACK_HANDLE;

#define     VS_UCB_BREAK            1        


/**    break callback
 *    @ingroup Callback
 *    @param[in]     mode        status
 *    @param[in]     Current    current process
 *    @param[in]     Total    how many memory are this segment
 *    @param[out]    Info        Status information text
 *    @param[in,out] para        application's informations
 *    @retval 0            ok
 *    @retval -1            break
 */
typedef int VS_BREAK_FUNC(int mode,int Current,int Total,void *para);

/**    pre-extract-arch callback function 
 *    @ingroup Callback
 *    @param[in]     path        path information contain in archive
 *    @param[in]     archivename    name of archive
 *    @param[in]     Para        user defined Parameter
 *    @retval 0            continue
 *    @retval 1            skip this file
 *    @retval -1            break the whole extraction
 */
typedef int PASCAL EXPORT VS_PRE_EXTRACT_ARCH_FUNC(VSCTYPE,char *path,char *archivename,void *Para);

/**    ask-action callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     File        file to process
 *    @param[out]    action    return value
 */
typedef int PASCAL EXPORT VS_ASK_ACTION_FUNC(VSCTYPE vcp ,char *,short *,VUCHAR *);

/**    log-file-path callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 *    @param[in,out] path        buffer to contain log file path
 */
typedef int PASCAL EXPORT VS_LOG_FILE_PATH_FUNC(VSCTYPE vsc,char *path);

/**    rename-file-name callback function
 *    @ingroup Callback
 *    @param[in]     OldName     the original file name
 *    @param[in,out] NewName    buffer to contain new file name
 */
typedef int PASCAL EXPORT VS_RENAME_FILE_NAME_FUNC(char *,char *);

/**    user-decode callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 */
typedef int VS_USER_DECODE_FUNC(VSCTYPE vsc,VSHANDLE *,VSHANDLE *,char *);


/*==================================================================*/
/* data structure contains file information when call user defined */
/* VSProcessFile call back function */
typedef struct {
    /* status code return from user defined process func */
    int pfcb_status;

    /* current compression layer */
    int pfcb_CompressLayer;

    /* user defined parameter passed from VSProcessFile */
    void *pfcb_Para;

    /* table of names in compressed archive */
    char *pfcb_NameInArchive[VS_MAX_DECOMP];

    /* data type of each compression layer */
    short pfcb_Type[VS_MAX_DECOMP];

    /* data sub type of each compression layer */
    short pfcb_SubType[VS_MAX_DECOMP];

    /* extra information set by user defined process function */
    void *pfcb_Info;

    /* name of original file */
    char *pfcb_FileName;

    /* new file name after renamed or moved if action is rename or move */
    char pfcb_NewFileName[MAX_PATH_LEN];

    /* path of current file been processed */
    char pfcb_FilePath[1];
} VSPFCB;


#define AFI_BLOCK_INVALID                   0x00000000
#define AFI_BLOCK_VALID                     0x00000001
#define AFI_BLOCK_ERR                       0x00000002

typedef struct _VSAFIH
{
    VULONG          ulID;
    VULONG          ulVersion;  /* version of the flags in structure */
    VULONG          ulStatus;
    VULONG          ulSize;     /* size of struture(the data after VSAFIH) */
    struct _VSAFIH* pNext;
} VSAFIH;

#define VSAFIH_SIZE_REQUIREMENT(s,m)      ( VS_STRUCTSIZE_REQUIREMENT(s,m) - sizeof(VSAFIH) )
#define VSAFIH_EXIST_MEMBER(h,s,m)       ( VSAFIH_SIZE_REQUIREMENT(s,m) <= (h)->ulSize )



/*************/
/* afi_def.h */
/*************/
#include "afi_def.h"



/**    Process file callback
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     pfcb        file information prepared by processing module
 *    @param[in]     time        represent time the call back been called for same file
 *    @param[in,out] action    Over written action desired
 *        - if time = 1, call back function can set to action
 *          different than current setting. -1 if no over written
 *        - if time = 2, this value tell call back function the
 *          result of action been taken, return value is ignored
 *    @return don't care
 */
typedef int PASCAL EXPORT VS_PROCESS_FILE_CALLBACK_FUNC(VSCTYPE vcp ,VSPFCB *,int, int *);

typedef int PASCAL EXPORT VS_ADV_FILE_INFO_CALLBACK_FUNC(VSCTYPE vsc, const VSAFIH* data, void* para); /*[AFI_PE]*/
/* AFI callback function return code */
#define AFI_RC_NORMAL               0
#define AFI_RC_SKIP_CURRULE         1
#define AFI_RC_SKIP_CURFILE         SKIP_ERR
#define AFI_RC_SKIP_ALLFILE         BREAK_ERR


/* VSFBCB Version 2 
   - Supported Generic Feedback Blocks
*/
#define VSFBCB_VER_2 2
#define VSFBCB_VER_3 3
#define VSFBCB_VER_LAST (VSFBCB_VER_3)

/*==================================================================*/
/* Structure that contains all information for Feedback in both Kernel and User VSAPI */
typedef struct {
    /* sizeof(VSFBCB) */
    VULONG  nSize;

    /* VSAPI's Feedback Schema ID */
    VCHAR   *pVsapiFBSchema;

    /* VSAPI's Version */
    VCHAR   *pModuleVersion;

    /* VSAPI's ID */
    VULONG  nModuleID;

    /* VSAPI's Feedback Blob */
    VCHAR   *pFeedbackBlob;

    /* Blob Size */
    VULONG  nFeedbackBlobSize;

    /* File SHA1 size = 20 bytes */
    VUCHAR  *pFileSHA1;

    /* File Full Path for Sending File */
    VCHAR   *pSendFilePath;

    /* File Size of pSendFilePath */
    VUINT64 qwSendFileSize;

    /* Structure version */
    VULONG  nStructVersion;

    /* Generic Feedback Blob */
    VCHAR   *pGenericFeedbackBlob;

    /* Generic Feedback Blob Size */
    VULONG  nGenericFeedbackBlobSize;

    /* Indicated to feedback process memory*/
    VULONG bFeedbackProcessMemory;

    /* --------------------------------------------------------------------- */
    /* ALWAYS ADD NEW MEMBERS IN THE END, 
     * NEVER INSERT NEW MEMBER IN OLD STRUCTURE */
    /* --------------------------------------------------------------------- */
} VSFBCB;

/**    Feedback callback
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     pfcb       file information prepared by processing module
 *    @param[in]     buf1       VSAPI's feedback blob
 *    @param[in]     len        VSAPI's feedback blob size
 *    @param[in]     buf2       reserved
 *    @param[in]     buf3       VSAPI's feedback schema
 *    @param[in]     buf4       VSAPI's version
 *    @param[in]     id         VSAPI's Feedback Module ID
 *    @return don't care
 */
typedef int PASCAL EXPORT VS_FEEDBACK_CALLBACK_FUNC(VSCTYPE vcp, VSPFCB *, VSFBCB *);

/*==================================================================*/
/* special data struct prepare by virus scan */
/* this structure will be casted to (void *) and place in pfcb_info in */
/* VSPFCB structure when call processing file call back */
/* virus information set by scan engine; one node per virus */
typedef struct VI {
    VULONG vi_FirstJump;
    VUCHAR vi_JumpDepth;
    VUCHAR vi_FirstByte;
    VUCHAR vi_VirusType;
    VUCHAR vi_VirusFlag;
    char vi_VirusName[20]; /* name of virus */
    int vi_VirusNumber; /* unique number of virus */
    VUSHORT vi_Offset;
} VIRINFO;

#define DI_TYPE_VIR 0
#define DI_TYPE_CDF 1
#define DI_TYPE_UDF 2
#define DI_TYPE_MAX 3

#define DETECTION_INFO_VER 1

typedef struct DETECTION_INFO {
    VUSHORT Version;            /* (ver=1) Version of structure */
    VUSHORT Type;               /* (ver=1) Type of detection 0:VIR, 1:CDF, 2:UDF (ref. DI_TYPE_xxx) */
    const char* LongVirusName;  /* (ver=1) Long virus name (a valid null-terminated string) */
    const char* SHA1;           /* (ver=1) SHA1 (NULL for some some reason. Ex. error occurred) */
    VULONG DirectAction;        /* (ver=1) Action value for custom defense (CDF & UDF) */
    struct DETECTION_INFO* next;
} DETECTION_INFO;

/* ATSE 9.755 supported, check vi_ExtendSize!=0 to handle Compatibility  */
typedef struct VIRINFO_EX {
    VULONG vi_FirstJump;
    VUCHAR vi_JumpDepth;
    VUCHAR vi_FirstByte;
    VUCHAR vi_VirusType;
    VUCHAR vi_VirusFlag;
    char vi_VirusName[20]; /* name of virus */
    int vi_VirusNumber; /* unique number of virus */
    VUSHORT vi_ExtendSize;

    /* Extend Info */
    const char* vi_long_virname;
    VULONG  vi_atse_rule_aggressive_level; /* 0 is not ATSE rule */
    VULONG  vi_atse_rule_category;  /* ATSE_RC_xxx */
    DETECTION_INFO *vi_detection_info; /* detection information for Official, CDF, UDF patterns */
} VIRINFO_EX;

#define VIRINFO_EXTEND_SIZE     (sizeof(VIRINFO_EX) - sizeof(VIRINFO))
#define EXIST_VIRINFOEX_MEMBER(vix, ex_member) \
    ( VS_STRUCTSIZE_REQUIREMENT(VIRINFO_EX, ex_member) <= (sizeof(VIRINFO) + (vix)->vi_ExtendSize) )
#define EXIST_VIRINFO_MEMBER(vi, ex_member) \
( \
    (_offsetof(VIRINFO_EX, ex_member) + sizeof(((VIRINFO_EX*)0)->ex_member) <= (sizeof(VIRINFO) + (vi->vi_Offset))) /* vi_Offset is equal to vi_ExtendSize */ \
)


/*  atse rule category */
#define ATSE_RC_UNKNOWN             0   /* Unknown category */
#define ATSE_RC_CVE                 1   /* rule is follow CVE's description */
#define ATSE_RC_FILE_NAME           2   /* rule detect abnormal file name (Ex. RTLO, double extension name) */
#define ATSE_RC_FILE_STRUCTURE      3   /* rule detect abnormal file structure */
#define ATSE_RC_PAYLOAD             4   /* rule detect abnormal file payload (Ex. append executable file in a document)  */
#define ATSE_RC_DOS_COMMAND         5   /* rule detect abnormal DOS command (Ex. create and execute file) */
#define ATSE_RC_JAVASCRIPT          6   /* rule detect abnormal Javascript (Ex. Javascript obfuscation) */
#define ATSE_RC_ACTIVEX             7   /* rule detect abnormal Active X */
#define ATSE_RC_ACTIONSCRIPT        8   /* rule detect abnormal Active Scripe */
#define ATSE_RC_MACRO               9   /* rule detect abnormal Macro */
#define ATSE_RC_XFA                 10  /* rule detect abnormal XFA */
#define ATSE_RC_SHELLCODE           11  /* rule detect possible shellcode */
#define ATSE_RC_PACKAGE             12  /* rule detect suspect components in file */
#define ATSE_RC_JAVA                13  /* rule detect abnormal Java Class */
#define ATSE_RC_VBSCRIPT            14  /* rule detect abnormal Java Class */

/* boot virus information structure */
typedef struct _BPMINFO {
    long bpm_DriveFlag;     /* drive number scanning */
    int bpm_VirusNumber;    /* unique number of boot virus */
    VUCHAR bpm_VirusType;    /* virus pattern attributes */
    char bpm_VirusName[20]; /* name of boot virus */
    VUCHAR *bpm_MemPtr;     /* memory pointer for memory virus */
} BPMINFO;

#define VSEZDriveFlag        0x00000001L

/*==================================================================*/
/* VSC information */
typedef struct {
    VSCTYPE    vi_vsc;     /* virus scan context token */
    long    vi_caller;    /* caller ID */
    char    vi_LogID[9];    /* Program group id for logging system */
    char    vi_Version[11]; /* version string of VSAPI */
    short    vi_VirusPatternVersion;
    long    vi_VirusPatternNumber;
} VSCINFO;

/* VSAPI version */
typedef struct {
    VULONG  major;
    VULONG  minor;
    VULONG  revision;
    VULONG  build;
} VSVERSION;

/*==================================================================*/
/**    post-extract-arch callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     Res        resource information contain in archive
 *    @param[in]     Para        user defined Parameter
 *    @retval 0            continue
 *    @retval -1            break the whole extraction
 */
typedef int PASCAL EXPORT VS_POST_EXTRACT_ARCH_FUNC(VSCTYPE,RESOURCE *Res,void *Para);

/**    proc callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 */
typedef int PASCAL EXPORT VS_PROC_FUNC(VSCTYPE vsc,RESOURCE *Res,void **,void *);

typedef struct {
    int       Layer;
    VSDTYPE   DType;
    RESOURCE* Res;
    void*     PExt;        /* Reserved ptr for extension in the future. */
} VS_OEI;
/**    OleEmbed callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 */
typedef int PASCAL EXPORT VS_OLE_EMBED_CALLBACK_FUNC(VSCTYPE vcp , VS_OEI *oei, void* para);


/*==================================================================*/
/* Process directory structure */
typedef struct _PROCDF {
    VSCTYPE pd_vsc;
    VULONG pd_Type; /* Type of path */
    VULONG pd_TotalDirCount;
    VULONG pd_TotalFileCount;
    VULONG pd_ProcDirCount;
    VULONG pd_ProcFileCount;
    char *pd_DirPath;    /* full file path */
    char *pd_FileName;    /* file name */
    char *pd_SearchMask;    /* search mask */
    void *pd_Para;
    VS_PROC_FUNC *pd_FileProcFunc;
    int pd_ProcessStatus;    /* return code from VSProcessFile */
    short pd_Mode;    /* Calling mode */
} PROCDF;

/** service callback function
 *    @ingroup Callback
 */
typedef int VS_SERVICE(PROCDF *);

/* all item are null terminated */
/* total header size 35 */
typedef struct _LOGH {
    char lg_ID[9];        /* Log group ID */
    char lg_Date[9];    /* Event date, ex: 19970304 (= 3/4/1997) */
    char lg_Time[7];    /* Event time, ex: 122334 (= 12:23:34) */
    char lg_Logger[9];    /* Calling Program id */
    char lg_Event[5];    /* Event ID */
} VSLOGH;

/** log callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     logh        buffer to store log entry header
 *    @param[in]     ExLog    how many extended log item
 *    @param[in]     ExLogV    table contains pointer to extended log items
 *    @param[in]     para        user defined parameter
 *    @retval 0            ok
 *    @retval -1            break
 */
typedef int VS_LOG_FUNC(VSCTYPE vsc,VSLOGH *logh,int ExLog,char **ExLogV,void *para);

/** user-log callback function
 *    @ingroup Callback
 *    @param[in]     vsc        virus scan context
 *    @param[in]     logh        log header
 *    @param[in]     ExtraItemNumber    like argc, tell homw many extra win ExtraItem table
 *    @param[in]     ExtraItem    like argv, store of pointer to each extra log item
 *    @return don't care
 */
typedef int PASCAL EXPORT VS_USER_LOG_FUNC(VSCTYPE vsc,VSLOGH *logh,int ExtraItemNumber,char **ExtraItem);

typedef struct
{
    VULONG    vi_Handle;
    VULONG    vi_VirusType;
    VULONG    vi_CleanInfo;
    char    vi_VirusName[17];
} VSVirusInfo;

typedef struct
{
    VUSHORT vspi_PatternVersion;
    VUSHORT vspi_DetectableVirusNumber;
    long    vspi_PatternFileLength;
    char    vspi_CreatedDate[3];    /* 0: year, 1: month, 2: day */
} VSPatternInfo;

typedef struct
{
    VUSHORT vspi_PatternVersion;
    VUSHORT vspi_DetectableVirusNumber;
    long    vspi_PatternFileLength;
    char    vspi_CreatedDate[3];    /* 0: year, 1: month, 2: day */
    VULONG    vspi_InternalVersion;
} VSPatternInfoEx;

typedef struct
{
    VUSHORT dwSize;             /*Please fill this variable with sizeof(VSPatternInformation), before call VSGetVirusPatternInformation.*/
    VUSHORT vspi_PatternVersion;
    long    vspi_PatternFileLength;
    char    vspi_CreatedDate[3];    /* 0: year, 1: month, 2: day */
    VULONG    vspi_InternalVersion;
    VULONG  vspi_DetectableVirusNumber;
} VSPatternInformation;


/* for P1K API */
#define PATTERN_CRC_ERR          -1 /* pattern CRC error */
#define PATTERN_NOT_FOUND_ERR    -2 /* can not find pattern file */
#define PATTERN_VERSION_ERR      -3 /* pattern version mismatch */
#define OLD_PATTERN_ERR          -4 /* old pattern file */
#define REMOVE_PATTERN_ERR       -5 /* remove pattern file error */
#define EXTTABLE_NOT_FOUND_ERR   -6 /* extension name table not found */

typedef struct
{
    char PatternFileName[16];
    int  PatternVersion;
    int  CheckFlag;
} VSPtnCheckInfo;

/* for Backup file header */
typedef struct _VSBackupFileInfo
{
    char FileName[MAX_PATH_LEN];
    char PlatformName[32];
    long FileAttrib;
    VULONG IsUnicode;
    VULONG BackupTime;
} VSBackupFileInfo;

/* string offset of each item in log entry header */
#define slg_ID          (&logbuf[0])
#define slg_Sep1        (logbuf[8])
#define slg_Date        (&logbuf[9])
#define slg_Sep2        (logbuf[17])
#define slg_Time        (&logbuf[18])
#define slg_Sep3        (logbuf[24])
#define slg_Logger      (&logbuf[25])
#define slg_Sep4        (logbuf[33])
#define slg_Event       (&logbuf[34])
#define slg_Sep5        (logbuf[38])
#define LOG_HEAD_SIZE   39
#define MAX_EXTLOG      20    /* maximum extened log item number */

/* reserved Log event mark */
#define VSLOG_BP_VIRUS      "VRBP" /* virus in Boot/partition */
#define VSLOG_COMP_VIRUS    "VRZF" /* virus in compressed file */
#define VSLOG_FILE_VIRUS    "VRFL" /* virus file */
#define VSLOG_MEM_VIRUS     "VRMM" /* virus in memory */
#define VSLOG_READ_PATTERN  "RVPN" /* read virus pattern */
#define VSLOG_SCAN_REPORT   "SNRP" /* scan report */
#define VSLOG_SCAN_START    "SNST" /* scan start */
#define VSLOG_SCAN_TARGET   "SNTG" /* scan target */
#define VSLOG_COMP_ACT      "VFAC" /* action on virus file */

#define VSVSoftMiceType     0x08
#define VSVBootVirusType    0x80
#define VSVFileVirusType    0x40
#define VSVDecryptVirusType 0x20
#define VSVRemoveVirusType  0x10

/* pattern type */
#define VS_MACRO_PATTERN    0x80000000L
#define VS_SOFTMICE_PATTERN 0x40000000L
#define VS_NORMAL_PATTERN   0x20000000L
#define VS_SCRIPT_PATTERN   0x10000000L

/* control_flag value to VSProcessDir() */
#define DP_ALL_SUB_DIR      0x00000001    /* search into sub dir */
#define DP_FOLLOW_SYMLNK    0x00000002    /* follow symbolic link */
#define DP_STOP_ON_ERROR    0x00000004    /* stop on any error */

/* structure for Zip update */
typedef struct _ZIPUPLIST {
    char FileNameInZip[MAX_PATH_LEN];    /* file name in zip */
    char FileNameReal[MAX_PATH_LEN];    /* file name of file */
    int    action;                            /* action : VS_ACT_CLEAN or VS_ACT_DELETE */
    VULONG FileNumber;
    struct _ZIPUPLIST *Next;
    VULONG CompSize;                    /* for gzip */
    void* ZipCleanFunc;
} ZIPUPLIST;

/* encode action */
#define VC_ENC_RENAME       0x0001
#define VC_ENC_MOVE         0x0002
#define VC_ENC_CLEANBACKUP  0x0004


typedef struct _PATTERNFILE_ITEM{
    VUCHAR patternName[32]; /* pattern name, not include path */
    VULONG interVersion;    /* inter version */
    VULONG patternSize;     /* pattern size */
    VLONG  status;          /* pattern status : == 0 ok, == 1 unkown, <0 error */
    VULONG flag;
    void * next;            /* internal used */
} PATTERNFILE_ITEM;

typedef struct _PATTERNFILE_LIST {
    VULONG listSize;           /* sizeof(PATTERN_LIST) */
    VULONG itemSize;           /* sizeof(PATTERN_ITEM) */
    VULONG itemNum;            /* number of item*/
    PATTERNFILE_ITEM *patternItem; /* pointer to patternItem */
} PATTERNFILE_LIST;

/*==================================================================*/
/* Valid values for VSCONF.cf_ScriptPatternOption                    */

#define VSCF_SCRIPT_PTN_DEFAULT         0x00
#define VSCF_SCRIPT_PTN_PARTIAL_SCAN    0x01
#define VSCF_SCRIPT_PTN_SKIP_SLOWPTN    0x02


/*==================================================================*/
/* Valid values for the last byte of script-based pattern            */

#define VSPTN_OPT_DEFAULT               0x00
#define VSPTN_OPT_OPTIONAL              0x01

/*==================================================================*/
/* generic query function prototype for normailizer(SMV) integration */

typedef int PASCAL EXPORT VS_NORMALIZER_START_FUNC(VSCTYPE vsc, RESOURCE *res, void *para, void** normalizer_ctx);
typedef int PASCAL EXPORT VS_NORMALIZER_END_FUNC(VSCTYPE vsc,RESOURCE *res,void *para, void* normalizer_ctx);
typedef int PASCAL EXPORT VS_NORMALIZER_QUERY_INFO(void *ctx, VULONG id, void *user_param);

typedef struct _VS_NORMALIZER_CALLBACK {
    VULONG size;
    VS_NORMALIZER_START_FUNC* normalizer_start;
    VS_NORMALIZER_END_FUNC* normalizer_end;
    VS_NORMALIZER_QUERY_INFO* normalizer_query;
} VS_NORMALIZER_CALLBACK;

typedef struct VS_PLUGIN_FUNC_LIST {
    VS_PROC_FUNC *scanner;
    struct VS_PLUGIN_FUNC_LIST *next;
} VS_PLUGIN_FUNC_LIST;
typedef struct VS_PLUGIN_MODULE {
    VULONG plugin_magic;
    struct VS_PLUGIN_FUNC_LIST *list_head;
} VS_PLUGIN_MODULE;
#define PLUGIN_MAGIC 0xbea8aaddUL

#include "tmvscomm.h"

#ifdef PACK_STRUCTURE
#pragma pack()
#endif

#ifdef __cplusplus
}
#endif

#endif /* __TMVSDEF_H__ */
