/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_wince.h */
/* Description: */
/*	Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: Win32 MS Embedded Visual C++ 4.0 sp3 */
#pragma pack(4)

typedef char                    VCHAR;/* 1 byte */
typedef long                    VLONG;	/* 4 byte */
typedef unsigned long           VULONG; /* 4 byte */
typedef __int64                 VINT64;  /* 8 byte */
typedef unsigned __int64        VUINT64; /* 8 byte */

extern void     VSLongLongToString(char *cp,VUINT64 v);
extern void     VSLongToString(char *cp,VULONG v);
extern void     VSShortToString(char *cp,VUSHORT v);

extern VUINT64  VSStringToLongLong(char *string);
extern VULONG   VSStringToLong(char *string);
extern VUSHORT  VSStringToShort(char *string);

#define VSCTYPE	                long
#define VSPTN_HANDLE            long
#define DEFAULT_MOVE_DIR	    "\\Temp"
#define DEFAULT_PATTERN_DIR 	"\\Temp"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR	    "\\Temp"
#define EXPORT
#define TMVSAPI                 __declspec(dllexport)
#define MAX_PATH_LEN		    1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM          102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM          204800
#define PACK_STRUCTURE

/* for Backup file header */
typedef struct
{
	WCHAR FileName[MAX_PATH_LEN];
	char PlatformName[32];
	long FileAttrib;
	VULONG IsUnicode;
	VULONG BackupTime;
} VSBackupFileInfoW;

/*==========================================================================*/
/* VSVirusScanFileW(): scan a file for known virus after the file name pass */
/*	the file name filter */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *file: IN : file for scan */
/*	void *para: IN : user defined parameter */
/* Return : */
/*	0: file is clean, or the file failed the file name filter and has */
/*		not been scanned */
/*	>0: file is infected, return virus number */
/*	-1: failed to scan */
/*	PARA_ERR: invalid Parameter */
TMVSAPI int PASCAL EXPORT VSVirusScanFileW(VSCTYPE vsc,WCHAR *file,void *para);

/*==========================================================================*/
/* VSFileNeedProcessW() : check if file need process */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *file: IN : file path */
/* Return: */
/*	0: no */
/*	1: yes */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSFileNeedProcessW(VSCTYPE vsc,WCHAR *file);

/*==========================================================================*/
/* VSOpenFileW(): like open(), portable version for all platforms/compilers */
/* Parameters: */
/*	WCHAR *FilePath: IN : file path to open */
/*	int mode: IN : read/write mode */
/* Return: */
/*	0: ok */
/*	OPEN_R_ERR: open error */
/*	PARA_ERR: invalid parameter */
TMVSAPI HANDLE PASCAL EXPORT VSOpenFileW(WCHAR *FilePath,int mode);

/*==========================================================================*/
/* VSCopyFileW(): copy a file */
/* Parameters: */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSCopyFileW(WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSCleanVirusW() : Clean Unicode virus file */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *name: IN : file path to be cleaned */
/*	char *bakname: OUT : full path of backuped file */
/*	int len: IN : length of buffer for backuped file */
/*	int* bakrc: OUT : 0 -- backuped ok, other -- backup failed */
/* Return: */
/*	0: clean successed */
/*	-1: unable to clean */
/*	-20: no pattern */
/*	OPEN_W_ERR: Clean file create failed */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSCleanVirusW(VSCTYPE vsc,WCHAR *name,VCHAR *bakname,int len,int *bakrc);

/*==========================================================================*/
/* VSActOnFileW() : Take action on UniCode file */
/* Parameters: */
/*	VSCTYPE vsc: IN : virus scan context */
/*	short action: IN : override action setting */
/*	WCHAR *file: IN : file path */
/*	WCHAR *NewPath: OUT: buffer to store new path after renamed or moved */
/* Return: */
/*	>0: action return when asked */
/*	0: action successed */
/*	-1: action failed */
/*	-2: failed to create uniq name for new file */
/*	-3: failed to delete original file */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSActOnFileW(VSCTYPE vsc,short action,WCHAR *filepath,WCHAR *NewPath);

/*==========================================================================*/
/* VSBackupResourceW() : Backup from resource */
/* Parameters: */
/*    VSCTYPE vsc: IN: virus scan context */
/*    VSHANDLE *pSrcResource: IN : resource to be backup */
/*    VSHANDLE *pDestResource: IN : resource for backup */
/*    VSBackupFileInfoW: IN: backup information for backup header */
/* Return: */
/*    0: backup successes */
/*    OPEN_W_ERR: Clean file create failed */
/*    READ_ERR: read error */
/*    WRITE_ERR: write error */
/*    BUFFER_TOO_SHORT_ERR: insufficient buffer */
/*    PARA_ERR: invalid parameter */
/*    BREAK_ERR: VSAPI is forced to stop */
int PASCAL EXPORT VSBackupResourceW(VSCTYPE vsc, struct _VSHANDLE *pSrcResource, struct _VSHANDLE *pDestResource, VSBackupFileInfoW *pBackupInfo);

/*==========================================================================*/
/* VSBackupFileW(): backup a file */
/* Parameters: */
/*  VSCTYPE vsc: IN : virus scan context */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSBackupFileW(VSCTYPE vsc,WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSRestoreFileW(): restore a backuped file */
/* Parameters: */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSRestoreFileW(WCHAR *SrcFile,WCHAR *DestFile);

/*==========================================================================*/
/* VSEncBackupFileW(): backup a file with decoding */
/* Parameters: */
/*	WCHAR *SrcFile: IN : path of source file */
/*	WCHAR *DestFile: IN : path of destination file */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	OPEN_W_ERR: dest file create error */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSEncBackupFileW(WCHAR *SrcFile, WCHAR *DestFile);

/*==========================================================================*/
/* VSGetBackupFileInfoW(): restore a backuped file */
/* Parameters: */
/*	WCHAR *FileName: IN : backup file name */
/*  VSBackupFileInfo *BackupInfo : OUT : backup file information */
/* Return: */
/*	0: ok */
/*	-1: source file lseek error */
/*	-2: dest file lseek error */
/*	-3: failed to get source file size */
/*	-4: Invalid copy offset */
/*  -5: Not a backup file */
/*	READ_ERR: read error */
/*	WRITE_ERR: write error */
/*	OPEN_R_ERR: file open error */
/*	PARA_ERR: invalid parameter */
TMVSAPI int PASCAL EXPORT VSGetBackupFileInfoW(WCHAR *BackupFileName, VSBackupFileInfoW *BackupInfo);
