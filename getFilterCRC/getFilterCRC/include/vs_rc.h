#ifndef __VS_RC_H__
#define __VS_RC_H__

/* vsapi reserve 0~-9999 */
#define VS_RC_NO_ERROR                0
#define PARA_ERR                    -99
#define NO_MEM_ERR                  -98
#define WRITE_ERR                   -97
#define READ_ERR                    -96
#define OPEN_W_ERR                  -95
#define OPEN_R_ERR                  -94
#define DISABLE_ERR                 -93 /* decode/decompress is disabled */
#define PSW_ZIP_ERR                 -92 /* password protected compressed file */
#define NO_SUPP_ERR                 -91 /* un-supported compression method or file version */
#define BAD_VSC_ERR                 -90
#define SKIP_ERR                    -89
#define BREAK_ERR                   -88
#define ACCESS_ERR                  -87
#define BAD_HANDLE_ERR              -86
#define BUFFER_TOO_SHORT_ERR        -85
#define BACKUP_NAME_FULL_ERR        -84
#define NEWPTN_STRUC_NOT_FOUND      -83 /* no new pattern structure found */
#define BAD_ZIP_ERR                 -82 /* corrupted compression file */
#define NOT_SUPPORTED_ERR           -81 /* function is not supoprted */
#define PLATFORM_ERR                -80 /* update engine from different platform file */
#define FILE_EXIST_ERR              -79 /* file exist when take action rename */
#define MAXDECOM_ERR                -78 /* exceed decompression layer */
#define DISK_FULL_ERR               -77 /* fail to write because disk is full */
#define EXTRACT_TOO_BIG_ERR         -76 /* Extract file size > cf_ExtractFileSizeLimit */
#define NEED_SCAN_COM               -75 /* This file muse scan .COM file type */
#define PSW_OFFICE_ERR              -74 /* password protected office file */
#define BAD_FILE_ERR                -73
#define SEEK_ERR                    -72
#define ZIP_RATIO_ERR               -71 /* ZIP RATIO exceed limit*/
#define ZIP_CRC_EQU_SKIP_ERR        -70 /* Files in ZIP file have the same CRC(they should be the same)*/
#define ZIP_FILE_COUNT_ERR          -69 /* The file count in ZIP file exceed the cf_ExtractFileCountLimit*/
#define DTYPE_NEED_MORE_DATA_ERR    -68 /* need more data for data type checking */    
#define PTN_UPDATE_ERR              -67 /* Fail to update pattern and no valid pattern available for VSC */
#define ICRC_QUERY_ERR              -66 /* ICRC query string error */
#define ZIP_64_UNSUPPORTED_ERR      -65 /* ZIP64 decompression is not supported */
#define UNKNOWN_LITE_OPCODE         -9999 /* unknown lite script engine operator */

/* softmice reserve -10000~-19999 */
#define ACCESS_VIOLATION_ERR            -10001
#define PROCESS_INIT_ERROR              -10002
#define GUARD_PAGE_VIOLATION_ERR        -10003
#define MOD_NOT_FOUND_ERR               -10004
#define INST_NOT_SUPPORTED_ERR          -10005
#define API_NOT_SUPPORTED_ERR           -10006
#define MAXIMUM_MILEAGE_EXCEED          -10007
#define MAXIMUM_DUP_EXCEPTION_EXCEED    -10008
#define MAXIMUM_EXCEPTION_EXCEED        -10009
#define PAGE_SIZE_TOO_SMALL_ERR         -10010
#define MAXIMUM_PROCESS_COUNT_EXCEED    -10011
#define MAXIMUM_THREAD_COUNT_EXCEED     -10012


#endif /* __VS_RC_H__ */


