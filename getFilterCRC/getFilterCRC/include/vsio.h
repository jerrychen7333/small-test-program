#ifndef __VSIO_H__
#define __VSIO_H__

#include "vspdt.h"

#ifdef VS_PACK_STRUCTURE
#pragma pack(push, 8)
#endif
/*======= pack begin =======*/



struct _VSIO;

/* open flag */
#define VSIO_READONLY                                                   (VSIO_FLAG_READ)
#define VSIO_READWRITE                                                  (VSIO_FLAG_READ|VSIO_FLAG_WRITE)
#define VSIO_CREATE                                                     (VSIO_FLAG_CREATE_ALWAYS|VSIO_FLAG_READ|VSIO_FLAG_WRITE)

/* basic interface */
#define VSIO_CLOSE(io)                                                  (io)->close(io)
#define VSIO_READ(io, mem, size, p_size, p_offset)                      (io)->read(io, mem, size, p_size, p_offset)
#define VSIO_WRITE(io, mem, size, p_size, p_offset)                     (io)->write(io, mem, size, p_size, p_offset)
#define VSIO_SEEK_SET(io, ofs)                                          (io)->seek(io, ofs, 0)
#define VSIO_SEEK_CUR(io, ofs)                                          (io)->seek(io, ofs, 1)
#define VSIO_SEEK_END(io, ofs)                                          (io)->seek(io, ofs, 2)
#define VSIO_GET_CUR_OFS(io, access_ofs)                                VSIO_SIZE_INFO(io, &access_ofs, NULL, NULL)
#define VSIO_GET_SIZE(io, ready_size)                                   VSIO_SIZE_INFO(io, NULL, &ready_size, NULL)
#define VSIO_FLUSH(io)                                                  (io)->flush(io)
#define VSIO_TRUNCATE(io, ready_size)                                   (io)->truncate(io, ready_size)

#define VSIO_FUNC_TEST(io, ctrl_code, p_ver)                            (io)->ctrl(io, IOC_FUNC_TEST, ctrl_code, 0, p_ver, NULL);       _IOC_PTYPE_CHECK(VULONG, p_ver)
#define VSIO_SET_IO_TYPE(io, io_type)                                   (io)->ctrl(io, IOC_SET_IO_TYPE, io_type, 0, NULL, NULL);        _IOC_VTYPE_CHECK(VULONG, io_type)
#define VSIO_GET_IO_TYPE(io, io_type)                                   (io)->ctrl(io, IOC_GET_IO_TYPE, 0, 0, &io_type, NULL);          _IOC_VTYPE_CHECK(VULONG, io_type)
#define VSIO_SET_IO_NAME(io, io_name)                                   (io)->ctrl(io, IOC_SET_IO_NAME, 0, 0, &io_name, NULL);          _IOC_VTYPE_CHECK(VSIO_NAME, io_name)
#define VSIO_GET_IO_NAME(io, io_name)                                   (io)->ctrl(io, IOC_GET_IO_NAME, 0, 0, &io_name, NULL);          _IOC_VTYPE_CHECK(VSIO_NAME, io_name)


/* Access Flags */
#define VSIO_FLAG_ACCESS_MASK           (VSIO_FLAG_READ|VSIO_FLAG_WRITE|VSIO_FLAG_CREATE_ALWAYS)
#define VSIO_FLAG_RESERVE_MASK          0xF0000000
#define VSIO_FLAG_READ                  0x00000001
#define VSIO_FLAG_WRITE                 0x00000002
#define VSIO_FLAG_CREATE_ALWAYS         0x00000004
#define VSIO_FLAG_DELETE_ON_CLOSE       0x00000008

/* advanced interface */
#define VSIO_SIZE_INFO(io, p_access_ofs, p_ready_size, p_limit_size)    (io)->size_info(io, p_access_ofs, p_ready_size, p_limit_size)
#define VSIO_GET_LIMIT_SIZE(io, limit_size)                             VSIO_SIZE_INFO(io, NULL, NULL, &limit_size)
#define VSIO_DUP(io, start_offset, limit_size, open_flag, new_io)       (io)->dupf(io, start_offset, limit_size, open_flag, &(new_io))
#define VSIO_ATTACH(io, new_io)                                         (io)->attach(io, 0, &(new_io))
#define VSIO_CA_ATTACH(io, unit, new_io)                                (io)->attach(io, unit, &(new_io))

#define _IOC_VTYPE_CHECK(type_name, para_name)                          {type_name* _ioc_ptr_ = &para_name;}
#define _IOC_PTYPE_CHECK(type_name, para_name)                          {type_name* _ioc_ptr_ = para_name;}

#define VSIO_CA_SYN_SIZE(io, unit, ca_page, p_ready_unit) \
    (io)->ctrl(io, IOC_CA_SYN_SIZE, unit, 0, &ca_page, p_ready_unit); \
    _IOC_VTYPE_CHECK(CA_PAGE, ca_page); \
    _IOC_PTYPE_CHECK(VS_SIZE_T, p_ready_unit);

#define VSIO_CA_PAGE_RENEW(io, unit, for_write, loc, ca_page) \
    (io)->ctrl(io, IOC_CA_PAGE_RENEW, unit, for_write, &loc, &ca_page); \
    _IOC_VTYPE_CHECK(VS_SIZE_T, loc); \
    _IOC_VTYPE_CHECK(CA_PAGE, ca_page); \



/*************************************
Function:
    Reads data from VSIO
Parameters:
    io[in]: available VSIO handle
    mem[in]: a pointer to the buffer that receives the data read from the VSIO
    size[in]: the maximum number of bytes to be read
    p_size[out, optional]: a pointer to the variable that receives the number of bytes read
    p_offset[in, optional]: a pointer to the specified offset for this read
Return:
    standard return code
Specification:
    1. if all of size were read return success, otherwise return ACCESS_ERR
    2. if p_offset was NULL read from current offset(inside VSIO), otherwise read from (*p_offset)
    3. change current offset(inside VSIO) to next position
    4. read offset over then ready size(inside VSIO) is not an io error, bytes read should be 0.
*************************************/
typedef int VSIO_READ_FUNC(struct _VSIO* io, void* mem, VULONG size, VULONG* p_size, VS_SIZE_T* p_offset);

/*************************************
Function:
    Writes data from VSIO
Parameters:
    io[in]: available VSIO handle
    mem[in]: a pointer to the buffer containing the data to be written to the VSIO
    size[in]: the number of bytes to be written to the VSIO
    p_size[out, optional]: a pointer to the variable that receives the number of bytes written
    p_offset[in, optional]: a pointer to the specified offset for this write.
Return:
    standard return code
Specification:
    1. if all of size were write return success, otherwise return ACCESS_ERR
    2. if p_offset was NULL write to current offset(inside VSIO), otherwise write to (*p_offset).
    3. change current offset(inside VSIO) to next position
    4. write offset over then ready size(inside VSIO) is not an io error, bytes written should be 0.
*************************************/
typedef int VSIO_WRITE_FUNC(struct _VSIO* io, const void* mem, VULONG size, VULONG* p_size, VS_SIZE_T* p_offset);

/*************************************
Function:
    Moves a VSIO offset to the specified location
Parameters:
    io[in]: available VSIO handle
    offset[in]: number of bytes from origin
    origin[in]: initial position
                0: (SEEK_SET) beginning of file
                1: (SEEK_CUR) current position of file pointer
                2: (SEEK_END) end of file
Return:
    N/A
Specification:
    1. always update current offset(inside VSIO) by new setting.
    2. does not change current offset when (2 < origin)
*************************************/
typedef void VSIO_SEEK_FUNC(struct _VSIO* io, VS_SIZE_T offset, VUINT32 origin);

/*************************************
Function:
    Get size information of VSIO
Parameters:
    io[in]: available VSIO handle
    p_access_ofs[out, optional]: a pointer to the variable that receives the current offset(inside VSIO)
    p_ready_size[out, optional]: a pointer to the variable that receives the ready size(inside VSIO)
    p_limit_size[out, optional]: a pointer to the variable that receives the limit size(inside VSIO)
Return:
    N/A
Specification:
    1. does not change any VSIO status
*************************************/
typedef void VSIO_SIZE_INFO_FUNC(struct _VSIO* io, VS_SIZE_T* p_access_ofs, VS_SIZE_T* p_ready_size, VS_SIZE_T* p_limit_size);

/*************************************
Function:
    Close the VSIO
Parameters:
    io[in]: available VSIO handle
Return:
    standard return code
*************************************/
typedef int VSIO_CLOSE_FUNC(struct _VSIO* io);

/*************************************
Function:
    Flushes data to the resource layer
Parameters:
    io[in]: available VSIO handle
Return:
    standard return code
Specification:
    1. does not change VSIO size information(current offset, ready size & limit size)
*************************************/
typedef int VSIO_FLUSH_FUNC(struct _VSIO* io);

/*************************************
Function:
    Truncate a VSIO to a specified size
Parameters:
    io[in]: available VSIO handle
    ready_size[in]: new ready size of the VSIO in bytes
Return:
    standard return code
Specification:
    1. if ready_size is over then the ready size inside VSIO return ACCESS_ERR
*************************************/
typedef int VSIO_TRUNCATE_FUNC(struct _VSIO* io, VS_SIZE_T ready_size);

/*************************************
Function:
    duplicate an open VSIO
Parameters:
    io[in]: available VSIO handle
    start_offset[in]: new VSIO is form this start offset
    limit_size[in, optional]: a specified limit size for new VSIO
    open_flag[in]:  open flag
    p_io[out]: a pointer to the variable that receives the new VSIO
Return:
    standard return code
Specification:
    1. if start_offset over then ready size inside VSIO return ACCESS_ERR
    2. if limit_size was 0 inheritance the remaining ready size and the remaining limit size
    3. if limit_size over the remaining ready size or the remaining limit size, then
       auto fix the ready size and the limit size for new VSIO    
*************************************/
typedef int VSIO_DUP_FUNC(struct _VSIO* io, VS_SIZE_T start_offset, VS_SIZE_T limit_size, VULONG open_flag, struct _VSIO** p_io);

/*************************************
Function:
    attach an open VSIO
Parameters:
    io[in]: available VSIO handle
    unit[in, optional]: requests common access with a specified unit(in byte)
    p_io[out]: a pointer to the variable that receives the attached VSIO
Return:
    standard return code
Specification:
    1. unit was 0: let (*p_io) equal to io(only increase reference counter inside VSIO) and return success.
    2. unit was not 0: if io does not support common access, just return NOT_SUPPORTED_ERR.
                       if io support common access, let (*p_io) equal to io(only increase reference counter
                       inside VSIO) and return success. Also io need implement IOC_CA_SYN_SIZE & IOC_CA_PAGE_RENEW
*************************************/
typedef int VSIO_ATTACH_FUNC(struct _VSIO* io, VULONG unit, struct _VSIO** p_io);

/*************************************
Function:
    VSIO control interface
Parameters:
    io[in]: available VSIO handle
    ctrl_code[in]: a VSIO control code
    para1,2[in, optional]: depend on the VSIO control code
    para3,4[in/out, optional]: depend on the VSIO control code
Return:
    depend on the VSIO control code
*************************************/
typedef int VSIO_CTRL_FUNC(struct _VSIO* io, VULONG ctrl_code, VULONG para1, VULONG para2, void* para3, void* para4);

/*********************/
/* VSIO control code */
/*********************/
#define IOC_USER_DEFINE_FLAG            0x80000000
#define IOC_INHERITANCE_FLAG            0x40000000

/* io name */
typedef struct _VSIO_NAME {
    VULONG type;
    VULONG size;
    const void*  data; /* ascii */
} VSIO_NAME;

/* io name type  */
#define IONT_NATIVE_FLAG                0x80000000
#define IONT_FORMAT_MASK                0x0000000F
#define IONT_ANSI_STRING                0x00000000
#define IONT_WIDE_STRING                0x00000001
#define IONT_UNKNOWN                    0x0000000F


/* io type */
#define IOT_BUFFER_FLAG                 0x80000000
#define IOT_ENCODE_FLAG                 0x40000000
#define IOT_RESOURCE_MASK               0x000000FF
#define IOT_RES_FILE                    0x00000000
#define IOT_RES_MEMORY                  0x00000001
#define IOT_RES_PROCESS                 0x00000002
#define IOT_RES_STREAM                  0x00000003
#define IOT_RES_UNKNOWN                 0x000000FF
#define IOT_INHERITANCE_MASK            (IOT_RESOURCE_MASK|IOT_ENCODE_FLAG)


/* ca page */
typedef struct _CA_PAGE {
    VS_SIZE_T   loc;    /* resource location of this page */
    VULONG      ofs;    /* current offset of data unit in this page */
    VULONG      rlen;   /* how many readable data unit in this page */
    VULONG      wlen;   /* how many writeable data unit in this page */
    VUCHAR*     buf;    /* accessible buffer of this page */
    void*       iop;    /* io page structure */
} CA_PAGE;


/*************************************
Function:
    function test
Parameters:
    para1[in]: control code for testing
    para2[  ]: 0
    para3[out, optional]: a pointer to the variable that receives the io version
    para4[  ]: NULL
Return:
    standard return code
*************************************/
#define IOC_FUNC_TEST                   0x00000100

/*************************************
Function:
    Set IO Type
Parameters:
    para1[  ]: 0
    para2[in]: the IO type
    para3[  ]: NULL
    para4[  ]: NULL
Return:
    standard return code
*************************************/
#define IOC_SET_IO_TYPE                 0x00000101

/*************************************
Function:
    Set IO Type
Parameters:
    para1[  ]: 0
    para2[  ]: the IO type
    para3[out]: a pointer to the variable that receives the IO type
    para4[  ]: NULL
Return:
    standard return code
*************************************/
#define IOC_GET_IO_TYPE                 0x00000102

/*************************************
Function:
    Set IO Name
Parameters:
    para1[  ]: 0
    para2[  ]: 0
    para3[in]: a pointer to the VSIO_NAME
    para4[  ]: NULL
Return:
    standard return code
*************************************/
#define IOC_SET_IO_NAME                 0x00000103

/*************************************
Function:
    Get IO Name
Parameters:
    para1[  ]: 0
    para2[  ]: 0
    para3[inout]: a VSIO_NAME to receives its contents
    para4[  ]: NULL
Return:
    standard return code
*************************************/
#define IOC_GET_IO_NAME                 0x00000104


/*************************************
Function:
    CA synchronize the ready size.
Parameters:
    para1[in]: the unit of CA
    para2[  ]: 0
    para3[in]: a pointer to the CA_PAGE
    para4[out, optional]: a pointer to the variable that receives the number of unit ready
Return:
    standard return code
*************************************/
#define IOC_CA_SYN_SIZE                 0x00000105

/*************************************
Function:
    CA page renew
Parameters:
    para1[in]: the unit of CA
    para2[in]: a boolean indicates whether to write
    para3[in]: a pointer to the specified location(in unit) for this renew
    para4[inout]: a pointer to the CA_PAGE
Return:
    standard return code
*************************************/
#define IOC_CA_PAGE_RENEW               0x00000106



typedef struct _VSIO {
    VSIO_READ_FUNC*        read;       /* != NULL */
    VSIO_WRITE_FUNC*       write;      /* != NULL */
    VSIO_SEEK_FUNC*        seek;       /* != NULL */
    VSIO_CLOSE_FUNC*       close;      /* != NULL */
    VSIO_FLUSH_FUNC*       flush;      /* != NULL */
    VSIO_TRUNCATE_FUNC*    truncate;   /* != NULL */
    VSIO_DUP_FUNC*         dupf;        /* != NULL */
    VSIO_ATTACH_FUNC*      attach;     /* != NULL */
    VSIO_SIZE_INFO_FUNC*   size_info;  /* != NULL */
    VSIO_CTRL_FUNC*        ctrl;       /* != NULL */
} VSIO;



/*======= pack end =======*/
#ifdef VS_PACK_STRUCTURE
#pragma pack(pop)
#endif

#ifdef VS_FET_ON
#include "_vs_fet.h"
#endif

#endif /* __VSIO_H__ */
