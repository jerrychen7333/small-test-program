/* $Date: 2001/2/12 AM 11:10:02$ */
/*
 * $Log: 
 *  3    NewScanEngine1.2         2001/2/12 AM 11:10:02Jason_lin       02-12-2001
 *       Jason Lin, for ifskit
 *  2    NewScanEngine1.1         1999/4/23 PM 03:44:28Joanna_tsai     Got from
 *       Jason Lin
 *  1    NewScanEngine1.0         1999/4/21 PM 04:06:20Joanna_tsai     
 * $
 * 
 * 2     99/04/23 3:44p Joanna
 * Got from Jason Lin
 * 
 * 1     99/04/21 4:06p Joanna
 * 
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
#ifndef __VSAPINT_H__
#define __VSAPINT_H__

/* 02-12-2001	Jason Lin, for ifskit */
#ifdef USE_IFS
#include <ntifs.h>
#else
#include <ntddk.h>
#endif

#define VSAPINT_DEVICE_NAME			L"\\Device\\VsapiNt"
#define VSAPINT_WIN32_DEVICE_NAME	L"\\DosDevices\\VsapiNt"
#define FILE_ASYNC					41000

extern PDEVICE_OBJECT				pVsapiNtDeviceObject;

NTSTATUS VsapiNtDispatch(PDEVICE_OBJECT DeviceObject, PIRP Irp) ;
VOID     VsapiNtDriverUnload(PDRIVER_OBJECT DriverObject);

#endif
