/* $Date: 2001/2/19 PM 05:04:21$ */
/*
 * $Log: 
 *  9    NewScanEngine1.8         2001/2/19 PM 05:04:21Wayne_chu       Synchronize
 *       cheetah and cheeath_rel
 *  8    NewScanEngine1.7         2001/1/19 AM 11:57:27Wayne_chu       add
 *       VSGetBackupFileInfo and fix the problem of convert between Unicode and
 *       Ansicode.
 *  7    NewScanEngine1.6         2000/5/27 PM 03:12:45Viking_ho       
 *  6    NewScanEngine1.5         1999/12/28 AM 08:49:39Viking_ho       Add ";"
 *  5    NewScanEngine1.4         1999/12/13 PM 06:27:57Joanna_tsai     for RAR,
 *       first using 0x100000, if fail to malloc such big space
 *       then using 0x10000
 *       move the maxwinsize definition to rar files
 *  4    NewScanEngine1.3         1999/11/2 PM 02:39:36Joanna_tsai    
 *       RAR_MAXWINSIZE should be 0x100000
 *       some platform like win16 cannot support it, so declare it as 0x10000
 *  3    NewScanEngine1.2         1999/5/24 PM 01:27:54Viking_ho       VCHAR should
 *       be defined separately by m_xxx.h
 *  2    NewScanEngine1.1         1999/4/7 PM 03:38:04 Ming_deng       Merge with
 *       cliff's
 *  1    NewScanEngine1.0         1998/11/6 AM 11:52:32Ming_deng       
 * $
 * 
 * 8     99/11/26 10:15a Viking
 * Ver5.000
 * 
 * 4     11/02/99 2:39p Joanna
 * RAR_MAXWINSIZE should be 0x100000
 * some platform like win16 cannot support it, so declare it as 0x10000
 * 
 * 3     99/05/24 1:27p Viking
 * VCHAR should be defined separately by m_xxx.h
 * 
 * 2     4/07/99 3:38p Ming
 * Merge with cliff's
 * 
 * 2     1/25/99 4:30p Ming
 * 
 * 1     11/06/98 11:52a Ming
 * 
 * 1     11/05/98 12:19p Cliff_liang
 * system dependend code for AS400
 * 
 * 1     11/05/98 12:19p Cliff_liang
 * 
 * Revision 1.0 1998/07/13 11:38:16 cliff
 * Initial revision
 *
 */
/* $Project: NewScanEngine$ */
/*
 *****************************************************************************
 *
 *	(C) Copyright 1989-1998 Trend Micro, Inc.
 *	All Rights Reserved.
 *
 *	This program is an unpublished copyrighted work which is proprietary
 *	to Trend Micro, Inc. and contains confidential information that is not
 *	to be reproduced or disclosed to any other person or entity without
 *	prior written consent from Trend Micro, Inc. in each and every instance.
 *
 *	WARNING:  Unauthorized reproduction of this program as well as
 *	unauthorized preparation of derivative works based upon the
 *	program or distribution of copies by sale, rental, lease or
 *	lending are violations of federal copyright laws and state trade
 *	secret laws, punishable by civil and criminal penalties.
 *
 */
/**************************************************************************/
/* m_as400.h */
/* Description: */
/*		Machine dependent code definition */
/* */
/**************************************************************************/

#ifdef __TMVS_MACHINE
Error, multiple machine defined
#endif
#define __TMVS_MACHINE

/* platform dependent environment */
/* platform: IBM AS400 */
typedef char                   VCHAR;   /* 1 byte */
typedef long                   VLONG;   /* 4 byte */
typedef unsigned long          VULONG;  /* 4 byte */
typedef VLONG                  VINT64;  /* 8 byte */ /* tempatory use 32 bits */
typedef VULONG                 VUINT64; /* 8 byte */ /* tempatory use 32 bits */

typedef long *	VSCTYPE;
typedef long * VSPTN_HANDLE;

#define DEFAULT_MOVE_DIR	"/virus"
#define DEFAULT_PATTERN_DIR "/etc/iscan"
#define DEFAULT_PATTERN_FILE	"lpt$vpn."
#define DEFAULT_TEMP_DIR	"/tmp"
#define EXPORT
#define TMVSAPI
#define HANDLE			int
#define MAX_PATH_LEN		1024
/* minimum memory size for decomp module when decomp into memory */
#define DECOMP_MIN_MEM 102400
/* Maximum memory size for decomp module when decomp into memory */
#define DECOMP_MAX_MEM 409600
#define PASCAL

VUSHORT VSStringToShort(char *);
VULONG VSStringToLong(char *);
VUINT64 VSStringToLongLong(char *);
void VSShortToString(char *,VUSHORT);
void VSLongToString(char *,VULONG);
void VSLongLongToString(char *,VUINT64);
