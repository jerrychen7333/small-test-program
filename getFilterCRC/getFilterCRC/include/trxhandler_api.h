#ifndef TRXHANDLER_API_H
#define TRXHANDLER_API_H

#ifdef _WIN32
#ifdef TRXHANDLER_EXPORTS
#define XHANDLER_API __declspec(dllexport)
#else
#define XHANDLER_API __declspec(dllimport)
#endif
#else
#define XHANDLER_API
#endif

#ifdef _WIN32
#include <windows.h>
#endif

#include "trxhandler.h"

typedef enum {
  XHDL_OK                     = 0,
  XHDL_FAIL                   = 1,
  XHDL_INVALID_PARAM          = 2,
  XHDL_NOT_IMPLEMENT          = 3,
  XHDL_NOT_FOUND              = 4,
  XHDL_OUT_OF_BOUNDARY        = 5,
  XHDL_OUT_OF_MEM             = 6,
  XHDL_FILE_ERROR             = 7,
  XHDL_ACCESS_CONST_ERROR     = 8,
  XHDL_ACCESS_FAIL            = 9,
  XHDL_INIT_FAIL              = 10,
  XHDL_GLOBAL_INIT_FAIL       = 11,
  XHDL_ALREADY_INIT           = 12,
  XHDL_NOT_INIT               = 13,
  XHDL_TIMEOUT                = 14,
  XHDL_INVALID_ENGINE_TYPE    = 15,
  XHDL_INVALID_OPERATION_TYPE = 16,
  XHDL_NETWORK_ERROR          = 17,
  XHDL_SERVER_ERROR           = 18,
  XHDL_SERVICE_STOPPED        = 19,
  XHDL_CLOUD_SCAN_DISABLED    = 20,
  XHDL_NO_AVAILABLE_RESOURCE  = 21
} X_RCTYPE;

typedef enum {
  XHDL_FILE = 1,
  XHDL_BEHAVIOR,
} xEngineType;

typedef enum {
  XHDL_QUERY = 1,
  XHDL_CHK_SVR_STATUS
} xOperationType;


typedef enum {
  XHDL_CONFIG_KEY_ENABLE_CURL_VERBOSE             = 1001,
  XHDL_CONFIG_KEY_ENABLE_TRUE_DNS                 = 1002,
  XHDL_CONFIG_KEY_ENABLE_PERF_COUNTER             = 1003,
  XHDL_CONFIG_KEY_ENABLE_CACHE                    = 1004,
  XHDL_CONFIG_KEY_ENABLE_CDT_LOG                  = 1005,
  XHDL_CONFIG_KEY_ENABLE_HTTPS                    = 1006,
  XHDL_CONFIG_KEY_ENABLE_HTTP_BODY_COMPRESSION    = 1007,

  XHDL_CONFIG_KEY_THREAD_COUNT                    = 2001,
  XHDL_CONFIG_KEY_TRUE_DNS_CACHE_PURGE_INTERVAL   = 2002,
  XHDL_CONFIG_KEY_FORCE_RECONNECT_PERIOD          = 2003,
  XHDL_CONFIG_KEY_CACHE_SIZE                      = 2004,
  XHDL_CONFIG_KEY_BUCKET_SIZE                     = 2005,
  XHDL_CONFIG_KEY_LOW_SPEED_TIMEOUT               = 2006,
  XHDL_CONFIG_KEY_LOW_SPEED_LIMIT                 = 2007,
  XHDL_CONFIG_KEY_QUERY_TIMEOUT                   = 2008,
  XHDL_CONFIG_KEY_DELETE_HANDLE_DELAY_TIME        = 2009,

  XHDL_CONFIG_KEY_DATA_PATH                       = 3001,
  XHDL_CONFIG_KEY_CA_INFO_PATH                    = 3002,

  XHDL_CONFIG_KEY_ENABLE_CLOUD_SCAN               = 11001,
  XHDL_CONFIG_KEY_USE_PROXY                       = 11002,
  XHDL_CONFIG_KEY_VERIFY_PEER                     = 11003,

  XHDL_CONFIG_KEY_TASK_TIMEOUT                    = 12001,
  XHDL_CONFIG_KEY_PROXY_PORT                      = 12002,
  XHDL_CONFIG_KEY_PROXY_AUTH                      = 12003,
  XHDL_CONFIG_KEY_CHK_SRV_BODY_SIZE               = 12004,

  XHDL_CONFIG_KEY_SERVER_HOST                     = 13001,
  XHDL_CONFIG_KEY_PROXY                           = 13002,
  XHDL_CONFIG_KEY_PROXY_USERNAME                  = 13003,
  XHDL_CONFIG_KEY_PROXY_PASSWORD                  = 13004,
  XHDL_CONFIG_KEY_PRODUCT_USER_AGENT              = 13005,
  XHDL_CONFIG_KEY_SSL_CIPHERS                     = 13006

} xConfigKey;

typedef enum {
  XHDL_RESULT_KEY_QUERY_API_ERROR_CODE            = 2001,
  XHDL_RESULT_KEY_HTTP_DETAIL_ERROR_CODE          = 2002,
  XHDL_RESULT_KEY_HTTP_STATUS_CODE                = 2003,
  XHDL_RESULT_KEY_MERGED_CODE                     = 2004,

  XHDL_RESULT_KEY_QUERY_API_ERROR_STRING          = 3001,
  XHDL_RESULT_KEY_HTTP_DETAIL_ERROR_STRING        = 3002,

} xResultKey;

XHANDLER_API X_RCTYPE PASCAL xCreateHandle(xEngineType type, xOperationType op, xHandle *handle);
XHANDLER_API X_RCTYPE PASCAL xStartService(xHandle handle);
XHANDLER_API X_RCTYPE PASCAL xAsyncStopService(xHandle handle);
XHANDLER_API X_RCTYPE PASCAL xDeleteHandle(xHandle handle);

XHANDLER_API X_RCTYPE PASCAL xSetConfigInteger(xHandle handle, xConfigKey key, XHDL_INT value);
XHANDLER_API X_RCTYPE PASCAL xSetConfigString(xHandle handle, xConfigKey key, const wchar_t *value);
XHANDLER_API X_RCTYPE PASCAL xGetConfigInteger(xHandle handle, xConfigKey key, XHDL_INT *value);
XHANDLER_API X_RCTYPE PASCAL xGetConfigString(xHandle handle, xConfigKey key, wchar_t *value, size_t value_len);
XHANDLER_API X_RCTYPE PASCAL xReloadService(xHandle handle);

XHANDLER_API X_RCTYPE PASCAL xQuery(xQueryStruct *para, void *keys, const XHDL_BYTE *input, XHDL_DWORD input_size, xResultHandle *result_handle);
XHANDLER_API X_RCTYPE PASCAL xGetResultBlob(xResultHandle result_handle, XHDL_BYTE **output, XHDL_DWORD *output_size);
XHANDLER_API X_RCTYPE PASCAL xGetResultInteger(xResultHandle result_handle, xResultKey key, XHDL_INT *value);
XHANDLER_API X_RCTYPE PASCAL xGetResultString(xResultHandle result_handle, xResultKey key, char *buffer, size_t buffer_len);
XHANDLER_API X_RCTYPE PASCAL xDeleteResult(xResultHandle result_handle);

XHANDLER_API X_RCTYPE PASCAL xChkServerStatus(xHandle handle, XHDL_DWORD timeout, XHDL_DWORD *error_code, double* total_time);


#endif //TRXHANDLER_API_H
