// getFilterCRC.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "tmvsdef.h"
#include "tmvsx.h"
#include "tmvs.h"

int initVSC(VSCTYPE* vsc) {
	VSInit(0, NULL, 0, vsc);
	VSSetPatternPath(*vsc, ".");
	VSReadVirusPattern(*vsc, 0, "icrc$oth.", NULL);
	return 0;
}

void quitVSC(VSCTYPE* vsc) {
	VSQuit(*vsc);
}

int main(int argc, char* argv[])
{
	int rc = 0;
	VSCTYPE vsc;
	VSHANDLE* fileHandle = NULL;
	char name[] = "C:\\SEG\\TMCM\\tmcm_6.0-sp3_ag.pdf";
	void* buffer = malloc(sizeof(struct _CUSTOM_DEFENSE_METHOD_DATA_V1));

	initVSC(&vsc);
	
	VSOpenResource(name, VS_RT_FILE, VS_READ, 0, 0, &fileHandle);
	VSGetCustomDefenseMethodData(vsc, fileHandle, 1, buffer);

	printf("filter crc: %8x\n", ((struct _CUSTOM_DEFENSE_METHOD_DATA_V1 *)buffer)->ulFilter);

	free(buffer);
	VSCloseResource(fileHandle);
	quitVSC(&vsc);
	return 0;
}

