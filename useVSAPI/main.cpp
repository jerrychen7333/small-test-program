#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tmvsdef.h"
#include "tmvsx.h"
#include "tmvs.h"

int initVSC(VSCTYPE* vsc) {
	VSInit(0, NULL, 0, vsc);
	VSSetPatternPath(*vsc, ".");
	VSReadVirusPattern(*vsc, 0, "icrc$oth.", NULL);
	return 0;
}

void quitVSC(VSCTYPE* vsc) {
	VSQuit(*vsc);
}

int main(int argc, char* argv[])
{
	int rc = 0;
	VSCTYPE vsc;
	VSHANDLE* fileHandle = NULL;
	RESOURCE resource;
	char name[] = "/root/test/ant-javafx tcffeit1r6sg01.jar";

	initVSC(&vsc);
	memset(&resource, 0, sizeof(RESOURCE));
	
	rc = VSOpenResource(name, VS_RT_FILE, VS_READ, 0, 0, &fileHandle);
	printf("VSOpenResource %d\n", rc);

	resource.r_Handle = fileHandle;
	resource.r_Name = name;
	resource.r_Type = 0;
	resource.r_File = name;
	rc = VSScanResource(vsc, &resource, NULL);
	printf("VSScanResource %d\n", rc);

	VSCloseResource(fileHandle);
	quitVSC(&vsc);
	return 0;
}


