// catch_heapcorruption.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>

#ifdef _WIN32_WINNT
#undef _WIN32_WINNT 
#endif

#define _WIN32_WINNT 0x0500 

LONG WINAPI UnhandledExceptionHandler(EXCEPTION_POINTERS* pExceptionInfo)
{
    __try
    {
		printf("in unhandle exception handler %08x", pExceptionInfo->ExceptionRecord->ExceptionCode);
    }
    __except (EXCEPTION_EXECUTE_HANDLER)
    {
    }
    return EXCEPTION_EXECUTE_HANDLER;
}

LONG VectoredHandler( PEXCEPTION_POINTERS ExceptionInfo)
{
    if (0xC0000374 == ExceptionInfo->ExceptionRecord->ExceptionCode)
    {
        printf("in vector excption handler");
    }
    return EXCEPTION_CONTINUE_SEARCH;
}

int _tmain(int argc, _TCHAR* argv[])
{
    SetUnhandledExceptionFilter(UnhandledExceptionHandler);
    //AddVectoredExceptionHandler(TRUE, VectoredHandler);

	// test 0xC0000005
	int a = 3;
	int* ptr = NULL;
	*ptr = a;

	/* test 0xC0000374
    int* ptr = new int();
    delete ptr;
    delete ptr;
	*/
    return 0;
}

